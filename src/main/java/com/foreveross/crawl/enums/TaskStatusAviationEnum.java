package com.foreveross.crawl.enums;

/**
 * 航空任务状态
 * @author luofangyi
 *
 */
public enum TaskStatusAviationEnum {
	
	//================业务异常开始======================
	//自定义的业务异常取值范围(-5至5之间的异常编码被系统占用): 错误异常 < -5, 成功的异常 > 5
	
	//失败异常
	
	//成功异常
	TASK_NOT_PRICE(9, "没有价格"),
	TASK_NOT_DIRECT_FLIGHT(8, "没有直达航班"),
	TASK_NOT_FIND_FLIGHT(7, "没有该航班"),
	TASK_NO_TICKET(6, "机票已售空");
	//===============业务异常结束===============
	
//	TASH_PAUSE(-5,"任务暂停"),
//	TASK_OTHER_FAIL(-4,"任务其他失败"),
//	TASK_PARSE_FAIL(-3,"任务解析失败"),
//	TASK_FETCH_FAIL(-2,"任务抓取失败"),
//	TASK_WATI(0,"任务等待中"),
//	TASK_DOING(2,"任务执行中"),
//	TASK_FINISH_SUCCESS(3,"任务成功完成");
	
	private int status;

	private String remark;
	
	private TaskStatusAviationEnum(int status, String remark){
		this.status = status;
		this.remark = remark;
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getRemark() {
		return remark;
	}
}
