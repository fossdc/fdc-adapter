package com.foreveross.crawl.enums;
/**
 * 航线的类型
 * 国内单程
 * 国际单程
 * 国内往返
 * 国际往返
 * @author guokenye
 *
 */
public enum RouteTypeEnum {
	
	DOMESTIC_ONEWAYTRIP(1,0,"国内单程"),
	DOMESTIC_ROUNDTRIP(1,1,"国内往返"),
	INTERNATIONAL_ONEWAY(0,0,"国际单程"),
	INTERNATIONAL_ROUND(0,1,"国际往返");
	
	private Integer isDomestic;
	private Integer isOneWay;;
	private String name;
	
	private RouteTypeEnum(Integer isDomestic, Integer isOneWay,String name) {
		this.isDomestic = isDomestic;
		this.isOneWay = isOneWay;
		this.name=name;
	}

	public Integer getIsDomestic() {
		return isDomestic;
	}


	public Integer getIsOneWay() {
		return isOneWay;
	}

	public String getName() {
		return name;
	}

	

	
	
	
}
