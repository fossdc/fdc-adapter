package com.foreveross.crawl.util;

/**
 * 字符串处理工具
 * @author stub
 *
 */
public class StringUtil {
	
	/**
	 * 判断字符串是否包含一些关键字中的一个
	 * @param src 要验证的字符串
	 * @param keys 关键字符数组
	 * @return
	 */
	public  static boolean indexOneOfKeys(String src,String... keys) {
		if(src == null){
			throw new IllegalArgumentException("src is null");
		}
		if(keys.length == 0){
			throw new IllegalArgumentException("keys is empty");
		}
		for(String key : keys){
			if(src.matches(String.format("[\\s\\S]*%s[\\s\\S]*", key))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 把若干个字符串按照指定分隔符拼装起来,为null的转换为空字符串
	 * @param separator 分隔符
	 * @param srcs 字符串组
	 * @return
	 */
	public static String join(String separator,String... srcs){
		if(separator == null){
			throw new IllegalArgumentException("separator is null");
		}
		if(srcs.length == 0){
			throw new IllegalArgumentException("srcs is empty");
		}
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(String s : srcs){
			if(first){
				first = false;
			}else{
				sb.append(separator);
			}
			sb.append(s);
		}
		return sb.toString();
	}
	
	/**
	 * 判断是否是空或者空字符串
	 * @param src
	 * @return
	 */
	public static boolean isNullOrEmpty(String src){
		if(src == null || src.equals("")){
			return true;
		}
		return false;
	}
	
	/**
	 * 空转换为空字符串
	 * @param src
	 * @return
	 */
	public static String nullToEmpty(String src){
		if(src == null){
			return "";
		}
		return src;
	}
	
	/**
	 * 空转换为空字符串
	 * @param src
	 * @return
	 */
	public static String isEmpty(String src){
		if(src == null){
			return "";
		}
		return src;
	}
	
	/**
	 * unicode 转成 utf-8
	 * @return String
	*/
	public static String unicodeToUtf8(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len;) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            value = (value << 4) + aChar - '0';
                            break;
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                            value = (value << 4) + 10 + aChar - 'a';
                            break;
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                            value = (value << 4) + 10 + aChar - 'A';
                            break;
                        default:
                            throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }
	
	public static void main(String[] args) {
		String src = "对于所选路线和等级 (具有限制的经济舱)，我们无法找到相应费用。 所示费用为最接近的费用。 (9206)";
		System.out.println(indexOneOfKeys(src, "无法找到指定日期.*相关建议","没有找到.*可搭乘航班","无对应航班或已满","我们无法找到您搜索的结果","我们无法找到相应费用"));
	}
}
