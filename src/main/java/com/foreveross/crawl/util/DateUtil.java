package com.foreveross.crawl.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 日期工具类
 * @author stub
 *
 */
public final class DateUtil {
	
	private static final String standard = "yyyy-MM-dd HH:mm";
	//private static final SimpleDateFormat standardFormat = new SimpleDateFormat(standard);
	
	public static Date add(Date date,int field,int count){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(field, count);
		return calendar.getTime();
	}
	
	public static String add(String src,String format,int field,int count) throws ParseException{
		SimpleDateFormat mat = new SimpleDateFormat(format);
		return mat.format(add(mat.parse(src), field, count));
	}
	
	public static String add(String src,int field,int count) throws ParseException{
		return add(src, standard, field, count);
	}
	
	/**
	 * 增加一天，格式是yyyy-MM-dd HH:mm
	 * @param src
	 * @param count
	 * @return
	 * @throws ParseException
	 */
	public static String addDate(String src,int count) throws ParseException{
		return add(src, Calendar.DAY_OF_YEAR, count);
	}
	
	/**
	 * @param Format
	 * @return
	 * @throws ParseException
	 */
	public static String addDays(String dateFormat,String dateStr ,int addDays)throws ParseException{
		String dateString ="";
		SimpleDateFormat format = new SimpleDateFormat(dateFormat) ;
		Date date=format.parse(dateStr);
		Calendar cn3 = Calendar.getInstance();
		cn3.setTime(date);
		cn3.set(Calendar.DATE, cn3.get(Calendar.DATE) + addDays);
		Date nowDateNext3 = cn3.getTime();
		dateString=format.format(nowDateNext3);
		return dateString;
	}
	
	
	
	/**
	 * 将字符串转化成 util.Date
	 * @param strDate		字符串日期
	 * @param format		格式
	 * @return
	 * @throws ParseException 
	 */
	public static Date string2Date(String strDate, String format) throws ParseException
	{
		DateFormat df = new SimpleDateFormat(format);
		Date date = null;
		date = df.parse(strDate);
		return date;
	}
	
	public static Date string2Date(String strDate, String format, Locale locale) throws ParseException
	{
		DateFormat df = new SimpleDateFormat(format, locale);
		Date date = null;
		date = df.parse(strDate);
		return date;
	}
	
	/**
	 * util.Date日期转字符串
	 * @param date			util.Date日期
	 * @param format		格式
	 * @return
	 */
	public static String date2String(Date date, String format) 
	{
		DateFormat df = new SimpleDateFormat(format);
		String strDate;
		strDate = df.format(date);
		return strDate;
	}
	
	public static String getFormatDate(String pattern)
	{
		Date date = new Date() ;
		return getFormatDate(pattern,date) ;
	}
	
	public static String getFormatDate(String pattern,Date date)
	{
		SimpleDateFormat format = new SimpleDateFormat(pattern) ;
		return format.format(date) ;
	}
	/**
	 * 将一种格式的字符串日期转成另外一种格式的字符串
	 * @param strDate1		原日期
	 * @param format1		原日期格式
	 * @param format2		目标日期格式
	 * @return
	 * @throws ParseException 
	 */
	public static String String2String(String strDate1, String format1, String format2) throws ParseException
	{
		Date date = string2Date(strDate1, format1);
		String strDate = date2String(date, format2);
		
		return strDate;
	}
	
	/**
	 * 取得当前日期前后n天的日期
	 * @param ndays 前后n天 后：n > 0  前 n < 0
	 * @param format 日期格式
	 * @return 当前日期前后n天的日期
	 * @throws ParseException 
	 */
	public static String getNdaysAfterDate(int ndays,String format)
	{
		Calendar nowCal = Calendar.getInstance();
		nowCal.add(Calendar.DAY_OF_MONTH, ndays);
		String strDate = date2String(nowCal.getTime(), format);
		return strDate;
	}
	/**
	 * 当前日期加上n小时
	 * @reutrn date
	 * @param 当前日期
	 * 
	 */
	public static Date dateAddHour(Date cudate ,int h){
		Date date=null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(cudate);
		calendar.add(Calendar.HOUR, h);
		date=calendar.getTime();
		return date;
	}
}
