package com.foreveross.crawl.adapter.sub.impl20140402.v3.airchina;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.CabinRelationEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * 工具类。
 *
 * @author luomingliang@foreveross.com
 * @version 1.0.0
 */
public class AirchinaAdapterUtil {

	/** 克隆集合。 */
	public static <T> Set<T> newObjecctSet(Set<T> inobjs) throws Exception {
		// 因为添加了每个返程最低价格，所以这里的返回对象不能复用，需要创建新对象，否则设置最低价格的时候混乱。
		Set<T> outobjs = Sets.newHashSet();
		for (T obj : inobjs) {
			outobjs.add((T) BeanUtils.cloneBean(obj));
		}
		return outobjs;
	}

	public static void orderByRelation(List<CabinRelationEntity> entitys) {
		Collections.sort(entitys, new Comparator<CabinRelationEntity>() {
			public int compare(CabinRelationEntity arg0, CabinRelationEntity arg1) {
				Double d0 = arg0.getFullPrice(), d1 = arg1.getFullPrice();
				if (d1 == null) return -1;
				if (d0 == null) return 1;
				return d0.compareTo(d1);
			}
		});
	}

	public static void orderByReCabinPrice(List<ReturnCabinEntity> entitys) {
		Collections.sort(entitys, new Comparator<ReturnCabinEntity>() {
			public int compare(ReturnCabinEntity arg0, ReturnCabinEntity arg1) {
				Double d0 = arg0.getPrice(), d1 = arg1.getPrice();
				if (d1 == null) return -1;
				if (d0 == null) return 1;
				return d0.compareTo(d1);
			}
		});
	}

	public static void orderByGoCabinPrice(List<CabinEntity> entitys) {
		Collections.sort(entitys, new Comparator<CabinEntity>() {
			public int compare(CabinEntity arg0, CabinEntity arg1) {
				Double d0 = arg0.getPrice(), d1 = arg1.getPrice();
				if (d1 == null) return -1;
				if (d0 == null) return 1;
				return d0.compareTo(d1);
			}
		});
	}

	/** 设置最高和最低价格。 */
	public static void setLowAndHihtPrice(List<AbstractPlaneInfoEntity> results, Map<String, Double> map) {
		for (AbstractPlaneInfoEntity absPlaneInfo : results) {
			DoublePlaneInfoEntity planeInfo = (DoublePlaneInfoEntity) absPlaneInfo;
			List<CabinRelationEntity> tempCabin = Lists.newArrayList();
			tempCabin.addAll(planeInfo.getCabinRelations());
			orderByRelation(tempCabin);
			CabinRelationEntity low = tempCabin.get(0);
			CabinRelationEntity high = tempCabin.get(tempCabin.size() - 1);

			absPlaneInfo.setTotalLowestPrice(low.getFullPrice());
			absPlaneInfo.setTotalLowestTaxesPrice(low.getTaxesPrice());
			absPlaneInfo.setSumLowestPrice(low.getTotalFullPrice());
			absPlaneInfo.setTotalHighestPrice(high.getFullPrice());
			absPlaneInfo.setTotalHighestTaxesPrice(high.getTaxesPrice());
			absPlaneInfo.setSumHighestPrice(high.getTotalFullPrice());

			// 获取去程的最低价格舱位
			List<CabinEntity> goCabins = Lists.newArrayList();
			goCabins.addAll(planeInfo.getCabins());
			orderByGoCabinPrice(goCabins);
			CabinEntity golow = goCabins.get(0);
			CabinEntity gohigh = goCabins.get(goCabins.size() - 1);

			// 设置回程最低价格。
			for (ReturnDoublePlaneInfoEntity returnInfo : planeInfo.getReturnPlaneInfos()) {

				// 获取去程的最低价格舱位
				List<ReturnCabinEntity> reCabins = Lists.newArrayList();
				reCabins.addAll(returnInfo.getReturnCabins());
				orderByReCabinPrice(reCabins);
				ReturnCabinEntity returnlow = reCabins.get(0);
				ReturnCabinEntity returnhigh = reCabins.get(reCabins.size() - 1);

				Double lowPrice = addDouble(golow.getPrice(), returnlow.getPrice());
				Double highPrice = addDouble(gohigh.getPrice(), returnhigh.getPrice());

				// 回程的最低打包价=当前回程航班舱位最低价格加上去程航班仓位最低价
				returnInfo.setTotalLowestPrice(lowPrice);
				returnInfo.setTotalHighestPrice(highPrice);
				returnInfo.setTotalLowestTaxesPrice(map.get(lowPrice));
				returnInfo.setTotalHighestTaxesPrice(map.get(highPrice));
				returnInfo.setSumLowestPrice(addDouble(lowPrice, map.get(lowPrice)));
				returnInfo.setSumHighestPrice(addDouble(highPrice, map.get(highPrice)));
			}
		}
	}

	public static Double addDouble(Double d1, Double d2) {
		if (d1 == null || d2 == null) {
			return null;
		}
		return d1 + d2;
	}
}
