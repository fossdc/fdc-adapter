package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import net.sf.json.JSONObject;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.elong.ElongDomesticOneWayTrip;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.elong.ElongInternationRoundTrip;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 艺龙适配器。
 */
public class ELongAdapter extends AbstractAdapter {

	public ELongAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	public String getUrl() throws Exception {
		return null;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (getRouteType()) {
		case DOMESTIC_ONEWAYTRIP:// 国内单程。 
		case DOMESTIC_ROUNDTRIP:// 国内往返,和单程一样处理。
			return new ElongDomesticOneWayTrip(taskQueue, this).fetchData();
		case INTERNATIONAL_ONEWAY:// 国际单程，和往返统一处理。
		case INTERNATIONAL_ROUND:// 国际往返。
			return new ElongInternationRoundTrip(this, taskQueue).fetchData();
		default:
			return null;
		}
	}

	@Override
	public List<Object> paraseToVo(Object fetchObject) throws Exception {
		return (List<Object>) fetchObject;
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return true;
	}
}
