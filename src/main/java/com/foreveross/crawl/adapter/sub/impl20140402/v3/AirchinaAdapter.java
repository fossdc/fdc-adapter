package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.airchina.AirchinaInterTrip;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 国航国际适配器。
 */
public class AirchinaAdapter extends AbstractAdapter {

	
	public AirchinaAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}


	/** ******************************** Override *********************************************** */

	@Override
	public String getUrl() throws Exception {
		return null;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ONEWAY:
		case INTERNATIONAL_ROUND:
		case DOMESTIC_ONEWAYTRIP:
			return new AirchinaInterTrip(taskQueue, this).fetchData();
		default:
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		return (List<Object>)obj;
	}


	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return true;
	}
}
