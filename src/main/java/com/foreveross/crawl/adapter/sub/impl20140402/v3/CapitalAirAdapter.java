package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.single.SinglePlaneInfoEntity;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * http://www.capitalairlines.com.cn/ 首都航空适配器
 * 
 * @author gan
 */
public class CapitalAirAdapter extends AbstractAdapter {

	public CapitalAirAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	public static void main(String[] args) throws Exception {
		TaskModel taskQueue = new TaskModel();

		// taskQueue.setGrabChannelId(2L);
		taskQueue.setIsInternational(0);
		taskQueue.setFlightDate("2014-07-06");
		taskQueue.setReturnGrabDate("2014-07-07");
		taskQueue.setIsReturn(0);
		taskQueue.setFromCity("CAN");
		taskQueue.setToCity("SYX");
		CapitalAirAdapter adapter = new CapitalAirAdapter(taskQueue);
		Object obj = adapter.fetch("");
		List list = adapter.paraseToVo(obj);
		System.out.println(list.size());
		System.out.println(list.get(0).toString());

	}

	// 明折明扣 抄底价 优飞优保 map
	private final static Map<String, String> airRoomMap = new HashMap<String, String>();
	static {
		airRoomMap.put("MZMK", "明折明扣");
		airRoomMap.put("CCDJ", "抄底价");
		airRoomMap.put("YFYB", "优飞优保");
		airRoomMap.put("WZZX", "阶梯go");
	}

	/**
	 * 国内单程
	 */
	public List<Object> paraseToVoS(Object fetchObject) throws Exception {
		List<Object> dataList = new ArrayList<Object>();
		SinglePlaneInfoEntity sigPI = new SinglePlaneInfoEntity();
		Set<CabinEntity> cabinSet = new HashSet<CabinEntity>();// 全程舱位
		List<String> priceList = new ArrayList<String>();
		JSONObject jsonObj = JSONObject.fromObject(fetchObject);
		String fligthDateStr = jsonObj.getString("flightDate");// 航班日期
		sigPI.setFlightDate(fligthDateStr);
		JSONArray jsonArr = jsonObj.getJSONArray("flights");
		if (jsonArr.size() > 0) {
			JSONObject obj = jsonArr.getJSONObject(0);// 只有一个"flights"信息
			String fromCityCode = obj.getString("firstDeparturePort");// 出发城市CODE
			String toCityCode = obj.getString("finalDestinationPort");// 到达城市CODE
			sigPI.setFromCity(fromCityCode);
			sigPI.setToCity(toCityCode);
			JSONArray segmentArray = obj.getJSONArray("segments");
			if (segmentArray.size() > 0) {
				JSONObject segmentObj = segmentArray.getJSONObject(0);
				String flightStyle = segmentObj.getString("aircraftStyle");// 飞机类型
				String arriveTimeStr = segmentObj.getString("arriveTime")
						.replace("T", " ");// 到在时间Str
				String departureTimeStr = segmentObj.getString("departureTime")
						.replace("T", " ");// 出发时间Str;
				String flightNo = segmentObj.getString("carrier")
						+ segmentObj.getString("flightNo");// 航班号
				String isStop = segmentObj.getString("stops");// 是否经停：0表示不停
				sigPI.setFlightType(flightStyle);
				sigPI.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm:ss",
						arriveTimeStr));
				sigPI.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm:ss",
						departureTimeStr));
				sigPI.setFlightNo(flightNo);

				// 舱位信息
				JSONArray cabinObje = segmentObj.getJSONArray("cabins");
				if (cabinObje.size() > 0) {// 共有小舱位数
					// int test=1;
					for (int i = 0; i < cabinObje.size(); i++) {
						JSONObject cabinsName = cabinObje.getJSONObject(i);
						String cabinNames = cabinsName.getString("cabinCode");// 小舱位（Y舱，Q舱......）
						JSONArray productS = cabinsName
								.getJSONArray("productOffers");
						if (productS.size() > 0) {
							for (int k = 0; k < productS.size(); k++) {
								CabinEntity cabinEn = new CabinEntity();
								cabinEn.setSubCabinName(cabinNames);
								JSONObject productObject = productS
										.getJSONObject(k);
								JSONObject canbinArrayS = productObject
										.getJSONObject("adultFare");
								String canbinCode = productObject
										.getString("code");
								double taxesPrice = canbinArrayS
										.getDouble("airportTax")
										+ canbinArrayS
												.getDouble("fuelSurcharge");
								String canbinName = airRoomMap.get(canbinCode);
								cabinEn.setProductName(canbinName);
								sigPI.setTotalLowestTaxesPrice(taxesPrice);
								sigPI.setTotalHighestTaxesPrice(taxesPrice);
								if (canbinArrayS.size() > 0) {
									String prices = canbinArrayS.get(
											"farePrice").toString();
									priceList.add(prices);
									cabinEn.setPrice(Double.parseDouble(prices));
									cabinEn.setTaxesPrice(taxesPrice);
									cabinEn.setOriginalPrice(cabinEn
											.getTaxesPrice()
											+ cabinEn.getPrice());
									cabinSet.add(cabinEn);
								} else {
									throw new FlightInfoNotFoundException(
											"没有相关舱位信息！");
								}
							}
						}
					}
				} else {
					throw new FlightInfoNotFoundException("票已经买完了！");
				}
			}

		} else {
			throw new FlightInfoNotFoundException("航班日期：" + fligthDateStr
					+ "没有航班！");
		}
		if (cabinSet.size() > 0) {
			sigPI.setCabins(cabinSet);
		}
		sigPI.setTotalHighestPrice(this.maxPice(priceList));
		sigPI.setTotalLowestPrice(this.minPice(priceList));
		sigPI.setSumHighestPrice(sigPI.getTotalHighestTaxesPrice()
				+ sigPI.getTotalHighestPrice());
		sigPI.setSumLowestPrice(sigPI.getTotalLowestPrice()
				+ sigPI.getTotalLowestTaxesPrice());
		sigPI.setFromCityName(taskQueue.getFromCityName());
		sigPI.setToCityName(taskQueue.getToCityName());
		sigPI.setCreateTime(new Date());
		sigPI.setAreaName(taskQueue.getAreaName());
		sigPI.setAreaCode(taskQueue.getAreaCode());
		sigPI.setGrabChannelId(taskQueue.getGrabChannelId());
		sigPI.setGrabChannelName(taskQueue.getGrabChannel());
		sigPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
		sigPI.setCarrierFullName("首都航空公司");
		sigPI.setCarrierKey("JD");
		sigPI.setCarrierName("首都航空");
		dataList.add(sigPI);
		return dataList;
	}

	/**
	 * 国内往返
	 */
	public List<Object> paraseToVoRT(Object fetchObject) throws Exception {
		// System.out.println(fetchObject);
		DoublePlaneInfoEntity doublePI = new DoublePlaneInfoEntity();// 往返;
		ReturnDoublePlaneInfoEntity returnPI = new ReturnDoublePlaneInfoEntity();// 回程信息实体
		List<String> priceList = new ArrayList<String>();// 全程价格List
		List<String> rePriceList = new ArrayList<String>();// 返程价List
		Set<ReturnCabinEntity> returnCabins = new HashSet<ReturnCabinEntity>();// 回程舱位信息
		Set<CabinEntity> cabinSet = new HashSet<CabinEntity>();// 全程舱位信息
		Set<ReturnDoublePlaneInfoEntity> returnPlanInfos = new HashSet<ReturnDoublePlaneInfoEntity>(); // 返程信息总和
		List<Object> dataList = new ArrayList<Object>();
		JSONObject jsonObj = JSONObject.fromObject(fetchObject);//System.out.println(fetchObject);
		JSONArray jsonArr = jsonObj.getJSONArray("jsonObject");
		if (jsonArr.size() == 2) {// 表示有往返数据
			for (int i = 0; i < jsonArr.size(); i++) {// 第1次为出发信息，第2次为返程信息
				String flightNo = "", toCityCode = "", fromCityCode = "", fligthDateStr = "", flightStyle = "", arriveTimeStr = "", departureTimeStr = "";
				JSONObject objs = jsonArr.getJSONObject(i);
				fligthDateStr = objs.getString("flightDate");
				JSONArray objectArray = objs.getJSONArray("flights");
				if (objectArray.size() > 0) {
					JSONObject obj = objectArray.getJSONObject(0);// 只有一个"flights"信息
					fromCityCode = obj.getString("firstDeparturePort");// 出发城市CODE
					toCityCode = obj.getString("finalDestinationPort");// 到达城市CODE
					JSONArray segmentArray = obj.getJSONArray("segments");
					if (segmentArray.size() > 0) {
						JSONObject segmentObj = segmentArray.getJSONObject(0);
						flightStyle = segmentObj.getString("aircraftStyle");// 飞机类型
						arriveTimeStr = segmentObj.getString("arriveTime")
								.replace("T", " ");// 到在时间Str
						departureTimeStr = segmentObj
								.getString("departureTime").replace("T", " ");// 出发时间Str;
						flightNo = segmentObj.getString("carrier")
								+ segmentObj.getString("flightNo");// 航班号
						String isStop = segmentObj.getString("stops");// 是否经停：0表示不停
						// 舱位信息
						JSONArray cabinObje = segmentObj.getJSONArray("cabins");
						if (cabinObje.size() > 0) {// 共有小舱位数
							for (int j = 0; j < cabinObje.size(); j++) {
								JSONObject cabinsName = cabinObje
										.getJSONObject(j);
								String cabinNames = cabinsName
										.getString("cabinCode");// 小舱位（Y舱，Q舱......）
								JSONArray productS = cabinsName
										.getJSONArray("productOffers");
								if (productS.size() > 0) {
									for (int k = 0; k < productS.size(); k++) {
										CabinEntity cabinEn = new CabinEntity();
										cabinEn.setSubCabinName(cabinNames);
										JSONObject productObject = productS
												.getJSONObject(k);
										JSONObject canbinArrayS = productObject
												.getJSONObject("adultFare");
										String canbinCode = productObject
												.getString("code");
										String canbinName = airRoomMap
												.get(canbinCode);
										cabinEn.setProductName(canbinName);
										if (canbinArrayS.size() > 0) {
											double prices = canbinArrayS
													.getDouble("farePrice");
											priceList.add(prices + "");
											cabinEn.setPrice(prices);
											double taxesPrice = canbinArrayS
													.getDouble("airportTax")
													+ canbinArrayS
															.getDouble("fuelSurcharge");// 税价
											cabinEn.setTaxesPrice(taxesPrice);
											cabinEn.setOriginalPrice(taxesPrice
													+ prices);// 增加总价
											cabinSet.add(cabinEn);
											doublePI.setTotalLowestTaxesPrice(taxesPrice);
											doublePI.setTotalHighestTaxesPrice(taxesPrice);
											if (i == 1) {// 回程舱位信息
												ReturnCabinEntity recabinEn = new ReturnCabinEntity();
												recabinEn.setPrice(prices);
												recabinEn
														.setSubCabinName(cabinNames);
												recabinEn
														.setProductName(canbinName);
												recabinEn
														.setTaxesPrice(taxesPrice);// 增加回程税价
												recabinEn
														.setOriginalPrice(taxesPrice
																+ prices);// 增加回程总价
												rePriceList.add(prices + "");
												returnCabins.add(recabinEn);
												returnPI.setTotalLowestTaxesPrice(taxesPrice);
												returnPI.setTotalHighestTaxesPrice(taxesPrice);
											}
										} else {
											throw new FlightInfoNotFoundException(
													"没有相关舱位信息！");
										}
									}
								}
							}
						}
					}
				} else {
					throw new FlightInfoNotFoundException("航班日期："
							+ fligthDateStr + "没有航班！");
				}
				if (i == 0) {// 组装去程信息
					doublePI.setFromCity(fromCityCode);
					doublePI.setToCity(toCityCode);
					doublePI.setFlightNo(flightNo);
					doublePI.setFlightType(flightStyle);
					doublePI.setFlightDate(fligthDateStr);
					doublePI.setStartTime(DateUtil.StringToDate(
							"yyyy-MM-dd HH:mm:ss", departureTimeStr));
					doublePI.setEndTime(DateUtil.StringToDate(
							"yyyy-MM-dd HH:mm:ss", arriveTimeStr));
					doublePI.setFromCityName(taskQueue.getFromCityName());
					doublePI.setToCityName(taskQueue.getToCityName());
					doublePI.setCreateTime(new Date());
					doublePI.setAreaName(taskQueue.getAreaName());
					doublePI.setAreaCode(taskQueue.getAreaCode());
					doublePI.setGrabChannelId(taskQueue.getGrabChannelId());
					doublePI.setGrabChannelName(taskQueue.getGrabChannel());
					doublePI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
					doublePI.setCarrierFullName("首都航空公司");
					doublePI.setCarrierKey("JD");
				}
				if (i == 1) {// 组装返程信息
					returnPI.setFromCity(fromCityCode);
					returnPI.setToCity(toCityCode);
					returnPI.setFlightNo(flightNo);
					returnPI.setFlightType(flightStyle);
					returnPI.setFlightDate(fligthDateStr);
					returnPI.setStartTime(DateUtil.StringToDate(
							"yyyy-MM-dd HH:mm:ss", departureTimeStr));
					returnPI.setEndTime(DateUtil.StringToDate(
							"yyyy-MM-dd HH:mm:ss", arriveTimeStr));
					returnPI.setReturnCabins(returnCabins);
					returnPI.setFromCityName(taskQueue.getFromCityName());
					returnPI.setToCityName(taskQueue.getToCityName());
					returnPI.setCreateTime(new Date());
					returnPI.setAreaName(taskQueue.getAreaName());
					returnPI.setAreaCode(taskQueue.getAreaCode());
					returnPI.setGrabChannelId(taskQueue.getGrabChannelId());
					returnPI.setGrabChannelName(taskQueue.getGrabChannel());
					returnPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
					returnPI.setCarrierFullName("首都航空公司");
					returnPI.setCarrierKey("JD");
					returnPI.setCarrierName("首都航空");
					returnPI.setTotalHighestPrice(this.maxPice(rePriceList));
					returnPI.setTotalLowestPrice(this.minPice(rePriceList));
					returnPI.setSumHighestPrice(returnPI.getTotalHighestPrice()
							+ returnPI.getTotalHighestTaxesPrice());
					returnPI.setSumLowestPrice(returnPI.getTotalLowestPrice()
							+ returnPI.getTotalLowestTaxesPrice());
					returnPlanInfos.add(returnPI);
				}
			}
			doublePI.setTotalHighestPrice(this.maxPice(priceList));
			doublePI.setTotalLowestPrice(this.minPice(priceList));
			doublePI.setSumHighestPrice(doublePI.getTotalHighestPrice()
					+ doublePI.getTotalHighestTaxesPrice());
			doublePI.setSumLowestPrice(doublePI.getTotalLowestPrice()
					+ doublePI.getTotalLowestTaxesPrice());
			doublePI.setCabins(cabinSet);
			doublePI.setReturnPlaneInfos(returnPlanInfos);
			dataList.add(doublePI);
			rollbackProxyIp(true);
			return dataList;
		} else {
			throw new FlightInfoNotFoundException("往返程信息获取错误！");
		}
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return null;// 国际往返
		case INTERNATIONAL_ONEWAY:
			return null;// 国际单程
		case DOMESTIC_ONEWAYTRIP:
			return paraseToVoS(obj);// 国内单程
		case DOMESTIC_ROUNDTRIP:
			return paraseToVoRT(obj);// 国内往返
		}
		return null;
	}

	// 国内单程ＵＲＬ

	public HttpPost getUrls() throws Exception {
		Map<String, String> pMap = new HashMap<String, String>();
		// http://www.capitalairlines.com.cn/flight/flightresult.action?dstCity=CKG&flightDate=2014-04-20&index=1&orgCity=CAN
//		String url = "http://www.capitalairlines.com.cn/flight/flightresult.action";
		String url="http://www.jdair.net/flight/flightresult.action";
		pMap.put("dstCity", taskQueue.getToCity());
		pMap.put("flightDate", taskQueue.getFlightDate());
		pMap.put("index", "1");
		pMap.put("orgCity", taskQueue.getFromCity());
		// logger.info(url);
		return this.getBasePost(url, pMap);
	}

	// 国内往返ＵＲＬ
	public HttpPost[] getUrlRe() throws Exception {
		Map<String, String> pMap = new HashMap<String, String>();
		Map<String, String> pMap2 = new HashMap<String, String>();
		// 去程ＵＲＬ
		String url = "http://www.capitalairlines.com.cn/flight/flightresult.action";
		pMap.put("dstCity", taskQueue.getToCity());
		pMap.put("flightDate", taskQueue.getFlightDate());
		pMap.put("index", "1");
		pMap.put("orgCity", taskQueue.getFromCity());
		HttpPost h1 = this.getBasePost(url, pMap);
		// 返程ＵＲＬ
		pMap2.put("dstCity", taskQueue.getFromCity());
		pMap2.put("flightDate", taskQueue.getReturnGrabDate());
		pMap2.put("index", "2");
		pMap2.put("orgCity", taskQueue.getToCity());
		HttpPost h2 = this.getBasePost(url, pMap2);
		HttpPost[] H = { h1, h2 };
		return H;
	}

	@Override
	public String getUrl() throws Exception {
		return null;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return null;// 国际往返
		case INTERNATIONAL_ONEWAY:
			return null;// 国际单程
		case DOMESTIC_ONEWAYTRIP:
			return fetchDomeOneWay();// 国内单程
		case DOMESTIC_ROUNDTRIP:
			return fetchDomeRT();// 国内往返
		}
		return null;
	}

	/**
	 * 各种舱位（超低价，优飞优保，明折明扣） 国内单程
	 * 
	 * @return
	 * @param getURL
	 *            ()
	 * @throws Exception
	 */
	private Object fetchDomeOneWay() throws Exception {
		HttpClient client = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpEntity entity = null;

		try {
			client = super.getHttpClient();
			httpPost = this.getUrls();
			httpPost.setHeader("x-requested-with", "XMLHttpRequest");
			httpPost.setHeader("Accept-Language",
					"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			httpPost.setHeader("Accept", "*/*");
			httpPost.setHeader("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			// httpPost.setHeader("Accept-Encoding",
			// "gzip, deflate");//跟你说，这是一定在注释掉的
			httpPost.setHeader("Content-Typ",
					"application/x-www-form-urlencoded; charset=UTF-8");
//			httpPost.setHeader("Host", "www.capitalairlines.com.cn");
			httpPost.setHeader("Host", "www.jdair.net");
			/*httpPost.setHeader("Referer",
					"http://www.capitalairlines.com.cn/flight/searchflight.action?dstCity="
							+ taskQueue.getFromCity() + "&flightDate="
							+ taskQueue.getFlightDate() + "&orgCity="
							+ taskQueue.getToCity()
							+ "&returnDate=&tripType=ONEWAY");*/
			httpPost.setHeader("Referer","http://www.jdair.net/flight/searchflight.action");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/31.0");
			httpPost.setHeader("Pragma", "	no-cache");
			httpPost.setHeader("Cache-Contro", "no-cache");
			httpPost.setHeader("Connection", "keep-alive");

			response = client.execute(httpPost);
			entity = response.getEntity();
			String result = EntityUtils.toString(entity);
			super.appendPageContents(result);
			int lenght = result.getBytes().length;// 设置流量统计
			setLenghtCount(lenght);
			if(lenght>2000) rollbackProxyIp(true);
			else rollbackProxyIp(false);
			return result;
		} finally {
			if (httpPost != null)
				httpPost.releaseConnection();
			client = null;
			httpPost = null;
			if (entity != null)
				EntityUtils.consume(entity);
			entity = null;
			response = null;
		}

	}

	/**
	 * 各种舱位（超低价，优飞优保，明折明扣） 国内往返
	 * 
	 * @return
	 * @param getRTUrl
	 *            ()
	 * @throws Exception
	 */
	private Object fetchDomeRT() throws Exception {

		HttpClient client = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpEntity entity = null;
		net.htmlparser.jericho.Source source = null;
		try {
			client = super.getHttpClient();
			String result = "", results = "";
			;
			String result1 = "", result2 = "";
			for (int i = 0; i < this.getUrlRe().length; i++) {
				httpPost = this.getUrlRe()[i];
				httpPost.setHeader("x-requested-with", "XMLHttpRequest");
				httpPost.setHeader("Accept-Language",
						"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
				httpPost.setHeader("Accept", "*/*");
				httpPost.setHeader("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				// httpPost.setHeader("Accept-Encoding",
				// "gzip, deflate");//跟你说，这是一定在注释掉的
				httpPost.setHeader("Content-Typ",
						"application/x-www-form-urlencoded; charset=UTF-8");
				httpPost.setHeader("Host", "www.capitalairlines.com.cn");
				httpPost.setHeader(
						"Referer",
						"http://www.capitalairlines.com.cn/flight/searchflight.action?dstCity="
								+ taskQueue.getFromCity() + "&flightDate="
								+ taskQueue.getFlightDate()
								+ "&lowestPrice=0&orgCity="
								+ taskQueue.getToCity() + "&returnDate="
								+ taskQueue.getReturnGrabDate()
								+ "&tripType=RETURN");
				httpPost.setHeader("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0");
				httpPost.setHeader("Pragma", "	no-cache");
				httpPost.setHeader("Cache-Contro", "	no-cache");
				httpPost.setHeader("Connection", "keep-alive");
				response = client.execute(httpPost);
				entity = response.getEntity();
				// result+=EntityUtils.toString(entity);
				results = EntityUtils.toString(entity);
				if (i == 0) {
					result1 = results;
				}
				if (i == 1) {
					result2 = results;
				}
			}
			result = "{'jsonObject':" + result1 + ",'jsonObject':" + result2
					+ "}";
			super.appendPageContents(result);
			int lenght = super.getSourceWebPageContents().toString().getBytes().length;// 设置流量统计
			if(lenght<2000)rollbackProxyIp(false);
			setLenghtCount(lenght);

			return result;
		} finally {
			if (httpPost != null)
				httpPost.releaseConnection();
			client = null;
			httpPost = null;
			if (entity != null)
				EntityUtils.consume(entity);
			entity = null;
			response = null;
		}

	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return false;
	}

	// List<String> price && max, min
	private Double maxPice(List<String> listSgtr) {
		Double maxPrices = 0.0;
		if (listSgtr.size() > 0) {
			for (String prices : listSgtr) {
				if (Double.parseDouble(prices) > maxPrices) {
					maxPrices = Double.parseDouble(prices);
				}
			}
		}
		return maxPrices;
	}

	private Double minPice(List<String> listSgtr) {
		Double minPrices = 999999999.0;
		if (listSgtr.size() > 0) {
			for (String prices : listSgtr) {
				if (Double.parseDouble(prices) < minPrices) {
					minPrices = Double.parseDouble(prices);
				}
			}
		}
		return minPrices;
	}

}
