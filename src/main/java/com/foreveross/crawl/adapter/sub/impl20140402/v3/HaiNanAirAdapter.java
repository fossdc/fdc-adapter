package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.exception.self.PageErrorResultException;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.RandomBrowserVersion;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.CabinRelationEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.domain.airfreight.single.SinglePlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.crawl.exception.NoTicketException;
import com.foreveross.proxyip.ProxyIpModel;
import com.foreveross.taskservice.common.bean.TaskModel;

public class HaiNanAirAdapter extends AbstractAdapter {
	
	//经济舱产品代码Map
	private final static Map<String,String> economyProductNameMap =new HashMap<String,String>();
	//头等、商务舱Map
	private final static Map<String,String> firstProductNameMap =new HashMap<String,String>();
	static{
		economyProductNameMap.put("SSAC", "尊享经济舱");
		economyProductNameMap.put("GCGF", "锦程添金");
		economyProductNameMap.put("WO", "网站专享");
		economyProductNameMap.put("TJ", "特价票");
		economyProductNameMap.put("NORMAL", "原价票");
		
		firstProductNameMap.put("R", "钻石头等舱");
		firstProductNameMap.put("F", "头等舱");
		firstProductNameMap.put("C", "公务舱");
		firstProductNameMap.put("SSAC", "舒适A舱");
		firstProductNameMap.put("WO", "网站专享");
		firstProductNameMap.put("NORMAL", "原价票");
	}
	
	//税价，一次航班查询的税价认为是一致的
	private double taxPrice;
	
	public HaiNanAirAdapter(TaskModel taskQueue){
		super(taskQueue);
		//统一处理下城市名称带三字码问题
		taskQueue.setFromCityName(taskQueue.getFromCityName().replaceAll("\\(.*\\)", ""));
		taskQueue.setToCityName(taskQueue.getToCityName().replaceAll("\\(.*\\)", ""));
	}
	public List<Object> paraseToVoS(Object fetchObject) throws Exception {
		String tempResult = fetchObject.toString().trim();
		List<Object> list;
		list = parseSource(new Source(tempResult));
		tempResult = null;
		return list;
	}
	/**
	 * 国际返往（头等，商务舱）+经济
	*/
	public List<Object> paraseToVoDI(Object fetchObject) throws Exception {
		return parseSourceDI(new Source(fetchObject.toString().trim()));
	}
	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return paraseToVoDI(obj);//国际头等,经济往返
		case INTERNATIONAL_ONEWAY:
			return paraseToVoDI(obj);//国际单程（头等/经济）
		case DOMESTIC_ONEWAYTRIP:
//			return paraseToVoS(obj);//国内单程
			return paraseDomeOneWay(obj);
		}
		return null;
	}
	
	
	//海南航空目前的情况是，如果不存在该航班信息，你是无法提交的，所以我们通过代码提交的情况就是出现页面的乱码，简单的就是乱码无航班
	//而如果有航班但是没有票了，会出现“已售完”字段
	
	/**
	 * 验证有没有该航班
	 * 
	 * @throws Exception
	 */
	private void validateFlight(List<Element> calendar_title) throws Exception{
		if(calendar_title==null || calendar_title.isEmpty()){
			throw new FlightInfoNotFoundException();
		}
	}
	/**
	 * 验证还有没有有票和航班
	 * @throws Exception
	 */
	private void validateTicketAndFlight(Element table,List<Element> trLists) throws Exception{
		if(trLists==null || trLists.size()==1){//有可能出现售完
			String tdStr = trLists.get(0).getAllElements("td").get(0).toString();
			if(tdStr.matches("(.*已售完.*)|(.*无适用的价格.*)")){
				throw new NoTicketException();
			}else if(tdStr.matches("(.*无航班.*)|(.*没有符合条件的查询结果.*)|(.*没有\\W*航班飞行.*)")){
				throw new FlightInfoNotFoundException("没有此航班!");
			}
			String info=getTableContent(table,0,0).getTextExtractor().toString();
			if(info!=null && info.matches("(.*已售完.*)|(.*无航班.*)|(.*没有符合条件的查询结果.*)")){
				throw new NoTicketException();
			}
			if(info==null ){
				throw new FlightInfoNotFoundException("没有此航班!");
			}
		}else{
			String tdStr = trLists.get(0).getAllElements("td").get(0).toString();
			if(tdStr.matches("(.*已售完.*)|(.*无适用的价格.*)")){
				throw new NoTicketException();
			}else if(tdStr.matches("(.*无航班.*)|(.*没有符合条件的查询结果.*)")){
				throw new FlightInfoNotFoundException("没有此航班!");
			}
		}
	}
	
	/**
	 * 海南——国际往返头等/商务+经济
	*/
	private List<Object> parseSourceDI(Source sources) throws Exception {
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		DoublePlaneInfoEntity doublePI = null;
		SinglePlaneInfoEntity sigPI=null;
		ReturnDoublePlaneInfoEntity returnPI=null;
		ArrayList<TransitEntity> transitSet =null;
		ArrayList<ReturnTransitEntity> reTransitSet =null;
		Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabinSet =null;
		com.foreveross.crawl.domain.airfreight.CabinEntity cabinEn=null;
		Set<ReturnDoublePlaneInfoEntity> returnPlanInfos=null; //返程信息总和
		int falg=0;
		List<Element> bodyList= sources.getAllElements("body");//经济和头等合在一起了，根据body分开两网页
		
		//没有航班的舱位数,现只有两种舱位
		int noFlight = 0;
		for(int allFlights=0;allFlights<bodyList.size();allFlights++){
			//一种舱位没航班或者没票则继续下一种舱位
			try{
			Element source=bodyList.get(allFlights);
			falg++;	
			List<Element> es= source.getAllElementsByClass("calendar_title");
			for (Element e : es) {//理论上只有一个，表示只有一条航线
				String a = e.getTextExtractor().toString();
				String citys[]=a.split(" - ");
				String fromCityNames=citys[0];
				String toCityNames=citys[1].split(" ")[0];
				
				List<Element> trLists = source.getAllElementsByClass("tbody");
				validateTicketAndFlight(e,trLists);
				for(int i=0;i<trLists.size();i++){
					sigPI=new SinglePlaneInfoEntity();//单程
					doublePI =new DoublePlaneInfoEntity();//往返
					 returnPI=new ReturnDoublePlaneInfoEntity();
					transitSet=new ArrayList<TransitEntity>();//去程中转Set
					reTransitSet=new ArrayList<ReturnTransitEntity>();//回程中转Set
					cabinSet =new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();//舱位信息
					returnPlanInfos=new HashSet<ReturnDoublePlaneInfoEntity>(); //返程信息总和
					
					if (i % 2 == 0) {//排除<tr class="tbody price_all">下的内容  //有多少条航线
						List<Element> aList=trLists.get(i).getAllElements("a");
						//舱位价格
						List<Element> priceList=trLists.get(i).getFirstElementByClass("left").getAllElements("b");
						String prices =null;
						if(priceList.size()>0){
							prices=trLists.get(i).getFirstElementByClass("left").getAllElements("b").get(0).getTextExtractor().toString();
						}else{
							throw new FlightInfoNotFoundException("没有航班");
						}
						for(int k=0;k<aList.size();k++){//aList.size()表示有多少条航班(包括来回程的)
							List<Element> divList=aList.get(k).getAllElementsByClass("list");
							if(divList.size()>0){
								String flightNo=divList.get(0).getAllElements("span").get(0).getTextExtractor().toString();//航班号
								String depFromP=divList.get(1).getAllElements("span").get(0).getTextExtractor().toString();//出发时间和出发地 
								//2013-12-12 10:10 星期四 北京首都机场  T3  航站楼
								String depDateTime[]=depFromP.split(" ");
								String arrTop=divList.get(2).getAllElements("span").get(0).getTextExtractor().toString();//到达时间和到达地
								String arrDateTime[]=arrTop.split(" ");
								String acturryConpany=divList.get(3).getAllElements("span").get(0).getTextExtractor().toString();//实际承运航空公司
								String isStop=divList.get(4).getAllElements("span").get(0).getTextExtractor().toString();//是否经停
								String flightType=divList.get(5).getAllElements("span").get(0).getTextExtractor().toString();//飞机类型
								
							if(DateUtil.compareDate(taskQueue.getReturnGrabDate(),depFromP.split(" ")[0])){//去程信息
								if(k==0){//第一趟表示去程
									if(taskQueue.getIsReturn()==1){//表示往返
										doublePI.setFlightNo(flightNo);
										doublePI.setFlightDate(depDateTime[0]);
										doublePI.setFromCityName(fromCityNames);
									//	doublePI.setToCityName(toCityNames);
										doublePI.setFlightType(flightType);
										doublePI.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",depDateTime[0]+" "+depDateTime[1]));
									}if(taskQueue.getIsReturn()==0){//表示单程
										sigPI.setFlightNo(flightNo);
										sigPI.setFlightDate(depDateTime[0]);
										sigPI.setFromCityName(fromCityNames);
										sigPI.setFlightType(flightType);
										sigPI.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",depDateTime[0]+" "+depDateTime[1]));
									}
									
								}
								if(!(arrDateTime[3].indexOf(toCityNames) !=-1 && depDateTime[3].indexOf(fromCityNames)!=-1 )){//去程中转
									TransitEntity transitEntity = new TransitEntity();
									transitEntity.setActuallyFlightNo("中国海南航空公司");
									transitEntity.setFlightNo(flightNo);
									transitEntity.setFromAirPortName(depDateTime[3]);
									transitEntity.setToAirPortName(arrDateTime[3]);
									transitEntity.setFlightType(flightType);
									transitEntity.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",depDateTime[0]+" "+depDateTime[1]));
									transitEntity.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",arrDateTime[0]+" "+arrDateTime[1]));
									transitSet.add(transitEntity);
								}
								if(arrDateTime[3].indexOf(toCityNames) !=-1 ){//表示最后一趟去程
	//								doublePI.setToCityName(toCityNames);
									if(taskQueue.getIsReturn()==1){
										doublePI.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",arrDateTime[0]+" "+arrDateTime[1]));
									}if(taskQueue.getIsReturn()==0){
										sigPI.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",arrDateTime[0]+" "+arrDateTime[1]));
									}
								}	
							}
							//回程信息
							if(k>0 && !DateUtil.compareDate(taskQueue.getReturnGrabDate(),depFromP.split(" ")[0]) && taskQueue.getIsReturn()==1){
								if( depDateTime[3].indexOf(toCityNames) != -1){//第一趟回程
								returnPI.setFlightNo(flightNo);
								returnPI.setFlightDate(depDateTime[0]);
								returnPI.setFromCityName(toCityNames);
								returnPI.setToCityName(fromCityNames);
								returnPI.setFlightType(flightType);
								returnPI.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",depDateTime[0]+" "+depDateTime[1]));
								
						    }if(!(depDateTime[3].indexOf(toCityNames) != -1 && arrDateTime[3].indexOf(fromCityNames) !=-1 )){//回程中转
						    	ReturnTransitEntity reTransitE=new ReturnTransitEntity();//回程中转
						    	reTransitE.setFlightNo(flightNo);
								reTransitE.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",depDateTime[0]+" "+depDateTime[1]));
								reTransitE.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",arrDateTime[0]+" "+arrDateTime[1]));
								reTransitE.setFromAirPortName(depDateTime[3]);
								reTransitE.setToAirPortName(arrDateTime[3]);
								reTransitE.setFlightType(flightType);
								
								reTransitSet.add(reTransitE);
						    }if( arrDateTime[3].indexOf(fromCityNames) !=-1){//表示回程最后一趟
	//					    	returnPI.setFromCityName(toCityNames);
						    	returnPI.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm",arrDateTime[0]+" "+arrDateTime[1]));
							}
						    if(reTransitSet.size()>0){//有中转航班
						    	returnPI.setReturnTransits(reTransitSet);
						    }
 						    returnPI.setTotalHighestPrice(Double.parseDouble(prices.replace("CNY", "")));
						    returnPI.setTotalLowestPrice(Double.parseDouble(prices.replace("CNY", "")));
						    returnPI.setFromCityName(taskQueue.getToCityName());
						    returnPI.setToCityName(taskQueue.getFromCityName());
							returnPI.setAreaCode(taskQueue.getAreaCode());
							returnPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
							returnPI.setGrabChannelName(taskQueue.getGrabChannel());
							returnPI.setCarrierKey("HU");
							returnPI.setCarrierName("海航");
							returnPI.setCarrierFullName("中国海南航空公司");
							
						    returnPlanInfos.add(returnPI);
							}
						}else{
							throw new FlightInfoNotFoundException("没有航班");
						}
					}
						
						//航行时间，日期
						/*List<Element> tdLists=trLists.get(i).getAllElements("td");
						List<Element> timeDateList=tdLists.get(2).getAllElements("img");
						String timeList[]=tdLists.get(3).getTextExtractor().toString().split(" ");
						for(int k=0;k<timeList.length;k++){
							String time=timeList[0];
						}*/
						
						if(taskQueue.getIsReturn()==1){
							doublePI.setTotalHighestPrice(Double.parseDouble(prices.replace("CNY", "")));
							doublePI.setTotalLowestPrice(Double.parseDouble(prices.replace("CNY", "")));
							doublePI.setFromCityName(taskQueue.getFromCityName());
							doublePI.setFromCity(taskQueue.getFromCity());
							doublePI.setToCityName(taskQueue.getToCityName());
							doublePI.setToCity(taskQueue.getToCity());
							doublePI.setAreaCode(taskQueue.getAreaCode());
							doublePI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
							doublePI.setGrabChannelName(taskQueue.getGrabChannel());
							doublePI.setCarrierKey("HU");
							doublePI.setCarrierName("海航");
							doublePI.setCarrierFullName("中国海南航空公司");
							doublePI.setFlightReturnDate(taskQueue.getReturnGrabDate());
						}if(taskQueue.getIsReturn()==0){
							sigPI.setTotalHighestPrice(Double.parseDouble(prices.replace("CNY", "")));
							sigPI.setTotalLowestPrice(Double.parseDouble(prices.replace("CNY", "")));
							sigPI.setFromCityName(taskQueue.getFromCityName());
							sigPI.setFromCity(taskQueue.getFromCity());
							sigPI.setToCityName(taskQueue.getToCityName());
							sigPI.setToCity(taskQueue.getToCity());
							sigPI.setAreaCode(taskQueue.getAreaCode());
							sigPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
							sigPI.setGrabChannelName(taskQueue.getGrabChannel());
							sigPI.setCarrierKey("HU");
							sigPI.setCarrierName("海航");
							sigPI.setCarrierFullName("中国海南航空公司");
						}
						
						cabinEn=new com.foreveross.crawl.domain.airfreight.CabinEntity();
						if(falg==2){
							cabinEn.setCabinType("头等舱");//头等舱，公务舱都一样
						}if(falg==1){
							cabinEn.setCabinType("经济舱");
						}
						cabinEn.setPrice(Double.parseDouble(prices.replace("CNY", "")));
						cabinSet.add(cabinEn);
						
						
					}
					
					if(i % 2 == 0){
						if(taskQueue.getIsReturn()==1){
							doublePI.setCabins(cabinSet);
						}if(taskQueue.getIsReturn()==0){
							sigPI.setCabins(cabinSet);
						}
					
					if(transitSet.size()>0){//含有中转信息
						if(taskQueue.getIsReturn()==1){
							doublePI.setTransits(transitSet);
						}if(taskQueue.getIsReturn()==0){
							sigPI.setTransits(transitSet);
						}
					}
					if(taskQueue.getIsReturn()==1){
						doublePI.setReturnPlaneInfos(returnPlanInfos);
						//不存在双程实体才添加，否则只添加舱位
						DoublePlaneInfoEntity tmpDI=containAndGetDoublePlaneInfoEntity(dataList, doublePI,cabinEn);
						//将双程返程的舱位设置为与双程去程的一样(由特定场景业务可以确定)
						toUnique(tmpDI.getReturnPlaneInfos()).setReturnCabins(convertToRetCabinEntitys(tmpDI.getCabins()));
						
						//增加舱位关系，来回程共享一个价格的  modify at 14/07/04
						for(CabinEntity ca : tmpDI.getCabins()){
							Set<ReturnDoublePlaneInfoEntity> rps = tmpDI.getReturnPlaneInfos();
							for(ReturnDoublePlaneInfoEntity rp : rps){
								for(ReturnCabinEntity rc : rp.getReturnCabins()){
									//同一个价格的  来回舱都记录了总价
									if(ca.getPrice() == rc.getPrice()){
										CabinRelationEntity relation = new CabinRelationEntity();
										relation.setCabinId(ca.getId());
										relation.setFullPrice(ca.getPrice()); 
										relation.setReturnCabinId(rc.getId());
										
										tmpDI.getCabinRelations().add(relation);
									}
								}
							}
						}
						
					}if(taskQueue.getIsReturn()==0){
						containAndGetSinglePlaneInfoEntity(dataList, sigPI, cabinEn);
						//dataList.add(sigPI);
					}
					
					}
				}
			}
			}catch(NoTicketException ne){
				logger.error("无票", ne);
				noFlight ++;
				continue;
			}catch(FlightInfoNotFoundException fe){
				logger.error("无航班", fe);
				noFlight ++;
				continue;
			}
		}
		
		//两种舱位都查到没有航班
		if(noFlight == bodyList.size()  && dataList.isEmpty()){
			throw new FlightInfoNotFoundException();
		}
		
		
		//排序价格，设置最高最低价
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		}catch(Exception e){
			logger.info(e);
			logger.info("国内往返数据排序价格错误，请检查数据");
			try{
				logger.info(dataList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
			throw e;
		}
		return Arrays.asList(dataList.toArray());
	}
	
	/**
	 * 返回集合类型的惟一值
	 * @param set
	 * @return
	 */
	 private <T> T toUnique(Set<T> set){
		 if(null==set || set.size()==0){
			 return null;
		 }
		 else{
			 for(T t:set){
				 return t;
			 }
		 }
		 return null;
			 
	 }
	/**
	 * CabinEntity转换为ReturnCabinEntity
	 * @param cbe
	 * @return
	 */
	  private ReturnCabinEntity convertToRetCabinEntity(CabinEntity cbe){
		  ReturnCabinEntity rtCbe=new ReturnCabinEntity();
		  rtCbe.setCabinType(cbe.getCabinType());
		  rtCbe.setPrice(cbe.getPrice());
		  return rtCbe;
	  }
	  /**
	   * CabinEntitys转换为ReturnCabinEntitys
	   * @param cbes
	   * @return
	   */
      private Set<ReturnCabinEntity> convertToRetCabinEntitys(Set<CabinEntity> cbes){
    	  Set<ReturnCabinEntity> retCabinSet =new HashSet<ReturnCabinEntity>();
    	  for(CabinEntity cbe:cbes){
    		  retCabinSet.add(convertToRetCabinEntity(cbe));
    	  }
		  return retCabinSet;
	  }
	
	  /**
	    * 如果己存在此双程(往返程)航班主体信息，则从LIST结果中返回，否则返回它本身(在同一个TaskModel作用域下)	
	    * @param result
	    * @param planeInfo
	    * @return
	    */
	   private DoublePlaneInfoEntity containAndGetDoublePlaneInfoEntity(List<AbstractPlaneInfoEntity> result,DoublePlaneInfoEntity planeInfo,com.foreveross.crawl.domain.airfreight.CabinEntity cabinEn){
		   String flighNo=planeInfo.getFlightNo();
		   if(null!=planeInfo.getTransits()){
			   for(TransitEntity t:planeInfo.getTransits()){
				   if(null!=t.getFlightNo())
					   flighNo=flighNo+t.getFlightNo();
			   }
				   
		   }
		   
		   ReturnDoublePlaneInfoEntity returnEntiy=planeInfo.getReturnPlaneInfos().iterator().next();
		   String returnFlightNo=returnEntiy.getFlightNo();
		   if(null!=returnEntiy.getReturnTransits()){
			   for(ReturnTransitEntity t: returnEntiy.getReturnTransits()){
				   if(null!=t.getFlightNo())
					   returnFlightNo+=t.getFlightNo(); 
			   }
				   
		   }
		   
		   for(Object obj:result){
			   DoublePlaneInfoEntity e=(DoublePlaneInfoEntity)obj;
			   String newFlightNo=e.getFlightNo();
			   if(null!=e.getTransits()){
				   for(TransitEntity t:e.getTransits()){
					   if(null!=t.getFlightNo())
						   newFlightNo+=t.getFlightNo();
				   }
			   }
			   ReturnDoublePlaneInfoEntity newReturnEntiy=e.getReturnPlaneInfos().iterator().next();
			   String nreReturnFlightNo=newReturnEntiy.getFlightNo();
			   if(null!=returnEntiy.getReturnTransits()){
				   for(ReturnTransitEntity t: newReturnEntiy.getReturnTransits()){
					   if(null!=t.getFlightNo())
						   nreReturnFlightNo+=t.getFlightNo(); 
				   }
					   
			   }
			   
			   boolean validate=true;
			 //  validate&=(e.getFlightNo().equals(planeInfo.getFlightNo()) && e.getStartTime().equals(planeInfo.getStartTime()) && e.getEndTime().equals(planeInfo.getEndTime()));
			   validate&=(flighNo.equals(newFlightNo) && returnFlightNo.equals(nreReturnFlightNo));
			   if(validate){
				   e.getCabins().add(cabinEn);
				   return e;
			   }
		   }
		   result.add(planeInfo);
		   return planeInfo;
	   }
	   
	   private SinglePlaneInfoEntity containAndGetSinglePlaneInfoEntity(List<AbstractPlaneInfoEntity> result,SinglePlaneInfoEntity planeInfo,com.foreveross.crawl.domain.airfreight.CabinEntity cabinEn){
		   for(Object obj:result){
			   SinglePlaneInfoEntity e=(SinglePlaneInfoEntity)obj;
			   boolean validate=true;
			   validate&=(e.getFlightNo().equals(planeInfo.getFlightNo()) && e.getStartTime().equals(planeInfo.getStartTime()) && e.getEndTime().equals(planeInfo.getEndTime()));
			   if(validate){
				   e.getCabins().add(cabinEn);
				   return e;
			   }
		   }
		   result.add(planeInfo);
		   return planeInfo;
	   }
	
	/**
	 * 国际地址往返解析
	*/
	public String getUrlDI(String bookSeatClasses) throws Exception {
		String url = "http://et.hnair.com/huet/bc10_av.do?";
		String tripType = "ONEWAY";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tdate = df.format(new Date());
		//系统日期
		int dt = DateUtil.compare_date(taskQueue.getFlightDate(),tdate);
		int dr=0;
		try {
			String takeoffdate =taskQueue.getFlightDate();
			String returndate = null;
		//	bookSeatClasses="F";//F表示头等/商务舱 //E表示经济舱
			if(taskQueue.getIsReturn()==1){//往返
				tripType = "ROUNDTRIP"; 
				returndate=taskQueue.getReturnGrabDate() ;
				dr = DateUtil.compare_date(taskQueue.getReturnGrabDate(), tdate);
			 }if(taskQueue.getIsReturn()==0){//单程
				 tripType = "ONEWAY";
				 returndate=taskQueue.getFlightDate();
				 dr = DateUtil.compare_date(taskQueue.getReturnGrabDate(), tdate);
			 }
			if(dt<0){takeoffdate = tdate;}
			if(dr<0){returndate = tdate;}
			String params = "adultNum=1&bookSeatClass="+bookSeatClasses+"&childNum=0&" +
					        "dstCity="+taskQueue.getToCity()+"&iscalendar=true&orgCity="+taskQueue.getFromCity()+"&"+
					        "preUrl=et.hnair.com&queryPassengerType=0&returnDate="+returndate+"&"+
							"sl_end2="+URLEncoder.encode(taskQueue.getToCityName(),"gb2312")+"&"+
							"sl_start2="+URLEncoder.encode(taskQueue.getFromCityName(),"gb2312")+"&" +
							"takeoffDate="+takeoffdate+"&tripType="+tripType+"&type=on";
			url +=params;
			logger.info(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}
	
	//经济舱
	private List<Object> parseSource(Source source) throws Exception {
//		List<TransitchangeEntity> transit =  new ArrayList<TransitchangeEntity>();//老模版
		ArrayList<TransitEntity> transit =  new ArrayList<TransitEntity>();//新模版
		List<Object> dataList = new ArrayList<Object>();
		List<Element> es= source.getAllElementsByClass("calendar_title");
		validateFlight(es);
		for (Element e : es) {
			String a = e.getTextExtractor().toString();
			if(a.substring(0, 2).equals("联程")) {// 中转
				List<Element> elementList = source.getAllElementsByClass("view_table_union");
				
				for(Iterator<Element> iterator = elementList.iterator(); iterator.hasNext();) {
					Element element = (Element) iterator.next();
//					List<Element> trLists = element.getAllElements("table");
					//changed by 20130715 渠道原型已修改
					List<Element> trLists = element.getAllElementsByClass("tbody");
					validateTicketAndFlight(element,trLists);
					for(int i =0;i<trLists.size();i++) {
						if (i % 2 == 0) {
							 String air =  getTableContent(element, i, 0).getTextExtractor().toString();
							 
							 String dtime = getTableContent(element, i, 1).getTextExtractor().toString().replace("+", "");
							 String filghtId = getTableContent(element, i, 3).getTextExtractor().toString();
							 String filgType = getTableContent(element, i, 4).getTextExtractor().toString();
							 String price = getTableContent(element, i, 6).getTextExtractor().toString().replace("￥", "");
							 air = air.replaceAll("- ", "");
							 String []airList = air.split(" ");
							 String []dtimeList = dtime.split(" ");
							 
							 String [] filghtIdList = filghtId.split(" ");
							 String temp1 = filgType.substring(filgType.length()-6, filgType.length()).replace("￥", "");
							 String temp2 = filgType.substring(filgType.length()-5, filgType.length()).replace("￥", "");
							 String [] filgTypeList  = null;
							 if(temp1.equals(price)) {
								 filgTypeList=  filgType.substring(0, filgType.length()-6).split(" ");
							 }else if (temp2.equals(price)) {
								 filgTypeList =  filgType.substring(0, filgType.length()-5).split(" ");
							 }
							 
							 for(int j=0;j<filgTypeList.length;j++) {
								 /*
								 TransitchangeEntity entity = new TransitchangeEntity();//老模版
								 entity.setLowerPrice(Double.parseDouble(price));
								 entity.setFlightTyte(filgTypeList[j]);
								 entity.setFromAir(airList[j*2]);
								 entity.setToAir(airList[j*2+1]);
								 entity.setFlightTime(dtimeList[j*3]);
								 entity.setStartTime(dtimeList[j*3+1]);
								 entity.setEndTime(dtimeList[j*3+2]);
								 entity.setFlightNo(filghtIdList[j]);
								 */
								 
								 TransitEntity entity=new TransitEntity();//新模版
//								 entity.setLowerPrice(Double.parseDouble(price));
								 entity.setFlightType(filgTypeList[j]);
								 entity.setFromAirPortName(airList[j*2]);
								 entity.setToAirPortName(airList[j*2+1]);
//								 entity.setFlightTime(dtimeList[j*3]);
								 entity.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dtimeList[j*3+1]));
								 entity.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dtimeList[j*3+2]));
								 entity.setFlightNo(filghtIdList[j]);
								 transit.add(entity);
							 }
							 //老模版
							 /*dataList.add(CrawlAdapterFactory.createPlaneInfo(taskQueue,
										"HU", "海航", "海南航空股份有限公司", dtimeList[1],
										dtimeList[2], filghtId,
										String.valueOf(price), null,
										String.valueOf(price), filgType,transit));*/
							 //新模版
							 SinglePlaneInfoEntity singleEntity;
							 singleEntity=(SinglePlaneInfoEntity)PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
									 "HU", "海航", "海南航空股份有限公司",dtimeList[1],dtimeList[2], filghtId, String.valueOf(price), 
										null,String.valueOf(price), filgType);
							 singleEntity.setTransits(transit);
							 dataList.add(singleEntity);
						}
					}
				}
			}else{//非中转
				//return _parseDirect(source.toString());
				List<Element> elementList = source.getAllElementsByClass("view_table");
				for (Iterator<Element> iterator = elementList.iterator(); iterator
						.hasNext();) {
					Element element = (Element) iterator.next();
					List<Element> trLists = element.getAllElements("tr");
					validateTicketAndFlight(element,trLists);
					for (int i = 0; i < trLists.size(); i++)
						if (i % 2 == 0) {
							String filghtId = getTableContent(element, i, 0)
									.getTextExtractor().toString();
							String filgType = getTableContent(element, i, 3)
									.getTextExtractor().toString();
							String dtime = getTableContent(element, i, 1)
									.getTextExtractor().toString().trim();
							String startAndEndTime[] = dtime.replaceAll("\\s+", "-")
									.split("-");
							String jg1 = getTableContent(element, i, 4)
									.getTextExtractor().toString();
							String jg2 = getTableContent(element, i, 5)
									.getTextExtractor().toString();
							String jg3 = getTableContent(element, i, 6)
									.getTextExtractor().toString();
							String jg4 = getTableContent(element, i, 7)
									.getTextExtractor().toString();
							if (jg1.length() > 15)
								jg1 = jg1.substring(0, jg1.indexOf(" "));
							if (jg2.length() > 15)
								jg2 = jg2.substring(0, jg2.indexOf(" "));
							if (jg3.length() > 15)
								jg3 = jg3.substring(0, jg3.indexOf(" "));
							if (jg4.length() > 15)
								jg4 = jg4.substring(0, jg4.indexOf(" "));
							List<String> priceList = new ArrayList<String>();
							priceList.add(jg1);priceList.add(jg2);
							priceList.add(jg3);priceList.add(jg4);
							if (StringUtils.isNotBlank(startAndEndTime[0]))
								startAndEndTime[0] = startAndEndTime[0]
										.endsWith("+") ? startAndEndTime[0]
										.substring(0,
												startAndEndTime[0].length() - 1)
										: startAndEndTime[0];
							if (StringUtils.isNotBlank(startAndEndTime[1]))
								startAndEndTime[1] = startAndEndTime[1]
										.endsWith("+") ? startAndEndTime[1]
										.substring(0,
												startAndEndTime[1].length() - 1)
										: startAndEndTime[1];
							
//							Set<CabinEntity> cabins = new HashSet<CabinEntity>();//老模版
							Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabins = new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();//新模版
							//各舱位类型价（悦享商务舱 	锦程添金 	网站专享 	特价票）
							String[] arr = new String[]{"悦享商务舱","锦程添金","网站专享","特价票"};
							for (int b=0;b<priceList.size();b++) {
								String p = priceList.get(b);
								if("-".equals(p))continue;
								//老模版
								/*CabinEntity c = new CabinEntity();
								c.setLevel(1);
								c.setCabinType(arr[b]);
								c.setPrice(p);*/
								//新模版
								com.foreveross.crawl.domain.airfreight.CabinEntity c = new com.foreveross.crawl.domain.airfreight.CabinEntity();
								//c.setLevel(1);
								c.setCabinType(arr[b]);
								c.setPrice(Double.parseDouble(p.replace("￥", "")));
								cabins.add(c);
							}
							// 各子舱位价
							List<Element> cabinNames = trLists.get(i+1)
									.getAllElementsByClass("flight_head").get(0)
									.getAllElements("b");
							List<Element> products = trLists.get(i+1)
									.getAllElementsByClass("flight_row");
							
							for (Element product : products) {
								Element left = product
										.getAllElementsByClass("left").get(0);
								String productName = left.getTextExtractor()
										.toString();
								List<Element> rights = product
										.getAllElementsByClass("wide");
								for (Element ul : rights) {
									List<Element> lis = ul.getAllElements("li");
									for (int j = 0; j < lis.size(); j++) {
										//CabinEntity tmp = new CabinEntity();
										com.foreveross.crawl.domain.airfreight.CabinEntity tmp = new com.foreveross.crawl.domain.airfreight.CabinEntity();
										Element li = lis.get(j);
										String tmpprice = li.getTextExtractor()
												.toString();
										tmp.setProductName(productName);
										//tmp.setLevel(2);
//										tmp.setCabinName(cabinNames.get(j).getTextExtractor().toString());
										tmp.setSubCabinName(cabinNames.get(j).getTextExtractor().toString());
										if ("-".equals(tmpprice.trim()))
											continue;
										if (tmpprice.length() > 1
												&& "锦程添金".equals(productName)) {
											String prices=tmpprice.substring(tmpprice.indexOf("￥"),tmpprice.indexOf(" "));
											tmp.setPrice(Double.parseDouble(prices.replace("￥", "")));
										} else {
											tmp.setPrice(Double.parseDouble(tmpprice.replace("￥", "")));
										}
										cabins.add(tmp);
									}
								}
							}
							List<Double> priceArr = new ArrayList<Double>(); 
							//System.out.println("海航仓位：");
							for (com.foreveross.crawl.domain.airfreight.CabinEntity c : cabins) {
//								if(c.getLevel()==2){
//									//System.out.println("CabinName:" + c.getCabinName());
//									//System.out.println("ProductName："
//										//+ c.getProductName());
//								}else{
//									//System.out.println("Cabintype:" + c.getCabinType());
//								}
								//System.out.println("price:" + c.getPrice());
								priceArr.add(Double.valueOf((c.getPrice().toString()).replace("￥", "")));
							}
							Double[] price = priceArr.toArray(new Double[0]);
							if(price.length == 0)
								continue;//这个航班没有价格
							super.sort(price,0,price.length-1);
							double lowerPriceValue = 0.0D;
							double highPriceValue = 0.0D;
							lowerPriceValue = price[0];
							highPriceValue = price[price.length-1];
							//老模版
							/*dataList.add(CrawlAdapterFactory.createPlaneInfo(
									taskQueue, "HU", "海航", "海南航空股份有限公司",
									startAndEndTime[0], startAndEndTime[1],
									filghtId, String.valueOf(lowerPriceValue),
									null, String.valueOf(highPriceValue), filgType,cabins));*/
							
							//新模版
							 SinglePlaneInfoEntity singleEntity;
							 singleEntity=(SinglePlaneInfoEntity)PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
									 "HU", "海航", "海南航空股份有限公司",startAndEndTime[0],startAndEndTime[1], filghtId, 
									 String.valueOf(lowerPriceValue),null,String.valueOf(highPriceValue), filgType);
							 singleEntity.setCabins(cabins);
							 dataList.add(singleEntity);
						}
				}
				
			
			}
		}
		return dataList;
	}
	/**
	 * 解析直达航班，包括仓位信息等
	 * @param source
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private List<Object> _parseDirect(String page) throws Exception{
		List<Object> finalResule=new ArrayList<Object>();
		//1、先获得产品类型，如“悦享商务舱”“锦绣添金”，海南【没有】定死每一个产品类型对应的子舱代码
		Map<String,String> name_code_map=new HashMap<String,String>();
		Document doc=Jsoup.parse(page);
		Elements as=doc.getElementsByAttributeValueMatching("onclick", "productNameClickForSdc.*").tagName("a");
		for(Iterator<org.jsoup.nodes.Element> it=as.iterator();it.hasNext();){
			org.jsoup.nodes.Element e=it.next();
			name_code_map.put(e.text(), e.attr("onclick").split(",")[1].replaceAll("'|\\(|\\)", ""));
		}
		name_code_map.put("原价票", "NORMAL");//这个扫描不到
		
		//2、页面规律很明显，所以进行全局扫描
		//<input type="radio" name="ticket0" class="Select_Price" 
		//onclick="setSelectNormal('HU7805','Y','WO','0840','1155',0,1380588000835,1380599700835)">
		//  <input type="radio" name="ticket0" onclick="setSelectNormal
		//('HU7279','F','WO','1135','1530',0,1380598500408,1380612600408)">
		Map<String,Map<String,Map<String,Double>>> no_data_=new HashMap<String,Map<String,Map<String,Double>>>();
		for(Iterator<org.jsoup.nodes.Element> it =doc.getElementsByClass("view_table").iterator();it.hasNext();){
			Elements inputs=it.next().getElementsByAttributeValueMatching("onclick", "^setSelectNormal\\(.*$").tagName("input");
			for(Iterator<org.jsoup.nodes.Element> i=inputs.iterator();i.hasNext();){
				org.jsoup.nodes.Element input=i.next();
				String[] strs=input.attr("onclick").replaceAll("'|\\(|\\)","").replaceAll("^setSelectNormal", "").split(",");
				if(!no_data_.containsKey(strs[0])){
					no_data_.put(strs[0], new HashMap<String,Map<String,Double>>());
				}
				if(!no_data_.get(strs[0]).containsKey(strs[2])){
					no_data_.get(strs[0]).put(strs[2], new HashMap<String,Double>());
				}
				try {
					Double d=Double.valueOf(input.parent().text().split(" ")[0].replaceAll("\\D", ""));
					if(no_data_.get(strs[0]).get(strs[2]).get(strs[1])==null ||
							no_data_.get(strs[0]).get(strs[2]).get(strs[1]).compareTo(d)!=0){
						no_data_.get(strs[0]).get(strs[2]).put(
								strs[1],d);
					}
				} catch (Exception e) {}
			}
		}
		for(Iterator<org.jsoup.nodes.Element> it =doc.getElementsByClass("view_table").iterator();it.hasNext();){
			org.jsoup.nodes.Element e=it.next();
			Elements trs=e.child(1).children();
			int index=0;
			for(Iterator<org.jsoup.nodes.Element> i=trs.iterator();i.hasNext();){
				index++;
				org.jsoup.nodes.Element tr=i.next();
				if(index%2!=0){
					String flightNo=tr.child(0).text();
					String start=tr.child(1).text().split(" ")[0];
					String end=tr.child(1).text().split(" ")[1].substring(0,5);
					String type=tr.child(3).text();
					Map<String,Map<String,Double>> myData=no_data_.get(flightNo);
//					Set<CabinEntity> cabin=getLv1CabinData(name_code_map, myData);//
					Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabin=getLv1CabinData(name_code_map, myData);
					cabin.addAll(getLv1CabinData(myData));
					//老模版
					/*finalResule.add(CrawlAdapterFactory.createPlaneInfo(
							taskQueue, flightNo.substring(0,2), "海航", "海南航空", start, end, flightNo,
							getLowOrHigPrice(myData,"low")+"", null, getLowOrHigPrice(myData,"hig")+"", type, cabin));*/
					
					 //新模版
					 SinglePlaneInfoEntity singleEntity;
					 singleEntity=(SinglePlaneInfoEntity)PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
							 "HU", "海航", "海南航空股份有限公司",start,end, flightNo, getLowOrHigPrice(myData,"low")+"", 
								null,getLowOrHigPrice(myData,"hig")+"", type);
					 singleEntity.setCabins(cabin);
					 finalResule.add(singleEntity);
					
				}
			}
		}
		return finalResule;
	}
	
	private Double getLowOrHigPrice(Map<String,Map<String,Double>> myData,String type){
		
		if(myData!=null && !myData.isEmpty()){
			List<Double> prices=new ArrayList<Double>();
			List<Map<String,Double>> temp=new ArrayList<Map<String,Double>>(myData.values());
			for(Map<String,Double> map:temp){
				prices.addAll(map.values());
			}
			if(prices.isEmpty()){
				return null;
			}else{
				Collections.sort(prices);
				if("hig".equals(type)){
					return prices.get(prices.size()-1);
				}else{
					return prices.get(0);
				}
			}
		}else{return null;}
	}
	
	private Set<com.foreveross.crawl.domain.airfreight.CabinEntity> getLv1CabinData(Map<String,Map<String,Double>> myData){
		Set<com.foreveross.crawl.domain.airfreight.CabinEntity> sc=new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();
		if(myData!=null && !myData.isEmpty()){
			List<Map<String,Double>> prices=new ArrayList<Map<String,Double>>(myData.values());
			for(Map<String,Double> map:prices){
				Entry<String, Double> en=null;
				for(Iterator<Entry<String, Double>> i=map.entrySet().iterator();i.hasNext();){
					en=i.next();
					com.foreveross.crawl.domain.airfreight.CabinEntity c=new com.foreveross.crawl.domain.airfreight.CabinEntity();
				//	c.setLevel(2);
//					c.setCabinName(en.getKey());
					c.setSubCabinName(en.getKey());
					c.setPrice(Double.parseDouble((en.getValue()+"").replace("￥", "")));
					sc.add(c);
				}
			}
		}
		return sc;
	}
	
	private Set<com.foreveross.crawl.domain.airfreight.CabinEntity> getLv1CabinData(Map<String,String> name_code_map,Map<String,Map<String,Double>> myData){
		Set<com.foreveross.crawl.domain.airfreight.CabinEntity> sc=new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();
		if(myData!=null && !myData.isEmpty()){
			for(Iterator<Entry<String, String>> it= name_code_map.entrySet().iterator();it.hasNext();){
				Entry<String, String> en=it.next();
				//name_prices_map.put(en.getValue(), new ArrayList<Double>());//一级仓位编码_所有价格
				if(myData.get(en.getValue())!=null && !myData.get(en.getValue()).isEmpty()){
					com.foreveross.crawl.domain.airfreight.CabinEntity c=new com.foreveross.crawl.domain.airfreight.CabinEntity();
				//	c.setLevel(1);
					c.setCabinType(en.getKey());
					for(Iterator<Entry<String, Double>> t=myData.get(en.getValue()).entrySet().iterator();t.hasNext();){
						Entry<String, Double> e=t.next();
						if(c.getPrice()!=null){
							if(Double.valueOf(c.getPrice()).compareTo(e.getValue())>0){
								c.setPrice(Double.parseDouble((e.getValue()+"").replace("￥", "")));
//								c.setCabinName(e.getKey());
								c.setSubCabinName(e.getKey());
							}
						}else{
							c.setPrice(Double.parseDouble((e.getValue()+"").replace("￥", "")));
//							c.setCabinName(e.getKey());
							c.setSubCabinName(e.getKey());
						}
					}
					if(c.getPrice()!=null){
						sc.add(c);	
					}
				}
			}
		}
		return sc;
	}
	
    
	
	//经济
	public String getUrl() {
	        String url = "http://et.hnair.com/huet/bc10_av.do?";
			String tripType = "ONEWAY";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String tdate = df.format(new Date());
			//系统日期
			int dt = DateUtil.compare_date(taskQueue.getFlightDate(),tdate);
			int dr=0;
			try {
				String takeoffdate = taskQueue.getFlightDate();
				String returndate = taskQueue.getReturnGrabDate() ;
				if("1".equals(taskQueue.getIsReturn()))
				{ tripType = "ROUNDTRIP"; 
				 dr = DateUtil.compare_date(taskQueue.getReturnGrabDate(), tdate);
				 }
				if(dt<0){takeoffdate = tdate;}
				if(dr<0){returndate = tdate;}
				String params = "preUrl=et.hnair.com&queryPassengerType=0&iscalendar=true&adultNum=1&bookSeatClass=E&type=on&childNum=0&" +
						        "dstCity="+taskQueue.getToCity()+"&returnDate="+returndate+"&"+
								"tripType="+tripType+"&takeoffDate="+takeoffdate+"&"+
								"sl_end2="+URLEncoder.encode(taskQueue.getToCityName(),"gb2312")+"&"+
								"sl_start2="+URLEncoder.encode(taskQueue.getFromCityName(),"gb2312")+"&" +
								"orgCity="+taskQueue.getFromCity();
				url +=params;
				logger.info(url);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return url;
	}
	/**
	 * 国际往返URL
	*/
	public Object fetchDI() throws Exception {
		StringBuilder resultHtml = new StringBuilder();
		HttpPost post=new HttpPost(getUrlDI("E"));
		headers(post);
//		getHttpClient();
//		HttpHost proxyHost = new HttpHost("187.58.4.11", 8080, "http");
//		globalHttpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxyHost);
		String html = super.excuteRequest(post);
		//出验证码则换IP重试一次
		if(html.matches(".*<input id='checkcode' name=\"checkcode\".*") && isUseProxyip()){
			Thread.sleep(2000);
			rollbackProxyIp(false);
			switchProxyipByHttClient();
			html = super.excuteRequest(post);
		}
		super.appendPageContents(html);
		resultHtml.append(html);
		//先保证上面的经济舱有数据，后面的再来请求多次(目前就一次)
		try{
			Thread.sleep(isUseProxyip() ? 1000 : 20000);
			html = super.excuteRequest(new HttpPost(getUrlDI("F")));
			super.appendPageContents(html);
			resultHtml.append(html);
		}catch(Exception e){
			if(isUseProxyip()) {
				proxyIp.setUseStatus(ProxyIpModel.UNENBLE);
			}
		}finally{
			if(this.isUseProxyip()) {
				ipProvider.rollBackProxyIp(proxyIp);
			}
		}
		return resultHtml.toString();
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return fetchDI();//国际往返
		case INTERNATIONAL_ONEWAY:
			return fetchDI();//国际单程
		case DOMESTIC_ONEWAYTRIP:
			/*return fetchS();//国内单程,经济（老适配器，不太确定是不是国内单程？？？）*/	
			return fetchDomeOneWay();
		case DOMESTIC_ROUNDTRIP:
			return null;
		}
		return null;
	}
	
	/**
	 * 
	 * 国内单程
	 * 同时获取经济舱 和 头等/公务舱
	 * 2014-01-17 guokenye
	 * @return
	 * @throws Exception
	 */
	private Object fetchDomeOneWay()throws Exception{
		List<String> result=new ArrayList<String>();
		HttpClient h;
		HttpPost post = null;
		Map<String,String> p=new HashMap<String,String>();
		String html;
		try{
			h=super.getHttpClient();
			p.put("adultNum", "1");
			p.put("bookSeatClass", "E");//经济舱
			p.put("childNum", "0");
			p.put("orgCity", taskQueue.getFromCity());
			p.put("dstCity", taskQueue.getToCity());
			p.put("iscalendar", "true");
			p.put("preUrl", "et.hnair.com");
			p.put("queryPassengerType", "0");
			p.put("returnDate", taskQueue.getFlightDate());
			p.put("takeoffDate", taskQueue.getFlightDate());
			p.put("sl_start2", taskQueue.getFromCityName());
			p.put("sl_end2", taskQueue.getToCityName());
			p.put("tripType", "ONEWAY");
			p.put("type", "on");
			post=super.getBasePost("http://et.hnair.com/huet/bc10_av.do", p);
			post.addHeader("Host","et.hnair.com");
			

			html = excuteRequest(h, post, true);
			//出验证码则换IP重试一次
			if(html.matches(".*<input id='checkcode' name=\"checkcode\".*") && isUseProxyip()){
				Thread.sleep(2000);
				rollbackProxyIp(false);
				switchProxyipByHttClient();
				html = excuteRequest(h, post, true);
			}

			
			super.appendPageContents(html);
			validateDomeOneWay(html);
			result.add(html);
//			try{ //先保证经济舱数据拿到,但还有一种情况没有去实现，当经济舱确实没有航班时,要去拿头等舱
//				Thread.sleep(2000);
//				p.put("bookSeatClass", "F");//头等舱
//				post=super.getBasePost("http://et.hnair.com/huet/bc10_av.do", p);
//				post.addHeader("Host","et.hnair.com");
//				html = excuteRequest(h, post, true);
//				super.appendPageContents(html);
//				validateDomeOneWay(html);
//				result.add(html);
//			}catch(Exception e){
//				logger.error(e);
//			}
		}finally{
			html=null;p=null;
			if(post!=null){post.releaseConnection();}
			h=null;
		}
		return result;
	}
	
	private void validateDomeOneWay(String html)throws Exception{
		if(html==null){
			throw new Exception("html is null");
		}
		html=html.replaceAll("\\s", "");
		if(html.matches(".*航线信息有误，请核实后重选航线再查询.*")){
			throw new FlightInfoNotFoundException();
		}
		if(html.matches(".*航班座位已售完.*")){
			throw new FlightInfoNotFoundException();
		}
		if(html.matches(".*<input id='checkcode' name=\"checkcode\".*"))
			throw new Exception("网站验证码");
		if(!html.matches(".*class=\"calendar_title\".*")){
			throw new Exception("html数据不完整");
		}
	}
	
	
	/**
	 * 解析国内单程
	 * 同时获取经济舱 和 头等/公务舱
	 * 2014-01-17 guokenye
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private List<Object> paraseDomeOneWay(Object obj)throws Exception{
		List<SinglePlaneInfoEntity> entitys=new ArrayList<SinglePlaneInfoEntity>();
		if(obj instanceof java.util.Collection){//同时解析经济舱 和 头等舱
			List<String> list=(List<String>)obj;
			try {
				paraseDomeOneWayInfo(list.get(0),"E",entitys);//先解析经济
				paraseDomeOneWayInfo(list.get(1),"F",entitys);//再解析头等
			} catch (Exception e) {}
		}
		if(entitys.isEmpty()){
			throw new FlightInfoNotFoundException();
		}
		
		//没有舱位的航班去掉
		List<SinglePlaneInfoEntity> result = new ArrayList<SinglePlaneInfoEntity>();
		for(SinglePlaneInfoEntity entity : entitys){
			if(!entity.getCabins().isEmpty()){
				result.add(entity);
			}
		}
		if(result.isEmpty()){
			throw new FlightInfoNotFoundException("没抓到任何舱位价格");
		}
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(result);
		}catch(Exception e){
			logger.info(e);
			logger.info("国内单程数据排序价格错误，请检查数据");
			try{
				logger.info(result);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}		
		return Arrays.asList(result.toArray());
	}
	
	private void paraseDomeOneWayInfo(String html,String cabinCode,List<SinglePlaneInfoEntity> entitys)throws Exception{
		Document doc;
		SinglePlaneInfoEntity singlePlaneInfoEntity = null;
		List<org.jsoup.nodes.Element> trs;
		List<String> cabinInfos=null;
		String tempInfo;
		List<Double> prices;
		try{
			doc=Jsoup.parse(html);
			trs=doc.getElementById("sort_table").select("tr");
			for(int i=0;i<trs.size();i=i+2){
				tempInfo=trs.get(i).toString().replaceAll("\\s", "");
				if(doc.select("div.calendar_title").first().html().replaceAll("\\s","").matches(".*单程.*")){//单程
					singlePlaneInfoEntity=this.buildAndAddDirectEntity(tempInfo, entitys);
					cabinInfos=RegHtmlUtil.retrieveLinks(tempInfo, "setSelectNormal(.*?￥.*?)<",1);//仓位价格标签信息
					if(cabinInfos==null){continue ;}
					if("E".equals(cabinCode)){//经济舱
						cabinInfos.addAll(
								RegHtmlUtil.retrieveLinks(trs.get(i+1).toString().replaceAll("\\s", ""), "setSelectNormal(.*?￥.*?)<",1));
						setSinglePlaneInfoCabins(singlePlaneInfoEntity,cabinInfos,"E");
					}else if("F".equals(cabinCode)){//头等商务
						cabinInfos.addAll(
								RegHtmlUtil.retrieveLinks(trs.get(i+1).toString().replaceAll("\\s", ""), "￥(.*?setSelectNormal.*?>)",1));
						setSinglePlaneInfoCabins(singlePlaneInfoEntity,cabinInfos,"F");
					}
				}else if(doc.select("div.calendar_title").first().html().replaceAll("\\s","").matches(".*联程.*")){
					tempInfo=trs.get(i).toString().replaceAll("\\s", "");
					singlePlaneInfoEntity=buildAndAddTransitEntity(tempInfo,entitys,cabinCode);
				}
				
				//这里以前是只排裸价，14年七月后加了税价和合计价，统一用buildLimit。。方法
				if(singlePlaneInfoEntity.getCabins()!=null && !singlePlaneInfoEntity.getCabins().isEmpty()){
					prices=new ArrayList<Double>();
					for(CabinEntity cabin:singlePlaneInfoEntity.getCabins()){
						if(cabin.getPrice()!=null){
							prices.add(cabin.getPrice());
						}
					}
				}
				
			}
		}finally{
			
		}
	}
	
	private SinglePlaneInfoEntity buildAndAddDirectEntity(String info,List<SinglePlaneInfoEntity> entitys)throws Exception{
		SinglePlaneInfoEntity entity=null;
		String flightNo=RegHtmlUtil.regStr(info, "<td.*?>(.*?)<", 1);
		String start=RegHtmlUtil.regStr(info, "<img.*?>(.*?)<", 1).substring(0, 5);
		String end=RegHtmlUtil.regStr(info, "<img.*?>(.*?)<.*?>(.*?)<",2).substring(0, 5);
		String type=RegHtmlUtil.regStr(info, "<td.*?<td.*?<td.*?<td.*?>(.*?)<",1);
		if(!entitys.isEmpty()){
			for(SinglePlaneInfoEntity e:entitys){
				if(e.getFlightNo().equals(flightNo)){
					entity=e;
					break;
				}
			}
		}
		if(entity==null){
			entity=PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
					flightNo.substring(0,2), "海航", "海南航空公司", start, end, flightNo, null, "海南自营", null, type, SinglePlaneInfoEntity.class);
			entitys.add(entity);
		}
		return entity;
	}
	
	private SinglePlaneInfoEntity buildAndAddTransitEntity(String info,List<SinglePlaneInfoEntity> entitys,String cabinCode)throws Exception{
		SinglePlaneInfoEntity entity=null;
		boolean isExist=false;
		//推算机场数量
		List<String> airPorts=RegHtmlUtil.retrieveLinks(
				RegHtmlUtil.regStr(info, "<td(.*?)td",1), ">(.*?)<", 1);	
		List<String> flightNos=RegHtmlUtil.retrieveLinks(
				RegHtmlUtil.regStr(info, "<td.*?<td.*?<td.*?<td.*?<td.*?(.*?)<td",1),">(.*?)<", 1);
		List<String> times=RegHtmlUtil.retrieveLinks(
				RegHtmlUtil.regStr(info, "<td.*?(<td.*?)<td",1),">(.*?<img.*?<img.*?)<", 1);
		List<String> types=RegHtmlUtil.retrieveLinks(
				RegHtmlUtil.regStr(info, "<td.*?<td.*?<td.*?(<td.*?)<td",1),"biaojicangwei.*?>(.*?)<", 1);
		String priceInfo=RegHtmlUtil.regStr(info, "setSelectLCComb(.*?)</b",1);
		String format="yyyy-mm-ddHH:mm";
		double tax = 0;
		try{
			tax = queryManyTaxesPrice(priceInfo);
		}catch(Exception e){
			tax = 170;
			logger.info("联程查询税价出错，税价置为170元，begin：" + taskQueue.getFromCity() + "，end：" + taskQueue.getToCity());
			logger.info(e);
		}
		
		if(flightNos.size()!=0 && airPorts.size()==flightNos.size()&& flightNos.size()==times.size()&& times.size()==types.size()){
			if(!entitys.isEmpty()){
				String flightNoGroup=getFlightNoInfo(flightNos);/*
				logger.info("航班组"+flightNoGroup);*/
				for(SinglePlaneInfoEntity e:entitys){
					if(flightNoGroup.equals(this.getFlightNoInfo(e))){
						entity=e;
						isExist=true;
						break;
					}
				}
			}
			if(entity==null){
				entity=PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
						flightNos.get(0).substring(0,2), "海航", "海南航空公司", null, null, flightNos.get(0), null, "海南自营", null, types.get(0), SinglePlaneInfoEntity.class);
				entitys.add(entity);
			}
			for(int i=0;i<flightNos.size();i++){
				if(i==0){
					//2014-02-1412:0513:45
					String price = RegHtmlUtil.regStr(priceInfo, "<b.*?>(.*)",1);
					String orginilPrice = String.valueOf(Double.valueOf(price) + tax);
					String time=times.get(0).replaceAll("<.*?>", "").replaceAll("\\s", "");
					entity.setStartTime(DateUtil.StringToDate(format,time.substring(0,15)));
					entity.setEndTime(DateUtil.StringToDate(format,time.substring(0,10)+time.substring(15)));
					entity.getCabins().add(PlaneInfoEntityBuilder.buildCabinInfo(
							"E".equals(cabinCode)?"经济舱":"头等/商务舱",
							RegHtmlUtil.regStr(priceInfo, ",.*?,.*?'(.*?)'",1), 
							"LC".equals(RegHtmlUtil.regStr(priceInfo, ",.*?,.*?,.*?'(.*?)'",1))?"特价票":"商务行",
									String.valueOf(tax),price, orginilPrice, null, null, CabinEntity.class));
				}
				if(!isExist){
					entity.getTransits().add(PlaneInfoEntityBuilder.buildTransitEntity(
							flightNos.get(i), null, flightNos.get(i).substring(0,2), null, null, 
							null, airPorts.get(i).replaceAll("-.*$", ""), 
							null, airPorts.get(i).replaceAll("^.*-", ""), types.get(i),TransitEntity.class
							));
				}
			}
		}
		return entity;
	}
	
	private String getFlightNoInfo(SinglePlaneInfoEntity entity)throws Exception{
		if(entity==null){return null;}
		StringBuffer bf=new StringBuffer();
		for(TransitEntity tr:entity.getTransits()){
			bf.append(tr.getFlightNo()+"/");
		}/*
		logger.info( bf.toString().replaceAll("\\/$", ""));*/
		return bf.toString().replaceAll("\\/$", "");
	}
	
	private String getFlightNoInfo(List<String> flightNos)throws Exception{
		StringBuffer bf=new StringBuffer();
		for(String no:flightNos){
			bf.append(no+"/");
		}
		return bf.toString().replaceAll("\\/$", "");
	}
	private void setSinglePlaneInfoCabins(SinglePlaneInfoEntity entity,List<String> cabinInfos,String cabinCode)throws Exception{
		if(cabinInfos==null || cabinInfos.isEmpty()){return;}
		CabinEntity cabinEntity = null;
		for(String cabinInfo:cabinInfos){
			String taxesPrice = null;
			double tax = 0;
			try{
				tax = queryOneTaxesPrice(cabinInfo);
			}catch(Exception e){
				tax = 170;
				logger.info("查询税价出错，税价置为170元");
				logger.info(e);
			}
			taxesPrice = String.valueOf(tax);
			String price = null;
			String originalPrice = null;
			if("E".equals(cabinCode)){
				price = RegHtmlUtil.regStr(cabinInfo, "￥(.*)",1);
				originalPrice = String.valueOf(Double.valueOf(price) + tax);
				cabinEntity=PlaneInfoEntityBuilder.buildCabinInfo(
						"经济舱", RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'",1), 
						economyProductNameMap.get(RegHtmlUtil.regStr(cabinInfo, ",.*?,.*?'(.*?)'",1)), 
						taxesPrice,price, originalPrice, null, null, CabinEntity.class);
				entity.getCabins().add(cabinEntity);
			}else if("F".equals(cabinCode)){
				if(cabinInfo.matches("^\\(.*")){
					price = RegHtmlUtil.regStr(cabinInfo, "￥(.*)",1);
					originalPrice = String.valueOf(Double.valueOf(price) + tax);
					cabinEntity=PlaneInfoEntityBuilder.buildCabinInfo(
							firstProductNameMap.get(RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'",1)), 
							RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'",1), 
							firstProductNameMap.get(RegHtmlUtil.regStr(cabinInfo, ",.*?,.*?'(.*?)'",1)), 
							price, originalPrice, null, null,taxesPrice, CabinEntity.class);
				}else{
					price = RegHtmlUtil.regStr(cabinInfo, "(.*?)<",1);
					originalPrice = String.valueOf(Double.valueOf(price) + tax);
					cabinEntity=PlaneInfoEntityBuilder.buildCabinInfo(
							firstProductNameMap.get(RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'",1)), 
							RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'",1), 
							firstProductNameMap.get(RegHtmlUtil.regStr(cabinInfo, ",.*?,.*?'(.*?)'",1)), 
							price, originalPrice, null, null,taxesPrice, CabinEntity.class);
				}
				if(cabinEntity.getCabinType()==null){
					cabinEntity.setCabinType("头等/商务舱");
				}
				entity.getCabins().add(cabinEntity);
			}
			logger.info(String.format("[%s] [%s]解析到仓位[%s][%s][%s][%s][%s][%s]", 
					"海南航空",entity.getFlightNo(),
					cabinEntity.getCabinType(),cabinEntity.getProductName(),cabinEntity.getSubCabinName(),cabinEntity.getPrice(),
					cabinEntity.getTaxesPrice(),cabinEntity.getOriginalPrice()));
		}
		
	}
	
	/**
	 * 单程-查询税价
	 * @param cabinInfo
	 * @return
	 * @throws Exception
	 */
	private double queryOneTaxesPrice(String cabinInfo) throws Exception{
		if(taxPrice != 0){
			return taxPrice;
		}
		Map<String,String> params = new HashMap<String, String>();
		params.put("agreementId", RegHtmlUtil.regStr(cabinInfo, ",.*?,'(.*?)'"));
		params.put("ajaxtimestamp", String.valueOf(System.currentTimeMillis()));
		params.put("cabin", RegHtmlUtil.regStr(cabinInfo, ",'(.*?)'"));
		params.put("comboIndex", "0");
		params.put("detailType", "2");
		params.put("fltNo", RegHtmlUtil.regStr(cabinInfo, "'(.*?)'"));
		params.put("step", RegHtmlUtil.regStr(cabinInfo, ",.*?,.*?,.*?,.*?,(.*?),"));
		return queryTaxesPrice(params);
	}
	
	/**
	 * 联程-查询税价
	 * @param Info
	 * @param head
	 * @return
	 * @throws Exception
	 */
	private double queryManyTaxesPrice(String info) throws Exception{
		if(taxPrice != 0){
			return taxPrice;
		}
		String regex = "('.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?)";
		Matcher m = Pattern.compile(regex).matcher(info);
		if(m.find()){
			String detailType = null;
			if(m.group(8).replaceAll("[\\']","").equals("1")){
				detailType = "5";
			}else{
				detailType = "6";
			}
			Map<String,String> params = new HashMap<String, String>();
			params.put("agreementId",replaceQuotes(m.group(4)));
			params.put("ajaxtimestamp", String.valueOf(System.currentTimeMillis()));
			params.put("cabin", replaceQuotes(m.group(3)));
			params.put("comboIndex",replaceQuotes(m.group(1)));
			params.put("detailType", detailType);
			params.put("fltNo", replaceQuotes(m.group(2)));
			params.put("step","0");
			double d = queryTaxesPrice(params);
		   /* params.put("agreementId", replaceQuotes(m.group(7)));
			params.put("ajaxtimestamp", String.valueOf(System.currentTimeMillis()));
			params.put("cabin", replaceQuotes(m.group(6)));
			params.put("comboIndex", replaceQuotes(m.group(1)));
			params.put("detailType", detailType);
			params.put("fltNo", replaceQuotes(m.group(5)));
			params.put("step","1");
			double d2 = queryTaxesPrice(params);*/
			return d;
		}else{
			logger.info("没有查到联程税价");
			return 0;
		}
	}
	
	/**
	 * @param src
	 * 替换引号
	 * @return
	 */
	private static String replaceQuotes(String src){
		if(src == null){
			return null;
		}
		return src.replaceAll("\\'", "");
	}
	
	/**
	 * 按参数查询税价，联程可能查不到税费信息
	 */
	private double queryTaxesPrice(Map<String,String> params) throws Exception{
		
		/*HttpGet get = new HttpGet("http://et.hnair.com/huet/bc10_getsegmentdetail.do?ajaxtimestamp=1404359501914&detailType=6&fltNo=HU7846&cabin=L&agreementId=LC&comboIndex=0&step=0");
		get.setHeader("Host","et.hnair.com");
		get.setHeader("Referer","http://et.hnair.com/huet/bc10_av.do");
		String page = EntityUtils.toString(getGlobalHttpClient().execute(get).getEntity());*/
		
		
		
		String url = "http://et.hnair.com/huet/bc10_getsegmentdetail.do";
		HttpGet g = super.getBaseGet(url, params);
		
		DefaultHttpClient httpClient  = (DefaultHttpClient)super.getHttpClient();
		//setCookiesToMethodByPing("http://et.hnair.com/huet/bc10_av.do", httpClient, g);
		String s = excuteRequest(httpClient, g, true);
		JSONObject r = JSONObject.fromObject(s);
		JSONObject detial = r.getJSONObject("detail");
		taxPrice = detial.getDouble("airportTax") + detial.getDouble("fuelTax");
		return taxPrice;
	}
	
	//经济
	public Object fetchS() throws Exception {
		HttpClient client = null;
		HttpPost httpPost = null;
		HttpResponse response = null;
		HttpEntity entity = null;
		net.htmlparser.jericho.Source source = null;
		try {
			client = super.getHttpClient();
			httpPost = new HttpPost(getUrl());
			response = client.execute(httpPost);
			entity = response.getEntity();
			
			source = new Source(entity.getContent());
//			System.out.println(source);
			super.appendPageContents(source.getParseText().toString());
			setLenghtCount(super.getSourceWebPageContents().length());
			return source;
		} catch(Exception e){
			if(this.isUseProxyip()) {
				proxyIp.setUseStatus(ProxyIpModel.UNENBLE);
			}
			throw e;
		}finally {
			if(httpPost != null) httpPost.releaseConnection();
			client = null;
			httpPost = null;
			if(entity != null) EntityUtils.consume(entity);
			entity = null;
			response = null;
			if(isUseProxyip()) {
				ipProvider.rollBackProxyIp(proxyIp);
			}
		}
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		if(super.getRouteType()==RouteTypeEnum.DOMESTIC_ONEWAYTRIP){
			return true;
		}
		
		if(fetchObject == null){
			return false;
		}else if(fetchObject instanceof String){
			if(((String)fetchObject).length() < 3000){
				throw new PageErrorResultException("抓取的数据不正确(长度少于3000)");
			}
			String result = RegHtmlUtil.regStr((String)fetchObject, this.getErrors());
			if(result != null){
				throw new PageErrorResultException(result);
			}
		}else if(StringUtils.isNotBlank(getSourceWebPageContents())){
			if(getSourceWebPageContents().length() < 3000){
				throw new PageErrorResultException("抓取的数据不正确(长度少于3000)");
			}
			String result = RegHtmlUtil.regStr(getSourceWebPageContents(), this.getErrors());
			if(result != null){
				throw new PageErrorResultException(result);
			}
		}
		return true;
	}	
	
	private List<String> getErrors(){
		List<String> errors = new ArrayList<String>();
		errors.add("404 Not Found");
		errors.add("<title>系统错误提示信息</title>");
		errors.add("<head><title>301 Moved Permanently</title></head>");
		errors.add("<title>502 Proxy Error</title>");
		errors.add("<title>ERROR: The requested URL could not be retrieved</title>");
		return errors;
	}
	
    /**
     * 数组排序  
     */
	public Double[] SortArray(Double[] x) {
		for (int i = 0; i < x.length; i++) {
			for (int j = i + 1; j < x.length; j++) {
				if (x[i] > x[j]) {
					double temp = x[i];
					x[i] = x[j];
					x[j] = temp;
				}
			}
		}
       return x;
	}
	/**
	 * 获取table 内容
	 * @param element
	 * @param rows
	 * @param cols
	 * @return
	 */
	public Element getTableContent(Element element,int rows,int cols)
	{
		Element resultElement=null;
		List<Element> trList=element.getAllElements(HTMLElementName.TR);
		if(rows<trList.size())
		{
			Element trElement=trList.get(rows);
			List<Element> tdList=trElement.getAllElements(HTMLElementName.TD);
			if(cols<tdList.size())
			{
				resultElement=tdList.get(cols);
			}
			
		}
		
		return resultElement;
	}

//	public static void  main(String[] arg) throws Exception{
//		CrawlContext.getInstance().setDebug(true);
//		
//		TaskModel taskQueue = new TaskModel();
//		taskQueue.setGrabChannelId(9l);
//		//taskQueue.setIsReturn(0);
////===================================================中转数据
////		taskQueue.setFromCity("CGQ");
////		taskQueue.setFromCityName("长春");
////		taskQueue.setToCity("HAK");
////		taskQueue.setToCityName("海口");
////		taskQueue.setFlightDate("2013-10-01");
////===================================================直达数据
////		taskQueue.setFromCity("CAN");
////		taskQueue.setFromCityName("广州");
////		taskQueue.setToCity("PEK");
////		taskQueue.setToCityName("北京");
////		taskQueue.setFlightDate("2013-10-01");
////=======================================================无航班
////		
//		taskQueue.setFromCity("SZX");
//		taskQueue.setFromCityName("深圳");
//		taskQueue.setToCity("SWA");
//		taskQueue.setToCityName("汕头");
//		taskQueue.setFlightDate("2013-10-01");
//		
//		
//		//logger.info("抓取引警系统配置文件统一注册......");
//		ConfigLoader.initLoader();
//		HaiNanAirAdapter c = new HaiNanAirAdapter(taskQueue);
//		Object obj = c.fetch(null);
//		//org.apache.commons.io.FileUtils.writeStringToFile(new File("F:/crawl-data-temp.txt"), c.getSourceWebPageContents(), false);
//		//Object obj = org.apache.commons.io.FileUtils.readFileToString(new File("F:/hainan.htm"),"GBK");
//		List<Object> results = c.paraseToVo(obj);
//		//System.out.println(results.size());
//		for (Object o : results) {
//			PlaneInfoEntity p = (PlaneInfoEntity)o;
//			System.out.println(p.toString());
//		}
//		Runtime.getRuntime().exit(0);
//	}
	
	  public static void main(String[] args) throws Exception {
	        TaskModel taskModel = new TaskModel();
	        //taskModel.setFromCity("PVG"); 
	        taskModel.setFromCity("PEK");
	        taskModel.setFromCityName("北京");
	        //taskModel.setToCity("HKG");
	        taskModel.setToCity("BOS");
	        taskModel.setToCityName("波士顿");
	        taskModel.setFlightDate("2014-08-31");
	        taskModel.setIsReturn(1);
	        taskModel.setIsInternational(1);
	        taskModel.setReturnGrabDate("2014-09-07");
	        HaiNanAirAdapter adapter = new HaiNanAirAdapter(taskModel);
	        Object obj = adapter.fetch(null);
			List<Object> objList = adapter.paraseToVo(obj);
			System.out.println(objList);
	    }
	  
	  private void headers(HttpPost httpPost){
		  	httpPost.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.setHeader("Accept-Encoding", "gzip, deflate");
			httpPost.setHeader("Accept-Language", "zh-cn,en-us;q=0.8,zh;q=0.5,en;q=0.3");
			httpPost.setHeader("Connection", "keep-alive");
			httpPost.setHeader("DNT", "1");
			httpPost.setHeader("Host", "et.hnair.com");
			httpPost.setHeader("User-Agent", RandomBrowserVersion.getBV().getUserAgent());
	  }
}



