package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.Common;
import com.foreveross.crawl.common.exception.self.PageErrorResultException;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.single.SinglePlaneInfoEntity;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.proxyip.ProxyIpModel;
import com.foreveross.taskservice.common.bean.TaskModel;
/**
 * http://www.juneyaoair.com
 * 吉祥航空
 * @author lijianning
 * date 2014-05-05
 */
public class JuneYaoAdapter extends AbstractAdapter {

	public JuneYaoAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		TaskModel taskModel = new TaskModel();
		taskModel.setFromCity("CAN");
		taskModel.setFromCityName("广州");
		taskModel.setToCity("PVG");
		taskModel.setToCityName("上海");
		taskModel.setFlightDate("2014-07-10");
		taskModel.setReturnGrabDate("2014-07-13");
		taskModel.setIsReturn(0);
		taskModel.setIsInternational(0);
		JuneYaoAdapter adapter = new JuneYaoAdapter(taskModel);
		Object obj = adapter.fetch(null);
		List<Object> objList = adapter.paraseToVo(obj);
		System.out.println(objList);
		// AirAsiaAdapter adapter = new AirAsiaAdapter(taskModel);
		// Map<String, String> params = adapter.prepareFetchParams();
		// System.out.println(params);
		//
		// System.out.println(new
		// ObjectMapper().writeValueAsString(adapter.parseOneway(adapter.fetchFltInfos(adapter.prepareOnewayFltKeys(params)))));
	}
	public List<Object> paraseReturnOnWay(Object fetchObject) throws Exception {
		List<Object> dataList = new ArrayList<Object>();
		Source source  = new Source(fetchObject.toString());
		Element queryOW = source.getElementById("queryOW");
		Element bookInfoOW=source.getElementById("bookInfoOW");
		if(null!=bookInfoOW){
			String strong=bookInfoOW.getChildElements().get(0).getContent().toString();
			if(null!=strong && strong.startsWith("您选择的日期没有航班信息")){
				throw new FlightInfoNotFoundException(strong);
			}
		}
		List<Element> wflt = queryOW.getAllElementsByClass("searchresult_flt");
		if(wflt.size()==0){
			throw new Exception("抓取错误!");
		}
		this.returnWay(wflt,source,dataList);
		Element queryRT = source.getElementById("queryRT");
		List<Element> rflt = queryRT.getAllElementsByClass("searchresult_flt");
		this.returnWayTo(rflt,source,dataList);
		return dataList;
	}
	public void returnWay(List<Element> flt,Source source,List<Object> dataList) throws Exception{
		List<SinglePlaneInfoEntity> listPlane=new ArrayList<SinglePlaneInfoEntity>();
		for (int a = 0; a < flt.size(); a++) {
			Element element2=flt.get(a);
			List<Element> table = element2.getAllElements(HTMLElementName.TABLE);
			List<Element> td  = table.get(0).getAllElements(HTMLElementName.TD);
			SinglePlaneInfoEntity singlePlaneInfoEntity = new SinglePlaneInfoEntity(); 
			
			setSingle(singlePlaneInfoEntity);
			//机型 321
			Element typeList = flt.get(a).getAllElementsByClass("searchresult_flt_other").get(a);
			String t = typeList.getContent().toString();
			t=t.substring(t.indexOf("机型")+2,t.indexOf("|"));
			t=trim(t);
			singlePlaneInfoEntity.setFlightType(t);
			
			Set<CabinEntity> cabins = new HashSet<CabinEntity>();
			CabinEntity cabinEntity = null;
			if(!"--".equals(td.get(4).getContent().toString())){
				//########################   商务舱价格   ################# //
				
				List<Element> div=td.get(4).getAllElements(HTMLElementName.DIV);
				List<Element> input=div.get(0).getAllElements(HTMLElementName.INPUT);
				String data = input.get(0).getAttributeValue("data");
				
				JSONObject json=JSONObject.fromObject(data);
				
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				singlePlaneInfoEntity.setStartTime(df.parse(json.getString("DepDateTime")));
				singlePlaneInfoEntity.setEndTime(df.parse(json.getString("ArrDateTime")));
				singlePlaneInfoEntity.setFlightNo(json.getString("FlightNo"));//航班号

				JSONArray jsonlist = json.getJSONArray("CabinFareList");
				
				for (Object object : jsonlist) {
					cabinEntity = new CabinEntity();
					cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
					cabinEntity.setCabinType("商务舱");
					cabinEntity.setProductName("商务舱");
					
					JSONObject j=(JSONObject) object;
					cabinEntity.setSubCabinName(j.getString("CabinCode"));
					cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(cabinEntity);
				}
				Element eid = source.getElementById(json.getString("FlightNo"));
				List<Element> table2=eid.getAllElements(HTMLElementName.TABLE);
				for (Element element : table2) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					if(!"".equals(td2.get(4).getContent().toString())){
						List<Element> div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							cabinEntity = new CabinEntity();
							cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
							cabinEntity.setCabinType("商务舱");
							cabinEntity.setProductName("商务舱");
							
							JSONObject j=(JSONObject) object;
							cabinEntity.setSubCabinName(j.getString("CabinCode"));
							cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(cabinEntity);
						}
					}
				}
			}
				//########################   经济舱价格    ################# //
			if(!"--".equals(td.get(5).getContent().toString())){
				List<Element> div5=td.get(5).getAllElements(HTMLElementName.DIV);
				List<Element> input5=div5.get(0).getAllElements(HTMLElementName.INPUT);
				String data5 = input5.get(0).getAttributeValue("data");
				JSONObject json5=JSONObject.fromObject(data5);
				JSONArray jsonlist5 = json5.getJSONArray("CabinFareList");
				for (Object object : jsonlist5) {
					cabinEntity = new CabinEntity();
					cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
					cabinEntity.setCabinType("经济舱");
					cabinEntity.setProductName("经济舱");
					
					JSONObject j=(JSONObject) object;
					cabinEntity.setSubCabinName(j.getString("CabinCode"));
					cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(cabinEntity);
				}
				Element eid5 = source.getElementById(json5.getString("FlightNo"));
				List<Element> table5=eid5.getAllElements(HTMLElementName.TABLE);
				for (Element element : table5) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					List<Element> div2 = null;
					if("--".equals(td.get(4).getContent().toString())){//商务舱为空
						if(!"".equals(td2.get(4).getContent().toString())){
							div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						}
					}else{
						if(!"".equals(td2.get(5).getContent().toString())){
							div2=td2.get(5).getAllElements(HTMLElementName.DIV);
						}
					}
					if(div2!=null){
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							cabinEntity = new CabinEntity();
							cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
							cabinEntity.setCabinType("经济舱");
							cabinEntity.setProductName("经济舱");
							
							JSONObject j=(JSONObject) object;
							cabinEntity.setSubCabinName(j.getString("CabinCode"));
							cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(cabinEntity);
						}
					}
				}
			}
				singlePlaneInfoEntity.setCabins(cabins);

				//setTotalPriceByCabinPrice(singlePlaneInfoEntity);
				listPlane.add(singlePlaneInfoEntity);
				
			}
		
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(listPlane);
		}catch(Exception e){
			logger.info("国际往返排序错误，请检查数据");
			logger.info(e);
			try{
				logger.info(dataList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}
		for(SinglePlaneInfoEntity entity:listPlane){
			dataList.add(entity);
		}
	}
	
	public void returnWayTo(List<Element> flt,Source source,List<Object> dataList) throws Exception{
		List<ReturnDoublePlaneInfoEntity> listReturn =new ArrayList<ReturnDoublePlaneInfoEntity>();
		for (int a = 0; a < flt.size(); a++) {
			Element element2=flt.get(a);
			List<Element> table = element2.getAllElements(HTMLElementName.TABLE);
			List<Element> td  = table.get(0).getAllElements(HTMLElementName.TD);
			
			ReturnDoublePlaneInfoEntity returnPI = new ReturnDoublePlaneInfoEntity(); 
			
			setSingleReturn(returnPI);
			//机型 321
			Element typeList = flt.get(a).getAllElementsByClass("searchresult_flt_other").get(a);
			String t = typeList.getContent().toString();
			t=t.substring(t.indexOf("机型")+2,t.indexOf("|"));
			t=trim(t);
			returnPI.setFlightType(t);
			
			
			Set<ReturnCabinEntity> cabins = new HashSet<ReturnCabinEntity>();
			ReturnCabinEntity returnCabinEntity = null;
			
			List<Element> DIV = td.get(0).getAllElements(HTMLElementName.DIV);
			String s = taskQueue.getFlightDate()+" 0"+DIV.get(0).getContent().toString().trim();
			String e = taskQueue.getFlightDate()+" 0"+DIV.get(1).getContent().toString().trim();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			returnPI.setStartTime(df.parse(s)) ;
			returnPI.setEndTime(df.parse(e));
			returnPI.setFlightNo(cleanHtmlTag(td.get(1).getContent().toString()));//航班号
			
			if(!"--".equals(td.get(4).getContent().toString())){
				//########################   商务舱价格   ################# //
				
				List<Element> div=td.get(4).getAllElements(HTMLElementName.DIV);
				List<Element> input=div.get(0).getAllElements(HTMLElementName.INPUT);
				String data = input.get(0).getAttributeValue("data");
				
				JSONObject json=JSONObject.fromObject(data);
				JSONArray jsonlist = json.getJSONArray("CabinFareList");
				
				for (Object object : jsonlist) {
					returnCabinEntity = new ReturnCabinEntity();
					returnCabinEntity.setPlaneInfoEntity(returnPI);
					returnCabinEntity.setCabinType("商务舱");
					returnCabinEntity.setProductName("商务舱");
					
					JSONObject j=(JSONObject) object;
					returnCabinEntity.setSubCabinName(j.getString("CabinCode"));
					returnCabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					returnCabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					returnCabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(returnCabinEntity);
				}
				Element eid = source.getElementById(returnPI.getFlightNo());
				List<Element> table2=eid.getAllElements(HTMLElementName.TABLE);
				for (Element element : table2) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					if(!"".equals(td2.get(4).getContent().toString())){
						List<Element> div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							returnCabinEntity = new ReturnCabinEntity();
							returnCabinEntity.setPlaneInfoEntity(returnPI);
							returnCabinEntity.setCabinType("商务舱");
							returnCabinEntity.setProductName("商务舱");
							
							JSONObject j=(JSONObject) object;
							returnCabinEntity.setSubCabinName(j.getString("CabinCode"));
							returnCabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							returnCabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							returnCabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(returnCabinEntity);
						}
					}
				}
			}
				//########################   经济舱价格    ################# //
			if(!"--".equals(td.get(5).getContent().toString())){
				List<Element> div5=td.get(5).getAllElements(HTMLElementName.DIV);
				List<Element> input5=div5.get(0).getAllElements(HTMLElementName.INPUT);
				String data5 = input5.get(0).getAttributeValue("data");
				JSONObject json5=JSONObject.fromObject(data5);
				JSONArray jsonlist5 = json5.getJSONArray("CabinFareList");
				for (Object object : jsonlist5) {
					returnCabinEntity = new ReturnCabinEntity();
					returnCabinEntity.setPlaneInfoEntity(returnPI);
					returnCabinEntity.setCabinType("经济舱");
					returnCabinEntity.setProductName("经济舱");
					
					JSONObject j=(JSONObject) object;
					returnCabinEntity.setSubCabinName(j.getString("CabinCode"));
					returnCabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					returnCabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					returnCabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(returnCabinEntity);
				}
				Element eid5 = source.getElementById(json5.getString("FlightNo"));
				List<Element> table5=eid5.getAllElements(HTMLElementName.TABLE);
				for (Element element : table5) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					List<Element> div2 = null;
					if("--".equals(td.get(4).getContent().toString())){//商务舱为空
						if(!"".equals(td2.get(4).getContent().toString())){
							div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						}
					}else{
						if(!"".equals(td2.get(5).getContent().toString())){
							div2=td2.get(5).getAllElements(HTMLElementName.DIV);
						}
					}
					if(div2!=null){
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							returnCabinEntity = new ReturnCabinEntity();
							returnCabinEntity.setPlaneInfoEntity(returnPI);
							returnCabinEntity.setCabinType("经济舱");
							returnCabinEntity.setProductName("经济舱");
							
							JSONObject j=(JSONObject) object;
							returnCabinEntity.setSubCabinName(j.getString("CabinCode"));
							returnCabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							returnCabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							returnCabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(returnCabinEntity);
						}
					}
				}
			}
				returnPI.setReturnCabins(cabins);

				//setTotalPriceByCabinPrice(returnPI);
				listReturn.add(returnPI);
				
			}
		
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(listReturn);
		}catch(Exception e){
			logger.info("国际往返排序错误，请检查数据");
			logger.info(e);
			try{
				logger.info(dataList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}
		for(ReturnDoublePlaneInfoEntity entity:listReturn){
			dataList.add(entity);
		}
	}
	
	public List<Object> paraseInternOnWay(Object fetchObject) throws Exception {
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		Source source  = new Source(fetchObject.toString());
		List<Element> trList = source.getAllElementsByClass("searchresult_flt_info");
		if(trList.size()==0){
			throw new FlightInfoNotFoundException("你选择的日期没有航班信息,请重新选择!");
		}
		for (int a = 0; a < trList.size(); a++) {
			Element element2=trList.get(a);
			List<Element> table = element2.getAllElements(HTMLElementName.TABLE);
			List<Element> td  = table.get(0).getAllElements(HTMLElementName.TD);
			SinglePlaneInfoEntity singlePlaneInfoEntity = new SinglePlaneInfoEntity(); 
			
			setSingle(singlePlaneInfoEntity);
			
			//机型 321
			Element typeList = source.getAllElementsByClass("searchresult_flt_other").get(a);
			String t = typeList.getContent().toString();
			t=t.substring(t.indexOf("机型")+2,t.indexOf("|"));
			t=trim(t);
			singlePlaneInfoEntity.setFlightType(t);
			
			Set<CabinEntity> cabins = new HashSet<CabinEntity>();
			CabinEntity cabinEntity = null;
			if(!"--".equals(td.get(4).getContent().toString())){
				//########################   商务舱价格   ################# //
				
				List<Element> div=td.get(4).getAllElements(HTMLElementName.DIV);
				List<Element> input=div.get(0).getAllElements(HTMLElementName.INPUT);
				String data = input.get(0).getAttributeValue("data");
				
				JSONObject json=JSONObject.fromObject(data);
				
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				singlePlaneInfoEntity.setStartTime(df.parse(json.getString("DepDateTime"))) ;
				singlePlaneInfoEntity.setEndTime(df.parse(json.getString("ArrDateTime")));
				singlePlaneInfoEntity.setFlightNo(json.getString("FlightNo"));//航班号

				JSONArray jsonlist = json.getJSONArray("CabinFareList");
				
				for (Object object : jsonlist) {
					cabinEntity = new CabinEntity();
					cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
					cabinEntity.setCabinType("商务舱");
					cabinEntity.setProductName("商务舱");
					
					JSONObject j=(JSONObject) object;
					cabinEntity.setSubCabinName(j.getString("CabinCode"));
					cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(cabinEntity);
				}
				Element eid = source.getElementById(json.getString("FlightNo"));
				List<Element> table2=eid.getAllElements(HTMLElementName.TABLE);
				for (Element element : table2) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					if(!"".equals(td2.get(4).getContent().toString())){
						List<Element> div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							cabinEntity = new CabinEntity();
							cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
							cabinEntity.setCabinType("商务舱");
							cabinEntity.setProductName("商务舱");
							
							JSONObject j=(JSONObject) object;
							cabinEntity.setSubCabinName(j.getString("CabinCode"));
							cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(cabinEntity);
						}
					}
				}
			}
				//########################   经济舱价格    ################# //
			if(!"--".equals(td.get(5).getContent().toString())){
				List<Element> div5=td.get(5).getAllElements(HTMLElementName.DIV);
				List<Element> input5=div5.get(0).getAllElements(HTMLElementName.INPUT);
				String data5 = input5.get(0).getAttributeValue("data");
				JSONObject json5=JSONObject.fromObject(data5);
				JSONArray jsonlist5 = json5.getJSONArray("CabinFareList");
				for (Object object : jsonlist5) {
					cabinEntity = new CabinEntity();
					cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
					cabinEntity.setCabinType("经济舱");
					cabinEntity.setProductName("经济舱");
					
					JSONObject j=(JSONObject) object;
					cabinEntity.setSubCabinName(j.getString("CabinCode"));
					cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
					cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
					cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
					cabins.add(cabinEntity);
				}
				Element eid5 = source.getElementById(json5.getString("FlightNo"));
				List<Element> table5=eid5.getAllElements(HTMLElementName.TABLE);
				for (Element element : table5) {
					List<Element> td2  = element.getAllElements(HTMLElementName.TD);
					List<Element> div2 = null;
					if("--".equals(td.get(4).getContent().toString())){//商务舱为空
						if(!"".equals(td2.get(4).getContent().toString())){
							div2=td2.get(4).getAllElements(HTMLElementName.DIV);
						}
					}else{
						if(!"".equals(td2.get(5).getContent().toString())){
							div2=td2.get(5).getAllElements(HTMLElementName.DIV);
						}
					}
					if(div2!=null){
						List<Element> input2=div2.get(0).getAllElements(HTMLElementName.INPUT);
						String data2 = input2.get(0).getAttributeValue("data");
						JSONObject json2=JSONObject.fromObject(data2);
						JSONArray jt = json2.getJSONArray("CabinFareList");
						for (Object object : jt) {
							cabinEntity = new CabinEntity();
							cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
							cabinEntity.setCabinType("经济舱");
							cabinEntity.setProductName("经济舱");
							
							JSONObject j=(JSONObject) object;
							cabinEntity.setSubCabinName(j.getString("CabinCode"));
							cabinEntity.setZhijian(Double.parseDouble(j.getString("PriceValue")));
							cabinEntity.setTuigaiqian(j.getString("RefundedComment")+" "+j.getString("ChangedComment"));//退改签信息
							cabinEntity.setLastSeatInfo(j.getString("CabinNumber"));
							cabins.add(cabinEntity);
						}
					}
				}
			}
				singlePlaneInfoEntity.setCabins(cabins);
				//setTotalPriceByCabinPrice(singlePlaneInfoEntity);
				dataList.add(singlePlaneInfoEntity);
		}
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		}catch(Exception e){
			logger.info("国际单程排序错误，请检查数据");
			logger.info(e);
			try{
				logger.info(dataList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}
		return Arrays.asList(dataList.toArray());
	}
	
	/**
	 * 国内单程
	 */
	public List<Object> paraseToVoS(Object fetchObject) throws Exception {
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		Source source  = new Source(fetchObject.toString());
		Element bookInfoOW=source.getElementById("bookInfoOW");
		if(null!=bookInfoOW){
			String strong=bookInfoOW.getChildElements().get(0).getContent().toString();
			if(null!=strong && strong.startsWith("您选择的日期没有航班信息")){
				throw new FlightInfoNotFoundException(strong);
			}
		}
		List<Element> trList = source.getAllElementsByClass("booking-main-base-flt-info");
		if(trList.size()==0){
			throw new Exception("抓取错误");
		}
		 boolean hasFlightNo=false;
		for (int a = 0; a < trList.size(); a++) {
			Element element2=trList.get(a);
			List<Element> lis = element2.getAllElements(HTMLElementName.LI);
			SinglePlaneInfoEntity singlePlaneInfoEntity = new SinglePlaneInfoEntity(); 
			Set<CabinEntity> cabins = new HashSet<CabinEntity>();
			
			//机型 321
			Element typeList = source.getAllElementsByClass("booking-main-base-flt-other1").get(a);
			String t = typeList.getContent().toString();
			double taxesPrice=170;
			String taxesStr=t.substring(t.indexOf("燃油")+2, t.indexOf("|"));
			if(null!=taxesStr && !"".equals(taxesStr)){
				String taxes[]=taxesStr.split("\\+");
				if(taxes.length>1){
					taxesPrice=PlaneInfoEntityBuilder.getDouble(taxes[0].replace(" ", ""))+
							PlaneInfoEntityBuilder.getDouble(taxes[1].replace(" ", ""));
				}
			}
			t=t.substring(t.indexOf("机型")+2,t.length());
			t=trim(t);
			singlePlaneInfoEntity.setFlightType(t);
			setSingle(singlePlaneInfoEntity);
			
			List<Element> span = lis.get(0).getAllElements(HTMLElementName.SPAN);
			String s = taskQueue.getFlightDate()+" 0"+span.get(0).getContent().toString().trim();
			String e = taskQueue.getFlightDate()+" 0"+span.get(1).getContent().toString().trim();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			singlePlaneInfoEntity.setStartTime(df.parse(s)) ;
			singlePlaneInfoEntity.setEndTime(df.parse(e));
			String flightNo = lis.get(1).getContent().toString();
			flightNo = cleanHtmlTag(flightNo);
			singlePlaneInfoEntity.setFlightNo(flightNo.trim());//航班号 
			
			List<Element> cabinList = source.getAllElementsByClass("order_gift_trcontent");
			int sun = 0;
			for (int i = 3; i < 7; i++) {
				String st = lis.get(i).getContent().toString().trim();
				if(!"--".equals(st)){
					sun++;
				}
			}
			if(sun>0){
				hasFlightNo=true;
			}else if(sun==0){
				continue;
			}
			for (int j = 0; j < cabinList.size(); j++) {
				if(j<(a+1)*sun && j>=a*sun){
					Element table = cabinList.get(j);
					List<Element> trs = table.getAllElements(HTMLElementName.TR);
					for (Element element : trs) {
						CabinEntity cabinEntity = new CabinEntity();
						List<Element> tds = element.getAllElements(HTMLElementName.TD);
						String st = tds.get(0).getContent().toString().trim();//所选产品
						st = st.substring(st.indexOf(">")+1,st.length());
						cabinEntity.setPlaneInfoEntity(singlePlaneInfoEntity);
						cabinEntity.setCabinType(st);
						cabinEntity.setProductName(st);
						
						String p = tds.get(1).getContent().toString();//舱位价格
						p=trim(p);
						cabinEntity.setPrice(Double.parseDouble(p));
						cabinEntity.setTaxesPrice(taxesPrice); 
						cabinEntity.setOriginalPrice(Double.parseDouble(p) + taxesPrice);
						
						String cw = tds.get(3).getContent().toString();//舱位
						cabinEntity.setSubCabinName(cw);
						String y = tds.get(1).getContent().toString();//机票原价
						String b = tds.get(2).getContent().toString();//折扣价
						Double r=Common.div(Double.parseDouble(b),Double.parseDouble(y));
						cabinEntity.setRebate(r);
						String tui = tds.get(4).getContent().toString();//退改签规定
						cabinEntity.setTuigaiqian(tui);
						String lastSeatInfo = tds.get(5).getContent().toString();//票量
						cabinEntity.setLastSeatInfo(lastSeatInfo);
						cabins.add(cabinEntity);
					}
					
				}
			}
			singlePlaneInfoEntity.setCabins(cabins);
		//	setTotalPriceByCabinPrice(singlePlaneInfoEntity);
			dataList.add(singlePlaneInfoEntity);
		}
		if(!hasFlightNo){
			throw new FlightInfoNotFoundException("你选择的日期没有航班信息,请重新选择!");
		}
		
		try{
			PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		}catch(Exception e){
			logger.info("国内单程排序错误，请检查数据");
			logger.info(e);
			try{
				logger.info(dataList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}
		return Arrays.asList(dataList.toArray());

	}
		/**
		  * 
		  * @param param
		  * @return
		  * @description 去掉字符串中前后空格
		  * @update 2014-05-21 
		  */
		 public  String trim(String param){
		  param=param.replaceAll("(^\\s{1,})|(\\s{1,}$)", "");
		  return param;
		 }
	public void setSingle(SinglePlaneInfoEntity singlePlaneInfoEntity){
		singlePlaneInfoEntity.setCreateTime(new Date());
		singlePlaneInfoEntity.setAreaName(taskQueue.getAreaName());					
		singlePlaneInfoEntity.setAreaCode(taskQueue.getAreaCode());
		singlePlaneInfoEntity.setGrabChannelId(taskQueue.getGrabChannelId());
		singlePlaneInfoEntity.setGrabChannelName(taskQueue.getGrabChannel());
		singlePlaneInfoEntity.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
		singlePlaneInfoEntity.setCarrierFullName("吉祥航空公司");
		singlePlaneInfoEntity.setCarrierKey("JX");
		singlePlaneInfoEntity.setCarrierName("吉祥");
		singlePlaneInfoEntity.setFromCity(taskQueue.getFromCity());
		singlePlaneInfoEntity.setFromCityName(taskQueue.getFromCityName());
		singlePlaneInfoEntity.setToCity(taskQueue.getToCity());
		singlePlaneInfoEntity.setToCityName(taskQueue.getToCityName());
		singlePlaneInfoEntity.setFlightDate(taskQueue.getFlightDate());
	}
	public void setSingleDouble(DoublePlaneInfoEntity d){
		d.setCreateTime(new Date());
		d.setAreaName(taskQueue.getAreaName());					
		d.setAreaCode(taskQueue.getAreaCode());
		d.setGrabChannelId(taskQueue.getGrabChannelId());
		d.setGrabChannelName(taskQueue.getGrabChannel());
		d.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
		d.setCarrierFullName("吉祥航空公司");
		d.setCarrierKey("HO");
		d.setCarrierName("吉祥");
		d.setFromCity(taskQueue.getFromCity());
		d.setFromCityName(taskQueue.getFromCityName());
		d.setToCity(taskQueue.getToCity());
		d.setToCityName(taskQueue.getToCityName());
		d.setFlightDate(taskQueue.getFlightDate());
	}
	public void setSingleReturn(ReturnDoublePlaneInfoEntity r){
		r.setCreateTime(new Date());
		r.setAreaName(taskQueue.getAreaName());					
		r.setAreaCode(taskQueue.getAreaCode());
		r.setGrabChannelId(taskQueue.getGrabChannelId());
		r.setGrabChannelName(taskQueue.getGrabChannel());
		r.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
		r.setCarrierFullName("吉祥航空公司");
		r.setCarrierKey("HO");
		r.setCarrierName("吉祥");
		r.setFromCity(taskQueue.getFromCity());
		r.setFromCityName(taskQueue.getFromCityName());
		r.setToCity(taskQueue.getToCity());
		r.setToCityName(taskQueue.getToCityName());
	}
	/**
	 * 国内往返
	 */
	public List<Object> paraseToVoRT(Object fetchObject) throws Exception {
		//DoublePlaneInfoEntity doublePI =null;//来回程（往返程）航班信息模型
		DoublePlaneInfoEntity doublePlanInfo=null;//去程程航班模型
		ReturnDoublePlaneInfoEntity returnPI=null;//回程航班的信息实体
		List<AbstractPlaneInfoEntity> doubleList = new ArrayList<AbstractPlaneInfoEntity>();
		List<ReturnDoublePlaneInfoEntity> returnList = new ArrayList<ReturnDoublePlaneInfoEntity>();
		Source source  = new Source(fetchObject.toString());
		List<Element> tort = source.getAllElementsByClass("booking-main-base-fltbox");
		if(tort.size()==0){
			throw new FlightInfoNotFoundException("你选择的日期没有航班信息,请重新选择!");
		}
		for (int i = 0; i < tort.size(); i++) {
			switch (i) {
			case 0://去程
				List<Element> trList =tort.get(0).getAllElementsByClass("booking-main-base-flt-info");
				for (int a = 0; a < trList.size(); a++) {
					Element element2=trList.get(a);
					List<Element> lis = element2.getAllElements(HTMLElementName.LI);
					doublePlanInfo = new DoublePlaneInfoEntity(); 
					Set<CabinEntity> cabins = new HashSet<CabinEntity>();
					setSingleDouble(doublePlanInfo);
					
					//机型 321
					Element typeList = tort.get(i).getAllElementsByClass("booking-main-base-flt-other1").get(a);
					String t = typeList.getContent().toString();
					double taxesPrice=170;
					String taxesStr=t.substring(t.indexOf("燃油")+2, t.indexOf("|"));
					if(null!=taxesStr && !"".equals(taxesStr)){
						String taxes[]=taxesStr.split("\\+");
						if(taxes.length>1){
							taxesPrice=PlaneInfoEntityBuilder.getDouble(taxes[0].replace(" ", ""))+
									PlaneInfoEntityBuilder.getDouble(taxes[1].replace(" ", ""));
						}
					}
					t=t.substring(t.indexOf("机型")+2,t.length());
					t=trim(t);
					doublePlanInfo.setFlightType(t);
					
					List<Element> span = lis.get(0).getAllElements(HTMLElementName.SPAN);
					String s = taskQueue.getFlightDate()+" 0"+span.get(0).getContent().toString().trim();
					String e = taskQueue.getFlightDate()+" 0"+span.get(1).getContent().toString().trim();
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					doublePlanInfo.setStartTime(df.parse(s)) ;
					doublePlanInfo.setEndTime(df.parse(e));
					doublePlanInfo.setFlightNo(cleanHtmlTag(lis.get(1).getContent().toString()));//航班号
					int sun = 0;
					for (int j = 3; j < 7; j++) {
						String st = lis.get(j).getContent().toString().toString();
						if(!"--".equals(st)){
							sun++;
						}
					}
					List<Element> cabinList = tort.get(0).getAllElementsByClass("order_gift_trcontent");
					for (int j = 0; j < cabinList.size(); j++) {
						if(j<(a+1)*sun && j>=a*sun){
							Element table = cabinList.get(j);
							List<Element> trs = table.getAllElements(HTMLElementName.TR);
							for (Element element : trs) {
								CabinEntity cabinEntity = new CabinEntity();
								List<Element> tds = element.getAllElements(HTMLElementName.TD);
								String st = tds.get(0).getContent().toString().trim();//所选产品
								st = st.substring(st.indexOf(">")+1,st.length());
								cabinEntity.setPlaneInfoEntity(doublePlanInfo);
								cabinEntity.setCabinType(st);
								cabinEntity.setProductName(st);
								
								String p = tds.get(1).getContent().toString();//舱位价格
								p=trim(p);
								cabinEntity.setPrice(Double.parseDouble(p));
								cabinEntity.setTaxesPrice(taxesPrice); //默认120
								cabinEntity.setOriginalPrice(Double.parseDouble(p) + taxesPrice);

								
								String cw = tds.get(3).getContent().toString();//舱位
								cabinEntity.setSubCabinName(cw);
								String y = tds.get(1).getContent().toString();//机票原价
								String b = tds.get(2).getContent().toString();//折扣价
								Double r=Common.div(Double.parseDouble(b),Double.parseDouble(y));
								cabinEntity.setRebate(r);
								String tui = tds.get(4).getContent().toString();//退改签规定
								cabinEntity.setTuigaiqian(tui);
								String lastSeatInfo = tds.get(5).getContent().toString();//票量
								cabinEntity.setLastSeatInfo(lastSeatInfo);
								cabins.add(cabinEntity);
							}
							
						}
					}
					doublePlanInfo.setCabins(cabins);
					//setTotalPriceByCabinPrice(doublePlanInfo);
					//dataList.add(doublePlanInfo);
					doubleList.add(doublePlanInfo);
				}
				break;

			case 1://返程
				List<Element> trList2 =tort.get(1).getAllElementsByClass("booking-main-base-flt-info");
				for (int a = 0; a < trList2.size(); a++) {
					Element element2=trList2.get(a);
					List<Element> lis = element2.getAllElements(HTMLElementName.LI);
					returnPI = new ReturnDoublePlaneInfoEntity(); 
					
					setSingleReturn(returnPI);
					//机型 321
					Element typeList = tort.get(i).getAllElementsByClass("booking-main-base-flt-other1").get(a);
					String t = typeList.getContent().toString();
					double taxesPrice=170;
					String taxesStr=t.substring(t.indexOf("燃油")+2, t.indexOf("|"));
					if(null!=taxesStr && !"".equals(taxesStr)){
						String taxes[]=taxesStr.split("\\+");
						if(taxes.length>1){
							taxesPrice=PlaneInfoEntityBuilder.getDouble(taxes[0].replace(" ", ""))+
									PlaneInfoEntityBuilder.getDouble(taxes[1].replace(" ", ""));
						}
					}
					t=t.substring(t.indexOf("机型")+2,t.length());
					t=trim(t);
					returnPI.setFlightType(t);
					
					Set<ReturnCabinEntity> cabins = new HashSet<ReturnCabinEntity>();
					returnPI.setFromCity(taskQueue.getFromCity());
					returnPI.setFromCityName(taskQueue.getFromCityName());
					returnPI.setToCity(taskQueue.getToCity());
					returnPI.setToCityName(taskQueue.getToCityName());
					
					List<Element> span = lis.get(0).getAllElements(HTMLElementName.SPAN);
					String s = taskQueue.getReturnGrabDate()+" 0"+span.get(0).getContent().toString().trim();
					String e = taskQueue.getReturnGrabDate()+" 0"+span.get(1).getContent().toString().trim();
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					returnPI.setStartTime(df.parse(s)) ;
					returnPI.setEndTime(df.parse(e));
					returnPI.setFlightNo(lis.get(1).getContent().toString().trim());//航班号
					int sun = 0;
					for (int j = 3; j < 7; j++) {
						String st = lis.get(j).getContent().toString().toString();
						if(!"--".equals(st)){
							sun++;
						}
					}
					List<Element> cabinList = tort.get(1).getAllElementsByClass("order_gift_trcontent");
					for (int j = 0; j < cabinList.size(); j++) {
						if(j<(a+1)*sun && j>=a*sun){
							Element table = cabinList.get(j);
							List<Element> trs = table.getAllElements(HTMLElementName.TR);
							for (Element element : trs) {
								ReturnCabinEntity cabinEntity = new ReturnCabinEntity();
								List<Element> tds = element.getAllElements(HTMLElementName.TD);
								String st = tds.get(0).getContent().toString().trim();//所选产品
								st = st.substring(st.indexOf(">")+1,st.length());
								cabinEntity.setPlaneInfoEntity(returnPI);
								cabinEntity.setCabinType(st);
								cabinEntity.setProductName(st);
								
								String p = tds.get(1).getContent().toString();//舱位价格
								p=trim(p);
								cabinEntity.setPrice(Double.parseDouble(p));
								cabinEntity.setTaxesPrice(taxesPrice); //默认120
								cabinEntity.setOriginalPrice(Double.parseDouble(p) + taxesPrice);

								
								String cw = tds.get(3).getContent().toString();//舱位
								cabinEntity.setSubCabinName(cw);
								String y = tds.get(1).getContent().toString();//机票原价
								String b = tds.get(2).getContent().toString();//折扣价
								Double r=Common.div(Double.parseDouble(b),Double.parseDouble(y));
								cabinEntity.setRebate(r);
								String tui = tds.get(4).getContent().toString();//退改签规定
								cabinEntity.setTuigaiqian(tui);
								String lastSeatInfo = tds.get(5).getContent().toString();//票量
								cabinEntity.setLastSeatInfo(lastSeatInfo);
								cabins.add(cabinEntity);
							}
							
						}
					}
					returnPI.setReturnCabins(cabins);
				//	setTotalPriceByCabinPrice(returnPI);
					returnList.add(returnPI);
					//PlaneInfoEntityBuilder.buildLimitPrice(returnList);
				}
				break;
			}
		}
		
		//添加去回程关系
		for(AbstractPlaneInfoEntity dp : doubleList){
			((DoublePlaneInfoEntity)dp).getReturnPlaneInfos().addAll(returnList);
		}
		
		try{
			PlaneInfoEntityBuilder.buildRelation(doubleList, returnList);
			PlaneInfoEntityBuilder.buildLimitPrice(doubleList);
		}catch(Exception e){
			logger.info("国内单程排序错误，请检查数据");
			logger.info(e);
			try{
				logger.info(doubleList);
			}catch(Exception e1){
				logger.info("打印数据出错");
			}
		}
		return Arrays.asList(doubleList.toArray());

	}
	/**
	 * 最低和最高价
	 * @param entity
	 * @throws Exception
	 */
	/*public void setTotalPriceByCabinPrice(AbstractPlaneInfoEntity entity)
			throws Exception {
		List<Double> fromPrice = new ArrayList<Double>();
		List<Double> toPrice = new ArrayList<Double>();
		SinglePlaneInfoEntity singleEntity;
		DoublePlaneInfoEntity doubleEntity;
		try {
			if (entity instanceof SinglePlaneInfoEntity) {
				singleEntity = (SinglePlaneInfoEntity) entity;
				for (CabinEntity cb : singleEntity.getCabins()) {
					if (cb != null && cb.getPrice() != null) {
						fromPrice.add(cb.getPrice());
					}
				}
				if (!fromPrice.isEmpty()) {
					entity.setTotalLowestPrice(super.getPriceByType(fromPrice,
							"lower"));
					entity.setTotalHighestPrice(super.getPriceByType(fromPrice,
							"hight"));
				}

			} else if (entity instanceof DoublePlaneInfoEntity) {
				doubleEntity = (DoublePlaneInfoEntity) entity;
				for (CabinEntity cb : doubleEntity.getCabins()) {
					if (cb != null && cb.getPrice() != null) {
						fromPrice.add(cb.getPrice());
					}
				}
				for (ReturnDoublePlaneInfoEntity reEntity : doubleEntity
						.getReturnPlaneInfos()) {
					for (ReturnCabinEntity reCb : reEntity.getReturnCabins()) {
						if (reCb != null && reCb.getPrice() != null) {
							toPrice.add(reCb.getPrice());
						}
					}
				}
				if (!fromPrice.isEmpty() && !toPrice.isEmpty()) {
					entity.setTotalLowestPrice(super.getPriceByType(fromPrice,
							"lower") + super.getPriceByType(toPrice, "lower"));
					entity.setTotalHighestPrice(super.getPriceByType(fromPrice,
							"hight") + super.getPriceByType(toPrice, "hight"));
				}
			}

		} finally {
			fromPrice.clear();
			fromPrice = null;
			toPrice.clear();
			toPrice = null;
			singleEntity = null;
			doubleEntity = null;
		}
	}*/
	@Override
	public List<Object> paraseToVo(Object fetchObject) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return paraseReturnOnWay(fetchObject);// 国际往返
		case INTERNATIONAL_ONEWAY:
			return paraseInternOnWay(fetchObject);// 国际单程
		case DOMESTIC_ONEWAYTRIP:
			return paraseToVoS(fetchObject);// 国内单程
		case DOMESTIC_ROUNDTRIP:
			return paraseToVoRT(fetchObject);// 国内往返
		}
		return null;
	}

	@Override
	public String getUrl() throws Exception {
		String url = null;
		url = "http://www.juneyaoair.com/pages/";
		 if(taskQueue.getIsInternational()==0 && taskQueue.getIsReturn()==0){//国内单程
			url+="flight/flightBookingSelect.aspx?tripType=D&flightType=OW";
		}else if(taskQueue.getIsInternational()==0 && taskQueue.getIsReturn()==1){//国内往返
			url+="flight/flightBookingRTSelect.aspx?tripType=D&directType=D&flightType=RT";
		}else if(taskQueue.getIsInternational()==1 && taskQueue.getIsReturn()==0){//国际单程
			url+="internate/bookingInterOWSelect.aspx?flightType=OW&tripType=I";
		}else if(taskQueue.getIsInternational()==1 && taskQueue.getIsReturn()==1){//国际往返
			url+="internate/bookingInterRTSelect.aspx?flightType=RT&tripType=I";
		}
		url += "&sendCity="+ taskQueue.getFromCityName() + "&sendCode=" +taskQueue.getFromCity()
				+"&arrCity="+ taskQueue.getToCityName()+ "&arrCode=" +taskQueue.getToCity();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String tdate = df.format(new Date());
		int dt = DateUtil.compare_date(taskQueue.getFlightDate(), tdate);
		int dr = 0;
		String takeoffdate = "";
		String returndate = "";
		if (taskQueue.getIsReturn() == 1) {// 往返
			takeoffdate = taskQueue.getFlightDate();
			returndate = taskQueue.getReturnGrabDate();
			dr = DateUtil.compare_date(returndate, tdate);
			if (dr < 0) {
				returndate = tdate;
			}
			if (dt < 0) {
				takeoffdate = tdate;
			}
			url += "&departureDate=" + takeoffdate + "&returnDate=" + returndate;
		} else if (taskQueue.getIsReturn() == 0) {// 单程
			takeoffdate = taskQueue.getFlightDate();
			dr = DateUtil.compare_date(takeoffdate, tdate);
			if (dt < 0) {
				takeoffdate = tdate;
			}
			url += "&departureDate=" + takeoffdate;
		}
		return url;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (getRouteType()) {
		case DOMESTIC_ONEWAYTRIP:
		case INTERNATIONAL_ONEWAY:
			return fetchOneway();
		default:
			return fetchOneway();
		}
	}

	private Object fetchOneway() throws Exception {
		String html = null;
		try {
			html = super.excuteRequest(new HttpPost(getUrl()));
			super.appendPageContents(html);
			return html;
		}finally {
			html = null;
		}

	}

	private List<String> getErrors() {
		List<String> errors = new ArrayList<String>();
		errors.add("404 Not Found");
		errors.add("<title>系统错误提示信息</title>");
		errors.add("<head><title>301 Moved Permanently</title></head>");
		errors.add("<title>502 Proxy Error</title>");
		errors.add("<title>ERROR: The requested URL could not be retrieved</title>");
		return errors;
	}
	
	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		if (fetchObject == null) {
				throw new Exception("航空网页数据为空");
		} else if (StringUtils.isNotBlank(fetchObject.toString())) {
			if (fetchObject.toString().length() < 1000) {
				throw new PageErrorResultException("抓取的数据不正确(长度少于1000)");
			}
			String result = RegHtmlUtil.regStr(fetchObject.toString(),this.getErrors());
			if (result != null) {
				throw new PageErrorResultException(result);
			}
		}
		return true;
	}

	//删除HTML标签代码 。
	private String cleanHtmlTag(String src){
		return src.replaceAll("\\s*?<.*?>\\s*?", "");
	}
}
