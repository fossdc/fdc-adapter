package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.exception.self.PageErrorResultException;
import com.foreveross.crawl.common.exception.self.SystemBusyException;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.JsonUtil;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.CabinRelationEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.domain.airfreight.single.SinglePlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.crawl.exception.NoTicketException;
import com.foreveross.crawl.util.StringUtil;
import com.foreveross.taskservice.common.bean.TaskModel;
import com.google.common.collect.Maps;

/**
 * 东方航空官网http://www.ceair.com
 */

public class MUAirAdapter extends AbstractAdapter {

	private static final Double domeTax = new Double(170); // 国内税费， 国内一般都是固定的，如果抓取不到税费则直接设置进去 。
	protected static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	private static int busyRetryCount = 3;//遭遇系统繁忙时，重试次数
	private static final long waitingTime = 6000L;
	private static final int fetchTaxesTime = 3;

	public MUAirAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		switch (super.getRouteType()) {
			case INTERNATIONAL_ROUND:
				return paraseToVoDI(obj);// 国际往返
			case INTERNATIONAL_ONEWAY:
				return paraseToVoSI(obj);// 国际单程
			case DOMESTIC_ONEWAYTRIP:
				return paraseToVoS(obj);// 国内单程
		}
		return null;
	}

	/**
	 * 国际单程
	 */
	public List<Object> paraseToVoSI(Object fetchObject) throws Exception {
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		String o = (String) fetchObject;
		SinglePlaneInfoEntity sigPI = null;
//		o = o.substring(o.indexOf(" =") + 2);
//		o = o.substring(o.indexOf(" = ") + 3);
		o = o.substring(o.indexOf("{"));
//		if (!o.substring(0, 1).equals("{")) {
//			throw new FlightInfoNotFoundException("航班抓取失败！");
//		}
		JSONObject jsonObj = JSONObject.fromObject(o);
		JSONArray jsonArr = jsonObj.getJSONArray("tripItemList");
		String sessionId = jsonObj.getString("sessionId");
		for (int i = 0; i < jsonArr.size(); i++) {// 事实上只有一个(表示只有一条航线(出发城市-到达城市))
			JSONObject obj = jsonArr.getJSONObject(i);
			JSONArray airRoutingList = obj.getJSONArray("airRoutingList");
			for (int j = 0; j < airRoutingList.size(); j++) {// 航线(共有多少条航线)
				JSONObject airRouting = airRoutingList.getJSONObject(j);

				JSONArray productInfoList = airRouting.getJSONArray("productInfoList");// 每个航班的仓位
				JSONArray linesInfoList = airRouting.getJSONArray("flightList");// 每个航班信息（解析共有多少条航线数据）
				if (productInfoList.size() > 0 && linesInfoList.size() > 0) {
					sigPI = new SinglePlaneInfoEntity();
					ArrayList<TransitEntity> transitEntitys = new ArrayList<TransitEntity>();// 存储中转
					Set<CabinEntity> cabinEntitys = new HashSet<CabinEntity>();// 存储舱位

					// 解析舱位信息
					List<Double> heightPrices = new ArrayList<Double>();

					// 最低价
					Double totalLowestPrice = 0.0;
					// 最高价
					Double totalhigPrice = 0.0;

					JSONObject priceDisp = airRouting.getJSONObject("priceDisp");
					String lowerPrice = priceDisp.getString("lowest");// 最低价
					String highPrice = this.getHeightPrice(priceDisp);// 统计出来的最高价

					totalLowestPrice = Double.parseDouble(lowerPrice);
					totalhigPrice = Double.parseDouble(highPrice);

					for (int p = 0; p < productInfoList.size(); p++) {
						JSONObject productInfo = productInfoList.getJSONObject(p);
						Double taxesPrice = fetchTaxes(airRouting.getString("index"),productInfo.getString("index"),sessionId,"0");
						com.foreveross.crawl.domain.airfreight.CabinEntity c = new com.foreveross.crawl.domain.airfreight.CabinEntity();
						c.setSubCabinName(productInfo.getString("cabinCode"));
						c.setPrice(Double.parseDouble(productInfo.getString("price")));
						c.setTaxesPrice(taxesPrice);
						c.setOriginalPrice(c.getTaxesPrice()!=null?c.getTaxesPrice() + c.getPrice():null); // 总票价
						heightPrices.add(Double.valueOf(c.getPrice()));
						c.setCabinType(productInfo.getString("productNm"));
						cabinEntitys.add(c);
					}
					// 解析航班信息
					int falg = 0;

					for (int k = 0; k < linesInfoList.size(); k++) {//
						JSONObject flightInfos = linesInfoList.getJSONObject(k);
						falg++;
						/*
						 * {"acfamily":"325","airlineName":"东方航空", "arrCd":"PVG","arrCity":"SHA","arrCityNm":"上海", "arrDistance":"","arrNm":"浦东机场","arrTerm":"T1",
						 * "arrTime":"2014-01-28 21:20","asrType":true,"carrier":"MU", "codeShare":false,"deptCd":"PEK","deptCity":"BJS", "deptCityNm":"北京","deptDay":"星期二","deptDistance":"",
						 * "deptNm":"首都机场","deptTerm":"T2","deptTime":"2014-01-28 19:05", "duration":"2:15","flightNo":"MU5130","goBackFlag":"1","index":0,"lingyanFlag":false,
						 * "mealType":true,"mobileType":true,"segVia":"0","shareAirlineName":"","shareCarrier":""," shareFlightNo":"","stayTime":"","taxiType":false,"trainNo":""},
						 */
						String acfamily = flightInfos.getString("acfamily");// 机型 (325)
						String companyName = flightInfos.getString("airlineName");// 航空公司名 （东方航空）
						String arrCd = flightInfos.getString("arrCd");// PVG
						String arrCity = flightInfos.getString("arrCity");// SHA
						String arrCityNm = flightInfos.getString("arrCityNm");// 上海
						String arrNm = flightInfos.getString("arrNm");// 浦东机场
						String arrTerm = flightInfos.getString("arrTerm");// T1
						String arrTime = flightInfos.getString("arrTime");// 2014-01-28 21:20
						// String arrType=flightInfos.getString("asrType");//
						String carrier = flightInfos.getString("carrier");// MU
						// Boolean codeShare=flightInfos.getString("acfamily");//是否共享航班号
						String deptCode = flightInfos.getString("deptCd");// 出发机场三字码(PEK)
						String deptCity = flightInfos.getString("deptCity");// 到达城市三字码(BJS)
						String deptCityNm = flightInfos.getString("deptCityNm");// 北京
						String deptDay = flightInfos.getString("deptDay");// 星期二
						String deptNm = flightInfos.getString("deptNm");// 首都机场
						String deptTerm = flightInfos.getString("deptTerm");// T2
						String deptTime = flightInfos.getString("deptTime");// 2014-01-28 19:05
						String goBackFlag = flightInfos.getString("goBackFlag");// 是否回程，1表示去程，2表示回程
						String flightNo = flightInfos.getString("flightNo");// 航班号

						if (falg == 1) {// 表示第一趟（去程第一趟）
							sigPI.setFlightDate(deptTime.split(" ")[0]);
							// sigPI.setCarrierKey(carrier);
							// sigPI.setCarrierName(companyName);
							sigPI.setFlightNo(flightNo);
							sigPI.setFlightType(acfamily);
							sigPI.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", deptTime));
							// sigPI.setFromCity(deptCode);

						}

						if (linesInfoList.size() > 1) {// 表示有中转航线
							TransitEntity transitEntity = new TransitEntity();// 中转

							transitEntity.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", arrTime));
							transitEntity.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", deptTime));
							transitEntity.setFlightType(acfamily);
							transitEntity.setFlightNo(flightNo);
							transitEntity.setFlightType(acfamily);
							transitEntity.setToAirPortName(arrNm);
							transitEntity.setToAirPort(arrCd);
							transitEntity.setFromAirPort(deptCode);
							transitEntity.setFromAirPortName(deptNm);
							transitEntity.setCarrierFullName("东方航空公司");
							transitEntity.setCarrierKey("MU");
							transitEntity.setCarrierName("东航");


							transitEntitys.add(transitEntity);
						}

						if (falg == linesInfoList.size()) {// 表示最后一趟（去程）
							// sigPI.setToCity(arrCd);
							sigPI.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", arrTime));
						}

						// }

					}

					// 组装整条航线信息
					if (transitEntitys.size() > 0) {// 中转
						sigPI.setTransits(transitEntitys);
					}
					if (cabinEntitys.size() > 0) {// 舱位信息
						sigPI.setCabins(cabinEntitys);
					}
					sigPI.setTotalHighestPrice(totalhigPrice);
					sigPI.setTotalLowestPrice(totalLowestPrice);
					sigPI.setFromCityName(taskQueue.getFromCityName());
					sigPI.setFromCity(taskQueue.getFromCity());
					sigPI.setToCityName(taskQueue.getToCityName());
					sigPI.setToCity(taskQueue.getToCity());
					sigPI.setCreateTime(new Date());
					sigPI.setAreaName(taskQueue.getAreaName());
					sigPI.setAreaCode(taskQueue.getAreaCode());
					sigPI.setGrabChannelId(taskQueue.getGrabChannelId());
					sigPI.setGrabChannelName(taskQueue.getGrabChannel());
					sigPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
					sigPI.setCarrierFullName("东方航空公司");
					sigPI.setCarrierKey("MU");
					sigPI.setCarrierName("东航");

					dataList.add(sigPI);

				}

			}

		}

		PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		return Arrays.asList(dataList.toArray());
	}

	/**
	 * 国际往返
	 */
	public List<Object> paraseToVoDI(Object fetchObject) throws Exception {
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		DoublePlaneInfoEntity doublePI = null;
		String o = (String) fetchObject;
//		o = o.substring(o.indexOf(" =") + 2);
//		o = o.substring(o.indexOf(" = ") + 3);
		o = o.substring(o.indexOf("{"));
//		if (!o.substring(0, 1).equals("{")) {
//			throw new FlightInfoNotFoundException("航班抓取失败！");
//		}
		JSONObject jsonObj = JSONObject.fromObject(o);
		JSONArray jsonArr = jsonObj.getJSONArray("tripItemList");
		String sessionId = jsonObj.getString("sessionId");
		int noTicketCount = 0;// 统计航线出错问题
		for (int i = 0; i < jsonArr.size(); i++) {// 事实上只有一个，可以考虑不用for循环执行?
			JSONObject obj = jsonArr.getJSONObject(i);
			// airRoutingList用来判断共有多少条航线()
			JSONArray airRoutingList = obj.getJSONArray("airRoutingList");
			for (int j = 0; j < airRoutingList.size(); j++) {
				doublePI = new DoublePlaneInfoEntity();// 国际往返一条航线的大实体
				JSONObject airRouting = airRoutingList.getJSONObject(j);
				// 每个航班的仓位
				JSONArray productInfoList = airRouting.getJSONArray("productInfoList");
				// 航线段，比如：出发MU111 返回 MU112 则flightList.size()==2
				JSONArray flightList = airRouting.getJSONArray("flightList");
				if (productInfoList.size() > 0 && flightList.size() > 1) {
					// 各种价钱
					// 因为是往返的价格，所以买往返程票的时候，只要付一次钱就可以了(打包价)，所以舱位价格也只有一个(往返，中转舱位全部都一样)
					JSONObject priceDisp = airRouting.getJSONObject("priceDisp");

					/*
					 * String business = priceDisp.getString("business");//公务舱 String economy = priceDisp.getString("economy");//经济舱 String first = priceDisp.getString("first");//经济舱 String more =
					 * priceDisp.getString("more");//更多优惠
					 */

					String lowerPrice = priceDisp.getString("lowest");// 最低价
					String highPrice = this.getHeightPrice(priceDisp);// 统计出来的最高价

					Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabins = new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();// 存储舱位
					Set<ReturnCabinEntity> returnCabinSet=new HashSet<ReturnCabinEntity> ();
					Set<CabinRelationEntity>cabinRelationEntitys=new HashSet<CabinRelationEntity>();
					List<Double> heightPrices = new ArrayList<Double>();
					for (int k = 0; k < productInfoList.size(); k++) {
						JSONObject productInfo = productInfoList.getJSONObject(k);
						Double taxesPrice = fetchTaxes(airRouting.getString("index"),productInfo.getString("index"),sessionId,"0");
						ReturnCabinEntity recabinEn=new ReturnCabinEntity();
						com.foreveross.crawl.domain.airfreight.CabinEntity c = new com.foreveross.crawl.domain.airfreight.CabinEntity();
						c.setSubCabinName(productInfo.getString("cabinCode").split("\\/")[0]);//网页取回来的是这样的N/V，配合业务需求统一取第一个变成“N”

						// c.setLevel(2);
						c.setPrice(Double.parseDouble(productInfo.getString("price")));
						heightPrices.add(Double.valueOf(c.getPrice()));
						// cabins.add(c);
						// com.foreveross.crawl.domain.airfreight.CabinEntity c2 = new com.foreveross.crawl.domain.airfreight.CabinEntity();
						c.setCabinType(productInfo.getString("productNm"));
						// c2.setLevel(1);
						// c2.setPrice(Double.parseDouble(productInfo.getString("price")));
						// heightPrices.add(Double.valueOf(c2.getPrice()));
						recabinEn.setCabinType(c.getCabinType());
						recabinEn.setId(c.getId());
						CabinRelationEntity cabinRelationEntity=new CabinRelationEntity();
						cabinRelationEntity.setTaxesPrice(taxesPrice);//税费需要注册才能拿
						cabinRelationEntity.setCabinId(c.getId());
						cabinRelationEntity.setTotalFullPrice(taxesPrice == null ? null : Double.parseDouble(productInfo.getString("price"))+ taxesPrice);
						cabinRelationEntity.setFullPrice(Double.parseDouble(productInfo.getString("price")));
						cabinRelationEntity.setReturnCabinId(recabinEn.getId());
						cabinRelationEntitys.add(cabinRelationEntity);
						returnCabinSet.add(recabinEn);
						cabins.add(c);
					}

					ArrayList<TransitEntity> transit = new ArrayList<TransitEntity>();// 去程中转
					ArrayList<ReturnTransitEntity> transit_re = new ArrayList<ReturnTransitEntity>();// 回程中转
					Set<ReturnDoublePlaneInfoEntity> returnTri = new HashSet<ReturnDoublePlaneInfoEntity>();// 回程实体
					if (flightList.size() >= 2) {// 表示有往返航班

						ReturnDoublePlaneInfoEntity returnPI = new ReturnDoublePlaneInfoEntity();
						/**
						 * 中转又分三种情况： 1.（去——中，回——直）去的时候中转，回来直达 ：1，1，2 2.（去——中，回——中）去的时候中转，回来的时候也中转：1，1，2，2 3.（去——直，回——中）去的时候直达，回来的时候中转：1，2，2
						 */
						// 每条航线的详细信息
						// System.out.println("flightList.size()="+flightList.size());

						for (int k = 0; k < flightList.size(); k++) {

							JSONObject flight = flightList.getJSONObject(k);
							String flagTransit = flight.getString("goBackFlag");// 用来判断是否中转 去程1，回程2

							String carrierKey = null;
							String carrierName = null;
							String filghtId = null;
							String filghtTyle = null;
							Date formTime = null;
							Date toTime = null;
							String fromAir = null;
							String toAir = null;
							String fromCityNames = null;
							String toCityNames = null;
							String fromCity = null;
							String toCity = null;

							carrierKey = flight.getString("carrier");// MU
							carrierName = flight.getString("airlineName");// 东方航空
							filghtId = flight.getString("flightNo");// 航班号flightNo
							filghtTyle = flight.getString("acfamily");// 机型 acfamily
							formTime = this.getTimeDI(flight.getString("deptTime"));
							toTime = this.getTimeDI(flight.getString("arrTime"));
							fromAir = flight.getString("deptNm");
							toAir = flight.getString("arrNm");
							fromCityNames = flight.getString("deptCityNm");
							toCityNames = flight.getString("arrCityNm");
							fromCity = flight.getString("deptCd");
							toCity = flight.getString("arrCd");

							if (flagTransit.equals("1")) {// 去程信息
								if (k == 0) {// 表示去程第一趟
									doublePI.setFlightDate(taskQueue.getFlightDate());//DateUtil.formatDay(formTime, "yyyy-MM-dd")
									doublePI.setCarrierKey(carrierKey);
									doublePI.setCarrierName(carrierName);
									doublePI.setFlightNo(filghtId);
									doublePI.setFlightType(filghtTyle);
									doublePI.setStartTime(formTime);
									// doublePI.setFromCity(fromCity);

								}
								if (!(fromCityNames.indexOf(taskQueue.getFromCityName()) != -1 && toCityNames.indexOf(taskQueue.getToCityName()) != -1)) {// 表示中转
									TransitEntity transitEntity = new TransitEntity();
									transitEntity.setCarrierKey(carrierKey);
									transitEntity.setCarrierName(carrierName);
									transitEntity.setFlightNo(filghtId);
									transitEntity.setFlightType(filghtTyle);
									transitEntity.setStartTime(formTime);
									transitEntity.setEndTime(toTime);
									transitEntity.setFromAirPortName(fromAir);
									transitEntity.setToAirPortName(toAir);
									transitEntity.setFromAirPort(fromCity);
									transitEntity.setToAirPort(toCity);

									String stayTime = flight.getString("stayTime");
									if(stayTime != null && !stayTime.trim().equals("")){
										Date stayDate = new SimpleDateFormat("HH:mm").parse(stayTime);
										Long stay = (long) ((stayDate.getHours() * 60 + stayDate.getMinutes())*60*1000);
//										System.out.println(fromAir + " 航班号1: " + filghtId + "    " + stay);
										transitEntity.setStayTime(stay);
									}
									transit.add(transitEntity);
								}
								if (toCityNames.indexOf(taskQueue.getToCityName()) != -1) {// 去程最后一趟
									doublePI.setEndTime(toTime);
									// doublePI.setToCity(toCity);

								}
							}
							if (flagTransit.equals("2")) {// 表示回程信息
								if (fromCityNames.indexOf(taskQueue.getToCityName()) != -1) {// 表示回程第一趟
									returnPI.setFlightDate(taskQueue.getReturnGrabDate());//DateUtil.formatDay(formTime, "yyyy-MM-dd")
									returnPI.setCarrierKey(carrierKey);
									returnPI.setCarrierName(carrierName);
									returnPI.setFlightNo(filghtId);
									returnPI.setFlightType(filghtTyle);
									returnPI.setStartTime(formTime);
									returnPI.setTotalHighestPrice(Double.parseDouble(highPrice));
									returnPI.setTotalLowestPrice(Double.parseDouble(lowerPrice));

								}
								if (!(fromCityNames.indexOf(taskQueue.getToCityName()) != -1 && toCityNames.indexOf(taskQueue.getFromCityName()) != -1)) {// 表示中转
									ReturnTransitEntity reTransitE = new ReturnTransitEntity();// 回程中转
									reTransitE.setCarrierKey(carrierKey);
									reTransitE.setCarrierName(carrierName);
									reTransitE.setFlightNo(filghtId);
									reTransitE.setFlightType(filghtTyle);
									reTransitE.setStartTime(formTime);
									reTransitE.setEndTime(toTime);
									reTransitE.setFromAirPortName(fromAir);
									reTransitE.setToAirPortName(toAir);
									reTransitE.setFromAirPort(fromCity);
									reTransitE.setToAirPort(toCity);
									String stayTime = flight.getString("stayTime");
									if(stayTime != null && !stayTime.trim().equals("")){
										Date stayDate = new SimpleDateFormat("HH:mm").parse(stayTime);
										Long stay = (long) ((stayDate.getHours() * 60 + stayDate.getMinutes())*60*1000);
//										System.out.println(fromAir + " 航班号: " + filghtId + "    " + stay);
										reTransitE.setStayTime(stay);
									}
									transit_re.add(reTransitE);
								}
								if (toCityNames.indexOf(taskQueue.getFromCityName()) != -1 || (k + 1) == flightList.size()) {// 回程最后一趟
									returnPI.setEndTime(toTime);
								}
							}
						}
						// dataList.add(PlaneInfoEntityBuilder.createPlaneInfo(taskQueue, "MU", "东方航空","东方航空", tmpStartTime, tmpEndTime,tmpFilghtId,lowerPrice, null,highPrice,
						// tmpFilghtTyle,transit,cabins,returnTri));

						if(returnCabinSet.size()>0){
							returnPI.setReturnCabins(returnCabinSet);
						}
						returnPI.setFromCityName(taskQueue.getToCityName());
						returnPI.setToCityName(taskQueue.getFromCityName());
						returnPI.setCarrierName("东航");
						returnPI.setToCity(taskQueue.getFromCity());
						returnPI.setFromCity(taskQueue.getToCity());
						returnPI.setAreaCode(taskQueue.getAreaCode());
						returnPI.setAreaName(taskQueue.getAreaName());
						returnPI.setGrabChannelId(taskQueue.getGrabChannelId());
						returnPI.setGrabChannelName(taskQueue.getGrabChannel());
						returnPI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
						returnPI.setTotalHighestPrice(Double.parseDouble(highPrice));
						returnPI.setTotalLowestPrice(Double.parseDouble(lowerPrice));
						if (transit_re.size() > 0) {
							returnPI.setReturnTransits(transit_re);
						}
						returnTri.add(returnPI);
						doublePI.setFromCityName(taskQueue.getFromCityName());
						doublePI.setToCityName(taskQueue.getToCityName());
						doublePI.setToCity(taskQueue.getToCity());
						doublePI.setFromCity(taskQueue.getFromCity());
						doublePI.setCarrierName("东航");
						doublePI.setAreaCode(taskQueue.getAreaCode());
						doublePI.setAreaName(taskQueue.getAreaName());
						doublePI.setGrabChannelId(taskQueue.getGrabChannelId());
						doublePI.setGrabChannelName(taskQueue.getGrabChannel());
						doublePI.setAttachHbaseKey(taskQueue.getAttachHbaseKey());
						doublePI.setFlightReturnDate(taskQueue.getReturnGrabDate());
					}

					if (transit.size() > 0) {
						doublePI.setTransits(transit);// 去程中转
					}
					if(cabinRelationEntitys.size()>0){
						doublePI.setCabinRelations(cabinRelationEntitys);
					}

					doublePI.setReturnPlaneInfos(returnTri);// 回程信息
					doublePI.setCabins(cabins);// 舱位信息
					doublePI.setTotalHighestPrice(Double.parseDouble(highPrice));// 打包最高价
					doublePI.setTotalLowestPrice(Double.parseDouble(lowerPrice));// 打包最低价
					doublePI.setTotalLowestTaxesPrice(0.0);
					doublePI.setSumHighestPrice(Double.parseDouble(highPrice));
					doublePI.setSumLowestPrice(Double.parseDouble(lowerPrice));

					dataList.add(doublePI);

				} else {// 只有一个回程，或者只有一个返程航班（航班出错，因为往返程的航班不可能只出现某一段航程)
					// System.out.println("加1了。为什么呢？");
					noTicketCount++;
				}
			}

		}
//		if (noTicketCount > 0) {
//			// System.out.println("此航线中共有"+noTicketCount+"个航班没有票！");
//			throw new FlightInfoNotFoundException("此航线中共有" + noTicketCount + "个航班没有票！");
//		}
//		JSONArray jsonarray = JSONArray.fromObject(dataList); 
//		org.apache.commons.io.FileUtils.writeStringToFile(new File("c:/muTest.txt"),jsonarray.toString());
//		System.out.println(jsonarray);
		PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		return Arrays.asList(dataList.toArray());
	}


	private Double fetchTaxes(String routeIdx,String productIdx,String sessionId,String tripIdx) throws Exception{
		Double taxes = null;
		for(int i = 0;i < fetchTaxesTime;i++){
			try{
				Map<String,String> params = Maps.newHashMap();
				params.put("productIdx", productIdx);
				params.put("rand", String.valueOf(Math.random()));
				params.put("routeIdx", routeIdx);
				params.put("sessionId", sessionId);
				params.put("tripIdx", tripIdx);
				HttpGet g = getBaseGet("http://easternmiles.ceair.com/booking/flight-select!getSimpleTaxInfo.shtml", params);
				String page = excuteRequest(g);
				JSONObject j = JSONObject.fromObject(page);
				if(!StringUtil.isNullOrEmpty(j.getString("adtTax"))){
					taxes = j.getDouble("adtTax");
					if(taxes == 0){
						continue;
					}else{
						break;
					}
				}else{
					logger.info("第" + (i + 1) + "次抓取税费失败");
				}
			}catch(Exception e){
				logger.info("第" + (i + 1) + "次抓取税费失败");
				e.printStackTrace();
				continue;
			}
		}
		return taxes;
	}


	/**
	 * 国内单程
	 */
	public List<Object> paraseToVoS(Object fetchObject) throws Exception {
		// System.out.println(fetchObject);
		List<AbstractPlaneInfoEntity> dataList = new ArrayList<AbstractPlaneInfoEntity>();
		JSONObject jsonObj = (JSONObject) fetchObject;
		String sessionId = jsonObj.getString("sessionId");
		JSONArray jsonArr = jsonObj.getJSONArray("tripItemList");
		JSONObject cabinNames = jsonObj.getJSONObject("cabinNames");
		int hasFlightCount = 0;// 航班总数
		int noTicketCount = 0;// 统计没有票的情况的，如果完全等于航班总数就代表完全没有航班
		for (int i = 0; i < jsonArr.size(); i++) {
			JSONObject obj = jsonArr.getJSONObject(i);
			JSONArray airRoutingList = obj.getJSONArray("airRoutingList");
			hasFlightCount += airRoutingList.size();
			// 航班
			for (int j = 0; j < airRoutingList.size(); j++) {
				JSONObject airRouting = airRoutingList.getJSONObject(j);
				// 航班信息 flightList大于1说明是中转航班
				JSONArray flightList = airRouting.getJSONArray("flightList");
				// 每个航班的仓位
				JSONArray productInfoList = airRouting.getJSONArray("productInfoList");
				if (productInfoList.size() > 0) {
					// 各种价钱
					JSONObject priceDisp = airRouting.getJSONObject("priceDisp");
					// "priceDisp":{"business":"","economy":"1190","first":"3740","lowest":"1190","more":""}
					String lowerPrice = priceDisp.getString("lowest");
					String highPrice = this.getHeightPrice(priceDisp);
					//TODO
					//String page = fetchTaxes();
					//System.out.println(page);
					if (flightList.size() > 1) {
						// 中转
						// List<TransitchangeEntity> transit = new ArrayList<TransitchangeEntity>();//老模版
						ArrayList<TransitEntity> transit = new ArrayList<TransitEntity>();
						String tmpStartTime = null, tmpEndTime = null, tmpFilghtId = null, tmpFilghtTyle = null;
						for (int k = 0; k < flightList.size(); k++) {
							JSONObject flight = flightList.getJSONObject(k);
							// TransitchangeEntity entity = new TransitchangeEntity();//老模版
							TransitEntity entity = new TransitEntity();
							String carrierKey = flight.getString("carrier");
							String carrierName = flight.getString("airlineName");
							// 航班号 flightNo
							String filghtId = flight.getString("flightNo");
							// 机型 acfamily
							String filghtTyle = flight.getString("acfamily");
							// deptTime
							String formTime = this.getTime(flight.getString("deptTime"));
							// arrTime
							String toTime = this.getTime(flight.getString("arrTime"));
							if (k == 0) {
								// 得到第一趟航班起飞时间
								tmpStartTime = formTime;
								tmpFilghtId = filghtId;
								tmpFilghtTyle = filghtTyle;
							} else if (k == flightList.size() - 1)
								// 得到最后一趟航班的到达时间
								tmpEndTime = toTime;
							// deptNm
							String fromAir = flight.getString("deptNm");
							// arrNm
							String toAir = flight.getString("arrNm");
							// 老模版
							/*
							 * entity.setLowerPrice(Double.parseDouble(lowerPrice)); entity.setHightPrice(Double.parseDouble(highPrice)); entity.setFlightTyte(filghtTyle); entity.setFromAir(fromAir);
							 * entity.setToAir(toAir); entity.setStartTime(formTime); entity.setEndTime(toTime); entity.setFlightNo(filghtId); entity.setCarrierKey(carrierKey);
							 * entity.setCarrierName(carrierName); entity.setCarrierFullName(carrierName);
							 */

							// 新模版
							// entity.setLowerPrice(Double.parseDouble(lowerPrice));
							// entity.setHightPrice(Double.parseDouble(highPrice));
							entity.setFlightType(filghtTyle);
							entity.setFromAirPortName(fromAir);
							entity.setToAirPortName(toAir);
							entity.setStartTime(DateUtil.StringToDate("HH:mm", formTime));
							entity.setEndTime(DateUtil.StringToDate("HH:mm", toTime));
							entity.setFlightNo(filghtId);
							entity.setCarrierKey(carrierKey);
							entity.setCarrierName(carrierName);
							entity.setCarrierFullName(carrierName);
							transit.add(entity);
							// System.out.println(fromAir+"-->"+toAir);
						}

						Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabins = new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();
						for (int k = 0; k < productInfoList.size(); k++) {
							JSONObject productInfo = productInfoList.getJSONObject(k);
							//不取税费（有）
							//	Double taxesPrice = fetchTaxes(airRouting.getString("index"),productInfo.getString("index"),sessionId,"0");
							Double taxesPrice=0.0;
							com.foreveross.crawl.domain.airfreight.CabinEntity c = new com.foreveross.crawl.domain.airfreight.CabinEntity();
							c.setSubCabinName(productInfo.getString("cabinCode"));
							c.setPrice(Double.parseDouble(productInfo.getString("price")));
							c.setTaxesPrice(null); // 税费 。抓取不到手动设置 。
							c.setOriginalPrice(taxesPrice == null ? null : taxesPrice + c.getPrice()); // 总价
							c.setCabinType(cabinNames.getString(productInfo.getString("baseCabin")));
							c.setProductName(productInfo.getString("productNm"));
							cabins.add(c);
						}

						SinglePlaneInfoEntity singleEntity = (SinglePlaneInfoEntity) PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, "MU", "东方航空", "东方航空", tmpStartTime, tmpEndTime, tmpFilghtId, lowerPrice, null, highPrice, tmpFilghtTyle);
						singleEntity.setTransits(transit);
						;						singleEntity.getCabins().addAll(cabins);
						dataList.add(singleEntity);
					} else {
						JSONObject flight = flightList.getJSONObject(0);
						String flightId = null;
						String filgType = null;
						String arrTime = null;
						String deptTime = null;
						String carrierKey = flight.getString("carrier");
						String carrierName = flight.getString("airlineName");
						// flightNo 航班号
						flightId = flight.getString("flightNo");
						// acfamily 机型
						filgType = flight.getString("acfamily");
						// 起飞时间 deptTime
						deptTime = this.getTime(flight.getString("deptTime"));
						// 到达时间 arrTime
						arrTime = this.getTime(flight.getString("arrTime"));
						// 添加仓位
						// Set<CabinEntity> cabins = new HashSet<CabinEntity>();//老模版
						Set<com.foreveross.crawl.domain.airfreight.CabinEntity> cabins = new HashSet<com.foreveross.crawl.domain.airfreight.CabinEntity>();
						List<Double> heightPrices = new ArrayList<Double>();
						for (int k = 0; k < productInfoList.size(); k++) {
							JSONObject productInfo = productInfoList.getJSONObject(k);
							//去掉税费（因为要进入太多层了）
//							Double taxesPrice = fetchTaxes(airRouting.getString("index"),productInfo.getString("index"),sessionId,"0");
							Double taxesPrice=0.0;
							com.foreveross.crawl.domain.airfreight.CabinEntity c = new com.foreveross.crawl.domain.airfreight.CabinEntity();
							c.setSubCabinName(productInfo.getString("cabinCode"));
							c.setPrice(Double.parseDouble(productInfo.getString("price")));
							c.setTaxesPrice(null); // 税费 。抓取不到手动设置 。
							c.setOriginalPrice(taxesPrice == null ? null : taxesPrice + c.getPrice()); // 总价
							c.setCabinType(cabinNames.getString(productInfo.getString("baseCabin")));
							c.setProductName(productInfo.getString("productNm"));
							heightPrices.add(Double.valueOf(c.getPrice()));
							cabins.add(c);
						}
						Double[] tmp = heightPrices.toArray(new Double[0]);
						super.sort(tmp, 0, tmp.length - 1);
						// //新模版
						SinglePlaneInfoEntity singleEntity;
						singleEntity = (SinglePlaneInfoEntity) PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, "MU", "东方航空", "东方航空", deptTime, arrTime, flightId, lowerPrice, null, tmp[tmp.length - 1].toString(), filgType);
						singleEntity.getCabins().addAll(cabins);
						dataList.add(singleEntity);
					}
				} else {// 有航班，但是卖光了
					noTicketCount++;
				}
			}
		}
		if (noTicketCount == hasFlightCount) {
			throw new NoTicketException("票木有了！");
		}

		PlaneInfoEntityBuilder.buildLimitPrice(dataList);
		return Arrays.asList(dataList.toArray());
	}

	private String getHeightPrice(JSONObject priceDisp) {
		if (!"".equals(priceDisp.getString("first"))) return priceDisp.getString("first");
		else if (!"".equals(priceDisp.getString("business"))) return priceDisp.getString("business");
		else if (!"".equals(priceDisp.getString("economy"))) return priceDisp.getString("economy");
		else return priceDisp.getString("lowest");
	}

	private String getTime(String datetime) {
		return datetime.split(" ")[1];
	}

	/**
	 * 国际单程URL解析
	 */
	public String getUrlSI() throws Exception {
		//http://easternmiles.ceair.com/flight2014/pek-cdg-140711-cdg-pek-140712_CNY.html
		StringBuilder builder = new StringBuilder();
//		StringBuilder url = new StringBuilder("http://easternmiles.ceair.com/flight2014/");
//		url.append(taskQueue.getFromCity().toLowerCase()+"-"+taskQueue.getToCity().toLowerCase()+"-"+taskQueue.getFlightDate().replaceAll("-", "").substring(2));
//		url.append("_CNY.html");
		StringBuilder url = new StringBuilder("http://easternmiles.ceair.com/booking/flight-search!doFlightSearch.shtml?rand=");
		url.append(Math.random()).append("&searchCond=");

		String fromCityName = taskQueue.getFromCityName();
		String fromCity = taskQueue.getFromCity();
		// String fromCitys=taskQueue.getFromCity();
		String toCityName = taskQueue.getToCityName();
		String toCity = taskQueue.getToCity();
		// String toCitys=taskQueue.getToCity();
		String depDate = taskQueue.getFlightDate();// 出发日期
		String fromCountryCode = "CN";// 出发国家二字代码
		String toCountryCode = "AM";// 到达国家二字代码

		builder.append("{\"segmentList\":[{\"deptCdTxt\":\"").append(fromCityName).append("\",\"deptCd\":\"").append(fromCity).append("#\"").append(",\"deptNation\":\"").append(fromCountryCode).append("\",\"deptRegion\":\"").append(fromCountryCode)
				.append("\",\"deptCityCode\":\"").append(fromCity).append("\",\"arrCd\":\"").append(toCity).append("#\",\"arrCdTxt\":\"").append(toCityName).append("\",\"arrNation\":\"").append(toCountryCode).append("\",\"arrRegion\":\"")
				.append(toCountryCode).append("\",\"arrCityCode\":\"").append(toCity).append("\",\"deptDt\":\"").append(depDate).append("\"}],\"").append("tripType\":\"OW\",\"")
				.append("adtCount\":\"1\",\"chdCount\":\"0\",\"infCount\":\"0\",\"currency\":\"CNY\",\"sortType\":\"t\"").append("}");

		String s = "";
		try {
			s = URLEncoder.encode(builder.toString(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url.append(s);
		// System.out.println(url);
		return url.toString();
	}

	/**
	 * 国际往返URL解析
	 */
	public String getUrlDI() throws Exception {
		// 组装post信息
		//	http://easternmiles.ceair.com/flight2014/pek-cdg-140711-cdg-pek-140715_CNY.html
//		StringBuilder url = new StringBuilder("http://easternmiles.ceair.com/flight2014/");
//		StringBuilder builder = new StringBuilder();
//		builder.append(taskQueue.getFromCity().toLowerCase()+"-"+taskQueue.getToCity().toLowerCase()+"-"+taskQueue.getFlightDate().replaceAll("-", "").substring(2));
//		builder.append("-");
//		builder.append(taskQueue.getToCity().toLowerCase()+"-"+taskQueue.getFromCity().toLowerCase()+"-"+taskQueue.getFlightDate().replaceAll("-", "").substring(2));
//		builder.append("_CNY.html");
		StringBuilder builder = new StringBuilder();
		StringBuilder url = new StringBuilder("http://easternmiles.ceair.com/booking/flight-search!doFlightSearch.shtml?rand=");
		url.append(Math.random()).append("&searchCond=");
		String fromCityName = taskQueue.getFromCityName();
		String fromCity = taskQueue.getFromCity() + "#";
//		 String fromCitys=taskQueue.getFromCity();
		String toCityName = taskQueue.getToCityName();
		String toCity = taskQueue.getToCity() + "#";
//		 String toCitys=taskQueue.getToCity();
		String depDate = taskQueue.getFlightDate();// 出发日期
		String reDate = taskQueue.getReturnGrabDate();// 返程日期
		String fromCountryCode = "CN";// 出发国家二字代码
		String toCountryCode = "JP";// 到达国家二字代码
		if (fromCityName.equals("北京(PEK)")) {
			fromCityName = "北京";
		}
		if (toCityName.equals("北京(PEK)")) {
			toCityName = "北京";
		}
		
		/* if(fromCitys.indexOf("PEK")!=-1){ fromCitys="BJS"; } if(toCitys.indexOf("PEK")!=-1){ toCitys="BJS"; }*/


		builder.append("{\"segmentList\":[{\"deptCdTxt\":\"").append(fromCityName).append("\",\"deptCd\":\"").append(fromCity).append("#\"").append(",\"deptNation\":\"").append(fromCountryCode).append("\",\"deptRegion\":\"").append(fromCountryCode)
				.append("\",\"deptCityCode\":\"").append(fromCity).append("\",\"arrCd\":\"").append(toCity).append("#\",\"arrCdTxt\":\"").append(toCityName).append("\",\"arrNation\":\"").append(toCountryCode).append("\",\"arrRegion\":\"")
				.append(toCountryCode).append("\",\"arrCityCode\":\"").append(toCity).append("\",\"deptDt\":\"").append(depDate).append("\"},");

		builder.append("{\"deptCdTxt\":\"").append(toCityName).append("\",\"deptCd\":\"").append(toCity).append("#\",\"deptNation\":\"").append(toCountryCode).append("\",\"deptRegion\":\"").append("").append("\",\"deptCityCode\":\"").append(toCity)
				.append("\",\"arrCd\":\"").append(fromCity).append("#\",\"arrCdTxt\":\"").append(fromCityName).append("\",\"arrNation\":\"").append(fromCountryCode).append("\",\"arrRegion\":\"").append(fromCountryCode)
				.append("\",\"arrCityCode\":\"").append(fromCity).append("\",\"deptDt\":\"").append(reDate).append("\"}],\"").append("tripType\":\"RT\",\"").append("adtCount\":1,\"chdCount\":0,\"currency\":\"CNY\",\"sortType\":\"t\"").append("}");
		String s = "";
		try {
			s = URLEncoder.encode(builder.toString(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url.append(s);
//		url.append(builder.toString());
		// System.out.println(url);
		return url.toString();

	}

	// 单程
	public String getUrl() {
		StringBuilder builder = new StringBuilder();
		StringBuffer url = new StringBuffer("http://easternmiles.ceair.com/booking/flight-search!doFlightSearch.shtml?rand=");
		url.append(Math.random()).append("&searchCond=");
		builder.append("{\"tripType\":\"OW\",\"adtCount\":\"1\",\"chdCount\":\"0\",\"currency\":\"CNY\",\"sortType\":\"t\",\"segmentList\":" + "[{\"deptCdTxt\":\"").append(taskQueue.getFromCityName()).append("\",\"deptCd\":\"")
				.append(taskQueue.getFromCity()).append("#\"").append(",\"deptNation\":\"CN\",\"deptRegion\":\"CN\",\"deptCityCode\":\"");
		if (taskQueue.getFromCity().indexOf("PEK") != -1) builder.append("BJS");
		else builder.append(taskQueue.getFromCity());
		builder.append("\",\"arrCd\":\"" + taskQueue.getToCity() + "#\"," + "\"arrCdTxt\":\"" + taskQueue.getToCityName() + "\",\"arrNation\":\"CN\",\"arrRegion\":\"CN\",\"arrCityCode\":\"");
		if (taskQueue.getToCity().indexOf("PEK") != -1) builder.append("BJS");
		else builder.append(taskQueue.getToCity());
		builder.append("\",\"deptDt\":\"").append(taskQueue.getFlightDate()).append("\"}]}");
		String s = "";
		try {
			s = URLEncoder.encode(builder.toString(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		url.append(s);
		// System.out.println(url);
		return url.toString();
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
			case INTERNATIONAL_ROUND:
				return fetchDI();// 国际往返
			case INTERNATIONAL_ONEWAY:
				return fetchSI();// 国际单程
			case DOMESTIC_ONEWAYTRIP:
				// return fetchS();//国内单程（老适配器，不太确定是不是国内单程？？？）
				return fetchDomeOneWay();
			case DOMESTIC_ROUNDTRIP:
				throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}
		return null;
	}

	private Object fetchDomeOneWay() throws Exception {
		HttpClient h;
		HttpGet g;
		HttpPost post;
		String jsonStr;
		JSONObject json =new JSONObject();
		HashMap<String, String> p = new HashMap<String, String>();
		try {
			h = super.getHttpClient();
			g = new HttpGet(getgetDomeOneWayPageUrl());
			g.addHeader("Host", "www.ceair.com");
			g.addHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			super.excuteRequest(h, g, false);
			p.put("searchCond", getDomeOneWayUrlJsonTemplate());
			post = super.getBasePost("http://www.ceair.com/booking/flight-search!doFlightSearch.shtml?", p);
			post.addHeader("Host", "www.ceair.com");
			post.addHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			post.addHeader("Referer", getgetDomeOneWayPageUrl());
			jsonStr = new String(super.excuteRequest(h, post, true).getBytes("ISO-8859-1"), "UTF-8");
			if (jsonStr.matches(".*您选择的航线没有航班计划.*")) {
				throw new FlightInfoNotFoundException();
			}
			json = JSONObject.fromObject(RegHtmlUtil.regStr(jsonStr, ".*?(\\{.*)", 1));
			if (json.isEmpty() || json.isNullObject()) {
				throw new Exception("json is null Object");
			}
		} finally {
			jsonStr = null;
			post = null;
			g = null;
			p = null;
			h = null;
		}

		super.appendPageContents(json.toString());
		super.setLenghtCount(super.getSourceWebPageContents().length());
		return json;
	}

	private String getgetDomeOneWayPageUrl() throws Exception {
		//		http: // easternmiles.ceair.com/flight/syx-nay-140123_CNY.html
		//http://www.ceair.com/flight2014/syx-nay-150302_CNY.html
		return "http://www.ceair.com/flight2014/" + taskQueue.getFromCity().toLowerCase() + "-" + taskQueue.getToCity().toLowerCase() + "-" + taskQueue.getFlightDate().substring(2, 4) + taskQueue.getFlightDate().substring(5).replaceAll("-", "")
				+ "_CNY.html";

	}

	private String getDomeOneWayUrlJsonTemplate() throws Exception {
		JSONObject json = new JSONObject(false);
		json.put("segmentList", new JSONArray());
		JSONObject temp = new JSONObject(false);
		temp.put("deptCdTxt", taskQueue.getFromCityName());
		temp.put("deptCd", taskQueue.getFromCity() + "#");
		temp.put("deptCityCode", getCityCode(taskQueue.getFromCity()));
		temp.put("arrCdTxt", taskQueue.getToCityName());
		temp.put("arrCd", taskQueue.getToCity() + "#");
		temp.put("arrCityCode", getCityCode(taskQueue.getToCity()));
		temp.put("deptDt", taskQueue.getFlightDate());
		temp.put("deptNation", "CN");
		temp.put("deptRegion", "CN");
		temp.put("arrNation", "CN");
		temp.put("arrRegion", "CN");
		json.getJSONArray("segmentList").add(temp);
		json.put("tripType", "OW");// 单程
		json.put("adtCount", 1);
		json.put("chdCount", 0);
		json.put("infCount", 0);
		json.put("currency", "CNY");
		json.put("sortType", "t");
		return json.toString();
	}

	private String getCityCode(String airCode) {
		if ("PEK".equals(airCode)) {
			return "BJS";
		} else if ("PVG".equals(airCode)) {
			return "SHA";
		}
		return airCode;
	}

	/**
	 * 国际往返
	 */
	public Object fetchDI() throws Exception {
		String result = null;
		HttpClient client = null;
		HttpGet httpGet = null;
		HttpEntity entity = null;

		try {
			for(int i = 0; i < busyRetryCount;i++){
				try{
					client = super.getHttpClient();
					httpGet = new HttpGet(getUrlDI());
					httpGet.setHeader("Host", "www.ceair.com");
					httpGet.setHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0");
					result = excuteRequest(client, httpGet, true);
					result = new String(((String) result).getBytes("ISO-8859-1"), "utf-8");
					super.appendPageContents(result);
					int lenght = super.getSourceWebPageContents().toString().getBytes().length;// 设置流量统计
					setLenghtCount(lenght);
					validateFetch(result);
					break;
				}catch(SystemBusyException se){
					logger.info("SystemBusyException");
					Thread.sleep(waitingTime);
					switchProxyipByHttClient();
					continue;
				}catch (FlightInfoNotFoundException e) {
					logger.info("FlightInfoNotFoundException");
					Thread.sleep(waitingTime);
					switchProxyipByHttClient();
					continue;
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpGet != null) httpGet.releaseConnection();
			if (entity != null) EntityUtils.consume(entity);
			entity = null;
			client = null;
			httpGet = null;
		}
		return null;
	}

	/**
	 * 国际单程
	 */
	public Object fetchSI() throws Exception {
		String result = null;
		HttpClient client = null;
		HttpGet httpGet = null;
		HttpResponse response = null;
		HttpEntity entity = null;

		try {
			client = super.getHttpClient();
			httpGet = new HttpGet(getUrlSI());
			httpGet.setHeader("Host", "easternmiles.ceair.com");
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; rv:26.0; WOW64; Trident/5.0");
			result = excuteRequest(client, httpGet, true);
			result = new String(((String) result).getBytes("ISO-8859-1"), "utf-8");
			super.appendPageContents(result);
			int lenght = super.getSourceWebPageContents().toString().getBytes().length;// 设置流量统计
			setLenghtCount(lenght);
			return result;

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (httpGet != null) httpGet.releaseConnection();
			if (entity != null) EntityUtils.consume(entity);
			entity = null;
			client = null;
			httpGet = null;
		}
		/*HttpClient httpClient = null;
		HttpPost httpPost = null;
		HttpResponse httpResponse = null;
		HttpEntity entity = null;
		InputStream gis = null;
		ByteArrayOutputStream os = null;
		InputStream is = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder parser = null;
		Document d = null;
		net.htmlparser.jericho.Source source = null;
		
		try{
		httpClient = this.getHttpClient();
		httpPost = new HttpPost(getUrlSI());
		
//		httpPost.setHeader("x-requested-with", "XMLHttpRequest");
//		httpPost.setHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
//		httpPost.setHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; rv:26.0; WOW64; Trident/5.0");
//		httpPost.setHeader("Host", "adserver.superairmedia.com");
		httpResponse = httpClient.execute(httpPost);
//		 entity = httpResponse.getEntity();
//		 String html = EntityUtils.toString(entity,"GBK");
//		 httpPost = new HttpPost("http://easternmiles.ceair.com/flight2014/pek-hnd-140711_CNY.html");
//		 httpResponse = httpClient.execute(httpPost);
		EntityUtils.consume(httpResponse.getEntity());
		httpPost = new HttpPost("http://easternmiles.ceair.com/flight2014/pek-hnd-140711_CNY.html");
		httpResponse = httpClient.execute(httpPost);
		entity = httpResponse.getEntity();
		
		
		result = EntityUtils.toString(entity);
		result = new String(((String) result).getBytes("ISO-8859-1"), "utf-8");
		super.setSourceWebPageContents(result);
		int lenght = super.getSourceWebPageContents().toString().getBytes().length;// 设置流量统计
		setLenghtCount(taskQueue, lenght);
		return super.getSourceWebPageContents();
		
		}catch (Exception e) {
			e.printStackTrace();
		}*/
		return null;
	}

	// 单程
	public Object fetchS() throws Exception {
		String result = null;
		HttpClient client = null;
		HttpGet httpGet = null;
		HttpResponse response = null;
		HttpEntity entity = null;
		try {

			client = super.getHttpClient();
			httpGet = new HttpGet(getUrl());
			httpGet.setHeader("Host", "www.ceair.com");
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0");
			result = excuteRequest(client, httpGet, true);
			result = new String(((String) result).getBytes("ISO-8859-1"), "utf-8");
			super.appendPageContents(result);
			int lenght = super.getSourceWebPageContents().toString().getBytes().length;// 设置流量统计
			setLenghtCount(lenght);
			return result;
		} finally {
			if (httpGet != null) httpGet.releaseConnection();
			if (entity != null) EntityUtils.consume(entity);
			entity = null;
			client = null;
			httpGet = null;
		}
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		if (fetchObject == null) {
			return false;
		} else if (super.getRouteType() == RouteTypeEnum.DOMESTIC_ONEWAYTRIP) {
			return true;
		} else if (fetchObject instanceof String) {

			// if(((String)fetchObject).length() < 5000){
			// throw new PageErrorResultException("抓取的数据不正确(长度少于5000)");
			// }

			/*if (fetchObject.toString().matches(".*您选择的航线没有航班计划.*")) {
				throw new FlightInfoNotFoundException("没有该航班计划！");
			}*/
			if(!fetchObject.toString().matches(".*airRoutingList.*")){//2014-07-18 通过是否能找到航线判断是否有航班计划
				if(fetchObject.toString().matches(".*SYS11206.*")){//2014-08-04 系统繁忙
					throw new SystemBusyException("系统繁忙异常");
				}
				throw new FlightInfoNotFoundException("没有该航班计划！");
			}

			// 验证字符串里是否包含抓取的错误信息
			String result = RegHtmlUtil.regStr((String) fetchObject, this.getErrors());
			if (result != null) {
				throw new PageErrorResultException(result);
			}
		} else if (StringUtils.isNotBlank(getSourceWebPageContents())) {
			// 验证字符串里是否包含抓取的错误信息
			String result = RegHtmlUtil.regStr(getSourceWebPageContents(), this.getErrors());
			if (result != null) {
				throw new PageErrorResultException(result);
			}
		}
		return true;
	}

	private List<String> getErrors() {
		List<String> errors = new ArrayList<String>();
		errors.add("404 Not Found");
		errors.add("Error 404: Not Found");
		errors.add("Access Denied");
		errors.add("HTTP .+ 404");
		errors.add("<title>Error</title>");
		errors.add("<li class=\"active\">\\s*航班查询\\s*</li>");// 返回的是航班查询页面
		return errors;
	}

	/**
	 * 国际往返 时间处理,2013-11-25 20:55 变成 2013-11-25
	 */
	private Date getTimeDI(String dateStr) throws Exception {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date queryDates = sdf.parse(dateStr);

		return queryDates;

	}
}
