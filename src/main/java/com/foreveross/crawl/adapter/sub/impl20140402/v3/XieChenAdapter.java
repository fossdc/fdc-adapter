package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.xiechen.XieChenDomesticOneWayTrip;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.xiechen.XieChenInterRoundTrip;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.taskservice.common.bean.TaskModel;
/**
 * 携程适配器
 * @author xiangsf 2014-01-05
 *
 */
public class XieChenAdapter extends AbstractAdapter{
	
	public final static long CHANNEL_ID = 12;

	public XieChenAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}
	/**
	 * 抓取数据入口
	 */
	@Override
	public Object fetch(String url) throws Exception {
		switch(getRouteType()){
			case DOMESTIC_ONEWAYTRIP:
				return new XieChenDomesticOneWayTrip(this, taskQueue).fetchData();
			case DOMESTIC_ROUNDTRIP:
				throw new UnableParseRouteTypeException(taskQueue,getRouteType().getName());
			case INTERNATIONAL_ONEWAY:
				throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
			case INTERNATIONAL_ROUND:
				return new XieChenInterRoundTrip(this, taskQueue).fetchData();
		}
		return null;
	}
	

	@Override
	public boolean validateFetch(Object obj) throws Exception {
		if(obj==null){
			throw new Exception("携程数据为空");
		}
		return true;
	}
	
	

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		return (List<Object>)obj;
	}
	

	@Override
	public String getUrl() throws Exception {
		return null;
	}


}


