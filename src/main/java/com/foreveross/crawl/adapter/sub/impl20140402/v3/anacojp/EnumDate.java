package com.foreveross.crawl.adapter.sub.impl20140402.v3.anacojp;

public enum EnumDate {

	TO_START_TIME,//去程起飞时间
	TO_END_TIME,//去程到达时间
	BACK_START_TIME,//回程起飞时间
	BACK_END_TIME//回程到达时间
}
