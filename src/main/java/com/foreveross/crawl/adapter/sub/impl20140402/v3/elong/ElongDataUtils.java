package com.foreveross.crawl.adapter.sub.impl20140402.v3.elong;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.RegHtmlUtil;

public final class ElongDataUtils {
	/**
	 * 获得指定日期距离今天多少天
	 * @param date yyyy-MM-dd
	 * @return 天
	 */
	public static int betweenDay(String date){
		Calendar cal = Calendar.getInstance();
		long time = DateUtil.StringToDate("yyyy-MM-dd", date).getTime()-cal.getTimeInMillis();
		return ((int) (time/24/60/60/1000))+1;
	}
	/**
	 * 将中文的日期字符串转成date对象
	 * @param date 8月23日
	 * @param time 11:24
	 * @return
	 */
	public static Date formatCNDate(String date, String time){
		Calendar cal = Calendar.getInstance();
		date = cal.get(Calendar.YEAR)+"-"+date.replace("月", "-").replace("日", "");
		Date d = DateUtil.StringToDate("yyyy-MM-dd HH:mm", date.trim()+" "+time.trim());
		return d.before(cal.getTime()) ? DateUtils.addYears(d, 1) : d; //少于今年，则加一年
	}
	/**
	 * 将日期字符串转成date对象
	 * @param date "2014-09-03 13:50"
	 * @param time 11:24
	 * @return
	 */
	public static Date formatDorADate(String datetime){
		return  DateUtil.StringToDate("yyyy-MM-dd HH:mm", datetime);
	}
	/**
	 * 格式化停留时间
	 * @param interval 13小时35分钟 
	 * @return
	 */
	public static long formatTimeInterval(String interval){
		String h = RegHtmlUtil.regStr(interval, "(\\d{1,2})小时\\d{1,2}分钟");
		String m = RegHtmlUtil.regStr(interval, "\\d{1,2}小时(\\d{1,2})分钟");
		return Integer.parseInt(h)*60*60*1000+Integer.parseInt(m)*60*1000;
	}
	public static void main(String[] arg){
		System.out.println(formatTimeInterval("13小时35分钟   "));
	}
}
