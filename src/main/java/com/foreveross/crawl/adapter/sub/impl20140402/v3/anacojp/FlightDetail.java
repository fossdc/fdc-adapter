package com.foreveross.crawl.adapter.sub.impl20140402.v3.anacojp;

import java.io.Serializable;

/**
 * 航班详情
 * @author luofangyi
 *
 */
public class FlightDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9124127492953515609L;
	
	private String rideDate;//搭乘日期
	private String flightNo;//航班号
	private String fromAirPortName;//起飞城市
	private String toAirPortName;//到达城市
	private String takeOffTime;//起飞时间
	private String reachTime;//到达时间
	private String flightType;//机型
	private String subCabinName;//子舱
	private String fightTime;//飞行时间
	private String carrierFullName;//运营商全名
	
	public String getRideDate() {
		return rideDate;
	}
	public void setRideDate(String rideDate) {
		this.rideDate = rideDate;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	
	public String getTakeOffTime() {
		return takeOffTime;
	}
	public void setTakeOffTime(String takeOffTime) {
		this.takeOffTime = takeOffTime;
	}
	public String getReachTime() {
		return reachTime;
	}
	public void setReachTime(String reachTime) {
		this.reachTime = reachTime;
	}
	public String getFlightType() {
		return flightType;
	}
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}
	public String getSubCabinName() {
		return subCabinName;
	}
	public void setSubCabinName(String subCabinName) {
		this.subCabinName = subCabinName;
	}
	public String getFightTime() {
		return fightTime;
	}
	public void setFightTime(String fightTime) {
		this.fightTime = fightTime;
	}
	@Override
	public String toString() {
		return "FlightDetail [rideDate=" + rideDate + ", flightNo=" + flightNo
				+ ", fromAirPort=" + fromAirPortName + ", toAirPort=" + toAirPortName
				+ ", takeOffTime=" + takeOffTime + ", reachTime=" + reachTime
				+ ", flightType=" + flightType + ", subCabinName="
				+ subCabinName + ", fightTime=" + fightTime
				+ ", carrierFullName=" + carrierFullName + "]";
	}
	public String getCarrierFullName() {
		return carrierFullName;
	}
	public void setCarrierFullName(String carrierFullName) {
		this.carrierFullName = carrierFullName;
	}
	public String getFromAirPortName() {
		return fromAirPortName;
	}
	public void setFromAirPort(String fromAirPortName) {
		this.fromAirPortName = fromAirPortName;
	}
	public String getToAirPortName() {
		return toAirPortName;
	}
	public void setToAirPortName(String toAirPortName) {
		this.toAirPortName = toAirPortName;
	}
}
