package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.asiana.AsianaInterAdapter;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 韩亚航空适配器。
 * 
 * @author luomingliang@foreveross.com
 * @version 1.0.0
 * @date 2014-07-18
 */
@SuppressWarnings({ "deprecation" })
public class AsianaAdapter extends AbstractAdapter {

	/** ******************************** Override *********************************************** */

	public AsianaAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	@Override
	public String getUrl() throws Exception {
		return null;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ONEWAY:
		case INTERNATIONAL_ROUND:
			return new AsianaInterAdapter(taskQueue).fetchRound();
		case DOMESTIC_ONEWAYTRIP:
		case DOMESTIC_ROUNDTRIP:
		default:// 暂时没有实现， 抛出异常。
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		return (List<Object>) obj;
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return true;
	}

}
