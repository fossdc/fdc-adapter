package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.tianxun.TianxunInterAdapter;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 天巡适配器。<br/>
 * 
 * 请求的数据格式为Json。
 * 
 * @author luomingliang@foreveross.com
 * @version 1.0.0
 * @date 2014-07-08
 */
public class TianxunAdapter extends AbstractAdapter {

	private TianxunInterAdapter adapter = null;

	/** ******************************** Constractors *********************************************** */

	public TianxunAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	private TianxunInterAdapter getAdapter() {
		if (adapter == null) adapter = new TianxunInterAdapter(taskQueue);
		return adapter;
	}

	/** ******************************** Override *********************************************** */

	@Override
	public String getUrl() throws Exception {
		return null;
	}

	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ONEWAY:
		case INTERNATIONAL_ROUND:
			return getAdapter().fetchInterRound();
		default:
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ONEWAY:
		case INTERNATIONAL_ROUND:
			return (List<Object>) getAdapter().parseInterRound(obj);
		default:
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return true;
	}
}