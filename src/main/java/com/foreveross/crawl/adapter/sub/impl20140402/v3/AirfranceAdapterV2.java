package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.airfrance.AirfranceRoundV2;
import com.foreveross.crawl.common.cfg.ConfigContainer;
import com.foreveross.crawl.common.exception.self.UnableParseRouteTypeException;
import com.foreveross.proxyip.ProxyipProperties;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 法国航空, 版本2.0
 * 网站内容在刚做好后就己改版，此版本为改版为的抓取改动
 * 
 * <ul>
 * <li>
 * 往返与单程入口：<a>http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/home/home/HomePageAction.do.
 * do</a></li>
 * </ul>
 * 
 * @author fb
 */
@SuppressWarnings("deprecation")
public class AirfranceAdapterV2 extends AbstractAdapter {

	public final static Logger logger = LoggerFactory.getLogger(AirfranceAdapterV2.class);

	private AirfranceRoundV2 airfranceRound;

	public AirfranceAdapterV2(TaskModel taskQueue) {
		super(taskQueue);
	}

	@Override
	public Object fetch(String arg0) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			airfranceRound = new AirfranceRoundV2(this);
			return airfranceRound.fetch();
		case INTERNATIONAL_ONEWAY:
			break;
		default:
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}

		return null;
	}

	@Override
	@Deprecated
	public String getUrl() throws Exception {
		return "http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/home/home/HomePageAction.do";
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object> paraseToVo(Object fetchObject) throws Exception {
		List result = null;

		switch (getRouteType()) {
		case INTERNATIONAL_ROUND:
			result = (List<Object>) fetchObject;
		case DOMESTIC_ONEWAYTRIP:
			break;
		case INTERNATIONAL_ONEWAY:
			break;
		default:
			return null;
		}

		return result;
	}

	@Override
	public boolean validateFetch(Object content) throws Exception {

		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return true;
		case INTERNATIONAL_ONEWAY:
			break;
		default:
			throw new UnableParseRouteTypeException(taskQueue, getRouteType().getName());
		}

		return true;
	}
	
	public static void main(String[] args) throws Exception {
		ConfigContainer.getInstance().register(ProxyipProperties.class);
		TaskModel tm = new TaskModel();
		tm.setFromCity("PVG"); 
		tm.setToCity("CDG");
		tm.setFlightDate("2014-10-31");
		tm.setReturnGrabDate("2015-01-15");
		tm.setIsInternational(1);
		tm.setIsReturn(1);
		
		Object content = null;
		AirfranceAdapterV2 afa = new AirfranceAdapterV2(tm);
		long start = System.currentTimeMillis();
		content = afa.fetch(null);
		long end = System.currentTimeMillis();
		afa.printJson((List<?>) content, "d:/test/json.txt");
		System.out.println(content);
		long time = end - start;
		System.out.println("毫秒：" + time + "\r 秒：" + time/1000 + "\r 分钟:" + time/1000/60);
	}
}
