package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpEntity;
import org.dayatang.excel.ExcelException;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.exception.NoTicketException;
import com.foreveross.taskservice.common.bean.TaskModel;
import com.google.common.collect.Lists;

public class ItaAdapter extends AbstractAdapter{

	private String session=null;
	
	private String solutionSet=null;
	
	private String solutionCount=null;
	
	private List<String> idLists;
	private List<String> resultList=Lists.newArrayList();
	
	private boolean flag=true;//是否考虑直飞
	
	
	//for linux
	
	private static final String homePathDir=new File("\\").getAbsolutePath()+"opt"+File.separator+"ITA\\round_trip_home";
	private static final String allDataPathDir="\\opt\\ITA\\allDetails";
	private static final String theDetailDataPath="\\opt\\ITA\\detail_"+new Random().nextInt(10000) + 1;
	
	//存放抓取文件 for windows
	//先创建目录，再生成文件
	//目录名
//	private static final String homePathDir="D:\\opt\\ITA\\round_trip_home";
//	private static final String allDataPathDir="D:\\opt\\ITA\\allDetails";
//	private static final String theDetailDataPath="D:\\opt\\ITA\\detail_"+new Random().nextInt(10000) + 1;
	//文件名
	private static final String homePath=homePathDir+"\\round_home_"+new Random().nextInt(10000) + 1;
	private static final String allDataPath=allDataPathDir+"\\allDetail_"+new Random().nextInt(10000) + 1;
	
	
	private static int count=3;//递归次数
	
	public ItaAdapter(TaskModel taskQueue) {
		super(taskQueue);
	}

	@Override
	public List<Object> paraseToVo(Object obj) throws Exception {
//		List<Object> list=new ArrayList<Object>();
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return paraseToVoDI(obj);//国际往返
		case INTERNATIONAL_ONEWAY:
			return paraseToVoDI(obj);//国际单程（头等/经济）
		case DOMESTIC_ONEWAYTRIP:
//			return paraseToVoS(obj);//国内单程
			return paraseToVoDI(obj);
		}
		
		return null;
	}

	/**
	 * 国际——适配器
	 */	
	private List<Object> paraseToVoDI(Object fetchObject) throws Exception {
		//读取目录下已经下载好的文件，并解释文件内容
		if(fetchObject ==null){
//			return null;
		}
		List<Object> dataList = new ArrayList<Object>();
//		File f=new File(theDetailDataPath);
//		File[] files=f.listFiles();
		int fag=0;
		if(resultList.size()>0){
			setLenghtCount(resultList.toString().getBytes().length);
			for(int i=0;i<resultList.size();i++){
				try{
				fag++;
				String fileStr=resultList.get(i);
				if(fileStr.getBytes().length>10){
					JSONObject obj = JSONObject.fromObject(fileStr);
					JSONObject resultJson=obj.getJSONObject("result");
					String priceStr= resultJson.getJSONObject("currencyNotice").getJSONObject("ext").get("price").toString();//总价格
					String currency=priceStr(priceStr)[0];//货币单位
					String totalPrice=priceStr(priceStr)[1];//往返总价格（最高，最低同一个价格,含 税）
					JSONObject infos=resultJson.getJSONObject("bookingDetails");//票务详细信息
					JSONArray flightInfos=infos.getJSONObject("itinerary").getJSONArray("slices");//往返共有两段
					
					//税收信息
					JSONArray taxInfs=infos.getJSONArray("tickets");
					Double totalTax=0.0;//总税收信息
					Map<Integer ,String > nakePrice =new HashMap<Integer, String>();//分航段裸价
					Map<String ,String> farePrics=new HashMap<String ,String >();//存储分段价格的信息
					if(taxInfs.size()>0){
						for(int tx=0;tx<taxInfs.size();tx++){
							JSONArray priceJsonA=taxInfs.getJSONObject(tx).getJSONArray("pricings");
							for(int pt=0;pt<priceJsonA.size();pt++){
								JSONObject taxObjs=priceJsonA.getJSONObject(pt);
								JSONObject taxObj=taxObjs.getJSONObject("ext");
								//每段相关价格
								JSONArray fareList=taxObjs.getJSONArray("fares");
								for(int fareL=0;fareL<fareList.size();fareL++){
									JSONObject farePrice=fareList.getJSONObject(fareL);
									String priceStrs=farePrice.getString("displayAdjustedPrice");
									JSONArray cityObjList=farePrice.getJSONArray("bookingInfos");
									String fromCityCode=null,toCityCode=null;
									if(cityObjList.size()>0){
										if(cityObjList.size()==1){
											JSONObject cityObj=cityObjList.getJSONObject(0).getJSONObject("segment");
											fromCityCode=cityObj.getString("origin");//此段价格中的出城市
											toCityCode=cityObj.getString("destination");//此段价格中的到达城市
										}else{
											JSONObject cityObj1=cityObjList.getJSONObject(0).getJSONObject("segment");
											JSONObject cityObj2=cityObjList.getJSONObject(cityObjList.size()-1).getJSONObject("segment");
											fromCityCode=cityObj1.getString("origin");//此段价格中的出城市
											toCityCode=cityObj2.getString("destination");//此段价格中的到达城市
										}
									}
									
									farePrics.put(fromCityCode+"-"+toCityCode, priceStr(priceStrs)[1]);
									nakePrice.put(fareL, priceStr(priceStrs)[1]);//json已经排序了，按段取出来  
								}
								
								//税收总信息
								JSONArray taxList=taxObj.getJSONArray("taxTotals");
								if(taxList.size()>0){
									for(int t=0;t<taxList.size();t++){//循环计算税收价格
										String taxPrices=taxList.getJSONObject(t).getString("totalDisplayPrice");
										totalTax+=Double.parseDouble(taxPrices !=null?priceStr(taxPrices)[1]:"0.0");
									}
								}else{
									logger.error("税收信息解释有误！");
								}
							}
						}
						
					}else{
						logger.info("没有税收相关信息");
					}
					//往返大实体
//					DoublePlaneInfoEntity entity=PlaneInfoEntityBuilder.buildPlaneInfo(
//							taskQueue, null,null,null, null, null, null, nakePriceStr(taskQueue.getFromCity(),taskQueue.getToCity(),farePrics), null, nakePriceStr(taskQueue.getFromCity(),taskQueue.getToCity(),farePrics), null,DoublePlaneInfoEntity.class);
					
					DoublePlaneInfoEntity entity=PlaneInfoEntityBuilder.buildPlaneInfo(
							taskQueue, null,null,null, null, null, null, null, null, null, null,DoublePlaneInfoEntity.class);
					entity.setCurrency(currency);//货币种类
					entity.setSumHighestPrice(Double.parseDouble(totalPrice));
					entity.setSumLowestPrice(Double.parseDouble(totalPrice));
					entity.setTotalHighestTaxesPrice(totalTax);
					entity.setTotalLowestTaxesPrice(totalTax);
					//现在修改：裸价=总价-税收
					entity.setTotalLowestPrice(entity.getSumLowestPrice()-entity.getTotalLowestTaxesPrice());
					entity.setTotalHighestTaxesPrice(entity.getSumHighestPrice()-entity.getTotalHighestTaxesPrice());
					
					ReturnDoublePlaneInfoEntity rEntity=null;//回程实体
					if(flightInfos.size()>0){//往返都 有两个的
						for(int fligt=0;fligt<flightInfos.size();fligt++){
							Set<CabinEntity> cabinSet =new HashSet<CabinEntity>();//去程舱位实体
							JSONObject flightDetail=flightInfos.getJSONObject(fligt);
							String departureTime="",arrivalTime="",originPortName="",destinationPortName="";
							departureTime=dateTime(flightDetail.get("departure").toString());//出发详细时间
							arrivalTime=dateTime(flightDetail.get("arrival").toString());//到达详细时间
							destinationPortName=flightDetail.getJSONObject("destination").getString("name");//到达机场名
							originPortName=flightDetail.getJSONObject("origin").getString("name");//出发机场名
							
							//1，去程；2，回程
							if(fligt==0){//去程
								entity.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", departureTime));
								entity.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", arrivalTime));
							}
								//中转信息
								JSONArray segmentsList=flightDetail.getJSONArray("segments");
								if(segmentsList.size()>0){//有信息1，表示没有中转
									//ArrayList<TransitEntity> transitSet=new ArrayList<TransitEntity>();//去程中转
									Long durations=0l;//总时间 =停留时间+飞行时间
									for(int seg=0;seg<segmentsList.size();seg++){
										JSONObject segment=segmentsList.getJSONObject(seg);
										String gTrFlightNo="";//中转航班号
										String carrierKey="";//中转航空公司代码 如：CA
										String carrierName="";//中转航空公司名称
										String fromAirPort="",fromAirPortName="",toAirPort="",toAirPortName="";//中转城市名和三字码相关信息
										String flightType="";//中转飞机类型
										String startTime="",endTime="";//中转出发、到达日期时间
										Long duration=0l;//此航段的飞行时间
										Long stayTime=0l;//停留时间
										startTime=segment.get("departure").toString();
										endTime=segment.get("arrival").toString();
										carrierKey=segment.getJSONObject("carrier").getString("code");
										carrierName=segment.getJSONObject("carrier").getString("shortName");
										gTrFlightNo=carrierKey+segment.getJSONObject("flight").getString("number");
										duration=(Long.parseLong(segment.getString("duration").isEmpty()? "0":segment.getString("duration")))*60*1000;
										stayTime=(Long.parseLong(segment.getJSONObject("connection").isNullObject()?"0":segment.getJSONObject("connection").getString("duration").toString()))*60*1000;
										durations=duration+durations+stayTime;//此航段总时间
										JSONObject legs=segment.getJSONArray("legs").getJSONObject(0);
										flightType=legs.getJSONObject("aircraft").getString("shortName");
										fromAirPort=legs.getJSONObject("origin").getString("code");
										fromAirPortName=null;
										toAirPort=legs.getJSONObject("destination").getString("code");
										toAirPortName=legs.getJSONObject("destination").getString("name");
										TransitEntity trEntity=null;//去程中转
										//舱位信息
										JSONArray cabinList=segment.getJSONArray("bookingInfos");
										if(fligt==0){//去程
											if(seg==0){
												entity.setFlightNo(gTrFlightNo);
												entity.setCarrierName(carrierName);
												entity.setCarrierFullName(carrierName);
												entity.setFlightType(flightType);
											}
											if(segmentsList.size()>1){//说明有中转信息
												//去程中转
												trEntity=PlaneInfoEntityBuilder.buildTransitEntity(
														gTrFlightNo, null, carrierKey, carrierName, carrierName, 
														fromAirPort, fromAirPortName, toAirPort, toAirPortName, flightType,TransitEntity.class);
												//把一趟的飞机信息存入“去程”大实体中
												
												trEntity.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dateTime(startTime)));
												trEntity.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dateTime(endTime)));
												trEntity.setStayTime(stayTime);
												entity.getTransits().add(trEntity);//增加中转信息到中转实体中
											}
											if(cabinList.size()>0){
												for(int c=0;c<cabinList.size();c++){
													CabinEntity cabinEntity=new CabinEntity();
													JSONObject bookCabin=cabinList.getJSONObject(c);
													String subCabin=bookCabin.getString("bookingCode");//k
													String cabinName=bookCabin.getString("cabin");//cabin : "COACH" FIRST
													cabinEntity.setCabinType(cabinName);
													cabinEntity.setSubCabinName(subCabin);
													cabinEntity.setPrice(Double.parseDouble(nakePrice.get(fligt).isEmpty()?"0.0":nakePrice.get(fligt)));
//													cabinSet.add(cabinEntity);
													entity.getCabins().add(cabinEntity);//存入舱位
												}
											}else{
												logger.info("去程没有对应舱位和价格，抛弃此数据!");
												return null;
											}
											if(seg==(segmentsList.size()-1)){
												entity.setFlightDuration(durations);//去程总时间长(包括停留)
											}
										}
										if(fligt==1){//回程
											//回程实体
											if(seg==0){
//											rEntity=PlaneInfoEntityBuilder.buildPlaneInfo(
//													taskQueue,carrierKey, carrierName, carrierName, departureTime,arrivalTime, gTrFlightNo,  nakePrice.get(fligt).isEmpty() ?"0.0":nakePrice.get(fligt), null,  nakePrice.get(fligt).isEmpty() ?"0.0":nakePrice.get(fligt), flightType, ReturnDoublePlaneInfoEntity.class);
											rEntity=PlaneInfoEntityBuilder.buildPlaneInfo(
													taskQueue,carrierKey, carrierName, carrierName, departureTime,arrivalTime, gTrFlightNo, null, null,  null, flightType, ReturnDoublePlaneInfoEntity.class);
											rEntity.setCurrency(currency);//货币种类
											rEntity.setSumHighestPrice(Double.parseDouble(totalPrice));
											rEntity.setSumLowestPrice(Double.parseDouble(totalPrice));
											rEntity.setTotalHighestTaxesPrice(totalTax);
											rEntity.setTotalLowestTaxesPrice(totalTax);
											rEntity.setTotalLowestPrice(rEntity.getSumLowestPrice()-rEntity.getTotalLowestTaxesPrice());
											rEntity.setTotalHighestPrice(rEntity.getSumHighestPrice()-rEntity.getTotalHighestTaxesPrice());
											}
											if(segmentsList.size()>1){//说明有中转信息
												//回程中转
												ReturnTransitEntity rTr=PlaneInfoEntityBuilder.buildTransitEntity(
														gTrFlightNo, null, carrierKey, carrierName, carrierName, 
														fromAirPort, fromAirPortName, toAirPort, toAirPortName, flightType,ReturnTransitEntity.class);
												rTr.setStartTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dateTime(startTime)));
												rTr.setEndTime(DateUtil.StringToDate("yyyy-MM-dd HH:mm", dateTime(endTime)));
												rTr.setStayTime(stayTime);
												rEntity.getReturnTransits().add(rTr);
											}
											
											if(cabinList.size()>0){
												for(int c=0;c<cabinList.size();c++){
													JSONObject bookCabin=cabinList.getJSONObject(c);
													String subCabin=bookCabin.getString("bookingCode");//k
													String cabinName=bookCabin.getString("cabin");//cabin : "COACH" FIRST
													ReturnCabinEntity rCabinE=PlaneInfoEntityBuilder.buildCabinInfo(
															cabinName, subCabin, null,nakePriceStr(taskQueue.getToCity(), taskQueue.getFromCity(), farePrics), null, null, null, ReturnCabinEntity.class);
													rEntity.getReturnCabins().add(rCabinE);
												}
												
											}else{
												logger.info("回程没有对应舱位和价格，抛弃此数据!");
												return null;
											}
											if(seg==(segmentsList.size()-1)){//表示最后一次执行
												rEntity.setFlightDuration(durations);//回程 时间总长
											}
										}
									}
									
								}else{
									logger.info("没有对应的航班信息");
									return null;
								}
						}
						//把回程信息放入
						entity.getReturnPlaneInfos().add(rEntity);
					}else{
						logger.info("往返数据解释有误！");
					}
					dataList.add(entity);//添加到list对象中
				}else{
					logger.error("文件内容有误!");
				}
				}catch (Exception e) {
					logger.error("分析失败");
				}
				
			}
		}else{
			logger.info("没有对应的数据");
			return null;
		}
		
		
		/*if(fag==resultList.size()){
			logger.info(String.format("目录：%s下所有数据已经解析完，执行删除...", theDetailDataPath));
			try{
				if(delDir(new File(theDetailDataPath))){
					logger.info(theDetailDataPath+"目录删除成功！");
				}else{
					logger.info(theDetailDataPath+"目录下文件删除失败！");
				}
				
			}catch (Exception e) {
				logger.info(theDetailDataPath+"目录执行删除异常！");
				e.printStackTrace();
			}
		}*/
		
		return dataList;
	}
	
	@Override
	/**
	 * 组装URL
	 */
	public String getUrl() throws Exception {
		
		
		return null;
	}
	
	
	/**
	 * 国际URL 往返
	*/
	public Object fetchDI() throws Exception {
		org.apache.commons.httpclient.HttpClient client=null;
		try{
			client=super.getComHttpCient();
			//进入第一层下载数据
			getHomePage(client);//把相应的信息存入全局变量中
			if(solutionSet !=null && session !=null && !solutionSet.equals("") && !session.equals("")){
				//配合中航信任务，取直飞数据
				if(!flag){//考虑直飞
					showAllData(client);
				}
			}else{
				logger.info("session信息为空了，抓取回滚...");
				while (count-->0) {
					logger.info("抓取第"+count+"次回滚");
					fetchDI();
				}
				
			}
			//进入detail下载详细信息文件
			logger.info("进入详细表单，下载数据...");
			showDetail(client);
			String allStr="";
			for(String str:resultList){
				allStr+=str;
			}
			super.appendPageContents(allStr);
			setLenghtCount(allStr.length());
			return "";
		}catch (Exception e) {
			logger.error("第一层头部信息，解释出错！");
		}
		
		return null;
	}
	
	/**
	 * 
	*/
	public String downLoadByURL(String url,HttpClient client){
//		HttpClient client = new org.apache.commons.httpclient.HttpClient();  
		if(client ==null){
			client=super.getComHttpCient();
		}
		GetMethod httpGet = new GetMethod(url);  
		HttpEntity entity = null;
		InputStream input=null;
		FileOutputStream out=null;
		String result=null;
		try {
			
			/*entity = getHttpResponse(url, Reqeust.GET).getEntity();
			input = entity.getContent();
			out= new FileOutputStream(new File(path));
			byte buffer[] = new byte[1024];
			int size = 0;
			while ((size = input.read(buffer)) > 0) {
				out.write(buffer, 0 ,size);
			}*/
            client.executeMethod(httpGet);
            
            result=httpGet.getResponseBodyAsString();
//            InputStream in = httpGet.getResponseBodyAsStream();
//            out = new FileOutputStream(new File(path));  
//            out.write(httpGet.getResponseBodyAsString().getBytes());
             
            
           /*
            byte[] b = new byte[1024];  
            int len = 0; 
            while((len=in.read(b))!= -1){  
                out.write(b,0,len);  
            }  */
//            in.close();  
//            out.close();  
            if(result !=null){
            	logger.info("下载操作完成。");
            	if(result.contains("{}&&")){
            		result=result.replace("{}&&", "");
            	}
            	return result;
            }
           
            
              
        }catch (Exception e){  
        	logger.info("下载失败。");
            e.printStackTrace();  
        } finally{  
        	entity=null;
        	
        } 
		return null;
	}
	
	/**
	 * 组装URL
	 * one_way
	 * for home page
	 */
	public String getOneWayURL(String date){
		JSONObject json = new JSONObject(false);
		json.put("slices",new JSONArray());
		JSONObject json2 = new JSONObject(false);
		json2.put("origins", "['SNA']");
		json2.put("originPreferCity", false);
		json2.put("destinations", "['PEK']");
		json2.put("destinationPreferCity", false);
		json2.put("date", "2014-07-11");
		json2.put("isArrivalDate", false);
		JSONObject json3 = new JSONObject(false);
		json3.put("minus", 0);
		json3.put("plus", 0);
		json2.put("dateModifier", json3);
		JSONObject json4 = new JSONObject(false);
		json4.put("adults", 1);
		json.put("pax", json4);
		json.put("cabin", "COACH");
		json.put("changeOfAirport", true);
		json.put("checkAvailability", true);
		JSONObject json5 = new JSONObject(false);
		json5.put("size", 30);
		json.put("page", json5);
		json.put("sorts", "default");
		
		json.getJSONArray("slices").add(json2);
		StringBuffer URL=new StringBuffer("http://matrix.itasoftware.com/xhr/shop/search?format=JSON&inputs=");
		Map<String,String > addOther=new HashMap<String,String>(); 
		addOther.put("name", "specificDates");
		String str="carrierStopMatrix,currencyNotice,solutionList,itineraryPriceSlider,itineraryCarrierList,itineraryDepartureTimeRanges,itineraryArrivalTimeRanges,durationSliderItinerary,itineraryOrigins,itineraryDestinations,itineraryStopCountList,warningsItinerary";
		addOther.put("summarizers", str);
		
		String url=null,urls=null;
		try {
			url = URLEncoder.encode(json.toString(), "utf-8");
			URL.append(url);
			urls=getBaseGetUrl(URL.toString(),addOther);
		} catch (Exception e) {
			logger.error("地址组装错误");
			e.printStackTrace();
		}
		
		return urls.toString();
	}
	
	
	/**
	 * 点击all列出所有的数据
	 */	
	public String getURLALL(){
		logger.info("第二层页面：点击“all”获取所有的数据！");
		JSONObject json = new JSONObject(false);
		json.put("slices",new JSONArray());
		JSONObject json_g = new JSONObject(false);
		//组装去程ＵＲＬ
		json_g.put("origins", "['"+taskQueue.getFromCity()+"']");
		json_g.put("originPreferCity", false);
		json_g.put("destinations", "['"+taskQueue.getToCity()+"']");
		json_g.put("destinationPreferCity", false);
		json_g.put("date", taskQueue.getFlightDate());
		json_g.put("isArrivalDate", false);
		JSONObject json3 = new JSONObject(false);
		json3.put("minus", 0);
		json3.put("plus", 0);
		json_g.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_g);
		//组装回程URL
		JSONObject json_r = new JSONObject(false);
		json_r.put("destinations", "['"+taskQueue.getFromCity()+"']");
		json_r.put("destinationPreferCity", false);
		json_r.put("origins", "['"+taskQueue.getToCity()+"']");
		json_r.put("originPreferCity", false);
		json_r.put("date", taskQueue.getReturnGrabDate());
		json_r.put("isArrivalDate", false);
		json_r.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_r);
		
		
		JSONObject json4 = new JSONObject(false);
		json4.put("adults", 1);
		json.put("pax", json4);
		json.put("cabin", "COACH");
		json.put("changeOfAirport", true);
		json.put("checkAvailability", true);
		JSONObject json5 = new JSONObject(false);
		json5.put("size", 20000);
		json5.put("current", 1);	
		json.put("page", json5);
		json.put("sorts", "default");
		
		StringBuffer URL=new StringBuffer("http://matrix.itasoftware.com/xhr/shop/summarize?format=JSON&inputs=");
		Map<String,String > addOther=new HashMap<String,String>(); 
		addOther.put("session", session);
		addOther.put("solutionSet", solutionSet);
		String str="solutionList";
		addOther.put("summarizers", str);
		
		String url=null,urls=null;
		try {
			url = URLEncoder.encode(json.toString(), "utf-8");
			URL.append(url);
			urls=getBaseGetUrl(URL.toString(),addOther);
		} catch (Exception e) {
			logger.error("地址组装错误");
			e.printStackTrace();
		}
		
		return urls.toString();
	}
	
	/**
	 * 点击列出每一条的详细信息
	 * URL
	 */	
	public String detailUrl(String id){
		logger.info("获取详情URL...");
		JSONObject json = new JSONObject(false);
		json.put("slices",new JSONArray());
		JSONObject json_g = new JSONObject(false);
		//组装去程ＵＲＬ
		json_g.put("origins", "['"+taskQueue.getFromCity()+"']");
		json_g.put("originPreferCity", false);
		json_g.put("destinations", "['"+taskQueue.getToCity()+"']");
		json_g.put("destinationPreferCity", false);
		json_g.put("date", taskQueue.getFlightDate());
		json_g.put("isArrivalDate", false);
		JSONObject json3 = new JSONObject(false);
		json3.put("minus", 0);
		json3.put("plus", 0);
		json_g.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_g);
		//组装回程URL
		JSONObject json_r = new JSONObject(false);
		json_r.put("destinations", "['"+taskQueue.getFromCity()+"']");
		json_r.put("destinationPreferCity", false);
		json_r.put("origins", "['"+taskQueue.getToCity()+"']");
		json_r.put("originPreferCity", false);
		json_r.put("date", taskQueue.getReturnGrabDate());
		json_r.put("isArrivalDate", false);
		json_r.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_r);
		
		
		JSONObject json4 = new JSONObject(false);
		json4.put("adults", 1);
		json.put("pax", json4);
		json.put("cabin", "COACH");
		json.put("changeOfAirport", true);
		json.put("checkAvailability", true);
		JSONObject json5 = new JSONObject(false);
		json5.put("size", 20000);
		json5.put("current", 1);	
		json.put("page", json5);
		json.put("sorts", "default");
		json.put("solution", solutionSet+"/"+id);
		
		StringBuffer URL=new StringBuffer("http://matrix.itasoftware.com/xhr/shop/summarize?format=JSON&inputs=");
		Map<String,String > addOther=new HashMap<String,String>(); 
		addOther.put("session", session);
		addOther.put("solutionSet", solutionSet);
		String str="currencyNotice,bookingDetails";
		addOther.put("summarizers", str);
		
		String url=null,urls=null;
		try {
			url = URLEncoder.encode(json.toString(), "utf-8");
			URL.append(url);
			urls=getBaseGetUrl(URL.toString(),addOther);
		} catch (Exception e) {
			logger.error("地址组装错误");
			e.printStackTrace();
		}
		
		return urls.toString();
		
	}
	
	/**
	 * 进入第一层取数据
	 * 并解释出对应的
	*/
	public void getHomePage(HttpClient client){
		
		//进入第一层时首先新建目录
	//	createDir(homePathDir, allDataPathDir, theDetailDataPath);
		
		logger.info("进入第一层页面，下载数据...");
		String fileTxt=null;
		fileTxt=downLoadByURL(getRoundTripURL(),client);
		//验证数据是否已经下载完成
		logger.info("验证并解释数据...");
		//读取目录中的数据
//		String fileTxt=readFile(homePath);
//		super.setSourceWebPageContents(fileTxt);//存入网页原数据(保存第一次抓取到的数据)
//		int lenght = fileTxt.getBytes().length;//设置流量统计
		if(fileTxt !=null && !fileTxt.contains("not found") ){
			JSONObject jsonObj = JSONObject.fromObject(fileTxt);
			JSONObject resultJson=jsonObj.getJSONObject("result");
			session=resultJson.getString("session");
			solutionSet=resultJson.getString("solutionSet");
			solutionCount=resultJson.getString("solutionCount");
			
			if(flag){//考虑直飞
				//解释此页面
				JSONObject solutionList=resultJson.getJSONObject("solutionList");
				String counts =solutionList.getJSONObject("pages").getString("count");
				boolean tFlag=true;
				if(counts !=null && !counts.equals("") && !counts.isEmpty()){
					if(Integer.parseInt(counts)>1) tFlag=false;
				}
				if(tFlag==false){//存在分页
					flag=false;
				}
				if(flag){
					try{
						if(solutionList.toString().contains("solutions")){
							JSONArray solutionLists=solutionList.getJSONArray("solutions");
							idLists =new ArrayList<String>();
							if(solutionLists.size()>0){
								for(int i=0;i<solutionLists.size();i++){
									JSONObject solution=solutionLists.getJSONObject(i);
									idLists.add(solution.getString("id"));
								}
								logger.info("对应的sessionID存入完毕,共有"+idLists.size()+"条！");
							}
						}else{
							throw new NoTicketException("没有对应的航班！");
						}
					}catch (Exception e) {
					}
				}
				
			}
		}else{//递归
			logger.info("数据没有抓取成功，递归执行...");
			while(count-->0){
				try {
					logger.info("递归抓取第"+count+"次");
					fetchDI();
				} catch (Exception e) {
					logger.error("第一层抓取失败");
					e.printStackTrace();
				}
			}
		}
		
		/*try{
			delFile(homePath);
		}catch (Exception e) {
			logger.info(homePath+"文件删除失败！");
			e.printStackTrace();
		}*/
		
	}
	/**
	 * 点击选择“ALL”
	 * 并解析出对就的数据
	 * id
	*/
	public String showAllData(HttpClient client){
		logger.info("进入第二层页面，下载数据...");
		String fileTxt=null;
		fileTxt =downLoadByURL( getURLALL(),client);
		//验证数据是否已经下载完成
		logger.info("验证'All'中数据，并解释数据...");
//		String fileTxt=readFile(allDataPath);
		super.appendPageContents(fileTxt);//存入网页原数据(保存第一次抓取到的数据)
		int lenght = fileTxt.getBytes().length;//设置流量统计
		setLenghtCount( lenght);//只存页面取回来的数据，不暂详细信息
		if(fileTxt !=null && !fileTxt.contains("not found") ){
			idLists =new ArrayList<String>();
			JSONObject jsonObj = JSONObject.fromObject(fileTxt);
			JSONObject resultJson=jsonObj.getJSONObject("result");
			session=resultJson.getString("session");
			solutionSet=resultJson.getString("solutionSet");
			solutionCount=resultJson.getString("solutionCount");
			JSONObject solutionList=resultJson.getJSONObject("solutionList");
			JSONArray solutionLists=solutionList.getJSONArray("solutions");
			
			if(solutionLists.size()>0){
				for(int i=0;i<solutionLists.size();i++){
					JSONObject solution=solutionLists.getJSONObject(i);
					idLists.add(solution.getString("id"));
				}
				logger.info("对应的sessionID存入完毕,共有"+idLists.size()+"条！");
			}
		}else{
			logger.error("数据有误 ...");
		}
		
		/*try{
			delFile(allDataPath);
		}catch (Exception e) {
			logger.info(allDataPath+"文件删除失败！");
			e.printStackTrace();
		}*/
		
		return null;
	}
			
	/**
	 * 点击选择“detail”
	 * 并解析出对就的数据
	 * 循环组装ULR进行下载数据
	 * @parm sessionId
	*/
	public String showDetail(HttpClient client){
		if(idLists.size()>0){
			int i=0;
			for(String id:idLists){
				i++;
				String detailURL=detailUrl(id);
				logger.info("下载详细数据，【"+i+"/"+idLists.size()+"】...");
//				downLoadByURL(theDetailDataPath+"\\"+id,detailURL);
				resultList.add(downLoadByURL(detailURL,client));
			}
		}else{
			logger.info("sessionID为空，进行回滚操作....");
			while (count-->0) {
				logger.info("抓取第"+count+"次回滚");
				try {
					fetchDI();
				} catch (Exception e) {
					logger.info("回滚操作失败!");
					e.printStackTrace();
				}
			}
		}
		
		
		return null;
	}
	
	
	/**
	 * 读取文件，String
	 * @parm path
	 */
	public String readFile(String path){
		StringBuffer fileStr=new StringBuffer();
		try {
			File file = new File(path);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), "GBK");//考虑编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					fileStr.append(lineTxt);
					lineTxt="";
				}
				read.close();
			} else {
				logger.error("找不到地址:"+path+"下对应的文件！");
			}

		} catch (Exception e) {
			logger.error("解释出错！");
			e.printStackTrace();
		}
		//System.out.println(fileStr.toString().replaceAll("\\{}&&", ""));
		return fileStr.toString().replaceAll("\\{}&&", "");
	}
	
	
	/**
	 * 抓取前第一件就是创建此适配器需要的目录
	*/
	public void createDir(String path1,String path2,String path3){
		logger.info("创建目录启动....");
		try{
			File myPath=new File(path1);
			File myPath2=new File(path2);
			File myPath3=new File(path3);
			if(!myPath.exists()){
				myPath.mkdirs();
			}
			if(!myPath2.exists()){
				myPath2.mkdirs();
			}
			if(!myPath3.exists()){
				myPath3.mkdirs();
			}
			logger.info("创建目录成功！");
		}catch (Exception e) {
			throw new ExcelException("创建目录失败，请手动创建！");
		}
		
	}
	
	/**
	 * logger.info("删除目录"+homePath+"下文件");
	*/
	public void delFile(String path){
		logger.info("删除"+path+"下文件...");
		File delfile = new File(path); 
		delfile.delete();
		logger.info("删除"+path+"下文件成功！");
		
	}
	
	/**
	 * 删除目录及其下的所有文件
	*/
	public boolean  delDir(File dir){
		if (dir.isDirectory()) {
			String[] children = dir.list();
			//递归删除目录中的子目录下
            for (int i=0; i<children.length; i++) {
                boolean success = delDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
		}
		// 目录此时为空，可以删除
		return dir.delete();
//		delfile.delete();
	}
	/**
	 * 组装URL
	 * round_trip
	 * for home page
	 */
	public String getRoundTripURL(){
		JSONObject json = new JSONObject(false);
		json.put("slices",new JSONArray());
		JSONObject json_g = new JSONObject(false);
		//组装去程ＵＲＬ
		json_g.put("origins", "['"+taskQueue.getFromCity()+"']");
		json_g.put("originPreferCity", false);
		json_g.put("destinations", "['"+taskQueue.getToCity()+"']");
		json_g.put("destinationPreferCity", false);
		json_g.put("date", taskQueue.getFlightDate());
		json_g.put("isArrivalDate", false);
		JSONObject json3 = new JSONObject(false);
		json3.put("minus", 0);
		json3.put("plus", 0);
		json_g.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_g);
		//组装回程URL
		JSONObject json_r = new JSONObject(false);
		json_r.put("destinations", "['"+taskQueue.getFromCity()+"']");
		json_r.put("destinationPreferCity", false);
		json_r.put("origins", "['"+taskQueue.getToCity()+"']");
		json_r.put("originPreferCity", false);
		json_r.put("date", taskQueue.getReturnGrabDate());
		json_r.put("isArrivalDate", false);
		json_r.put("dateModifier", json3);
		json.getJSONArray("slices").add(json_r);
		
		
		JSONObject json4 = new JSONObject(false);
		json4.put("adults", 1);
		json.put("pax", json4);
		json.put("cabin", "COACH");
		if(flag==true){//只要直飞的
			json.put("maxStopCount", 0);
		}
		json.put("changeOfAirport", true);
		json.put("checkAvailability", true);
		JSONObject json5 = new JSONObject(false);
		json5.put("size", 30);
		json.put("page", json5);
		json.put("sorts", "default");
		
		StringBuffer URL=new StringBuffer("http://matrix.itasoftware.com/xhr/shop/search?format=JSON&inputs=");
		Map<String,String > addOther=new HashMap<String,String>(); 
		addOther.put("name", "specificDates");
		String str="carrierStopMatrix,currencyNotice,solutionList,itineraryPriceSlider,itineraryCarrierList,itineraryDepartureTimeRanges,itineraryArrivalTimeRanges,durationSliderItinerary,itineraryOrigins,itineraryDestinations,itineraryStopCountList,warningsItinerary";
		addOther.put("summarizers", str);
		
		String url=null,urls=null;
		try {
			url = URLEncoder.encode(json.toString(), "utf-8");
			URL.append(url);
			urls=getBaseGetUrl(URL.toString(),addOther);
		} catch (Exception e) {
//			logger.error("地址解析错误");
			e.printStackTrace();
		}
		return urls.toString();
	}
	
	
	
	
	@Override
	public Object fetch(String url) throws Exception {
		switch (super.getRouteType()) {
		case INTERNATIONAL_ROUND:
			return fetchDI();//国际往返
		case INTERNATIONAL_ONEWAY:
			return fetchDI();//国际单程
		case DOMESTIC_ONEWAYTRIP:
			return fetchDI();
		case DOMESTIC_ROUNDTRIP:
			return null;
		}
		return null;
	}

	@Override
	public boolean validateFetch(Object fetchObject) throws Exception {
		return true;
	}
	
	
	public String getBaseGetUrl(String url, Map<String, String> params)throws Exception {
		Iterator<Entry<String, String>> i;
		Entry<String, String> en;
		StringBuffer sb=new StringBuffer(url);
		try {
			i = params.entrySet().iterator();
			if(!sb.toString().matches(".*\\?$")){
				sb.append("&");
			}
			while (i.hasNext()) {
				en = i.next();
				sb.append(java.net.URLEncoder.encode(en.getKey())+"="+java.net.URLEncoder.encode(en.getValue())+"&");
			}
			return sb.toString().replaceAll("&+$", "");
		} finally {
			en = null;
			i = null;
			sb=null;
		}

	}
	//USD122.00
	public String[] priceStr(String str){
		String[] prices=new String[2];
		//过滤出字母：货币
		prices[0]=str.replaceAll("[^a-zA-Z]*", "");
		//过滤出数据：价格
		prices[1]=str.replaceAll("[a-zA-Z]*", "");
		
		return prices;
	}
	
	/*
	 * 传入出发城市三字码，到达城市三字码，返回此段航线的裸价
	 * {SFO-TES=4400,TES-PEK=3330, SEA-SFO=395, PEK-SEA=3500}  SEA-PEK
	*/
	public String nakePriceStr(String fromCityCode,String toCityCode,Map<String ,String>priceMap){
		String nakePriceStr="0.0";
		String key=null;
		try{
			if(fromCityCode !=null && !fromCityCode.equals("") && priceMap.size()>0 && priceMap !=null && toCityCode !=null && !toCityCode.equals("")){
				key=fromCityCode+"-"+toCityCode;
				if(priceMap.get(key)!=null){
					nakePriceStr=priceMap.get(key);
				}else{
					String []priceStr=new String[priceMap.size()];
					for(Entry<String, String> entry : priceMap.entrySet()){
						if(fromCityCode.equalsIgnoreCase(entry.getKey().split("-")[0])){
							if(toCityCode.equalsIgnoreCase(entry.getKey().split("-")[1])){
								nakePriceStr=entry.getValue();
								break;
							}
							priceStr[0]=entry.getValue();
							int k=0;
							String testCity=entry.getKey().split("-")[1];
							for(Entry<String, String> entry2 : priceMap.entrySet()){
								if(testCity.equalsIgnoreCase(entry2.getKey().split("-")[0])){
									testCity=entry2.getKey().split("-")[1];
									priceStr[k]=entry2.getValue();
									k++;
									if(testCity.equalsIgnoreCase(toCityCode)){
										break;
									}
								}
								
							}
						}
					}
					Double doubPrices=0.0;
					if(priceStr.length>0){
						for(int i=0;i<priceStr.length;i++){
							if(priceStr[i] !=null)
							doubPrices+=Double.parseDouble(priceStr[i]);
						}
					}
					nakePriceStr=doubPrices+"";
				}
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//裸价=去程裸价+回程裸价
		//递归一次
		//nakePriceStr+=nakePriceStr(toCityCode,fromCityCode,priceMap);
		return nakePriceStr;
	}
	
	// "2014-07-18T13:00-07:00"
	public String dateTime(String dateStr){
		String dateTimeStr="";
		dateTimeStr=RegHtmlUtil.regStr(dateStr.replaceAll("T", " "),"(\\d{4}-\\d{2}-\\d{2}\\s+\\d{1,2}:\\d{1,2})");
		return dateTimeStr;
	}
	

	public static void main(String[] args) {
		TaskModel taskQueue = new TaskModel();
		taskQueue.setIsReturn(1);
		taskQueue.setIsInternational(1);
		ItaAdapter adapter = new ItaAdapter(taskQueue);
		
		try {
			adapter.fetch(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		String test="Session not found.";
//		System.out.println(test.contains("not found"));
		
		Set<String > test=new HashSet<String>();
		test.add("abc");
		test.add("a");
		test.add("b");
		test.add("abc");
		
//		readFile(homePath);
		/*
		String REMOTE_FILE_URL = "http://matrix.itasoftware.com/xhr/shop/search?format=JSON&inputs=%7B%22slices%22%3A%5B%7B%22origins%22%3A%5B%22SNA%22%5D%2C%22originPreferCity%22%3Afalse%2C%22destinations%22%3A%5B%22PEK%22%5D%2C%22destinationPreferCity%22%3Afalse%2C%22date%22%3A%222014-07-11%22%2C%22isArrivalDate%22%3Afalse%2C%22dateModifier%22%3A%7B%22minus%22%3A0%2C%22plus%22%3A0%7D%7D%5D%2C%22pax%22%3A%7B%22adults%22%3A1%7D%2C%22cabin%22%3A%22COACH%22%2C%22changeOfAirport%22%3Atrue%2C%22checkAvailability%22%3Atrue%2C%22page%22%3A%7B%22size%22%3A30%7D%2C%22sorts%22%3A%22default%22%7D&summarizers=carrierStopMatrix%2CcurrencyNotice%2CsolutionList%2CitineraryPriceSlider%2CitineraryCarrierList%2CitineraryDepartureTimeRanges%2CitineraryArrivalTimeRanges%2CdurationSliderItinerary%2CitineraryOrigins%2CitineraryDestinations%2CitineraryStopCountList%2CwarningsItinerary&name=specificDates";  
		HttpClient client = new HttpClient();  
	       GetMethod httpGet = new GetMethod(REMOTE_FILE_URL);  
	        try {  
	            client.executeMethod(httpGet);  
	              
	            InputStream in = httpGet.getResponseBodyAsStream();  
	             
	            FileOutputStream out = new FileOutputStream(new File("c:\\test"));  
	             
	            byte[] b = new byte[1024];  
	            int len = 0;  
	            while((len=in.read(b))!= -1){  
	                out.write(b,0,len);  
	            }  
	            in.close();  
	            out.close();  
	              
	        }catch (HttpException e){  
	            e.printStackTrace();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }finally{  
	            httpGet.releaseConnection();  
	        }  
	        System.out.println("download, success!!");  */
		
	}
	
}
