package com.foreveross.crawl.adapter.sub.impl20140402.v3.qunar;

import java.util.Collections;
import java.util.List;

import com.foreveross.crawl.common.PlaneInfoCommon;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 *价格对象，包括裸价，税费和合计价
 * @author stub
 *
 */
public class PriceCollect implements Comparable<PriceCollect>{
	private Double taxesPrice;
	private Double price;
	private Double originalPrice;
	
	public PriceCollect(){
	}
	
	public PriceCollect(Double taxesPrice,Double price,Double originalPrice){
		this.taxesPrice = taxesPrice;
		this.price = price;
		//如果没税，总价留空
		if(!PlaneInfoCommon.isNullOrZero(taxesPrice)){
			setOriginalPrice(originalPrice);
		}
	}
	
	@Override
	public int compareTo(PriceCollect o) {
		if (originalPrice != null && o.originalPrice != null) {
			return compareTo(originalPrice, o.originalPrice);
		}
		return compareTo(price, o.price);
	}

	private int compareTo(double arg0, double arg1) {
		if (arg0 < arg1) {
			return -1;
		}
		return 1;
	}
	
	/**
	 * 排序价格,是指到planInfo实体
	 * 
	 * @param entity
	 * @param list
	 */
	public static void buildPrice(AbstractPlaneInfoEntity entity, List<PriceCollect> list) {
		Collections.sort(list);
		PriceCollect lowerPc = list.get(0);
		PriceCollect highPc = list.get(list.size() - 1);
		entity.setTotalHighestPrice(highPc.getPrice());
		entity.setTotalLowestPrice(lowerPc.getPrice());
		entity.setTotalHighestTaxesPrice(PlaneInfoCommon.isNullOrZero(highPc.getTaxesPrice()) ? null : highPc.getTaxesPrice());
		entity.setTotalLowestTaxesPrice(PlaneInfoCommon.isNullOrZero(lowerPc.getTaxesPrice()) ? null : lowerPc.getTaxesPrice());
		entity.setSumHighestPrice(PlaneInfoCommon.isNullOrZero(highPc.getOriginalPrice())? null : highPc.getOriginalPrice());
		entity.setSumLowestPrice(PlaneInfoCommon.isNullOrZero(lowerPc.getOriginalPrice())? null :lowerPc.getOriginalPrice());
	}
	
	/**
	 * 获得一个价格对象
	 * 
	 * @param price
	 * @param taxesPrice
	 * @param originalPrice
	 * @return
	 */
	public static PriceCollect getPriceCollect(Double price, Double taxesPrice, Double originalPrice) {
		PriceCollect pc = new PriceCollect(taxesPrice, taxesPrice, originalPrice);
		return pc;
	}

	public Double getTaxesPrice() {
		return taxesPrice;
	}

	public void setTaxesPrice(Double taxesPrice) {
		this.taxesPrice = taxesPrice;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}
}
