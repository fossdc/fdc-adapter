package com.foreveross.crawl.adapter.sub.impl20140402.v3.elong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.ELongAdapter;
import com.foreveross.crawl.common.exception.self.SourceDataParseNullException;
import com.foreveross.crawl.common.exception.self.SourceWebPageNullException;
import com.foreveross.crawl.common.util.DateUtil;
import com.foreveross.crawl.common.util.RandomBrowserVersion;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.taskservice.common.bean.TaskModel;


/**
 * 艺龙国际往返程
 * @author xiangsf 2014-07-18
 *
 */
public class ElongInternationRoundTrip {
	protected Log logger = LogFactory.getLog(getClass());
	
	private final static int secondLayer_fail_retry_times = 1;//第二层级抓取失败时重试次数
	private final static int moreCabin_fail_retry_times = 1;//除经济舱外其它更多舱位（商务，头等）抓取失败时重试次数
	
	private final static boolean is_secondLayer_fetch = true;//是否抓取第二层级内容
	private final static boolean is_moreCabin_fetch = true;//是否抓取更多舱位内容

	
	private TaskModel taskQueue;
	
	private ELongAdapter adapter;
	
	private HttpClient httpClient;
	
	private String browserString = null;
	
	private String fromPinyin;
	private String toPinyin;
	
	private Map<String, String> queryParams = new HashMap<String, String>();
	private Map<String, String> cabinParams = new HashMap<String, String>();
	public ElongInternationRoundTrip(ELongAdapter adapter,TaskModel taskQueue){
		this.adapter = adapter;
		this.taskQueue = taskQueue;
		fromPinyin = taskQueue.getFromCityName().split("\\|")[1];
		toPinyin = taskQueue.getToCityName().split("\\|")[1];
		//this.taskQueue.setFromCityName(taskQueue.getFromCityName().split("\\|")[0]);
		//this.taskQueue.setToCityName(taskQueue.getToCityName().split("\\|")[0]);
	}
	/**
	 * 抓取的入口
	 * @return
	 * @throws Exception
	 */
	public List<Object> fetchData() throws Exception{
		List<Object> results = new ArrayList<Object>();
		browserString = RandomBrowserVersion.getBV().getUserAgent();
		try{
			cabinParams.put("Y", "经济舱");
			cabinParams.put("C", "商务舱");
			cabinParams.put("F", "头等舱");
			//根据三个舱位信息请求三次,经济Y，商务C，头等F ,如果三个舱位信息都没有航班数据，则返回没有航班异常
			//1、根据查询条件直接进入到去程航班列表(默认是经济舱的,代码是Y)
			logger.info("获得经济舱Y的数据");
			this.httpClient = adapter.getHttpClient();
			List<DoublePlaneInfoEntity> yList = this.fetchSingleCabinType("Y");//经济Y
			results.addAll(yList);
			if(is_moreCabin_fetch){ //统一控制是否获得更多舱位数据
				//2、取商务舱，代码是C
					//2.1、重复上面,1里的1,2,3,4
				for(int i = 0; i < moreCabin_fail_retry_times; i++){
					Thread.sleep(15000);
					try{
						logger.info("获得商务舱C的数据");
						results.addAll(this.fetchSingleCabinType("C"));
						break;
					}catch(Exception e){
						if(e instanceof FlightInfoNotFoundException) break;
						logger.error(e.getMessage());
						adapter.switchProxyipByHttClient(); //切换IP
					}finally{
						
					}
				}
				
				//3、取头等舱，代码是F
				    //3.1、重复上面,1里的1,2,3,4
				for(int i = 0; i < moreCabin_fail_retry_times; i++){
					Thread.sleep(15000);
					try{
						logger.info("获得头等舱F的数据");
						results.addAll(this.fetchSingleCabinType("F"));
						break;
					}catch(Exception e){
						if(e instanceof FlightInfoNotFoundException) break;
						logger.error(e.getMessage());
						adapter.switchProxyipByHttClient(); //切换IP
					}finally{
						
					}
				}
			}
		}finally{
			cabinParams = null;
			queryParams = null;
		}
		//不进行合并结果集了(同一个航班进行返程舱位的合并)
		return results;
	}
	/**
	 * 组装去程中转数据 
	 * @param table
	 * @return
	 * @throws Exception
	 */
	private List<TransitEntity> parseGoTrantsInfo(JSONArray arr) throws Exception{
		List<TransitEntity> transits = new ArrayList<TransitEntity>();
		JSONObject json = null;
		TransitEntity transitEntity = null;
		try{
			logger.info(String.format("获得的去程中转航班数量：%s",arr.size()));
			for(int j = 0; j < arr.size(); j++){//一个就是一个航班
				json = arr.getJSONObject(j);
				transitEntity = PlaneInfoEntityBuilder.buildTransitEntity(json.getString("fltno"), null, 
						json.getString("corp"), json.getString("corpn"), json.getString("corpn"), 
						json.getString("dport"), json.getString("dportn"), json.getString("aport"),
						json.getString("aportn"), json.getString("plane"));
				transitEntity.setStartTime(DateUtil.StringToDate("yyyy/MM/dd HH:mm:ss", json.getString("dtime")));
				transitEntity.setEndTime(DateUtil.StringToDate("yyyy/MM/dd HH:mm:ss", json.getString("atime")));
				transitEntity.setStayTime(Long.parseLong(json.getString("ftime"))*60*1000);
				
				transits.add(transitEntity);
				//这时有经停，但没有找到经停的事例，暂时不记入
				
				
			 }
		}finally{
			json = null; 
			transitEntity = null; 
		}
		return transits;
	}

	
	/**
	 * 组装回程中转数据 
	 * @param table
	 * @return
	 * @throws Exception
	 */
	private List<ReturnTransitEntity> parseBackTrantsInfo(JSONArray arr) throws Exception{
		List<ReturnTransitEntity> transits = new ArrayList<ReturnTransitEntity>();
		JSONObject json = null;
		ReturnTransitEntity transitEntity = null;
		try{
			logger.info(String.format("获得的返程中转航班数量：%s",arr.size()));
			for(int j = 0; j < arr.size(); j++){//一个就是一个航班
				json = arr.getJSONObject(j);
				transitEntity = PlaneInfoEntityBuilder.buildReturnTransitEntity(json.getString("fltno"), null, 
						json.getString("corp"), json.getString("corpn"), json.getString("corpn"), 
						json.getString("dport"), json.getString("dportn"), json.getString("aport"),
						json.getString("aportn"), json.getString("plane"));
				transitEntity.setStartTime(DateUtil.StringToDate("yyyy/MM/dd HH:mm:ss", json.getString("dtime")));
				transitEntity.setEndTime(DateUtil.StringToDate("yyyy/MM/dd HH:mm:ss", json.getString("atime")));
				transitEntity.setStayTime(Long.parseLong(json.getString("ftime"))*60*1000);
				
				transits.add(transitEntity);
				//这时有经停，但没有找到经停的事例，暂时不记入
			 }
		}finally{
			json = null; 
			transitEntity = null; 
		}
		return transits;
	}
	/**
	 * 获得返程的数据,返程暂不分页
	 * @param table
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("deprecation")
	private List<ReturnDoublePlaneInfoEntity> fetchReturnData(JSONObject flight) throws Exception{
		//统一控制变量
		if(!is_secondLayer_fetch) return null;
		logger.info(String.format("开始获取的返程航班"));
		List<ReturnDoublePlaneInfoEntity> results = new ArrayList<ReturnDoublePlaneInfoEntity>();
		HttpGet httpGet = null;
		String roundHtml = null;
		List<ReturnTransitEntity> transits = null;
		//List<ReturnStopOverEntity> stopOvers = null;
		ReturnDoublePlaneInfoEntity returnDoublePlaneinfo = null;
		net.sf.json.JSONObject jsonObject = null;
		JSONObject obj = null;
		net.sf.json.JSONArray flightArray = null;

		for(int count =0 ;count < secondLayer_fail_retry_times; count++){ //fail_retry_times次获取不成功，则退出
			if(!adapter.isUseProxyip()) Thread.sleep(5000);
			try{
				httpGet = this.getFirstYudingPageUrl(flight);
				roundHtml = adapter.excuteRequest(httpGet);
				adapter.appendPageContents(roundHtml);
				this.validateHtml(roundHtml);
				//FileUtils.writeStringToFile(new File("E:\\mytemp\\elong-back.html"), roundHtml);
				roundHtml = RegHtmlUtil.regStr(roundHtml, "<script>bigpipe.set\\('flights',(.*)\\)</script></body></html>");
				
				//根据三个舱位信息请求三次,经济Y，商务C，头等F ,如果三个舱位信息都没有航班数据，则返回没有航班异常
				//1、根据查询条件直接进入到去程航班列表(默认是经济舱的,代码是Y)
				//2、循环取多页
				jsonObject = JSONObject.fromObject(roundHtml);
				flightArray = jsonObject.getJSONArray("FlightLegList");
				
				logger.info(String.format("获得的返程航班数量：%s",flightArray.size()));
				for(int i = 0 ;i < flightArray.size(); i++){
					 obj = flightArray.getJSONObject(i);
					 logger.info(String.format("获得的返程航空公司[%s]税费[%s]", obj.getString("stpCountry"),obj.getString("tax")));
					 transits = this.parseBackTrantsInfo(obj.getJSONArray("segs"));
					 returnDoublePlaneinfo = PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
							 transits.get(0).getCarrierKey(), transits.get(0).getCarrierName(), transits.get(0).getCarrierFullName(), 
								null, null, transits.get(0).getFlightNo(),
								null, null, null, transits.get(0).getFlightType(), ReturnDoublePlaneInfoEntity.class);
					 if(transits.size() > 1) //只有多个时，才算中转，中转对象才有值
						 returnDoublePlaneinfo.setReturnTransits((ArrayList)transits);
					 returnDoublePlaneinfo.setStartTime(transits.get(0).getStartTime());
					 returnDoublePlaneinfo.setEndTime(transits.get(transits.size()-1).getEndTime());
					long flightDuration = 0;
					for(ReturnTransitEntity e : transits){//累加时长，所有中转时长加起来就是总的去程时长
						flightDuration += e.getStayTime();
					}
					returnDoublePlaneinfo.setFlightDuration(flightDuration);
					//设置最低价
					//获得价格
					returnDoublePlaneinfo.setSumLowestPrice(obj.getJSONArray("cabs").getJSONObject(0).getDouble("price"));
					returnDoublePlaneinfo.setTotalLowestTaxesPrice(Double.valueOf(obj.getString("tax")));
	
					results.add(returnDoublePlaneinfo);
				}
				break; //成功后，即要退出循环
			}catch(Exception e){
				if(e instanceof FlightInfoNotFoundException) break;
				logger.error(e.getMessage());
				e.printStackTrace();
				adapter.switchProxyipByHttClient(); //抛出异常则切换IP
			}finally{
				httpGet = null;
				roundHtml = null;
				transits = null;
				returnDoublePlaneinfo = null;
			}
		}
		return results;
	}

	/**
	 * 组装出指定格式的航班字符串，形式如：OS434,CA987
	 * @param flight
	 * @return
	 */
	private String assembledFlightNo(JSONObject flight){
		String flightNo = "";
		for(int i = 0; i < flight.getJSONArray("segs").size(); i++){
			flightNo += flight.getJSONArray("segs").getJSONObject(i).getString("fltno")+",";
		}
		return flightNo.substring(0, flightNo.length()-1);
	}
	/**
	 * 获得“选此去程”的cacheKey，只有获得这个值后才能进入到返程列表页面
	 * @param flight
	 * @return
	 * @throws Exception 
	 */
	private String getYudingCacheKey(JSONObject flight) throws Exception{
		//http://iflight.elong.com/isajax/FlightListSearch/BookingOrder?unikey=0OS064SOS071S0OS072LOS063L08980&channel=BSP
		//&pageName=golist&flightNo=OS064,OS071&classNo=S,S&cacheKey=&price=08980&journeyno=
		//http://iflight.elong.com/isajax/FlightListSearch/BookingOrder?FlightType=1&FlightTypeName=null
		//&StartCity=BJS&StartCityName=%E5%8C%97%E4%BA%AC&EndCity=YTO&
		//EndCityName=%E5%A4%9A%E4%BC%A6%E5%A4%9A&StartDate=2014%2F8%2F23&EndDate=2014%2F8%2F30
		//&PassengerCount=1&ChildCount=0&SeatLevel=Y&SeatLevelName=null&AirCorp=All
		//&AirCorpNameCn=null&AirCorpNameEn=null&AdultType=0&AdultTypeName=null
		//&CampaignID=null&MultiRequestInfo=[object%20Object]&TRequestInfo=null&OrderBy=0
		//&SearchChannel=6&&&unikey=0OS064SOS071S0OS072LOS063L08980&channel=BSP&pageName=golist
		//&flightNo=OS064,OS071&classNo=S,S&cacheKey=&price=8980&journeyno=
		StringBuilder url = new StringBuilder("http://iflight.elong.com/isajax/FlightListSearch/BookingOrder?");
//		for(String key : queryParams.keySet()){
//			url.append(key).append("=").append(queryParams.get(key)).append("&");
//		}
		url.append("FlightType=1");
		url.append("&FlightTypeName=null");
		url.append("&StartCity=").append(queryParams.get("StartCity"));
		url.append("&StartCityName=").append(queryParams.get("StartCityName"));
		url.append("&EndCity=").append(queryParams.get("EndCity"));
		url.append("&EndCityName=").append(queryParams.get("EndCityName"));
		url.append("&StartDate=").append(taskQueue.getFlightDate().replaceAll("-", "/"));
		url.append("&EndDate=").append(taskQueue.getReturnGrabDate().replaceAll("-", "/"));
		url.append("&PassengerCount=1").append("&ChildCount=0").append("&SeatLevel=").append(queryParams.get("SeatLevel"));
		url.append("&SeatLevelName=null").append("&AirCorp=All").append("&AirCorpNameCn=null").append("&AirCorpNameEn=null");
		url.append("&AdultType=0").append("&AdultTypeName=null").append("&CampaignID=null").append("&MultiRequestInfo=[object%20Object]");
		url.append("&TRequestInfo=null").append("&OrderBy=0");
		url.append("&SearchChannel=").append(queryParams.get("SearchChannel"));
		
		url.append("&unikey=").append(flight.getJSONArray("cabs").getJSONObject(0).getString("unikey"));
		url.append("&channel=").append(flight.getJSONArray("cabs").getJSONObject(0).getString("chl"));
		url.append("&pageName=").append("golist");
		url.append("&flightNo=").append(this.assembledFlightNo(flight));
		url.append("&classNo=").append(flight.getJSONArray("cabs").getJSONObject(0).getString("cab"));
		url.append("&cacheKey=").append("");
		url.append("&price=").append(flight.getJSONArray("cabs").getJSONObject(0).getString("price"));
		url.append("&journeyno=").append("");
		HttpGet get = null;
		String retV = null;
		JSONObject bookObj = null;
		try{
		    get = new HttpGet(url.toString());
		    this.setRequestHeadInfo(get);
		     retV = adapter.excuteRequest(get);
		     bookObj = JSONObject.fromObject(retV);
		     if(bookObj.getString("value").contains("the price is sold out."))
		    	 throw new FlightInfoNotFoundException();
		     return bookObj.getString("value");
		}finally{
			 get = null;
			 retV = null;
			 bookObj = null;
		}
	}
	/**
	 * 获得去程页面上的"选此去程"按钮URL
	 * @param table
	 * @return
	 * @throws Exception
	 */
	private HttpGet getFirstYudingPageUrl(JSONObject flight) throws Exception{
		//http://iflight.elong.com/search/cn_backlist_NH956,NH010_BSP_379605759938967069.html
		StringBuilder url = new StringBuilder("http://iflight.elong.com/search/cn_backlist_");
		 HttpGet get = null;
		try{
			url.append(this.assembledFlightNo(flight)).append("_")
			.append(flight.getJSONArray("cabs").getJSONObject(0).getString("chl"))
			.append("_").append(this.getYudingCacheKey(flight)).append(".html");
	        logger.info(String.format("组装出的去程航班预订按钮URL：%s", url.toString()));
	        get = new HttpGet(url.toString());
			return get;
		}finally{
			url = null;
		}
	}
	/**
	 * 构建公共的查询条件
	 * @param f
	 * @throws Exception
	 */
	private void buildFormParams(JSONObject object)throws Exception{
		JSONObject request = object.getJSONObject("Request");
		for(Object key : request.keySet()){
			if(!"MultiRequestInfo".equalsIgnoreCase(key.toString()))
				queryParams.put(key.toString(), request.getString(key.toString()));
		}
	}
	//获得舱位的数据
	private List<DoublePlaneInfoEntity> fetchSingleCabinType(String cabinType) throws Exception{
		String html = null;
		String url = null;
		try{
			//通过页面请求组装成请求的URL字符串
				url = buildEnterUrl()+"&classType="+cabinType;
				logger.info(String.format("获得舱位[%s]的数据URL[%s]",cabinType, url));
				html = this.requestHtmlPage(url);
				adapter.appendPageContents(html);
				return this.parseGoFlightListData(html, cabinType);
		}finally{
			url = null;
			html = null;
		}
	}
	/**
	 * 通过URL调用适配器通用请求方法，取得响应页面集
	 * @param url
	 * @return
	 * @throws Exception
	 */
	private String requestHtmlPage(String url) throws Exception{
		HttpGet g = null;
		String html = null;
		try{
			//通过页面请求组装成请求的URL字符串
				g = new HttpGet(url);
				setRequestHeadInfo(g);
			
				html = adapter.excuteRequest(httpClient, g, true);
				this.validateHtml(html);
		}finally{
			g = null;
			url = null;
		}
		return html;
	}
	/**
	 * 组装去程模型
	 * @param html
	 * @param cabinType
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private List<DoublePlaneInfoEntity> parseGoFlightListData(String html,String cabinType) throws Exception{
		List<DoublePlaneInfoEntity> results = new ArrayList<DoublePlaneInfoEntity>();
		//保证第一个页面的数据正确入库,解析出数据
		DoublePlaneInfoEntity doublePlaneinfo = null;
		List<TransitEntity> transits = null;
		//List<StopOverEntity> stopOvers = null;
		List<ReturnDoublePlaneInfoEntity> returnList = null;
		net.sf.json.JSONObject jsonObject = null;
		JSONObject obj = null;
		net.sf.json.JSONArray flightArray = null;
		try{
			String jsonStr = RegHtmlUtil.regStr(html, "<script>bigpipe.set\\('flights',(.*)\\)</script></body></html>");
			//FileUtils.writeStringToFile(new File("E:\\mytemp\\elong.html"), jsonStr);
			//根据三个舱位信息请求三次,经济Y，商务C，头等F ,如果三个舱位信息都没有航班数据，则返回没有航班异常
			//1、根据查询条件直接进入到去程航班列表(默认是经济舱的,代码是Y)
			//2、循环取多页
			jsonObject = JSONObject.fromObject(jsonStr);
			flightArray = jsonObject.getJSONArray("FlightLegList");
			if(this.queryParams.isEmpty()) this.buildFormParams(jsonObject); //因为会用到公共的查询条件，所以这个单独放置出来
			logger.info(String.format("获得的去程航班数量：%s",flightArray.size()));
			for(int i = 0 ;i < flightArray.size(); i++){
				 obj = flightArray.getJSONObject(i);
				 logger.info(String.format("获得的去程航空公司[%s]税费[%s]", obj.getString("stpCountry"),obj.getString("tax")));
				 transits = this.parseGoTrantsInfo(obj.getJSONArray("segs"));
				 doublePlaneinfo = PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, 
						 transits.get(0).getCarrierKey(), transits.get(0).getCarrierName(), transits.get(0).getCarrierFullName(), 
							null, null, transits.get(0).getFlightNo(),
							null, null, null, transits.get(0).getFlightType(), DoublePlaneInfoEntity.class);
				 doublePlaneinfo.setFromCityName(taskQueue.getFromCityName().split("\\|")[0]);
				 doublePlaneinfo.setToCityName(taskQueue.getToCityName().split("\\|")[0]);
				 doublePlaneinfo.setStartTime(transits.get(0).getStartTime());
				doublePlaneinfo.setEndTime(transits.get(transits.size()-1).getEndTime());
				long flightDuration = 0;
				for(TransitEntity e : transits){//累加时长，所有中转时长加起来就是总的去程时长
					flightDuration += e.getStayTime();
				}
				doublePlaneinfo.setFlightDuration(flightDuration);
				//设置最低价
				//获得价格
				doublePlaneinfo.setSumLowestPrice(obj.getJSONArray("cabs").getJSONObject(0).getDouble("price"));
				doublePlaneinfo.setTotalLowestTaxesPrice(Double.valueOf(obj.getString("tax")));
				//获取返程数据,点击预订按钮
				//3、循环去程航班列表，一个一个取航班返程（点预订按钮）,得到的是一个对应于去程航班的返程航班列表
				returnList = this.fetchReturnData(obj);
				if(returnList != null && !returnList.isEmpty()) doublePlaneinfo.getReturnPlaneInfos().addAll(returnList);
				
				this.compareHightestLowestPrice(doublePlaneinfo);
				
				results.add(doublePlaneinfo);
			}
		}finally{
			 doublePlaneinfo = null;
			 transits = null;
		}
		return results;
	}
	/**
	 * 验证抓取到的html是否有效
	 * @param html
	 * @throws Exception
	 */
	private void validateHtml(String html) throws Exception{
		if(html==null){
			throw new SourceWebPageNullException("网页数据为空");
		}
		if(html.length() < 5000){
			throw new SourceDataParseNullException("网页数据错误!");
		}
		html=html.replaceAll("\\s", "");
		if(html.matches(".*往返机票暂时无法进行网上查询和预订.*")){
			throw new FlightInfoNotFoundException();
		}
	}
	/**
	 * 构建需要的请求URL
	 */
	private String buildEnterUrl()throws Exception{
		StringBuilder sbuilder = new StringBuilder("http://iflight.elong.com/");
		sbuilder.append(fromPinyin);
		sbuilder.append("-");
		sbuilder.append(toPinyin);
		sbuilder.append("/round/cn_day"+
				ElongDataUtils.betweenDay(taskQueue.getFlightDate())+"_day"+
				ElongDataUtils.betweenDay(taskQueue.getReturnGrabDate())+".html?");
		sbuilder.append("direct=0");
		sbuilder.append("adultType=0");
		sbuilder.append("psgCount=1");
		sbuilder.append("childCount=0");
		return sbuilder.toString();
	}


	/**
	 * 设置的请求的头部
	 */
	private void setRequestHeadInfo(HttpRequestBase request)throws Exception{
		Map<String,String> map=new HashMap<String, String>();
		map.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		map.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		map.put("Host", "iflight.elong.com");
		map.put("Referer", "http://iflight.elong.com/");
		map.put("User-Agent", browserString);
		adapter.setRequestHead(request, map);
	}
	
	/**
	 * 比较一下得出最低价
	 * @param list
	 */
	private void compareHightestLowestPrice(DoublePlaneInfoEntity entity){
		if(entity != null){
			Double hightestPrice = 0d;
			Double lowestPrice = 10000000000d;

				if(entity.getReturnPlaneInfos()!=null){
					for(ReturnDoublePlaneInfoEntity re : entity.getReturnPlaneInfos()){
						if(re.getSumLowestPrice() != null){
							if(hightestPrice < re.getSumLowestPrice())
								hightestPrice = re.getSumLowestPrice();
							if(lowestPrice > re.getSumLowestPrice())
								lowestPrice = re.getSumLowestPrice();
						}
					}
				}
				if(hightestPrice > 0) entity.setSumHighestPrice(hightestPrice);
				if(lowestPrice < 10000000000d) entity.setSumLowestPrice(lowestPrice);
		}
	}
}
