package com.foreveross.crawl.adapter.sub.impl20140402.v3.airfrance;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.taskservice.common.bean.TaskModel;


/**
 * 法国航空常工具方法
 * 
 * @author fb
 */
@SuppressWarnings("deprecation")
public class AirfranceUtils {
	
	private final static Logger logger = LoggerFactory.getLogger(AirfranceUtils.class);


	/**
	 * 去程基本信息数据组装
	 * 
	 * @param bound
	 */
	public static DoublePlaneInfoEntity flightBaseInfoAssembly(List<Map<String, String>> bounds, TaskModel taskQueue, String cabinTypeLabe) {
		DoublePlaneInfoEntity dpie = null;
		// 去程第一班
		Map<String, String> bound = bounds.get(0);
		String company = bound.get("company");
		String duration = bound.get("duration");
		String fromDate = bound.get("fromDate").replaceAll("\\s", "");
		String toDate = bounds.get(bounds.size() - 1).get("toDate").replaceAll("\\s", "");
		String flightNo = bound.get("flightNo");
		
		try {
			dpie = PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, company, company, company, fromDate, toDate, flightNo, null, null, null, null, DoublePlaneInfoEntity.class);
			dpie.setCurrency("CNY");
			dpie.setFlightDuration(timeParse(duration));
			dpie.getTransits().addAll(transitAssembly(bounds, taskQueue.getFlightDate(), cabinTypeLabe) );
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return dpie;

	}
	

	
	/**
	 * 回程基本信息数据组装
	 * 
	 * @param bound
	 */
	public static ReturnDoublePlaneInfoEntity flightReturnBaseInfoAssembly(List<Map<String, String>> bounds, TaskModel taskQueue, String cabinTypeLabe) {
		ReturnDoublePlaneInfoEntity rdpie = null;
		Map<String, String> bound = bounds.get(0);
		String company = bound.get("company");
		String duration = bound.get("duration");
		String fromDate = bound.get("fromDate").replaceAll("\\s", "");
		String toDate = bounds.get(bounds.size() - 1).get("toDate").replaceAll("\\s", "");
		String flightNo = bound.get("flightNo");
		
		try {
			rdpie = PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, company, company, company, fromDate, toDate, flightNo, null, null, null, null, ReturnDoublePlaneInfoEntity.class);
			rdpie.setCurrency("CNY");
			rdpie.setFlightDuration(timeParse(duration));
			rdpie.getReturnTransits().addAll(returnTransitAssembly(bounds, taskQueue.getReturnGrabDate(), cabinTypeLabe) );
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return rdpie;
	}
	
	public static <T extends BaseHBaseEntity> List<TransitEntity> transitAssembly(List<Map<String, String>> bounds, String currDate, String cabinTypeLabe) {
		List<TransitEntity> tes = new ArrayList<TransitEntity>();
		TransitEntity te = null;
		TransitEntity frist = null;
		TransitEntity second = null;
		
		// 航段小于2的没有中转信息
		if (bounds.size() < 2) {
			return tes;
		}
		
		try {
			for (Map<String, String> map : bounds) {
				te = PlaneInfoEntityBuilder.buildTransitEntity(map.get("flightNo"), null, map.get("company"), map.get("company"), map.get("company"), null, map.get("fromAddress"), null, map.get("toAddress"), null, TransitEntity.class);
				te.setStartTime(PlaneInfoEntityBuilder.getFlightTime(currDate, map.get("fromDate").replaceAll("\\s", "")));
				te.setEndTime(PlaneInfoEntityBuilder.getFlightTime(currDate, map.get("toDate").replaceAll("\\s", "")));
				te.setCabinName(cabinTypeLabe);
				tes.add(te);
			}
		} catch (Exception e) {
			logger.error("中转装配异常", e);
		}
		
		// 计算中转时间
		for(int i = 0; i < tes.size(); i++) {
			if (i == tes.size() -1) {
				break;
			}
			
			frist = tes.get(i);
			second = tes.get(i + 1);
			frist.setStayTime(second.getStartTime().getTime() - frist.getEndTime().getTime());
		}
		
		return tes;
	}
	
	public static List<ReturnTransitEntity> returnTransitAssembly(List<Map<String, String>> bounds, String currDate, String cabinTypeLabe) {
		List<ReturnTransitEntity> tes = new ArrayList<ReturnTransitEntity>();
		ReturnTransitEntity te = null;
		ReturnTransitEntity frist = null;
		ReturnTransitEntity second = null;
		
		// 航段小于2的没有中转信息
		if (bounds.size() < 2) {
			return tes;
		}
		
		try {
			for (Map<String, String> map : bounds) {
				te = PlaneInfoEntityBuilder.buildTransitEntity(map.get("flightNo"), null, map.get("company"), map.get("company"), map.get("company"), null, map.get("fromAddress"), null, map.get("toAddress"), null, ReturnTransitEntity.class);
				te.setStartTime(PlaneInfoEntityBuilder.getFlightTime(currDate, map.get("fromDate").replaceAll("\\s", "")));
				te.setEndTime(PlaneInfoEntityBuilder.getFlightTime(currDate, map.get("toDate").replaceAll("\\s", "")));
				te.setCabinName(cabinTypeLabe);
				tes.add(te);
			}
		} catch (Exception e) {
			logger.error("中转装配异常", e);
		}
		
		// 计算中转时间
		for(int i = 0; i < tes.size(); i++) {
			if (i == tes.size() -1) {
				break;
			}
			
			frist = tes.get(i);
			second = tes.get(i + 1);
			frist.setStayTime(second.getStartTime().getTime() - frist.getEndTime().getTime());
		}
		
		return tes;
	}
	
	/**
	 * 仓位数据组装
	 * 
	 * @param bounds
	 * @param cabins
	 * @param clazz
	 * @param cabinTypeLable
	 */
	public static <T extends BaseHBaseEntity> Set<T> cabinInfoAssembly(List<Map<String, String>> bounds , Set<T> cabins, Class<T> clazz, String cabinTypeLable) {
		Map<String, String> bound = bounds.get(0);
		Set<T> returnCabins = new HashSet<T>();
		String priceOne = bound.get("priceOne");
		String priceTwo = bound.get("priceTwo");
		T cabin = null;
		
		try {
			if (StringUtils.isNotBlank(priceOne)) {
				priceOne = priceOne.replaceAll("[^0-9]", "");
				cabin = PlaneInfoEntityBuilder.buildCabinInfo(cabinTypeLable, cabinTypeLable, null, priceOne, null, null, null, clazz);
				returnCabins.add(cabin);
				cabins.add(cabin);
			}
			
			if (StringUtils.isNotBlank(priceTwo)) {
				priceTwo = priceTwo.replaceAll("[^0-9]", "");
				cabinTypeLable = "尊尚" + cabinTypeLable;
				cabin = PlaneInfoEntityBuilder.buildCabinInfo(cabinTypeLable, cabinTypeLable, null, priceTwo, null, null, null, clazz);
				returnCabins.add(cabin);
				cabins.add(cabin);
			}
		} catch (Exception e) {
			logger.error("仓位组装异常：", e);
		}
		
		return returnCabins;
	}
	
	/**
	 * 时间格式化
	 * 
	 * @param date  xx小时xx分钟等这样格式 
	 * @return
	 */
	public static long timeParse(String date) {
		long time = 0;
		List<Integer> dates = new ArrayList<Integer>();
		Pattern pattern = Pattern.compile("(\\d{1,})");
		Matcher matcher = pattern.matcher(date);
		
		while (matcher.find()) {
			dates.add(Integer.parseInt(matcher.group()));
		}
		
		if (dates.size() == 2) {
			time += dates.get(0) * 60 * 60 * 1000;
			time += dates.get(1) * 60 * 1000;
		} else {
			time += dates.get(0) * 60 * 1000;
		}
		
		return time;
	}
	
}
