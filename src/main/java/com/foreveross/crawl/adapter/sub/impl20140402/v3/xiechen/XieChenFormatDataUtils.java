package com.foreveross.crawl.adapter.sub.impl20140402.v3.xiechen;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

public class XieChenFormatDataUtils {

	private static Map<String,Integer> numberMap=null;
	static{
		numberMap=new HashMap<String, Integer>();
		numberMap.put("一", 1);
		numberMap.put("二", 2);
		numberMap.put("三", 3);
		numberMap.put("四", 4);
		numberMap.put("五", 5);
		numberMap.put("六", 6);
		numberMap.put("七", 7);
		numberMap.put("八", 8);
		numberMap.put("九", 9);
		numberMap.put("十", 10);
		numberMap.put("十一", 11);
		numberMap.put("十二", 12);
	}
	public final static String[] WEEK_SHORT_NAMES = new String[]{"sun","mon","tue","wed","thur","fri","sat"};
	/**
	 * 获得指定日期的周简称
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static String getWeekNameByDate(String date) throws ParseException{
		Date d = DateUtils.parseDate(date, "yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int w = cal.get(Calendar.DAY_OF_WEEK);
		return WEEK_SHORT_NAMES[w-1];
	}
	/**
	 * 格化化特殊的日期,格式如：23  十月  2014 11:26
	 * @param str
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public static  Date formatCNDate(String str) throws IOException, ParseException{
		String[] tmpArr=str.split("[ ]+");
		Integer aInteger=numberMap.get(tmpArr[1].replaceAll("月", "").trim());
		String dateStr=tmpArr[2]+"-"+aInteger+"-"+tmpArr[0]+" "+tmpArr[3];
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date=format.parse(dateStr);
		return date;
	}
	/**
	 * 将xxh:xxm 格式转换成时长，毫秒
	 * @param time
	 * @return
	 */
	public static Long formatTimeInterval(String time){
		if(StringUtils.isNotBlank(time)){
			time = time.replaceAll(":", "").trim();
			long ss = 0;
			if(time.indexOf("h")>-1){
				ss = Integer.parseInt(time.substring(0, time.indexOf("h")))*60*60*1000;
				time = time.substring(time.indexOf("h")+1);
			}
			if(time.indexOf("m")>-1){
				ss += Integer.parseInt(time.substring(0, time.indexOf("m")))*60*1000;
			}
			return ss;
		}
		return null;
	}
	/**
	 * 将页面上的价格转成double类型，格式如:5,345
	 * @param price
	 * @return
	 */
	public static Double formatPrice(String price){
		if(StringUtils.isNotBlank(price)){
			return Double.parseDouble(price.replaceAll(",", ""));
		}
		return null;
	}
	/**
	 * 将格式时间转换为毫秒，格式如:7h25m
	 * @param time
	 * @return
	 */
	public static Long formatStayTime(String time){
		if(time == null) return null;
		String temp = time.replace("m", "");
		String[] ss = temp.split("h");
		return Long.valueOf(ss[0])*60*60*1000+Long.valueOf(ss[1])*60*1000;
	}
}
