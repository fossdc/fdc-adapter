package com.foreveross.crawl.adapter.sub.impl20140402.v3.tianxun;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.hadoop.hbase.util.Strings;
import org.drools.conf.MaxThreadsOption;

import com.foreveross.taskservice.common.bean.TaskModel;
import com.google.common.collect.Maps;

public class TianxunCarrierFilter {

	// 天巡要过滤的航班
	private final static Map<String, String> carriers = Maps.newHashMap();
	static{
		carriers.put("PEKSEA", "HU,UA,DL");
		carriers.put("PEKORD", "HU,UA,AA");
		carriers.put("PEKBOS", "HU,UA,AA,DL");
		carriers.put("PEKYYZ", "HU,AC,DL");
		carriers.put("PEKHNL", "CA,MU,HA,OZ,KE,NH,JL");
		carriers.put("PEKIAD", "CA,UA,KE,NH");
		carriers.put("PEKIAH", "CA,UA,AA");
		carriers.put("PEKJFK", "CA,MU,AA,UA,DL,OZ,KE,NH,JL,CX");
		carriers.put("PEKLAX", "CA,CZ,MU,AA,UA,DL,OZ,KE,NH,JL,CX");
		carriers.put("PEKSFO", "CA,MU,UA,DL,OZ,KE,NH");
		carriers.put("PEKYVR", "CA,MU,AC");
		carriers.put("PEKARN", "CA");
		carriers.put("PEKSVO", "CA,HU,SU");
		carriers.put("PEKMUC", "CA,CZ,EK,SU,LH");
		carriers.put("PEKFRA", "CA,MU,CZ,EK,SU,LH");
		carriers.put("PEKCDG", "CA,MU,CZ,EK,SU,AF");
		carriers.put("PEKLHR", "CA,MU,CZ,EK,SU");
		carriers.put("PVGFRA", "CA,MU,EK,SU,LH");
		carriers.put("PVGCDG", "CA,MU,EK,SU,AF");
		carriers.put("PEKHKG", "CA,CZ,HU,HX,CX");
	}

	/**
	 * 去程或回程中的运营公司是否在需求成功列表中。
	 * 
	 * @param model
	 * @param outkey
	 * @param inkey
	 * @param flights
	 * @return
	 */
	public static Boolean checkCarrier(TaskModel model, String outkey, String inkey, JSONObject flights) {
		try {
			String needCarriers = carriers.get(model.getFromCity() + model.getToCity());
			if (Strings.isEmpty(needCarriers)) return Boolean.FALSE; // 需求中没有记录，不抓

			Map<String, JSONObject> outbounds = TianxunAdapterUtil.jsonToMap(flights.getJSONArray("OutboundItineraryLegs"));
			Map<String, JSONObject> inbounds = TianxunAdapterUtil.jsonToMap(flights.getJSONArray("InboundItineraryLegs"));

			// 去程或回程的运营都由指定公司
			return checkCarrier(outbounds.get(outkey), needCarriers) && checkCarrier(inbounds.get(inkey), needCarriers);
		} catch (Exception e) {
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	/**
	 * 去程或回程中是否有三段中转信息。
	 * 
	 * @param model
	 * @param outkey
	 * @param inkey
	 * @param flights
	 * @param maxTransit
	 * @return
	 */
	public static Boolean checkMultiTransit(TaskModel model, String outkey, String inkey, JSONObject flights, Integer maxTransit) {
		try {
			Map<String, JSONObject> outbounds = TianxunAdapterUtil.jsonToMap(flights.getJSONArray("OutboundItineraryLegs"));
			Map<String, JSONObject> inbounds = TianxunAdapterUtil.jsonToMap(flights.getJSONArray("InboundItineraryLegs"));
			// 去程或回程中转超过2个
			return checkTransit(outbounds.get(outkey), maxTransit) && checkTransit(inbounds.get(inkey), maxTransit);
		} catch (Exception e) {
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	private static Boolean checkCarrier(JSONObject bound, String needCarriers) {
		List<String> carrierIds = TianxunAdapterUtil.jsonToStrList(bound.getJSONArray("MarketingCarrierIds"));//OperatingCarrierIds
		if (carrierIds.size() > 1) return Boolean.FALSE;
		if (needCarriers.indexOf(carrierIds.get(0)) != -1) { // 检查需要的航空
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}

	private static Boolean checkTransit(JSONObject json, Integer maxTransit) {
		Integer stopsSize = json.getInt("StopsCount");
		if (stopsSize <= maxTransit) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
