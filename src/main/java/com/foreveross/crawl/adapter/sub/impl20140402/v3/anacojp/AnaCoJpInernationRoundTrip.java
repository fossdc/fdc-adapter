package com.foreveross.crawl.adapter.sub.impl20140402.v3.anacojp;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.AnaCoJpAdapter;
import com.foreveross.crawl.common.exception.self.SourceWebPageNullException;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.crawl.util.DateUtil;
import com.foreveross.taskservice.common.bean.TaskModel;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

/**
 * 全日航空国际往返
 * 因为需要执行js，所以选择WebClient技术
 * @author luofangyi
 *
 */
public class AnaCoJpInernationRoundTrip {

	private AnaCoJpAdapter anaAdapter;
	private TaskModel taskQueue;
	private WebClient webClient;
	public Map<String, String> cityMap = new HashMap<String, String>();
	
	public AnaCoJpInernationRoundTrip(AnaCoJpAdapter anaAdapter, TaskModel taskQueue){
		
		this.anaAdapter = anaAdapter; 
		this.taskQueue = taskQueue;
		this.webClient = anaAdapter.getWebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setAppletEnabled(false);
		webClient.getCookieManager().setCookiesEnabled(false);
	}

	public List<DoublePlaneInfoEntity> fetchData() throws Exception{
		
		HtmlPage homePage = webClient.getPage("https://www.ana.co.jp/asw/wws/cn/c/");
		HtmlForm form = homePage.getFormByName("segConditionForm");
		String url = "https://aswbe-i.ana.co.jp/p_per/sky_ip_per_jp/preReSearchRoundtripResult.do";
		HtmlPage researchPage = webClient.getPage(anaAdapter.getWebRequest(url, HttpMethod.POST, spliteResearchParams(form)));
		anaAdapter.appendPageContents(researchPage.asXml());
		HtmlForm form1 = researchPage.getForms().get(1);
		HtmlInput btnInput = null;
		try {
			btnInput = form1.getInputByName("buttons.toNextConfirmButton");
		} catch (Exception e) {
			throw new FlightInfoNotFoundException("没有找到对应的航班信息!");
		}
		HtmlPage detailPage = btnInput.click();
		anaAdapter.appendPageContents(detailPage.asXml());
		return parseRearchPage(detailPage);
	}
	
	private List<DoublePlaneInfoEntity> parseRearchPage(HtmlPage page) throws Exception{
		String flightNo = null;
		String carrierKey = null;
		String carrierName = null;
		String carrierFullName = null;
		String flightType = null;
		Date startTime = null;//起飞时间 
		Date endTime = null;//到达时间
		String fromAirPort = null;//起飞城市机场码
		String fromAirPortName = null;//飞城市机场名称
		String toAirPort = null;//到达城市机场码
		String toAirPortName = null;//到达城市机场名称
		String cabinType = null;//仓位等级
		String subCabinName = null;//子舱名称
//		Long flightDuration = 0L;//每段的飞行时长
		Long toFlightDuration = 0L;//去程飞行总时长
		Long backFlightDuration = 0L;//回程飞行总时长
		String totalLowestPrice = null;//最低裸价
		String totalLowestTaxesPrice = null;//最低税费价
		String sumLowestPrice = null;//最低合计价
		String lastSeatInfo = null;//剩余舱位信息
		
		Document doc = Jsoup.parse(page.asXml());
		Elements values = doc.select("td.data_title04");
		
		List<FlightDetail> detailList = new ArrayList<FlightDetail>();
		FlightDetail fd = null;
		List<Integer> rowspanList = this.mergeRowspan(page);
		if(rowspanList == null || rowspanList.size() == 0 || values == null || values.size() == 0){
			throw new SourceWebPageNullException("抓取确认内容页面数据异常");
		}
		int currentStep = 0;
		int partTotal = 0;
		for(int j = 0; j < rowspanList.size(); j++){
			int rowspan = rowspanList.get(j);
			partTotal += 11 + (rowspan - 1) * 10;
			int x = 0;
			for(int i = currentStep; i < partTotal; i++){
				Element ele = values.get(i);
				if(x < 11){
					fd = this.parseDetailPage(fd, ele, x, 11);
				} else {
					fd = this.parseDetailPage(fd, ele, x - 11, 10);
				}
				if(x == 10 || (x - 10 > 0 && (x - 10) % 10 == 0)){
    				detailList.add(fd);
    				fd = new FlightDetail();
				}
				x++;
			}
			currentStep += partTotal;
		}
		
		String endFlightNo = null;
		for(int i = 0; i < detailList.size(); i++){
			FlightDetail detail = detailList.get(i);
//			System.out.println(detail.toString());
			if(endFlightNo == null){
				endFlightNo = this.obtainToFlightOfEnd(detail);
			} else {
				break;
			}
		}
		Map<EnumDate, Date> dateMap = this.obtainStartAndEndTimeOfFlightByToAndBack(detailList, endFlightNo);
		Date flightStartTime = dateMap.get(EnumDate.TO_START_TIME);//整个航线飞行的起飞时间
		Date flightEndTime = dateMap.get(EnumDate.TO_END_TIME);//整个航线飞行的结束时间
		
		Date retFlightStartTime = dateMap.get(EnumDate.BACK_START_TIME);//整个航线飞行的起飞时间 - 回程
		Date retFlightEndTime = dateMap.get(EnumDate.BACK_END_TIME);//整个航线飞行的结束时间 - 回程
		
		sumLowestPrice = anaAdapter.getPatternString(page.asXml(), "style=\"font-size:small\">(.+?)</font>").replaceAll("元", "").replaceAll(",", "");
		List<String> prices = anaAdapter.getPatternList(page.asXml(), "合计(.+?)元", ",", "");;
		totalLowestPrice = prices.get(0);
		totalLowestTaxesPrice = prices.size() == 2 ? prices.get(1) : null;
		
		List<DoublePlaneInfoEntity> list = new ArrayList<DoublePlaneInfoEntity>();
		List<ReturnDoublePlaneInfoEntity> retList = new ArrayList<ReturnDoublePlaneInfoEntity>();
		DoublePlaneInfoEntity doubleEntity = null;
		ReturnDoublePlaneInfoEntity retEntity = null;
		boolean isReturn = false;//是否返程
		boolean hasAddGo = false;
		boolean hasAddBack = false;
		
		String toYear = this.taskQueue.getFlightDate().split("-")[0];//任务执行的年份
//		String backYear = this.taskQueue.getReturnGrabDate().split("-")[0];//任务执行的年份
		String year = toYear;
		
		for(int i = 0; i < detailList.size(); i++){
			FlightDetail detail = detailList.get(i);
			flightNo = detail.getFlightNo();
			carrierFullName = (detail.getCarrierFullName() == null || detail.getCarrierFullName().trim() == "" || detail.getCarrierFullName().trim().equals("")) ? "ANA Group" : detail.getCarrierFullName();
			carrierKey = carrierFullName.contains("ANA") ? "NH" : null;
			carrierName = carrierFullName.contains("ANA") ? "全日空航空公司" : null;
			flightType = detail.getFlightType();
			
			String rideDate = detail.getRideDate().split("\\(")[0].replaceAll("/", "-");
			startTime = DateUtil.string2Date(year + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm");//起飞时间 
			String arrTime = detail.getReachTime();
			endTime = obtainEndTime(year, rideDate, arrTime);
			
			String[] cabinInfo = detail.getSubCabinName().split(":");
			cabinType = cabinInfo[0];//仓位等级
			subCabinName = cabinInfo.length == 2 ? cabinInfo[1] : null;//子舱名称
//			flightDuration = anaAdapter.timeToLong(detail.getFightTime());//飞行总时长
			if(!isReturn){
//				toFlightDuration += flightDuration;
				if(doubleEntity == null){
					doubleEntity = PlaneInfoEntityBuilder.buildPlaneInfo(this.taskQueue, carrierKey, carrierName, carrierFullName, null,
							null, flightNo, totalLowestPrice, null, null, flightType, DoublePlaneInfoEntity.class);
					doubleEntity.getCabins().add(PlaneInfoEntityBuilder.buildCabinInfo("经济舱"/*cabinType*/, null/*subCabinName*/, null, null, null, null, null, lastSeatInfo, CabinEntity.class));
					doubleEntity.setStartTime(flightStartTime);
					doubleEntity.setEndTime(flightEndTime);
					doubleEntity.setSumLowestPrice(sumLowestPrice != null ? Double.parseDouble(sumLowestPrice) : null);
					doubleEntity.setTotalLowestPrice(totalLowestPrice != null ? Double.parseDouble(totalLowestPrice) : null);
					doubleEntity.setTotalLowestTaxesPrice(totalLowestTaxesPrice != null ? Double.parseDouble(totalLowestTaxesPrice) : null);
				}
//				doubleEntity.setFlightDuration(toFlightDuration);
				if(detailList.size() / 2d > 1){//包含中转
					TransitEntity transit = PlaneInfoEntityBuilder.buildTransitEntity(flightNo, null, carrierKey, carrierName, carrierFullName, fromAirPort, detail.getFromAirPortName(), toAirPort, detail.getToAirPortName(), flightType, TransitEntity.class);
					transit.setStartTime(startTime);
					transit.setEndTime(endTime);
//					transit.setStayTime(flightDuration);
					transit.setCabinName(subCabinName);
					doubleEntity.getTransits().add(transit);
				}
				
				if(!hasAddGo){
					list.add(doubleEntity);
					hasAddGo = true;
				}
			} else {
//				backFlightDuration += flightDuration;
				if(retEntity == null){
					retEntity = PlaneInfoEntityBuilder.buildPlaneInfo(this.taskQueue, carrierKey, carrierName, carrierFullName, null,
							null, flightNo, totalLowestPrice, null, null, flightType, ReturnDoublePlaneInfoEntity.class);
					retEntity.getReturnCabins().add(PlaneInfoEntityBuilder.buildCabinInfo("经济舱"/*cabinType*/, null/*subCabinName*/, null, null, null, null, null, lastSeatInfo, ReturnCabinEntity.class));
					retEntity.setStartTime(retFlightStartTime);
					retEntity.setEndTime(retFlightEndTime);
					retEntity.setSumLowestPrice(sumLowestPrice != null ? Double.parseDouble(sumLowestPrice) : null);
					retEntity.setTotalLowestPrice(totalLowestPrice != null ? Double.parseDouble(totalLowestPrice) : null);
					retEntity.setTotalLowestTaxesPrice(totalLowestTaxesPrice != null ? Double.parseDouble(totalLowestTaxesPrice) : null);
				}
				retEntity.setFlightDuration(backFlightDuration);
				if(detailList.size() / 2d > 1){//包含中转
					ReturnTransitEntity retTransit = PlaneInfoEntityBuilder.buildReturnTransitEntity(flightNo, null, carrierKey, carrierName, carrierFullName, fromAirPort, detail.getFromAirPortName(), toAirPort, detail.getToAirPortName(), flightType);
					retTransit.setStartTime(startTime);
					retTransit.setEndTime(endTime);
//					retTransit.setStayTime(flightDuration);
					retTransit.setCabinName(subCabinName);
					retEntity.getReturnTransits().add(retTransit);
				}
				
				if(!hasAddBack){
					retList.add(retEntity);
					hasAddBack = true;
				}
			}
			if(endFlightNo == detail.getFlightNo()){
				isReturn = true;
			}
		}
		
		List<DoublePlaneInfoEntity> retDoubleList = anaAdapter.dealToEntityAndReturnEntity(list, retList, sumLowestPrice, totalLowestPrice, totalLowestTaxesPrice);
		return retDoubleList;
	}
	
	/**
	 * 获取去程以及回程的起飞、到达时间
	 * @param detailList
	 * @return
	 * @throws ParseException 
	 */
	public Map<EnumDate, Date> obtainStartAndEndTimeByToAndBack(List<FlightDetail> detailList, String endFilghtNo) throws ParseException{
		
		Map<EnumDate, Date> map = new HashMap<EnumDate, Date>();
		String toYear = this.taskQueue.getFlightDate().split("-")[0];//任务执行的年份
		String backYear = this.taskQueue.getReturnGrabDate().split("-")[0];//任务执行的年份
		String year = toYear;
		Date startTime = null;
		Date endTime = null;
		for(int i = 0; i < detailList.size(); i++){
			FlightDetail detail = detailList.get(i);
			String rideDate = detail.getRideDate().split("\\(")[0].replaceAll("/", "-");
			startTime = DateUtil.string2Date(year + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm");//起飞时间 
			String arrTime = detail.getReachTime();
			endTime = obtainEndTime(year, rideDate, arrTime);
			if(i == 0){
				map.put(EnumDate.TO_START_TIME, startTime);
			}
			if(i == detailList.size() - 1){
				map.put(EnumDate.BACK_END_TIME, endTime);
			}
			if (endFilghtNo == detail.getFlightNo()) {
				map.put(EnumDate.TO_END_TIME, endTime);
				if(i + 1 <= detailList.size()){
					detail = detailList.get(i+1);
					rideDate = detail.getRideDate().split("\\(")[0].replaceAll("/", "-");
					map.put(EnumDate.BACK_START_TIME, DateUtil.string2Date(backYear + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm"));
				} else {
					map.put(EnumDate.BACK_START_TIME, DateUtil.string2Date(backYear + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm"));
				}
			}
		}
		return map;
	}
	
	/**
	 * 得到设置去程、返程第一段的起始时间
	 * @param detailList
	 * @param endFilghtNo
	 * @return
	 * @throws ParseException
	 */
	public Map<EnumDate, Date> obtainStartAndEndTimeOfFlightByToAndBack(List<FlightDetail> detailList, String endFilghtNo) throws ParseException{
		
		Map<EnumDate, Date> map = new HashMap<EnumDate, Date>();
		String toYear = this.taskQueue.getFlightDate().split("-")[0];//任务执行的年份
		String backYear = this.taskQueue.getReturnGrabDate().split("-")[0];//任务执行的年份
		String year = toYear;
		Date startTime = null;
		Date endTime = null;
		for(int i = 0; i < detailList.size(); i++){
			FlightDetail detail = detailList.get(i);
			String rideDate = detail.getRideDate().split("\\(")[0].replaceAll("/", "-");
			startTime = DateUtil.string2Date(year + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm");//起飞时间 
			endTime = obtainEndTime(year, rideDate, detail.getReachTime());
			if(i == 0){
				map.put(EnumDate.TO_START_TIME, startTime);
				map.put(EnumDate.TO_END_TIME, endTime);
			}
			if (endFilghtNo == detail.getFlightNo()) {
				if(i + 1 <= detailList.size()){
					detail = detailList.get(i+1);
					rideDate = detail.getRideDate().split("\\(")[0].replaceAll("/", "-");
					endTime = obtainEndTime(year, rideDate, detail.getReachTime());
					map.put(EnumDate.BACK_START_TIME, DateUtil.string2Date(backYear + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm"));
					map.put(EnumDate.BACK_END_TIME, endTime);
				} else {
					map.put(EnumDate.BACK_START_TIME, DateUtil.string2Date(backYear + "-" + rideDate + " " + detail.getTakeOffTime(), "yyyy-MM-dd HH:mm"));
					map.put(EnumDate.BACK_END_TIME, endTime);
				}
			}
		}
		return map;
	}

	private Date obtainEndTime(String year, String rideDate, String arrTime)
			throws ParseException {
		Date endTime;
		if(!arrTime.contains("天")){
			endTime = DateUtil.string2Date(year + "-" + rideDate + " " + arrTime, "yyyy-MM-dd HH:mm");//到达时间
		} else {
			String[] dayInfos = arrTime.split("天");
			int operatDay = Integer.parseInt(dayInfos[0]);//需要加或减的天数
			arrTime = dayInfos.length == 2 ? dayInfos[1] : null;
			String endTimeStr = DateUtil.addDate(year + "-" + rideDate + " " + arrTime, operatDay);//到达时间
			endTime = DateUtil.string2Date(endTimeStr, "yyyy-MM-dd HH:mm");
		}
		return endTime;
	}
	
	/**
	 * 判断是否回程
	 * @param detail
	 * @return
	 */
	private boolean isReturnFlight(FlightDetail detail){
		//TODO 如果能获得网页抓取的目的地，城市中文名称（toAirPortName），则该方法成立
		if(this.taskQueue.getToCityName().contains(detail.getToAirPortName()) || detail.getToAirPortName().contains(this.taskQueue.getToCityName())){
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * 获取去程最后一段航线的航班号
	 * @param detail
	 * @return
	 */
	private String obtainToFlightOfEnd(FlightDetail detail){
		//TODO 如果能获得网页抓取的目的地，城市中文名称（toAirPortName），则该方法成立
		if(this.taskQueue.getToCityName().contains(detail.getToAirPortName()) || detail.getToAirPortName().contains(this.taskQueue.getToCityName())){
			return detail.getFlightNo();
		} else {
			return null;
		}
	}
	
	private FlightDetail parseDetailPage(FlightDetail fd, Element ele, int current, int step){
		switch(current % step){
		case 0:
			fd = new FlightDetail();
			fd.setRideDate(replaceSpecialCharacters(ele.text()));
			break;
		case 1:
			String regEx = "alt=\"(.+?)";
			if(ele.html().contains("承运")){
				regEx += "承运";
			} else {
				regEx +="\"";
			}
			fd.setFlightNo(replaceSpecialCharacters(ele.text()));
			Element nextElement = ele.nextElementSibling();
			List<String> cityList = anaAdapter.getPatternList(ele.nextElementSibling().html(), "<td width=\"110\">(.+?)</td>", " ", "");
			fd.setFromAirPort(Jsoup.parse(cityList.get(0)).text());
			fd.setToAirPortName(cityList.size() > 1 ? Jsoup.parse(cityList.get(1)).text() : null);
			fd.setCarrierFullName(nextElement.select("td.fnt12px").text());
			break;
		case 2:
			fd.setTakeOffTime(replaceSpecialCharacters(ele.text()));
			break;
		case 3:
			fd.setReachTime(replaceSpecialCharacters(ele.text()));
			break;
		case 4:
			fd.setFlightType(replaceSpecialCharacters(ele.text()));
			break;
		case 6:
			String subCabinName = ele.text();
			if(subCabinName == null || subCabinName.equals("") || "" == subCabinName){
				subCabinName = anaAdapter.getPatternString(ele.html(), "alt=\"(.+?)\"");
			}
			fd.setSubCabinName(replaceSpecialCharacters(subCabinName));
			break;
		case 8:
			if(step == 10) {
				fd.setFightTime(replaceSpecialCharacters(ele.text()));
			}
			break;
		case 9:
			if(step == 11){
				fd.setFightTime(replaceSpecialCharacters(ele.text()));
			}
			break;
		}
		return fd;
	}
	
	private List<Integer> mergeRowspan(HtmlPage page) throws XPatherException{
		
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode root = cleaner.clean(page.asXml());

		String xPath = "//tbody/tr[@bgcolor='#FFFFFF']//td";
		List<Integer> retList = new ArrayList<Integer>();
		Object[] inputs = root.evaluateXPath(xPath);
		for (Object input : inputs) {
			TagNode node = (TagNode) input;
			if (node.hasAttribute("rowspan")) {
				retList.add(Integer.parseInt(node.getAttributeByName("rowspan")));
			}
		}
		return retList;
	}
	
	private String obtainSumLowestPrice(HtmlPage page) throws XPatherException{
		
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode root = cleaner.clean(page.asXml());

		String xPath = "//tr/td[@bgcolor='#FFFFFF']//b/td";
		Object[] inputs = root.evaluateXPath(xPath);
		for (Object input : inputs) {
			TagNode node = (TagNode) input;
			if (node.hasAttribute("style")) {
				if((node.getAttributeByName("rowspan") == "font-size:small"))
				return (String) node.getText();
			}
		}
		return null;
	}
	
	private String replaceSpecialCharacters(String target){
		String dest = "";
        if (target!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(target);
            dest = m.replaceAll("");
        }
        return dest;
	}
	
	/**
	 * 查询页面参数封装
	 * @param form
	 * @return
	 */
	private List<NameValuePair> spliteResearchParams(HtmlForm form){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new NameValuePair("rand", DateUtil.date2String(new Date(), "yyyyMMddHHmmssss")));
		list.add(new NameValuePair("CONNECTION_KIND", "CHN"));
		list.add(new NameValuePair("LANG", "cn"));
		//往返
		list.add(new NameValuePair("intSrch00", form.getInputByName("intSrch00").getValueAttribute()));
		
		list.add(new NameValuePair("depApo", this.taskQueue.getFromCity()));
		list.add(new NameValuePair("arrApo", this.taskQueue.getToCity()));
		list.add(new NameValuePair("DepApoText", this.taskQueue.getFromCityName()));
		list.add(new NameValuePair("ArrApoText", this.taskQueue.getToCityName()));

		String[] flightDate = this.taskQueue.getFlightDate().split("-");
		String[] retGrabDate = this.taskQueue.getReturnGrabDate().split("-");
		list.add(new NameValuePair("wayToMonth", flightDate[1]));
		list.add(new NameValuePair("wayToDay", flightDate[2]));
		list.add(new NameValuePair("wayBackMonth", retGrabDate[1]));
		list.add(new NameValuePair("wayBackDay", retGrabDate[2]));
		
		list.add(new NameValuePair("sortInfo", "2"));//form.getInputByName("sortInfo").getValueAttribute()));
		//成人数
		list.add(new NameValuePair("adultCount", "1"));//form.getSelectByName("adultCount").getDefaultValue()));
		//小孩数
		list.add(new NameValuePair("childCount", "0"));//form.getSelectByName("childCount").getDefaultValue()));
		//婴儿数
		list.add(new NameValuePair("babyCount", "0"));//form.getSelectByName("babyCount").getDefaultValue()));
		//搭乘舱位
		list.add(new NameValuePair("seatKind", "Y"));//form.getSelectByName("seatKind").getDefaultValue()));
		//选择机票类型
		list.add(new NameValuePair("searchOption", "1"));//form.getSelectByName("searchOption").getDefaultValue()));
		//TODO 官网参数还有一个x,y  暂时不知道什么用途  姑且如此设置
		if(webClient.getBrowserVersion().isIE()){
			list.add(new NameValuePair("x", "72"));
			list.add(new NameValuePair("y", "29"));
		} else if(webClient.getBrowserVersion().isChrome()){
			list.add(new NameValuePair("x", "55"));
			list.add(new NameValuePair("y", "25"));
		} else {
			list.add(new NameValuePair("x", "55"));
			list.add(new NameValuePair("y", "21"));
		}
		
		return list;
	}
	
	private List<NameValuePair> spliteDetailParamsByForm0(HtmlForm form){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new NameValuePair("rand", DateUtil.date2String(new Date(), "yyyyMMddHHmmssss")));
		list.add(new NameValuePair("button.var", form.getInputByName("button.var").getValueAttribute()));
		list.add(new NameValuePair("TOKEN", form.getInputByName("TOKEN").getValueAttribute()));
		list.add(new NameValuePair("DISPBEAN_KEY", form.getInputByName("DISPBEAN_KEY").getValueAttribute()));
		list.add(new NameValuePair("selectFareRadio", form.getInputByName("selectFareRadio").getValueAttribute()));
		list.add(new NameValuePair("officeCd", form.getInputByName("officeCd").getValueAttribute()));
		
		return list;
	}
	
	private List<NameValuePair> spliteDetailParamsByForm1(HtmlForm form){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new NameValuePair("selectFlightFromRadio", form.getInputByName("selectFlightFromRadio").getValueAttribute()));
		list.add(new NameValuePair("selectFlightToRadio", form.getInputByName("selectFlightToRadio").getValueAttribute()));
		list.add(new NameValuePair("buttons.toNextConfirmButton", "确定"));
		list.add(new NameValuePair("tmpSelectFlightFromRadio", form.getInputByName("tmpSelectFlightFromRadio").getValueAttribute()));
		list.add(new NameValuePair("tmpSelectFlightToRadio", form.getInputByName("tmpSelectFlightToRadio").getValueAttribute()));
		
		return list;
	}
	
}
