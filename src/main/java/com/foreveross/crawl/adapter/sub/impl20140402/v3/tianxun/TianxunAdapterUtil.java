package com.foreveross.crawl.adapter.sub.impl20140402.v3.tianxun;

import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dayatang.utils.DateUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class TianxunAdapterUtil {

	/** JSON转Map，Key值为Id */
	public static Map<String, JSONObject> jsonToMap(JSONArray jsonArray) {
		Map<String, JSONObject> map = Maps.newHashMap();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			String key = json.get("Id") == null ? i + "" : json.getString("Id");
			map.put(key, json);
		}
		return map;
	}

	/** JSON转换为List */
	public static List<JSONObject> jsonToList(JSONArray jsonArray) {
		List<JSONObject> list = Lists.newArrayList();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			list.add(json);
		}
		return list;
	}

	/** JSON转换为List */
	public static List<String> jsonToStrList(JSONArray jsonArray) {
		List<String> list = Lists.newArrayList();
		for (int i = 0; i < jsonArray.size(); i++) {
			String jsonval = jsonArray.getString(i);
			list.add(jsonval);
		}
		return list;
	}

	/** 日期转换格式如：2014-07-18T13:11:32 */
	public static Date convertDate(String inputDate) {
		if (inputDate == null) return null;
		Date outDate = null;
		inputDate = inputDate.replace("T", " ");
		outDate = DateUtils.parseDateTime(inputDate);
		if (outDate == null) {
			System.err.println(String.format("字符串[%s]转换为日期类型失败.", inputDate));
		}
		return outDate;
	}

}
