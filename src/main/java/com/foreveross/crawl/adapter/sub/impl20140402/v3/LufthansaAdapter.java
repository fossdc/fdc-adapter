package com.foreveross.crawl.adapter.sub.impl20140402.v3;

import com.foreveross.crawl.adapter.AbstractAdapter;
import com.foreveross.crawl.adapter.PlaneInfoEntityBuilder;
import com.foreveross.crawl.common.PlaneInfoCommon;
import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.*;
import com.foreveross.crawl.exception.FlightInfoNotFoundException;
import com.foreveross.taskservice.common.bean.TaskModel;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.util.*;

/**
 * 德国汉莎航空
 * http://www.lufthansa.com
 *
 * @author stub
 */
public class LufthansaAdapter extends AbstractAdapter {

    //查询航班结束时间和中转结束时间，中转城市等信息次数
    private static final int queryMoreTranTimes = 1;

    //private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

    //航班号组-航班实体关系 用于去重航班
    private Map<String, AbstractPlaneInfoEntity> entityMap = new HashMap<String, AbstractPlaneInfoEntity>();

    //回程航班对应的所有舱位标志，如果存在过则不需要添加回程了
    private Map<ReturnDoublePlaneInfoEntity, Map<String, ReturnCabinEntity>> returnCabinsMap = new HashMap<ReturnDoublePlaneInfoEntity, Map<String, ReturnCabinEntity>>();

    private static final String carrierKey = "LH";
    private static final String carrierName = "德国汉莎";
    private static final String carrierFullName = "德国汉莎航空";

    public LufthansaAdapter(TaskModel taskQueue) {
        super(taskQueue);
    }

    @Override
    public String getUrl() throws Exception {
        return null;
    }

    @Override
    public Object fetch(String arg0) throws Exception {
        return fetchRT();
    }

    /**
     * 抓取往返
     *
     * @return
     * @throws Exception
     */
    private Object fetchRT() throws Exception {
        //抓取对应舱位(E:经济舱，N Premium Economy, B:公务舱 ，  F:头等舱)
        JSONObject json = queryCabins();
        JSONArray bookCla = json.getJSONArray("BookingClass");
        //按价格顺序排序舱位，如果抓到有最低价的，高价的舱位抛出异常则忽略
        //String[] array = {"E","N","B","F"};
        String[] array = {"E"};
        int[] tmp = new int[bookCla.size()];
        for (int i = 0; i < bookCla.size(); i++) {
            JSONObject cabinObject = bookCla.getJSONObject(i);
            for (int j = 0; j < array.length; j++) {
                if (array[j].equals(cabinObject.get("code"))) {
                    tmp[i] = j;
                    break;
                }
            }
        }

        JSONObject[] bookinArray = new JSONObject[array.length];
        for (int i = 0; i < tmp.length; i++) {
            bookinArray[tmp[i]] = bookCla.getJSONObject(i);
        }
        List<JSONObject> bookinClass = new ArrayList<JSONObject>();
        for (JSONObject j : bookinArray) {
            if (j != null) {
                bookinClass.add(j);
            }
        }

        String url = "http://book.lufthansa.com/lh/dyn/air-lh/revenue/viewFlights";
        Map<String, String> params = buildParams();
        int noFlight = 0;
        //是否已经抓到最低价格舱位的数据
        boolean hasFetchLow = false;
        for (int i = 0; i < bookinClass.size(); i++) {
            String data = null;
            JSONObject cabinObject = null;
            try {
                cabinObject = bookinClass.get(i);
                params.put("CABIN", cabinObject.getString("code"));//code,name
                logger.info("开始获得舱位" + cabinObject.getString("code"));
                HttpPost p = getBasePost(url, params);
                String s = excuteRequest(p);
                super.appendPageContents(s);
                super.setLenghtCount(s.length());
                data = RegHtmlUtil.regStr(s, "var\\s*?clientSideData\\s*?=(.*?\\})\\s*;");
            } catch (Exception e) {
                //暂时还没发现没航班的关键字
                if (e instanceof FlightInfoNotFoundException) {
                    noFlight++;
                    continue;
                }
                if (!hasFetchLow) {
                    throw e;
                }
                continue;
            }

            try {
                parseRT(JSONObject.fromObject(data), cabinObject);
            } catch (FlightInfoNotFoundException e) {
                noFlight++;
                continue;
            }
            hasFetchLow = true;
        }
        if (noFlight == bookinClass.size()) {
            throw new FlightInfoNotFoundException();
        }
        return entityMap;
    }

    @Override
    public List<Object> paraseToVo(Object arg0) throws Exception {
        Map<String, AbstractPlaneInfoEntity> map = (Map<String, AbstractPlaneInfoEntity>) arg0;
        List<AbstractPlaneInfoEntity> list = PlaneInfoCommon.mapToList(map);
        PlaneInfoEntityBuilder.buildLimitPrice(list);
        return Arrays.asList(list.toArray());
    }

    /**
     * 解析往返
     *
     * @throws Exception
     */
    private void parseRT(JSONObject json, JSONObject cabinNameObj) throws Exception {
        //所有舱位的数据
        JSONObject page = json.getJSONObject("PAGE");
        JSONObject panels = page.getJSONObject("PANELS");
        //航班数据
        JSONObject data = panels.getJSONObject("FLIGHTS").getJSONObject("DATA");
        //价格数据
        JSONArray reco_prices = panels.getJSONObject("PRICE").getJSONObject("DATA").getJSONArray("RECO_PRICES");
        //暂时只发现一个价格对象，如果出现问题再查
        JSONObject priceObjs = reco_prices.getJSONObject(0);
        //去程航班
        JSONObject bound0 = data.getJSONObject("BOUND_0");
        JSONArray goFlights = bound0.getJSONArray("FLIGHTS");
        //回程航班
        JSONObject bound1 = data.getJSONObject("BOUND_1");
        JSONArray returnFlights = bound1.getJSONArray("FLIGHTS");

        JSONObject recommendations = data.getJSONObject("RECOMMENDATIONS");
        JSONArray list_flight = recommendations.getJSONArray("LIST_BOUND").getJSONObject(0).getJSONArray("LIST_FLIGHT");
        //舱位数据（这是多个jsonObject组成的）
        JSONObject fare_families = recommendations.getJSONObject("FARE_FAMILIES");

        Map<String, ReturnDoublePlaneInfoEntity> returnEntitys = new HashMap<String, ReturnDoublePlaneInfoEntity>();

        //组装回程实体
        for (Object object : returnFlights) {
            JSONObject flight = (JSONObject) object;
            int stops = flight.getInt("STOPS");
            JSONArray flightNos = flight.getJSONArray("FLIGHT_NUMBERS");
            JSONArray list_segment = flight.getJSONArray("LIST_SEGMENT");
            ReturnDoublePlaneInfoEntity returnEntity =
                    PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, carrierKey, carrierName, carrierFullName,
                    /*DateUtil.join(taskQueue.getFlightDate(), flight.getString("B_TIME"))*/null,
                            null, flightNos.getString(0), null, null, null, list_segment.getJSONObject(0).getString("EQUIPMENT"), ReturnDoublePlaneInfoEntity.class);
            returnEntity.setFlightDuration(PlaneInfoCommon.getStayTimeWithhMin(flight.getString("DURATION")));

            returnEntity.setStartTime(new Date(((JSONObject) list_segment.get(0)).getLong("DEPARTURE_TIME_MILLIS")));
            returnEntity.setEndTime(new Date(((JSONObject) list_segment.get(list_segment.size() - 1)).getLong("ARRIVAL_TIME_MILLIS")));
            //JSONArray more = queryMoreTransInfo(page, flight);
//			if(more != nulla){
//				JSONObject firstjsonObject = more.getJSONObject(0);
//				JSONObject lastjsonObject = more.getJSONObject(more.size() - 1);
//				returnEntity.setStartTime(new Date(firstjsonObject.getLong("DEPARTURE_TIME_MILLIS")));
//				returnEntity.setEndTime(new Date(lastjsonObject.getLong("ARRIVAL_TIME_MILLIS")));
//			}

            if (stops > 0) {
                for (int i = 0; i < list_segment.size(); i++) {
                    JSONObject tranJson = list_segment.getJSONObject(i);
                    ReturnTransitEntity tran = PlaneInfoEntityBuilder.buildTransitEntity(flightNos.getString(i), flightNos.getString(i), carrierKey, carrierName, carrierFullName,
                            tranJson.getString("B_LOCATION"), null, tranJson.getString("E_LOCATION"),
                            null, tranJson.getString("EQUIPMENT"), ReturnTransitEntity.class);
                    //tran.setStartTime(format.parse(tranJson.getString("B_DATE")));


//					if(more != null){
//						try{
//							JSONObject tranObj = more.getJSONObject(i);
                    tran.setStartTime(new Date(tranJson.getLong("DEPARTURE_TIME_MILLIS")));
                    tran.setEndTime(new Date(tranJson.getLong("ARRIVAL_TIME_MILLIS")));
                    if (i != list_segment.size() - 1) {
                        tran.setStayTime(PlaneInfoCommon.getStayTimeWithhMin(tranJson.getString("LAYOVER")));
                    }
                    tran.setFromAirPortName(tranJson.getString("DEPARTURE_AIRPORT_NAME"));
                    tran.setToAirPortName(tranJson.getString("ARRIVAL_AIRPORT_NAME"));
//						}catch(Exception e){
//							logger.info("增加回程中转失败");
//						}
//					}
                    returnEntity.getReturnTransits().add(tran);
                }
            }//组装回城实体end

            String returnFlight_id = flight.getString("FLIGHT_ID");
            returnEntitys.put(returnFlight_id, returnEntity);
        }

        //组装航班实体
        for (Object object : goFlights) {
            JSONObject flight = (JSONObject) object;
            int stops = flight.getInt("STOPS");
            JSONArray flightNos = flight.getJSONArray("FLIGHT_NUMBERS");
            String flightNoStr = flightNos.toString();
            JSONArray list_segment = flight.getJSONArray("LIST_SEGMENT");
            DoublePlaneInfoEntity entity = null;
            if (entityMap.containsKey(flightNoStr)) {
                entity = (DoublePlaneInfoEntity) entityMap.get(flightNoStr);
            } else {
                entity = PlaneInfoEntityBuilder.buildPlaneInfo(taskQueue, carrierKey, carrierName, carrierFullName,
                        /*DateUtil.join(taskQueue.getFlightDate(), flight.getString("B_TIME"))*/null,
                        null, flightNos.getString(0), null, null, null, list_segment.getJSONObject(0).getString("EQUIPMENT"), DoublePlaneInfoEntity.class);
                entity.setFlightDuration(PlaneInfoCommon.getStayTimeWithhMin(flight.getString("DURATION")));
                entity.setCurrency(recommendations.getString("CURRENCY_CODE"));

                entity.setStartTime(new Date(((JSONObject) list_segment.get(0)).getLong("DEPARTURE_TIME_MILLIS")));
                entity.setEndTime(new Date(((JSONObject) list_segment.get(list_segment.size() - 1)).getLong("ARRIVAL_TIME_MILLIS")));
                //JSONArray more = queryMoreTransInfo(page, flight);
                // DEPARTURE_TIME_MILLIS 出发时间，和到达时间应该都是格林威治时间 0时区，比北京时间晚8个小时
                //ARRIVAL_TIME 到达时间 没有日期的
//				if(more != null){
//					JSONObject firstjsonObject = more.getJSONObject(0);
//					JSONObject lastjsonObject = more.getJSONObject(more.size() - 1);
//					entity.setStartTime(new Date(firstjsonObject.getLong("DEPARTURE_TIME_MILLIS")));
//					entity.setEndTime(new Date(lastjsonObject.getLong("ARRIVAL_TIME_MILLIS")));
//				}
//				//没有航班结束时间，不要
//				if(entity.getEndTime() == null){
//					logger.info("航班无结束时间，拒绝添加");
//					continue;
//				}

                if (stops > 0) {
                    for (int i = 0; i < list_segment.size(); i++) {
                        JSONObject tranJson = list_segment.getJSONObject(i);
                        TransitEntity tran = PlaneInfoEntityBuilder.buildTransitEntity(flightNos.getString(i), flightNos.getString(i), carrierKey, carrierName, carrierFullName,
                                tranJson.getString("B_LOCATION"), null, tranJson.getString("E_LOCATION"), null, tranJson.getString("EQUIPMENT"), TransitEntity.class);
                        //tran.setStartTime(format.parse(tranJson.getString("B_DATE")));
//						if(more != null){
//							try{
//								JSONObject tranObj = more.getJSONObject(i);
                        tran.setStartTime(new Date(tranJson.getLong("DEPARTURE_TIME_MILLIS")));
                        tran.setEndTime(new Date(tranJson.getLong("ARRIVAL_TIME_MILLIS")));
                        if (i != list_segment.size() - 1) {
                            tran.setStayTime(PlaneInfoCommon.getStayTimeWithhMin(tranJson.getString("LAYOVER")));
                        }
                        tran.setFromAirPortName(tranJson.getString("DEPARTURE_AIRPORT_NAME"));
                        tran.setToAirPortName(tranJson.getString("ARRIVAL_AIRPORT_NAME"));
//							}catch(Exception e){
//								logger.info("增加中转失败");
//							}
//						}
                        entity.getTransits().add(tran);
                    }
                }

                entityMap.put(flightNoStr, entity);
            }

            String flight_id = flight.getString("FLIGHT_ID");
            JSONObject flightObj = getObjByFightId(list_flight, flight_id).getJSONObject("FF");
            for (Object f : flightObj.keySet()) {
                //去程舱位标志
                String key = f.toString();
                JSONObject cabinObj = flightObj.getJSONObject(key).getJSONObject("RECOMMENDATIONS");
                String proName = null;
                try {
                    //有可能fare对象没有这个舱位的解析，在去程和回程有，列key上去
                    proName = fare_families.getJSONObject(key).getString("BRAND_NAME");
                } catch (Exception e) {
                    proName = key;
                }
                CabinEntity cabin = PlaneInfoEntityBuilder.buildCabinInfo(cabinNameObj.getString("name"), null, proName,
                        null, null, null, null, null, CabinEntity.class);
                entity.getCabins().add(cabin);

                boolean first = true;
                for (Object r : cabinObj.keySet()) {
                    String returnKey = r.toString();
                    String arr[] = returnKey.split("-");
                    String returnCabinKey = arr[0];
                    String returnFlightId = arr[1];
                    int rid = cabinObj.getJSONObject(returnKey).getInt("RID");
                    //回程航班对象
                    //JSONObject returnFlightObj = getObjByFightId(returnFlights, returnCabinKey);

                    JSONObject priceObject = priceObjs.getJSONObject("RECO_ID_" + rid);
                    if (priceObject.getInt("RID") != rid) {
                        throw new Exception("这个rid有可能不是这么用");
                    }

                    JSONObject adt = priceObject.getJSONObject("PRICE_PER_PAX").getJSONObject("ADT");
                    JSONArray boundPrices = adt.getJSONArray("BOUND_PRICES");
                    JSONObject boundPrice0 = boundPrices.getJSONObject(0);
                    JSONObject boundPrice1 = boundPrices.getJSONObject(1);

                    //只设一次舱位
                    if (first) {
                        cabin.setTaxesPrice(boundPrice0.getDouble("TAX"));
                        cabin.setPrice(boundPrice0.getDouble("TICKET"));
                        cabin.setOriginalPrice(boundPrice0.getDouble("TOTAL"));
                    }
                    first = false;

                    ReturnDoublePlaneInfoEntity returnEntity = returnEntitys.get(returnFlightId);
                    //没有结束时间，不要
                    if (returnEntity.getEndTime() != null) {
                        ReturnDoublePlaneInfoEntity rdEntity = PlaneInfoCommon.getExistReturnEntity(entity, PlaneInfoCommon.getReturnFlightNos(returnEntity));
                        if (rdEntity == null) {
                            entity.getReturnPlaneInfos().add((ReturnDoublePlaneInfoEntity) BeanUtils.cloneBean(returnEntity));// 多个航班不能都引用同一个回程航班的地址，这样后面会有问题。
                        }
                    } else {
                        logger.info("返程没结束时间，忽略添加");
                        continue;
                    }

                    ReturnCabinEntity returnCabin = null;
                    if (!returnCabinsMap.containsKey(returnEntity)) {
                        String returnProName = null;
                        try {
                            returnProName = fare_families.getJSONObject(returnCabinKey).getString("BRAND_NAME");
                        } catch (Exception e) {
                            returnProName = returnCabinKey;
                        }
                        returnCabin = PlaneInfoEntityBuilder.buildCabinInfo(cabinNameObj.getString("name"), null, returnProName,
                                boundPrice1.getString("TAX"), boundPrice1.getString("TICKET"), boundPrice1.getString("TOTAL"), null, null, ReturnCabinEntity.class);
                        returnEntity.getReturnCabins().add(returnCabin);

                        Map<String, ReturnCabinEntity> map = new HashMap<String, ReturnCabinEntity>();
                        map.put(returnCabinKey, returnCabin);
                        returnCabinsMap.put(returnEntity, map);
                    } else {
                        Map<String, ReturnCabinEntity> map = returnCabinsMap.get(returnEntity);
                        if (map.containsKey(returnCabinKey)) {
                            returnCabin = map.get(returnCabinKey);
                        } else {
                            String returnProName = null;
                            try {
                                returnProName = fare_families.getJSONObject(returnCabinKey).getString("BRAND_NAME");
                            } catch (Exception e) {
                                returnProName = returnCabinKey;
                            }
                            returnCabin = PlaneInfoEntityBuilder.buildCabinInfo(cabinNameObj.getString("name"), null, returnProName,
                                    boundPrice1.getString("TAX"), boundPrice1.getString("TICKET"), boundPrice1.getString("TOTAL"), null, null, ReturnCabinEntity.class);
                            returnEntity.getReturnCabins().add(returnCabin);
                            map.put(returnCabinKey, returnCabin);
                        }
                    }

                    CabinRelationEntity relation = new CabinRelationEntity();
                    relation.setCabinId(cabin.getId());
                    relation.setFullPrice(adt.getDouble("TICKET"));
                    relation.setReturnCabinId(returnCabin.getId());
                    relation.setTaxesPrice(adt.getDouble("TAX"));
                    relation.setTotalFullPrice(adt.getDouble("TOTAL"));
                    entity.getCabinRelations().add(relation);
                }
            }
        }
    }

    /**
     * 查询中转更多的信息
     *
     * @param page
     * @param flight
     * @return
     * @throws Exception
     */
    private JSONArray queryMoreTransInfo(JSONObject page, JSONObject flight) throws Exception {
        if (queryMoreTranTimes <= 0) {
            return null;
        }
        JSONObject json = null;
        JSONArray TranObj = null;
        try {
            JSONObject form = page.getJSONObject("PANELS").getJSONObject("FLIGHT_SEARCH").getJSONObject("FORMS").getJSONObject("FLIGHT_SEARCH_FORM");
            String url = form.getString("ACTION");
            //JSONObject parameters = form.getJSONObject("PARAMETERS");
            //JSONArray flightNos = flight.getJSONArray("FLIGHT_NUMBERS");
            JSONArray list_segment = flight.getJSONArray("LIST_SEGMENT");
            JSONArray equipment_icon = flight.getJSONArray("EQUIPMENT_ICON");
            Map<String, String> params = new HashMap<String, String>();
            params.put("COUNTRY_SITE", "CN");
            params.put("LANGUAGE", "CN");
            params.put("OUTPUT_FORMAT", "json");
            params.put("SITE", "LUFTLUFT");
            //params.put("WDS_FLIF_BOUND",flight.getString("BOUND"));
            //params.put("WDS_FLIF_FLIGHT",flight.getString("FLIGHT_ID"));

            for (int i = 1; i <= list_segment.size(); i++) {
                JSONObject segment = list_segment.getJSONObject(i - 1);
                params.put("AIRLINE" + i, segment.getString("AIRLINE_CODE"));
                params.put("B_DATE_" + i, segment.getString("B_DATE"));
                params.put("B_LOCATION_" + i, segment.getString("B_LOCATION"));
                params.put("CABIN", segment.getString("CABIN"));
                params.put("DISABLE_SPIRAL_ALGO", "false");
                params.put("E_LOCATION_" + i, segment.getString("ARRIVAL_LOCATION_CODE"));
                //params.put("WDS_FLIF_BOOKING_CLASS_" + i,"S");
                params.put("NB_ADT", "1");
                params.put("NB_CHD", "0");
                params.put("NB_INF", "0");
                params.put("PORTAL", segment.getString("AIRLINE_CODE"));
                params.put("TRIP_TYPE", "R");
//				params.put("WDS_FLIF_DIFFERENT_CABIN_" + i,segment.getString("DIFFERENT_CABIN").toLowerCase());
//				params.put("WDS_FLIF_EQUIPMENT_" + i,segment.getString("EQUIPMENT"));
                //params.put("WDS_FLIF_FLIGHT_NUMBER_" + i,segment.getString("FLIGHT_NUMBER"));
//				if(!equipment_icon.getString(i - 1).equals("")){
//					params.put("WDS_FLIF_EQUIPMENT_ICON_" + i, equipment_icon.getString(i - 1));
//				}
            }

            //尝试n次
            for (int i = 1; i <= queryMoreTranTimes; i++) {
                try {
                    HttpGet g = getBaseGet(url, params);
                    g.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
                    String s = excuteRequest(g);
                    super.appendPageContents(s);
                    super.setLenghtCount(s.length());
                    json = JSONObject.fromObject(s);
                    if (json != null) {
                        TranObj = ((JSONObject) json.getJSONObject("mapDataUI").getJSONObject("PAGE").getJSONObject("PANELS").getJSONObject("FLIGHTS").getJSONObject("DATA").getJSONObject("BOUND_0").getJSONArray("FLIGHTS").get(0)).getJSONArray("LIST_SEGMENT");
                        break;
                    }
                } catch (Exception e) {
                    logger.error("弟" + i + "查询中转信息失败");
                }
            }
        } catch (Exception e) {
            logger.error("查询中转信息失败");
        }
        return TranObj;
    }

    /**
     * 根据去程舱位的id找到回程舱位对象
     *
     * @param list_flight
     * @return
     * @throws Exception
     */
    private JSONObject getObjByFightId(JSONArray list_flight, String flight_id) throws Exception {
        for (int i = 0; i < list_flight.size(); i++) {
            if (list_flight.getJSONObject(i).getString("FLIGHT_ID").equals(flight_id)) {
                return list_flight.getJSONObject(i);
            }
        }
        throw new Exception("航班" + flight_id + "不能找到相应的回程信息，请检查");
    }


    /**
     * 填了城市之后浏览器出发blur事件发送这个请求，查询出该城市对应的舱位
     *
     * @return
     * @throws Exception
     */
    private JSONObject queryCabins() throws Exception {
        String url = "http://www.lufthansa.com/ODControl";
        Map<String, String> params = new HashMap<String, String>();
        params.put("bookingclass", "E");
        params.put("cff", "false");
        params.put("date", taskQueue.getFlightDate());
        params.put("destination", taskQueue.getToCity());
        params.put("language", "mi");
        params.put("origin", taskQueue.getFromCity());
        params.put("pos", "CN");
        params.put("type", "bookingclass");
        HttpGet g = getBaseGet(url, params);
        String s = excuteRequest(g);
        super.appendPageContents(s);
        super.setLenghtCount(s.length());
        JSONObject json = JSONObject.fromObject(s);
        if (!json.getBoolean("Success")) {
            throw new Exception("查询舱位错误");
        }
        return json;
    }

    /**
     * 构建参数
     *
     * @return
     */
    private Map<String, String> buildParams() {
        Map<String, String> params = new HashMap<String, String>();
        boolean isRT = taskQueue.getIsReturn() == 1;
        params.put("B_DATE_1", getRequestDate(taskQueue.getFlightDate()));
        if (isRT) {
            params.put("B_DATE_2", getRequestDate(taskQueue.getReturnGrabDate()));
        }

        params.put("B_LOCATION_1", taskQueue.getFromCity());
        //params.put("CABIN","E");
        params.put("E_LOCATION_1", taskQueue.getToCity());
        //params.put("PORTAL_SESSION","xrjFt6y-b3C3gpcuQqjErpg");

        params.put("TRIP_TYPE", isRT ? "R" : "O");
        params.put("WDS_WR_DCSEXT_SCENARIO", isRT ? "1" : "2");
        params.put("WDS_WR_SCENARIO", isRT ? "S1" : "S2");

        params.put("COUNTRY_SITE", "CN");
        params.put("DEVICE_TYPE", "DESKTOP");
        params.put("IP_COUNTRY", "CN");
        params.put("LANGUAGE", "CN");
        params.put("NB_ADT", "1");
        params.put("NB_CHD", "0");
        params.put("NB_INF", "0");
        params.put("PORTAL", "LH");
        params.put("POS", "CN");
        params.put("SECURE", "FALSE");
        params.put("SITE", "LUFTLUFT");
        params.put("SO_SITE_COUNTRY_OF_RESIDENCE", "CN");
        params.put("SO_SITE_LH_FRONTEND_URL", "www.lufthansa.com");
        params.put("WDS_LIST_CD_CODE_1", "ZIN812400");
        params.put("WDS_LIST_CD_CODE_2", "ZDL208200");
        params.put("WDS_LIST_CD_CODE_3", "ZE782675");
        params.put("WDS_SUPER_FLEX_FARE", "FALSE");
        params.put("WDS_WR_AREA", "SIN");
        params.put("WDS_WR_BFT", "IK");
        params.put("WDS_WR_CHANNEL", "LHCOM");
        params.put("WDS_WR_DCSID", "dcsi8dl8n100000kbqfxzervo_7i6g");
        return params;
    }

    //获取请求日期的格式
    private static String getRequestDate(String date) {
        return date.replaceAll("\\D", "") + "0000";
    }

    @Override
    public boolean validateFetch(Object arg0) throws Exception {
        return true;
    }

}
