package com.foreveross.crawl.exception;

import com.foreveross.crawl.common.exception.DefindSuccessException;
import com.foreveross.crawl.enums.TaskStatusAviationEnum;

/**
 * 航班没有价格
 * 引擎默认为成功
 * @author luofangyi
 *
 */
public class FlightsNotPriceException extends DefindSuccessException{

	private static final long serialVersionUID = 1L;

	public FlightsNotPriceException(String msg){
		super(TaskStatusAviationEnum.TASK_NOT_PRICE.getStatus(), msg);
	}
	public FlightsNotPriceException(){
		super(TaskStatusAviationEnum.TASK_NOT_PRICE.getStatus(), TaskStatusAviationEnum.TASK_NOT_PRICE.getRemark());
	}
	
}
