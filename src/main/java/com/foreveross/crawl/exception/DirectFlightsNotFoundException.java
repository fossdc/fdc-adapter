package com.foreveross.crawl.exception;

import com.foreveross.crawl.common.exception.DefindSuccessException;
import com.foreveross.crawl.enums.TaskStatusAviationEnum;

/**
 * 没有直达航班异常
 * 直达来回
 * 目前业务仅仅要求集中于直达，中转解析过于费劲
 * 所以有这个异常，引擎默认为成功
 * @author guokenye
 *
 */
public class DirectFlightsNotFoundException extends DefindSuccessException{

	private static final long serialVersionUID = 1L;

	public DirectFlightsNotFoundException(String msg){
		super(TaskStatusAviationEnum.TASK_NOT_DIRECT_FLIGHT.getStatus(), msg);
	}
	public DirectFlightsNotFoundException(){
		super(TaskStatusAviationEnum.TASK_NOT_DIRECT_FLIGHT.getStatus(), TaskStatusAviationEnum.TASK_NOT_DIRECT_FLIGHT.getRemark());
	}
	
}
