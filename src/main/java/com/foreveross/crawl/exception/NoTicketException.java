package com.foreveross.crawl.exception;

import com.foreveross.crawl.common.exception.DefindSuccessException;
import com.foreveross.crawl.enums.TaskStatusAviationEnum;

/**
 * 票售完异常
 * @author Administrator
 *
 */
public class NoTicketException extends DefindSuccessException{
	private static final long serialVersionUID = 1L;
	public NoTicketException(String msg){
		super(TaskStatusAviationEnum.TASK_NO_TICKET.getStatus(), msg);
	}
	public NoTicketException(){
		super(TaskStatusAviationEnum.TASK_NO_TICKET.getStatus(), TaskStatusAviationEnum.TASK_NO_TICKET.getRemark());
	}
}
