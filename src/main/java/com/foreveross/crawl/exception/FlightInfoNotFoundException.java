package com.foreveross.crawl.exception;

import com.foreveross.crawl.common.exception.DefindSuccessException;
import com.foreveross.crawl.enums.TaskStatusAviationEnum;

public class FlightInfoNotFoundException extends DefindSuccessException{

	private static final long serialVersionUID = 1L;

	public FlightInfoNotFoundException(String msg){
		super(TaskStatusAviationEnum.TASK_NOT_FIND_FLIGHT.getStatus(), msg);
	}
	public FlightInfoNotFoundException(){
		super(TaskStatusAviationEnum.TASK_NOT_FIND_FLIGHT.getStatus(), TaskStatusAviationEnum.TASK_NOT_FIND_FLIGHT.getRemark());
	}
	
}
