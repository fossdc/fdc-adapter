package com.foreveross.crawl.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jsoup.nodes.Element;

import com.foreveross.crawl.common.util.RegHtmlUtil;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.google.common.collect.Maps;
/**

 * 一些关系到planInfo的常用方法
 * @author stub
 *
 */
public class PlaneInfoCommon {
	/**
	 * 查看某个去程对用的回程是否已经存在,如果不存在则返回null
	 * @param entity
	 * @param flightNos
	 * @return
	 */
	public static ReturnDoublePlaneInfoEntity getExistReturnEntity(DoublePlaneInfoEntity entity,String flightNos){
		ReturnDoublePlaneInfoEntity returnEntity = null;
		Set<ReturnDoublePlaneInfoEntity> returnPlaneInfos = entity.getReturnPlaneInfos();
		if(returnPlaneInfos != null && !returnPlaneInfos.isEmpty()){
			for(ReturnDoublePlaneInfoEntity e : returnPlaneInfos){
				String nos = "";
				if(e.getReturnTransits() == null || e.getReturnTransits().isEmpty()){
					nos = e.getFlightNo();
				}else{
					for(ReturnTransitEntity t : e.getReturnTransits()){
						nos += t.getFlightNo();
					}
				}
				if(flightNos.equals(nos)){
					returnEntity = e;
					break;
				}
			}
		}
		return returnEntity;
	}
	
	/**
	 * 去除价格为空或零的航班
	 * @param list
	 */
	public static <T extends AbstractPlaneInfoEntity>  void removeNoPrice(List<T> list){
		List<T> tempList = new ArrayList<T>();
		tempList.addAll(list);
		list.clear();
		for(T entity : tempList){
			if(!isNullOrZero(entity.getTotalLowestPrice()) || !isNullOrZero(entity.getSumLowestPrice())){
				list.add(entity);
			}
		}
	}	
	
	/**
	 * 是否为空或者零
	 * @param d
	 * @return
	 */
	public static boolean isNullOrZero(Double d){
		if(d == null || d.doubleValue() == 0){
			return true;
		}
		return false;
	}
	
	/**
	 * 统一格式为yyyy-MM-dd HH:mm
	 * @param date
	 * @param time
	 * @return
	 */
	public static String getFormatTime(String date,String time){
		StringBuilder sb = new StringBuilder();
		date = date.replaceAll("[年月]", "-").replaceAll("[^\\d\\-]", "");
		String[] arr = date.split("-");
		sb.append(arr[0]);
		sb.append("-");
		sb.append(arr[1].length() == 1 ? ("0" + arr[1]) : arr[1]);
		sb.append("-");
		sb.append(arr[1].length() == 1 ? ("0" + arr[2]) : arr[2]);
		sb.append(" ");
		arr = time.split(":");
		String hour = arr[0];
		String minute = arr[1];
		if(time.indexOf("p.m") >= 0){
			hour = String.valueOf(Integer.valueOf(hour) + 12);
		}
		if(hour.length() == 1){
			hour = "0" + hour;
		}
		if(minute.length() == 1){
			minute = "0" + minute;
		}
		sb.append(hour).append(":").append(minute);
		return sb.toString();
	}
	
	/**
	 * XX小时XX分钟转换成毫秒
	 * @param time
	 * @return
	 */
	public static Long getStayTime(String time){
		if(time == null){
			return null;
		}
		String hourStr = RegHtmlUtil.regStr(time,"(\\d*)\\D*?小时");
		String minuteStr = RegHtmlUtil.regStr(time,"(\\d*)\\D*?分");
		int hour = hourStr == null ? 0 : Integer.valueOf(hourStr);
		int minute = minuteStr == null ? 0 : Integer.valueOf(minuteStr);
		return (hour * 60 + minute) * 60 * 1000l;
	}
	/**
	 * XX.X小时转换成毫秒
	 * @param time
	 * @return
	 */
	public static Long getLongtime(String time){
		if(time == null){
			return null;
		}
		String hourStr = RegHtmlUtil.regStr(time,"(\\d*\\.*\\d*)小时");
		double hour=hourStr==null? 0:Double.parseDouble(hourStr);
		return (long) (hour*60*60*1000);
	}
	
	/**
	 * 把map的值取出来放到list
	 * @param map
	 * @return
	 */
	public static List<AbstractPlaneInfoEntity> mapToList(Map<String,AbstractPlaneInfoEntity> map){
		List<AbstractPlaneInfoEntity> list = new ArrayList<AbstractPlaneInfoEntity>();
		for(String key : map.keySet()){
			list.add(map.get(key));
		}
		return list;
	}
	
	/**
	 * 获取一个节点所有name属性的参数
	 * @param parent
	 * @return
	 */
	public static Map<String,String> getNameParams(Element parent){
		Map<String,String> params = Maps.newHashMap();
		for(Element nameSub : parent.select("[name]")){
			params.put(nameSub.attr("name"), nameSub.val());
		}
		return params;
	}
	
	/**
	 * xxhxxmin转换成停留时间 可以只有h 或min
	 * @param time
	 * @return
	 */
	public static Long getStayTimeWithhMin(String time){
		if(time == null){
			return null;
		}
		String hourStr = RegHtmlUtil.regStr(time,"(\\d*)\\D*?h");
		String minuteStr = RegHtmlUtil.regStr(time,"(\\d*)\\D*?min");
		int hour = hourStr == null ? 0 : Integer.valueOf(hourStr);
		int minute = minuteStr == null ? 0 : Integer.valueOf(minuteStr);
		return (hour * 60 + minute) * 60 * 1000l;
	}
	/**
	 * 获得返程对象航班号
	 * @param entity
	 * @return
	 */
	public static String getReturnFlightNos(ReturnDoublePlaneInfoEntity entity){
		if(entity.getReturnTransits().isEmpty()){
			return entity.getFlightNo();
		}
		String flightNos = "";
		for(ReturnTransitEntity tran : entity.getReturnTransits()){
			flightNos += tran.getFlightNo();
		}
		return flightNos;
	}
}
