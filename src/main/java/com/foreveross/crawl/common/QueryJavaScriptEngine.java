package com.foreveross.crawl.common;

import net.sourceforge.htmlunit.corejs.javascript.Context;
import net.sourceforge.htmlunit.corejs.javascript.ContextAction;
import net.sourceforge.htmlunit.corejs.javascript.Function;
import net.sourceforge.htmlunit.corejs.javascript.Script;
import net.sourceforge.htmlunit.corejs.javascript.Scriptable;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;
import com.gargoylesoftware.htmlunit.javascript.host.Window;

public class QueryJavaScriptEngine extends JavaScriptEngine {

	public QueryJavaScriptEngine(WebClient webClient) {
		super(webClient);
		// TODO Auto-generated constructor stub
	}
		

	   public Object callFunction(
	            final HtmlPage htmlPage,
	            final Function javaScriptFunction,
	            final Scriptable thisObject,
	            final Object [] args,
	            final DomNode htmlElement) {
		   	if(javaScriptFunction.toString().indexOf("scroll();")>0)
		   		return null;
//		  	if(javaScriptFunction.toString().indexOf("q.queryNext")>0)
//		   		return null;
//			if(javaScriptFunction.toString().indexOf("g(C);")>0)
//		   		return null;
//			if(javaScriptFunction.toString().indexOf("updateStatus")>0){
//				//System.out.println(javaScriptFunction.toString());
//				return null;
//			}
	
		
	        final Scriptable scope = getScope(htmlPage, htmlElement);
	        
	        return callFunction(htmlPage, javaScriptFunction, scope, thisObject, args);
	    }
	   
	    private Scriptable getScope(final HtmlPage htmlPage, final DomNode htmlElement) {
	        if (htmlElement != null) {
	            return htmlElement.getScriptObject();
	        }
	        return (Window) htmlPage.getEnclosingWindow().getScriptObject();
	    }
	    
	    

}
