package com.foreveross.crawl.domain.airfreight.doub;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.OneToMany;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;

import com.dayatang.utils.Assert;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AgentEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.StopOverEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;

/**
 * 来回程（往返程）航班信息模型
 * 
 * @author Administrator
 * 
 */
@javax.persistence.Table(name = "CRAWL_PLANE_FREIGHT_DOUBLE")
public class DoublePlaneInfoEntity extends AbstractDoublePlaneInfoEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4691742810848723699L;
	/**
	 * 回程航班的信息实体集合
	 */
	@OneToMany
	@HBaseColumn(qualifier = "returnPlaneInfos", family = "returnPlaneInfosInfo")
	private Set<ReturnDoublePlaneInfoEntity> returnPlaneInfos = new HashSet<ReturnDoublePlaneInfoEntity>();

	/**
	 * 去程的仓位信息和回程的仓位信息的对应关系 去程的经济舱对应回程的商务舱，包含了二者的打包价
	 */
	@OneToMany
	@HBaseColumn(qualifier = "cabinRelations", family = "cabinRelationsInfo")
	private Set<CabinRelationEntity> cabinRelations = new HashSet<CabinRelationEntity>();

	/**
	 * 中转的实体set，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "transits", family = "transitsInfo")
	private ArrayList<TransitEntity> transits = new ArrayList<TransitEntity>();

	/**
	 * 仓位信息，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "cabins", family = "cabinsInfo")
	private Set<CabinEntity> cabins = new HashSet<CabinEntity>();

	/**
	 * 代理信息，指去程 (列簇信息保留老的PlaneInfoEntity)
	 */
	@OneToMany
	@HBaseColumn(qualifier = "agents", family = "agentInfos")
	private Set<AgentEntity> agents = new HashSet<AgentEntity>();

	/**
	 * 经停信息，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "stopOvers", family = "stopOversInfo")
	private Set<StopOverEntity> stopOvers = new HashSet<StopOverEntity>();

	
	public Set<CabinEntity> getCabins() {
		return cabins;
	}

	public void setCabins(Set<CabinEntity> cabins) {
		this.cabins = cabins;
	}

	public Set<AgentEntity> getAgents() {
		return agents;
	}

	public void setAgents(Set<AgentEntity> agents) {
		this.agents = agents;
	}

	
	
	public ArrayList<TransitEntity> getTransits() {
		return transits;
	}

	public void setTransits(ArrayList<TransitEntity> transits) {
		this.transits = transits;
	}

	public Set<StopOverEntity> getStopOvers() {
		return stopOvers;
	}

	public void setStopOvers(Set<StopOverEntity> stopOvers) {
		this.stopOvers = stopOvers;
	}

	/**
	 * 生成航班数据的hbase主键
	 * 渠道_抓取日期(年月日时)_起点(使用三字码)_终点(使用三字码)_起飞时间(年月日)_返程时间(年月日)_航班ID_5位随即数 
	 * 10位 +5位 +2位+3位 +3位 +8位+8位+8位 +5位
	 * 
	 * 2014-03-18  将渠道调到最前面 去掉区域码
	 */
	@Override
	public String generateRowKey() {
		Assert.notEmpty(this.getAttachHbaseKey());
		Assert.notEmpty(this.getFromCity());
		Assert.notEmpty(this.getToCity());
		Assert.notEmpty(this.getFlightDate());
		Assert.notEmpty(this.getFlightReturnDate());
		Assert.notEmpty(this.getFlightNo());
		// 如2013012318
		return new StringBuilder()
				.append(StringUtils.leftPad(String.valueOf(this.getAttachHbaseKey()), 5, '0'))
				.append(DateFormatUtils.format(this.getCreateTime(),"yyyyMMddHH"))
				.append(this.getFromCity())
				.append(this.getToCity())
				.append(this.getFlightDate().replaceAll("-", ""))
				.append(this.getFlightReturnDate().replaceAll("-", ""))
				.append(StringUtils.rightPad(this.getFlightNo(), 8, '0'))
				.append(StringUtils.leftPad(String.valueOf(RandomUtils.nextInt(99999)), 5, '0'))
				.toString();
	}

	public Set<ReturnDoublePlaneInfoEntity> getReturnPlaneInfos() {
		return returnPlaneInfos;
	}

	public void setReturnPlaneInfos(
			Set<ReturnDoublePlaneInfoEntity> returnPlaneInfos) {
		this.returnPlaneInfos = returnPlaneInfos;
	}

	public Set<CabinRelationEntity> getCabinRelations() {
		return cabinRelations;
	}

	public void setCabinRelations(Set<CabinRelationEntity> cabinRelations) {
		this.cabinRelations = cabinRelations;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
