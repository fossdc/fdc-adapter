package com.foreveross.crawl.domain.airfreight.doub;

import java.util.Date;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 * 回程经停城市实体模型
 * 
 * @author Administrator
 * 
 */

public class ReturnStopOverEntity extends BaseHBaseEntity implements JSONString {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2659208751159357431L;
	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;

	/**
	 * 经停城市
	 */
	@HBaseColumn(qualifier = "stopCity", family = "returnStopOversInfo")
	private String stopCity;
	/**
	 * 航班号
	 */
	@HBaseColumn(qualifier = "flightNo", family = "returnStopOversInfo")
	private String flightNo;
	/**
	 * 经停时长（毫秒）（如在某座城市停留了三个小时 3*3600*1000）
	 */
	@HBaseColumn(qualifier = "stayTime", family = "returnStopOversInfo")
	private Long stayTime;

	/**
	 * 到达这座城市的时间
	 */
	@HBaseColumn(qualifier = "arrTime", family = "returnStopOversInfo",format = "yyyyMMddHHmmss")
	private Date arrTime;
	/**
	 * 离开这座城市的时间
	 */
	@HBaseColumn(qualifier = "leaveTime", family = "returnStopOversInfo",format = "yyyyMMddHHmmss")
	private Date leaveTime;

	@Override
	public String generateRowKey() {
		// TODO Auto-generated method stub
		return planeInfoEntity.generateRowKey();
	}

	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	public String getStopCity() {
		return stopCity;
	}

	public void setStopCity(String stopCity) {
		this.stopCity = stopCity;
	}

	public Long getStayTime() {
		return stayTime;
	}

	public void setStayTime(Long stayTime) {
		this.stayTime = stayTime;
	}

	public Date getArrTime() {
		return arrTime;
	}

	public void setArrTime(Date arrTime) {
		this.arrTime = arrTime;
	}

	public Date getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(Date leaveTime) {
		this.leaveTime = leaveTime;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	@Override
	public String toString() {
//		return ToStringBuilder.reflectionToString(this,
//				ToStringStyle.SHORT_PREFIX_STYLE);
		return this.toJSONString();
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
