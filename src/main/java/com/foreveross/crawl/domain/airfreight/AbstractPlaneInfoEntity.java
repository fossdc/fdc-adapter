package com.foreveross.crawl.domain.airfreight;

import java.util.Date;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;

/**
 * 航班模型抽象类
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractPlaneInfoEntity extends BaseHBaseEntity {

	private static final long serialVersionUID = 3355658128572370404L;

//	/**
//	 * 外键，对应表CRAWL_PF_SOURCE_DATA的rowkey 源网页数据rowKey
//	 * 
//	 */
//
//	private String sourceRowkey;

	/**
	 * 渠道的hbasekey
	 */

	private String attachHbaseKey;
	/**
	 * 创建时间(目前来看是模型被创建的时间，不一定是入库或者抓取任务开启的时间)
	 */

	private Date createTime = new Date();
	/**
	 * 抓取渠道ID
	 * */

	private Long grabChannelId;
	/**
	 * 抓取渠道名称
	 */

	private String grabChannelName;
	/**
	 * 运营商简称英文，如：MU
	 */

	private String carrierKey;

	/**
	 * 运营商简称中文，如：南航
	 */

	private String carrierName;

	/**
	 * 运营商全名，如：中国海南航空公司
	 */

	private String carrierFullName;
	/**
	 * 航班号，如： MU3540
	 */

	private String flightNo;

	/**
	 * 实际乘坐航班号，如 SC1234 实际乘坐CZ1234
	 */

	private String actuallyFlightNo;

	/**
	 * 飞机类型
	 */

	private String flightType;
	/**
	 * 航班批次日期。如:2012-05-19,格式:yyyy-MM-dd
	 */

	private String flightDate;

	/**
	 * 航班起飞时间
	 */

	private Date startTime;

	/**
	 * 航班预计到达时间
	 */

	private Date endTime;
	/**
	 * 起飞城市三字码（是城市，不是机场三字码）
	 * 
	 */

	private String fromCity;
	/**
	 * 起飞城市名称
	 * 
	 */

	private String fromCityName;
	/**
	 * 目的城市三字码三字码
	 */

	private String toCity;
	/**
	 * 目的城市名称
	 */

	private String toCityName;
	/**
	 * 区域码
	 */

	private String areaCode;
	/**
	 * 区域名称
	 */

	private String areaName;

	/**
	 * 最低价裸价(为了照顾之前数据不动名称)
	 */
	private Double totalLowestPrice;
	/**
	 * 合计最低价格（裸价+税费）
	 */
	private Double sumLowestPrice;
	/**
	 * 最低价税费
	 */
	private Double totalLowestTaxesPrice;
	/**
	 * 最高价裸价(为了照顾之前数据不动名称)
	 */

	private Double totalHighestPrice;
	/**
	 * 合计最高价（祼价+税费）
	 */
	private Double sumHighestPrice;
	/**
	 * 最高价税费
	 */
	private Double totalHighestTaxesPrice;
	/**
	 * 全程最低裸价代理提供商名称
	 */

	private String agentName;

//	public String getSourceRowkey() {
//		return sourceRowkey;
//	}
//
//	public void setSourceRowkey(String sourceRowkey) {
//		this.sourceRowkey = sourceRowkey;
//	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getGrabChannelId() {
		return grabChannelId;
	}

	public void setGrabChannelId(Long grabChannelId) {
		this.grabChannelId = grabChannelId;
	}

	public String getGrabChannelName() {
		return grabChannelName;
	}

	public void setGrabChannelName(String grabChannelName) {
		this.grabChannelName = grabChannelName;
	}

	public String getCarrierKey() {
		return carrierKey;
	}

	public void setCarrierKey(String carrierKey) {
		this.carrierKey = carrierKey;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getCarrierFullName() {
		return carrierFullName;
	}

	public void setCarrierFullName(String carrierFullName) {
		this.carrierFullName = carrierFullName;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getActuallyFlightNo() {
		return actuallyFlightNo;
	}

	public void setActuallyFlightNo(String actuallyFlightNo) {
		this.actuallyFlightNo = actuallyFlightNo;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getFromCityName() {
		return fromCityName;
	}

	public void setFromCityName(String fromCityName) {
		this.fromCityName = fromCityName;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getToCityName() {
		return toCityName;
	}

	public void setToCityName(String toCityName) {
		this.toCityName = toCityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Double getTotalLowestPrice() {
		return totalLowestPrice;
	}

	public void setTotalLowestPrice(Double totalLowestPrice) {
		this.totalLowestPrice = totalLowestPrice;
	}

	public Double getTotalHighestPrice() {
		return totalHighestPrice;
	}

	public void setTotalHighestPrice(Double totalHighestPrice) {
		this.totalHighestPrice = totalHighestPrice;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAttachHbaseKey() {
		return attachHbaseKey;
	}

	public void setAttachHbaseKey(String attachHbaseKey) {
		this.attachHbaseKey = attachHbaseKey;
	}

	public Double getSumLowestPrice() {
		return sumLowestPrice;
	}

	public void setSumLowestPrice(Double sumLowestPrice) {
		this.sumLowestPrice = sumLowestPrice;
	}

	public Double getTotalLowestTaxesPrice() {
		return totalLowestTaxesPrice;
	}

	public void setTotalLowestTaxesPrice(Double totalLowestTaxesPrice) {
		this.totalLowestTaxesPrice = totalLowestTaxesPrice;
	}

	public Double getSumHighestPrice() {
		return sumHighestPrice;
	}

	public void setSumHighestPrice(Double sumHighestPrice) {
		this.sumHighestPrice = sumHighestPrice;
	}

	public Double getTotalHighestTaxesPrice() {
		return totalHighestTaxesPrice;
	}

	public void setTotalHighestTaxesPrice(Double totalHighestTaxesPrice) {
		this.totalHighestTaxesPrice = totalHighestTaxesPrice;
	}

	@Override
	public String toString() {
		return "AbstractPlaneInfoEntity [attachHbaseKey=" + attachHbaseKey
				+ ", createTime=" + createTime + ", grabChannelId="
				+ grabChannelId + ", grabChannelName=" + grabChannelName
				+ ", carrierKey=" + carrierKey + ", carrierName=" + carrierName
				+ ", carrierFullName=" + carrierFullName + ", flightNo="
				+ flightNo + ", actuallyFlightNo=" + actuallyFlightNo
				+ ", flightType=" + flightType + ", flightDate=" + flightDate
				+ ", startTime=" + startTime + ", endTime=" + endTime
				+ ", fromCity=" + fromCity + ", fromCityName=" + fromCityName
				+ ", toCity=" + toCity + ", toCityName=" + toCityName
				+ ", areaCode=" + areaCode + ", areaName=" + areaName
				+ ", totalLowestPrice=" + totalLowestPrice
				+ ", sumLowestPrice=" + sumLowestPrice
				+ ", totalLowestTaxesPrice=" + totalLowestTaxesPrice
				+ ", totalHighestPrice=" + totalHighestPrice
				+ ", sumHighestPrice=" + sumHighestPrice
				+ ", totalHighestTaxesPrice=" + totalHighestTaxesPrice
				+ ", agentName=" + agentName + "]";
	}



}
