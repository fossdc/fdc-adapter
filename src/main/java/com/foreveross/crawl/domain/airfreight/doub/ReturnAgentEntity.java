package com.foreveross.crawl.domain.airfreight.doub;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 * 回程代理模型
 * 
 * @author Administrator
 * 
 */

public class ReturnAgentEntity extends BaseHBaseEntity implements JSONString {

	private static final long serialVersionUID = -2993587767621246667L;

	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;

	/**
	 * 代理商名称如（兄弟旅行团） 多见于淘宝
	 */
	@HBaseColumn(qualifier = "agentName", family = "returnAgentsInfo")
	private String agentName;
	/**
	 * 机票原价
	 */
	@HBaseColumn(qualifier = "originalPrice", family = "returnAgentsInfo")
	private Double originalPrice;
	/**
	 * 机票优惠价
	 */
	@HBaseColumn(qualifier = "price", family = "returnAgentsInfo")
	private Double price;
	/**
	 * 代理商类型 0 表示直营(如 深航自己的航班) 1 表示承运(如 深航飞国航的航班) 2 表示普通代理(如 携程艺龙淘宝卖票)
	 */
	@HBaseColumn(qualifier = "agentType", family = "returnAgentsInfo")
	private Integer agentType;

	/**
	 * 代理商所在的公司,如（成都悦途信息技术有限公司）
	 */
	@HBaseColumn(qualifier = "companyName", family = "returnAgentsInfo")
	private String companyName;
	/**
	 * 代理商公司所在地区，如（成都市锦江工业园区三色路38号 ）
	 */
	@HBaseColumn(qualifier = "address", family = "returnAgentsInfo")
	private String address;
	
	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getAgentType() {
		return agentType;
	}

	public void setAgentType(Integer agentType) {
		this.agentType = agentType;
	}

	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
