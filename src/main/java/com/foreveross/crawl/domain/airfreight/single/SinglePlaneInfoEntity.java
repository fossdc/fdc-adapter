package com.foreveross.crawl.domain.airfreight.single;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.OneToMany;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;

import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AgentEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.StopOverEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;

/**
 * 单程航班模型
 * 
 * @author Administrator
 * 
 */
@javax.persistence.Table(name = "CRAWL_PLANE_FREIGHT")
public class SinglePlaneInfoEntity extends AbstractSinglePlaneInfoEntity {

	private static final long serialVersionUID = -5830998346091644800L;

	/**
	 * 中转的实体set，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "transits", family = "transitsInfo")
	private ArrayList<TransitEntity> transits = new ArrayList<TransitEntity>();

	/**
	 * 仓位信息，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "cabins", family = "cabinsInfo")
	private Set<CabinEntity> cabins = new HashSet<CabinEntity>();

	/**
	 * 代理信息，指去程 (列簇信息保留老的PlaneInfoEntity)
	 */
	@OneToMany
	@HBaseColumn(qualifier = "agents", family = "agentInfos")
	private Set<AgentEntity> agents = new HashSet<AgentEntity>();

	/**
	 * 经停信息，指去程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "stopOvers", family = "stopOversInfo")
	private Set<StopOverEntity> stopOvers = new HashSet<StopOverEntity>();

	

	

	public ArrayList<TransitEntity> getTransits() {
		return transits;
	}

	public void setTransits(ArrayList<TransitEntity> transits) {
		this.transits = transits;
	}

	public Set<CabinEntity> getCabins() {
		return cabins;
	}

	public void setCabins(Set<CabinEntity> cabins) {
		this.cabins = cabins;
	}

	public Set<AgentEntity> getAgents() {
		return agents;
	}

	public void setAgents(Set<AgentEntity> agents) {
		this.agents = agents;
	}

	

	public Set<StopOverEntity> getStopOvers() {
		return stopOvers;
	}

	public void setStopOvers(Set<StopOverEntity> stopOvers) {
		this.stopOvers = stopOvers;
	}

	/**
	 * 生成航班数据的hbase主键
	 * 渠道_抓取日期(年月日时)_起点(使用三字码)_终点(使用三字码)_起飞时间(年月日时)_航班ID_5位随即数 
	 * 10位 +5位 +2位+3位 +3位 +10位+8位 +5位
	 * 
	 * 2014-03-18  将渠道调到最前面 去掉区域码
	 */
	@Override
	public String generateRowKey() {
		// 如2013012318
		return new StringBuilder()
				.append(StringUtils.leftPad(
						String.valueOf(this.getAttachHbaseKey()), 5, '0'))
				.append(DateFormatUtils.format(this.getCreateTime(),
						"yyyyMMddHH"))
				.append(this.getFromCity())
				.append(this.getToCity())
				.append(DateFormatUtils.format(this.getStartTime(),
						"yyyyMMddHH"))
				.append(StringUtils.rightPad(this.getFlightNo(), 8, '0'))
				.append(StringUtils.leftPad(
						String.valueOf(RandomUtils.nextInt(99999)), 5, '0'))
				.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SinglePlaneInfoEntity [getSourceRowkey()=");
		builder.append(getSourceRowkey());
		builder.append(", getCreateTime()=");
		builder.append(getCreateTime());
		builder.append(", getGrabChannelId()=");
		builder.append(getGrabChannelId());
		builder.append(", getGrabChannelName()=");
		builder.append(getGrabChannelName());
		builder.append(", getCarrierKey()=");
		builder.append(getCarrierKey());
		builder.append(", getCarrierName()=");
		builder.append(getCarrierName());
		builder.append(", getCarrierFullName()=");
		builder.append(getCarrierFullName());
		builder.append(", getFlightNo()=");
		builder.append(getFlightNo());
		builder.append(", getActuallyFlightNo()=");
		builder.append(getActuallyFlightNo());
		builder.append(", getFlightType()=");
		builder.append(getFlightType());
		builder.append(", getFlightDate()=");
		builder.append(getFlightDate());
		builder.append(", getStartTime()=");
		builder.append(getStartTime());
		builder.append(", getEndTime()=");
		builder.append(getEndTime());
		builder.append(", getFromCity()=");
		builder.append(getFromCity());
		builder.append(", getFromCityName()=");
		builder.append(getFromCityName());
		builder.append(", getToCity()=");
		builder.append(getToCity());
		builder.append(", getToCityName()=");
		builder.append(getToCityName());
		builder.append(", getAreaCode()=");
		builder.append(getAreaCode());
		builder.append(", getAreaName()=");
		builder.append(getAreaName());
		builder.append(", getTotalLowestPrice()=");
		builder.append(getTotalLowestPrice());
		builder.append(", getTotalHighestPrice()=");
		builder.append(getTotalHighestPrice());
		builder.append(", getAgentName()=");
		builder.append(getAgentName());
		builder.append(", getAttachHbaseKey()=");
		builder.append(getAttachHbaseKey());
		builder.append(", getSumLowestPrice()=");
		builder.append(getSumLowestPrice());
		builder.append(", getTotalLowestTaxesPrice()=");
		builder.append(getTotalLowestTaxesPrice());
		builder.append(", getSumHighestPrice()=");
		builder.append(getSumHighestPrice());
		builder.append(", getTotalHighestTaxesPrice()=");
		builder.append(getTotalHighestTaxesPrice());
		builder.append("]");
		return builder.toString();
	}

	
}
