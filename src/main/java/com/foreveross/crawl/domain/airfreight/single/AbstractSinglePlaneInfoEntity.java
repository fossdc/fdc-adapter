package com.foreveross.crawl.domain.airfreight.single;

import java.util.Date;

import net.sf.json.JSONString;

import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 * 航班模型抽象类
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractSinglePlaneInfoEntity extends
		AbstractPlaneInfoEntity implements JSONString {

	private static final long serialVersionUID = 3355658128572370404L;

	/**
	 * 外键，对应表CRAWL_PF_SOURCE_DATA的rowkey 源网页数据rowKey
	 * 
	 */
	@HBaseColumn(qualifier = "SOURCE_ROWKEY", family = "planeinfo")
	private String sourceRowkey;

	/**
	 * 渠道的hbasekey
	 */
	@HBaseColumn(qualifier = "grabChannelNum", family = "planeinfo")
	private String attachHbaseKey;
	/**
	 * 创建时间(目前来看是模型被创建的时间，不一定是入库或者抓取任务开启的时间)
	 */
	@HBaseColumn(qualifier = "grabDate", family = "planeinfo", format = "yyyyMMddHHmmssSSS")
	private Date createTime = new Date();
	/**
	 * 抓取渠道ID
	 * */
	@HBaseColumn(qualifier = "grabChannelId", family = "planeinfo")
	private Long grabChannelId;
	/**
	 * 抓取渠道名称
	 */
	@HBaseColumn(qualifier = "grabChannel", family = "planeinfo")
	private String grabChannelName;
	/**
	 * 运营商简称英文，如：MU
	 */
	@HBaseColumn(qualifier = "carrierKey", family = "planeinfo")
	private String carrierKey;

	/**
	 * 运营商简称中文，如：南航
	 */
	@HBaseColumn(qualifier = "carrierName", family = "planeinfo")
	private String carrierName;

	/**
	 * 运营商全名，如：中国海南航空公司
	 */
	@HBaseColumn(qualifier = "carrierFullName", family = "planeinfo")
	private String carrierFullName;
	/**
	 * 航班号，如： MU3540
	 */
	@HBaseColumn(qualifier = "flightId", family = "planeinfo")
	private String flightNo;

	/**
	 * 实际乘坐航班号，如 SC1234 实际乘坐CZ1234
	 */
	@HBaseColumn(qualifier = "actuallyFlightNo", family = "planeinfo")
	private String actuallyFlightNo;

	/**
	 * 飞机类型
	 */
	@HBaseColumn(qualifier = "type", family = "planeinfo")
	private String flightType;
	/**
	 * 航班批次日期。如:2012-05-19,格式:yyyy-MM-dd
	 */
	@HBaseColumn(qualifier = "flightBatchDate", family = "planeinfo")
	private String flightDate;

	/**
	 * 航班起飞时间
	 */
	@HBaseColumn(qualifier = "flightBatchStartTime", family = "planeinfo", format = "yyyyMMddHHmmss")
	private Date startTime;

	/**
	 * 航班预计到达时间
	 */
	@HBaseColumn(qualifier = "flightBatchEndTime", family = "planeinfo", format = "yyyyMMddHHmmss")
	private Date endTime;
	/**
	 * 起飞城市三字码（是城市，不是机场三字码）
	 * 
	 */
	@HBaseColumn(qualifier = "fromCity", family = "planeinfo")
	private String fromCity;
	/**
	 * 起飞城市名称
	 * 
	 */
	@HBaseColumn(qualifier = "fromCityName", family = "planeinfo")
	private String fromCityName;
	/**
	 * 目的城市三字码三字码
	 */
	@HBaseColumn(qualifier = "toCity", family = "planeinfo")
	private String toCity;
	/**
	 * 目的城市名称
	 */
	@HBaseColumn(qualifier = "toCityName", family = "planeinfo")
	private String toCityName;
	/**
	 * 区域码
	 */
	@HBaseColumn(qualifier = "areaCode", family = "planeinfo")
	private String areaCode;
	/**
	 * 区域名称
	 */
	@HBaseColumn(qualifier = "areaName", family = "planeinfo")
	private String areaName;

	/**
	 * 全程最低裸价代理提供商名称
	 */
	@HBaseColumn(qualifier = "agentName", family = "planeinfo")
	private String agentName;

	/**
	 * 最低价裸价(为了照顾之前数据不动名称)
	 */
	@HBaseColumn(qualifier = "lowerPrice", family = "planeinfo")
	private Double totalLowestPrice;
	/**
	 * 合计最低价格（裸价+税费）
	 */
	@HBaseColumn(qualifier = "sumLowerPrice", family = "planeinfo")
	private Double sumLowestPrice;
	/**
	 * 最低价税费
	 */
	@HBaseColumn(qualifier = "totalLowestTaxesPrice", family = "planeinfo")
	private Double totalLowestTaxesPrice;
	/**
	 * 最高价裸价(为了照顾之前数据不动名称)
	 */
	@HBaseColumn(qualifier = "highPrice", family = "planeinfo")
	private Double totalHighestPrice;
	/**
	 * 合计最高价（祼价+税费）
	 */
	@HBaseColumn(qualifier = "sumHighestPrice", family = "planeinfo")
	private Double sumHighestPrice;
	/**
	 * 最高价税费
	 */
	@HBaseColumn(qualifier = "totalHighestTaxesPrice", family = "planeinfo")
	private Double totalHighestTaxesPrice;
	
	/**
	 * 去程总时长
	 */
	@HBaseColumn(qualifier = "flightDuration", family = "planeinfo")
	private Long flightDuration;
	
	/**
	 * 货币单位
	 * 如：USD
	*/
	@HBaseColumn(qualifier = "currency", family = "planeinfo")
	private String currency;
	
	public String getSourceRowkey() {
		return sourceRowkey;
	}

	public void setSourceRowkey(String sourceRowkey) {
		this.sourceRowkey = sourceRowkey;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getGrabChannelId() {
		return grabChannelId;
	}

	public void setGrabChannelId(Long grabChannelId) {
		this.grabChannelId = grabChannelId;
	}

	public String getGrabChannelName() {
		return grabChannelName;
	}

	public void setGrabChannelName(String grabChannelName) {
		this.grabChannelName = grabChannelName;
	}

	public String getCarrierKey() {
		return carrierKey;
	}

	public void setCarrierKey(String carrierKey) {
		this.carrierKey = carrierKey;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getCarrierFullName() {
		return carrierFullName;
	}

	public void setCarrierFullName(String carrierFullName) {
		this.carrierFullName = carrierFullName;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getActuallyFlightNo() {
		return actuallyFlightNo;
	}

	public void setActuallyFlightNo(String actuallyFlightNo) {
		this.actuallyFlightNo = actuallyFlightNo;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getFromCity() {
		return fromCity;
	}

	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}

	public String getFromCityName() {
		return fromCityName;
	}

	public void setFromCityName(String fromCityName) {
		this.fromCityName = fromCityName;
	}

	public String getToCity() {
		return toCity;
	}

	public void setToCity(String toCity) {
		this.toCity = toCity;
	}

	public String getToCityName() {
		return toCityName;
	}

	public void setToCityName(String toCityName) {
		this.toCityName = toCityName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Double getTotalLowestPrice() {
		return totalLowestPrice;
	}

	public void setTotalLowestPrice(Double totalLowestPrice) {
		this.totalLowestPrice = totalLowestPrice;
	}

	public Double getTotalHighestPrice() {
		return totalHighestPrice;
	}

	public void setTotalHighestPrice(Double totalHighestPrice) {
		this.totalHighestPrice = totalHighestPrice;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAttachHbaseKey() {
		return attachHbaseKey;
	}

	public void setAttachHbaseKey(String attachHbaseKey) {
		this.attachHbaseKey = attachHbaseKey;
	}

	public Long getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(Long flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getSumLowestPrice() {
		return sumLowestPrice;
	}

	public void setSumLowestPrice(Double sumLowestPrice) {
		this.sumLowestPrice = sumLowestPrice;
	}

	public Double getTotalLowestTaxesPrice() {
		return totalLowestTaxesPrice;
	}

	public void setTotalLowestTaxesPrice(Double totalLowestTaxesPrice) {
		this.totalLowestTaxesPrice = totalLowestTaxesPrice;
	}

	public Double getSumHighestPrice() {
		return sumHighestPrice;
	}

	public void setSumHighestPrice(Double sumHighestPrice) {
		this.sumHighestPrice = sumHighestPrice;
	}

	public Double getTotalHighestTaxesPrice() {
		return totalHighestTaxesPrice;
	}

	public void setTotalHighestTaxesPrice(Double totalHighestTaxesPrice) {
		this.totalHighestTaxesPrice = totalHighestTaxesPrice;
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}

}
