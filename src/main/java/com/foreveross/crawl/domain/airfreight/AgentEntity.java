package com.foreveross.crawl.domain.airfreight;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;

/**
 * 代理实体模型啊骚年
 * 
 * @author Administrator
 * 
 */

public class AgentEntity extends BaseHBaseEntity implements JSONString {

	private static final long serialVersionUID = -2993587767621246667L;

	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;

	/**
	 * 代理商名称如（兄弟旅行团） 多见于淘宝
	 */
	@HBaseColumn(qualifier = "name", family = "agentInfos")
	private String name;
	/**
	 * 机票原价
	 */
	@HBaseColumn(qualifier = "originalPrice", family = "agentInfos")
	private Double originalPrice;
	/**
	 * 机票优惠价
	 */
	@HBaseColumn(qualifier = "price", family = "agentInfos")
	private Double price;
	/**
	 * 税费
	 */
	@HBaseColumn(qualifier = "taxesPrice", family = "agentInfos")
	private Double taxesPrice;
	/**
	 * 代理商类型 0 表示直营(如 深航自己的航班) 1 表示承运(如 深航飞国航的航班) 2 表示普通代理(如 携程艺龙淘宝卖票)
	 */
	@HBaseColumn(qualifier = "agentType", family = "agentInfos")
	private Integer type;

	
	/**
	 * 代理商所在的公司,如（成都悦途信息技术有限公司）
	 */
	@HBaseColumn(qualifier = "companyName", family = "agentInfos")
	private String companyName;
	/**
	 * 代理商公司所在地区，如（成都市锦江工业园区三色路38号 ）
	 */
	@HBaseColumn(qualifier = "address", family = "agentInfos")
	private String address;
	
	
	
	
	
	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	

	public Double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getTaxesPrice() {
		return taxesPrice;
	}

	public void setTaxesPrice(Double taxesPrice) {
		this.taxesPrice = taxesPrice;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
