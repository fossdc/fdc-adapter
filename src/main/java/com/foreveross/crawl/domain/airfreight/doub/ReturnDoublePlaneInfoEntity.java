package com.foreveross.crawl.domain.airfreight.doub;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 * 回程航班的信息实体
 * 
 * @author Administrator
 * 
 */
public class ReturnDoublePlaneInfoEntity extends AbstractReturnPlaneInfoEntity implements JSONString {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4058960524929986157L;

	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;

	/**
	 * 中转的实体set，指回程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "returnTransits", family = "returnTransitsInfo")
	private ArrayList<ReturnTransitEntity> returnTransits = new ArrayList<ReturnTransitEntity>();

	/**
	 * 仓位信息，指回程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "returnCabins", family = "returnCabinsInfo")
	private Set<ReturnCabinEntity> returnCabins = new HashSet<ReturnCabinEntity>();

	/**
	 * 代理信息，指回程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "returnAgents", family = "returnAgentsInfo")
	private Set<ReturnAgentEntity> returnAgents = new HashSet<ReturnAgentEntity>();

	/**
	 * 经停信息，指回程
	 */
	@OneToMany
	@HBaseColumn(qualifier = "returnStopOvers", family = "returnStopOversInfo")
	private Set<ReturnStopOverEntity> returnStopOvers = new HashSet<ReturnStopOverEntity>();

	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	

	public Set<ReturnCabinEntity> getReturnCabins() {
		return returnCabins;
	}

	public void setReturnCabins(Set<ReturnCabinEntity> returnCabins) {
		this.returnCabins = returnCabins;
	}

	public Set<ReturnAgentEntity> getReturnAgents() {
		return returnAgents;
	}

	public void setReturnAgents(Set<ReturnAgentEntity> returnAgents) {
		this.returnAgents = returnAgents;
	}

	public ArrayList<ReturnTransitEntity> getReturnTransits() {
		return returnTransits;
	}

	public void setReturnTransits(ArrayList<ReturnTransitEntity> returnTransits) {
		this.returnTransits = returnTransits;
	}

	public Set<ReturnStopOverEntity> getReturnStopOvers() {
		return returnStopOvers;
	}

	public void setReturnStopOvers(Set<ReturnStopOverEntity> returnStopOvers) {
		this.returnStopOvers = returnStopOvers;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
