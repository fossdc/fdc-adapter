package com.foreveross.crawl.domain.airfreight;

import java.util.Date;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;

/**
 * 中转实体模型
 * 
 * @author Administrator
 * 
 */
public class TransitEntity extends BaseHBaseEntity implements JSONString {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4634127082540123740L;

	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;
	/**
	 * 运营商简称英文，如：MU
	 */
	@HBaseColumn(qualifier = "carrierKey", family = "transitsInfo")
	private String carrierKey;

	/**
	 * 运营商简称中文，如：南航
	 */
	@HBaseColumn(qualifier = "carrierName", family = "transitsInfo")
	private String carrierName;

	/**
	 * 运营商全名，如：中国海南航空公司
	 */
	@HBaseColumn(qualifier = "carrierFullName", family = "transitsInfo")
	private String carrierFullName;
	/**
	 * 航班号，如： MU3540
	 */
	@HBaseColumn(qualifier = "flightNo", family = "transitsInfo")
	private String flightNo;

	/**
	 * 实际乘坐航班号，如 SC1234 实际乘坐CZ1234
	 */
	@HBaseColumn(qualifier = "actuallyFlightNo", family = "transitsInfo")
	private String actuallyFlightNo;

	/**
	 * 飞机类型
	 */
	@HBaseColumn(qualifier = "flightType", family = "transitsInfo")
	private String flightType;

	/**
	 * 中转程中的起飞机场机场码
	 */
	@HBaseColumn(qualifier = "fromAirPort", family = "transitsInfo")
	private String fromAirPort;

	/**
	 * 中转程中的起飞机场名称
	 */
	@HBaseColumn(qualifier = "fromAirPortName", family = "transitsInfo")
	private String fromAirPortName;

	/**
	 * 中转程中的目的机场机场码
	 */
	@HBaseColumn(qualifier = "toAirPort", family = "transitsInfo")
	private String toAirPort;

	/**
	 * 中转程中的目的机场名称
	 */
	@HBaseColumn(qualifier = "toAirPortName", family = "transitsInfo")
	private String toAirPortName;

	/**
	 * 航班起飞时间
	 */
	@HBaseColumn(qualifier = "startTime", family = "transitsInfo", format = "yyyyMMddHHmmss")
	private Date startTime;

	/**
	 * 航班预计到达时间
	 */
	@HBaseColumn(qualifier = "endTime", family = "transitsInfo", format = "yyyyMMddHHmmss")
	private Date endTime;

	/**
	 * 距离下一次再次起飞的间隔时间（如北京--纽约 中途中转上海，就是在上海停留的时间，再次起飞时间减去到达时间）
	 */
	@HBaseColumn(qualifier = "stayTime", family = "transitsInfo")
	private Long stayTime;
	/**
	 * 中转时的舱位信息
	 */
	@HBaseColumn(qualifier = "cabinName", family = "transitsInfo")
	private String cabinName;
	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	public String getCarrierKey() {
		return carrierKey;
	}

	public void setCarrierKey(String carrierKey) {
		this.carrierKey = carrierKey;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getCarrierFullName() {
		return carrierFullName;
	}

	public void setCarrierFullName(String carrierFullName) {
		this.carrierFullName = carrierFullName;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getActuallyFlightNo() {
		return actuallyFlightNo;
	}

	public void setActuallyFlightNo(String actuallyFlightNo) {
		this.actuallyFlightNo = actuallyFlightNo;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getFromAirPort() {
		return fromAirPort;
	}

	public void setFromAirPort(String fromAirPort) {
		this.fromAirPort = fromAirPort;
	}

	public String getFromAirPortName() {
		return fromAirPortName;
	}

	public void setFromAirPortName(String fromAirPortName) {
		this.fromAirPortName = fromAirPortName;
	}

	public String getToAirPort() {
		return toAirPort;
	}

	public void setToAirPort(String toAirPort) {
		this.toAirPort = toAirPort;
	}

	public String getToAirPortName() {
		return toAirPortName;
	}

	public void setToAirPortName(String toAirPortName) {
		this.toAirPortName = toAirPortName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getStayTime() {
		return stayTime;
	}

	public void setStayTime(Long stayTime) {
		this.stayTime = stayTime;
	}

	public String getCabinName() {
		return cabinName;
	}

	public void setCabinName(String cabinName) {
		this.cabinName = cabinName;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
	
}
