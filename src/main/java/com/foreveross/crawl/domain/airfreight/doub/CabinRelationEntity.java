package com.foreveross.crawl.domain.airfreight.doub;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;

/**
 * 往返程仓位关系对应实体 去程的某个子舱 对应回程的某个子舱 打包价不一定等于两者相加
 * 
 * @author guokenye
 * 
 */
public class CabinRelationEntity extends BaseHBaseEntity implements JSONString {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6438536103656347478L;
	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;

	/**
	 * 仓位去程id
	 */
	@HBaseColumn(qualifier = "cabinId", family = "cabinRelationsInfo")
	private String cabinId;
	/**
	 * 回程仓位id
	 */
	@HBaseColumn(qualifier = "returnCabinId", family = "cabinRelationsInfo")
	private String returnCabinId;
	/**
	 * 打包价，裸价,很多网站来回程都有一个打包价，并不一定等于来回程加起来之和，很多时候我们只关心这个价格
	 */
	@HBaseColumn(qualifier = "fullPrice", family = "cabinRelationsInfo")
	private Double fullPrice;
	/**
	 * 打包价，税费
	 */
	@HBaseColumn(qualifier = "taxesPrice", family = "cabinRelationsInfo")
	private Double taxesPrice;
	/**
	 * 打包价合计价，裸价+税费
	 */
	@HBaseColumn(qualifier = "totalFullPrice", family = "cabinRelationsInfo")
	private Double totalFullPrice;
	
	public String getCabinId() {
		return cabinId;
	}

	public void setCabinId(String cabinId) {
		this.cabinId = cabinId;
	}

	public String getReturnCabinId() {
		return returnCabinId;
	}

	public void setReturnCabinId(String returnCabinId) {
		this.returnCabinId = returnCabinId;
	}

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Double getTaxesPrice() {
		return taxesPrice;
	}

	public void setTaxesPrice(Double taxesPrice) {
		this.taxesPrice = taxesPrice;
	}

	public Double getTotalFullPrice() {
		return totalFullPrice;
	}

	public void setTotalFullPrice(Double totalFullPrice) {
		this.totalFullPrice = totalFullPrice;
	}

	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
