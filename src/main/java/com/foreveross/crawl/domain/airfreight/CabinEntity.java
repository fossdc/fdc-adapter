package com.foreveross.crawl.domain.airfreight;

import java.util.UUID;

import javax.persistence.ManyToOne;

import net.sf.json.JSONString;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.foreveross.crawl.common.domain.BaseHBaseEntity;
import com.foreveross.crawl.common.jpa.annontation.HBaseColumn;

/**
 * 仓位实体
 * 
 * @author Administrator
 * 
 */
public class CabinEntity extends BaseHBaseEntity implements JSONString {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1508169393090681111L;
	/**
	 * 所属航班
	 */
	@ManyToOne
	private AbstractPlaneInfoEntity planeInfoEntity;
	/**
	 * 去程仓位信息的id，作为回程里面关系对应的一个键值
	 */
	@HBaseColumn(qualifier = "id", family = "cabinsInfo")
	private  String id ;
	/**
	 * 仓位等级（仓位类型）名称（如 商务舱、高端舱）
	 */
	@HBaseColumn(qualifier = "cabinType", family = "cabinsInfo")
	private String cabinType;
	/**
	 * 产品名称（如 海南的好客之旅 国航的高端商务） 一般依赖于仓位类型下面
	 */
	@HBaseColumn(qualifier = "productName", family = "cabinsInfo")
	private String productName;

	/**
	 * 子舱名称（如Y，A，C舱等） 一般依赖于仓位类型下面
	 */
	@HBaseColumn(qualifier = "subCabinName", family = "cabinsInfo")
	private String subCabinName;

	/**
	 * 折扣（如7.5折，0.75）
	 */
	@HBaseColumn(qualifier = "rebate", family = "cabinsInfo")
	private Double rebate;
	/**
	 * 此仓位裸价
	 */
	@HBaseColumn(qualifier = "price", family = "cabinsInfo")
	private Double price;
	/**
	 * 税费
	 */
	@HBaseColumn(qualifier = "taxesPrice", family = "cabinsInfo")
	private Double taxesPrice;
	/**
	 * 仓位实际支付价格（包括了税务燃油等）
	 */
	@HBaseColumn(qualifier = "originalPrice", family = "cabinsInfo")
	private Double originalPrice;
	/**
	 * 返现金额
	 */
	@HBaseColumn(qualifier = "backCash", family = "cabinsInfo")
	private Double backCash;
	/**
	 * 里程累计的比率（如 50%，0.5）
	 */
	@HBaseColumn(qualifier = "mileageRatio", family = "cabinsInfo")
	private Double mileageRatio;
	/**
	 * 退改签信息
	 */
	@HBaseColumn(qualifier = "tuigaiqian", family = "cabinsInfo")
	private String tuigaiqian;

	/**
	 * 直减优惠（不同于返现，多见于艺龙携程，是属于网站自己的专享服务）
	 */
	@HBaseColumn(qualifier = "zhijian", family = "cabinsInfo")
	private Double zhijian;
	/**
	 * 剩余座位信息(如 此仓位座位少5 预购从速等)
	 */
	@HBaseColumn(qualifier = "lastSeatInfo", family = "cabinsInfo")
	private String lastSeatInfo;

	@Override
	public String generateRowKey() {
		return planeInfoEntity.generateRowKey();
	}

	public AbstractPlaneInfoEntity getPlaneInfoEntity() {
		return planeInfoEntity;
	}

	public void setPlaneInfoEntity(AbstractPlaneInfoEntity planeInfoEntity) {
		this.planeInfoEntity = planeInfoEntity;
	}

	public String getCabinType() {
		return cabinType;
	}

	public void setCabinType(String cabinType) {
		this.cabinType = cabinType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubCabinName() {
		return subCabinName;
	}

	public void setSubCabinName(String subCabinName) {
		this.subCabinName = subCabinName;
	}

	public Double getRebate() {
		return rebate;
	}

	public void setRebate(Double rebate) {
		this.rebate = rebate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getBackCash() {
		return backCash;
	}

	public void setBackCash(Double backCash) {
		this.backCash = backCash;
	}

	public Double getMileageRatio() {
		return mileageRatio;
	}

	public void setMileageRatio(Double mileageRatio) {
		this.mileageRatio = mileageRatio;
	}

	public String getTuigaiqian() {
		return tuigaiqian;
	}

	public void setTuigaiqian(String tuigaiqian) {
		this.tuigaiqian = tuigaiqian;
	}

	public Double getZhijian() {
		return zhijian;
	}

	public void setZhijian(Double zhijian) {
		this.zhijian = zhijian;
	}

	public String getLastSeatInfo() {
		return lastSeatInfo;
	}

	public void setLastSeatInfo(String lastSeatInfo) {
		this.lastSeatInfo = lastSeatInfo;
	}

	public Double getTaxesPrice() {
		return taxesPrice;
	}

	public void setTaxesPrice(Double taxesPrice) {
		this.taxesPrice = taxesPrice;
	}

	public String getId() {
		if(id==null){
			id=UUID.randomUUID().toString();
		}
		return id;
	}

	public Double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	@Override
	public String toJSONString() {
		
		return super.convertJson(this);
	}
}
