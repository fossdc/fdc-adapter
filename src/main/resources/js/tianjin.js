Array.prototype.remove = function(dx) {
	if (isNaN(dx) || dx > this.length) {
		return false;
	}
	for ( var i = 0, n = 0; i < this.length; i++) {
		if (this[i] != this[dx]) {
			this[n++] = this[i]
		}
	}
	this.length -= 1
}
// 定义map
function Map() {
	this.container = {};
}
// 将key-value放入map中
Map.prototype.put = function(key, value) {
	try {
		if (key != null && key != "")
			this.container[key] = value;
	} catch (e) {
	}
};
// 根据key从map中取出对应的value
Map.prototype.get = function(key) {
	try {
		return this.container[key];
	} catch (e) {
		return e;
	}
};
// 判断map中是否包含指定的key
Map.prototype.containsKey = function(key) {
	try {
		for ( var p in this.container) {
			if (p == key)
				return true;
		}
		return false;
	} catch (e) {
		return e;
	}
};
// 返回map中的key值数组
Map.prototype.keyArray = function() {
	var keys = new Array();
	for ( var p in this.container) {
		keys.push(p);
	}
	return keys;
};
function renderFlight(request, selected) {
	var JSONobj1 = eval('(' + request + ')');
	var JSONobj = eval('(' + request + ')');
	var txt = '';
	var ass1 = 0;
	var ass = 0;
	if (JSONobj.flights == null || JSONobj.flights.length == 0) {
		if (JSONobj.route == 0) {
			txt += '<tr>&nbsp<table width="100%"><td width="100%" style="font-size:15pt;color:#F90;text-align:center;padding: 20px 10px;">SORRY！您所选择的航段天航当天无此航班，十分抱歉！</td></table></tr>';
		} else {
			txt += '<tr>&nbsp<table width="100%"><td width="100%" style="font-size:15pt;color:#F90;text-align:center;padding: 20px 10px;">sorry,票被抢光啦，请试试查询其他日期的航班。<br/>有问题拨打950710问我吧！</td></table></tr>'
		}
		return txt;
	}
	// 经津进京
	var txt5 = '<tr>&nbsp<div class="flight_result"><table class="fly_table"><thead><tr><th width="555px">航空公司</th><th width="555px">航班号</th><th width="555px">起飞/到达</th><th width="888px">经津进京</th></tr></thead><tbody>';
	var txt6 = '';
	// 经济舱
	var txt2 = '<tr>&nbsp<div class="flight_result"><table class="fly_table"><thead><tr><th width="555px">航空公司</th><th width="555px">航班号</th><th width="555px">起飞/到达</th><th width="555px">飞尝头等</th><th width="555px">全价票</th><th width="555px">当前折扣</th><th width="555px">网站专享</th></tr></thead><tbody>';
	var txt1 = '';
	// 头等舱
	var txt4 = '<tr>&nbsp<div class="flight_result"><table class="fly_table"><thead><tr><th width="555px">航空公司</th><th width="555px">航班号</th><th width="555px">起飞/到达</th><th width="888px">头等舱</th><th width="888px">头等悠享</th><th width="888px">飞尝头等</th></tr></thead><tbody>';
	var txt3 = '';
	// 删除非经津进京产品
	for ( var i = 0; i < JSONobj1.flights.length; i++) {
		var flight = JSONobj1.flights[i];
		var isJ = false;
		for ( var j = 0; j < flight.segments.length; j++) {
			var arr = new Array();
			var segment = flight.segments[j];
			for ( var k = 0; k < segment.cabins.length; k++) {
				var cabin = segment.cabins[k];
				for ( var l = 0; l < cabin.productOffers.length; l++) {
					var product = cabin.productOffers[l];
					if (segment.carrier == 'GS' && product.code == 'JJJJ') {
						isJ = true;
						break;
					}
				}
				if (isJ) {
					break;
				}
			}
			if (!isJ) {
				flight.segments.remove(j);
			}
		}
	}
	// 删除经津进京产品
	for ( var i = 0; i < JSONobj.flights.length; i++) {
		var flight = JSONobj.flights[i];
		var isJ = false;
		for ( var j = 0; j < flight.segments.length; j++) {
			var arr = new Array();
			var segment = flight.segments[j];
			for ( var k = 0; k < segment.cabins.length; k++) {
				var cabin = segment.cabins[k];
				for ( var l = 0; l < cabin.productOffers[l]; l++) {
					var product = cabin.productOffers[l];
					if (segment.carrier == 'GS' && product.code == 'JJJJ') {
						isJ = true;
						break;
					}
				}
				if (isJ) {
					break;
				}
			}
			if (isJ) {
				flight.segments.remove(j);
			}
		}
	}
	// 算折扣用
	var cabinyprice = 0;
	// jjjj
	for ( var i = 0; i < JSONobj1.flights.length; i++) {
		var flight = JSONobj1.flights[i];
		for ( var j = 0; j < flight.segments.length; j++) {
			var segment = flight.segments[j];
			var cabins = segment.cabins;
			var fltBlockId = '' + i + '' + j + segment.carrier
					+ segment.flightNo;
			var lowestCabin = cabins[0], lowestPd = cabins[0].productOffers[0];
			var lowCabinMap = new Map();
			// jjjj
			var displaycount2 = 0;
			for ( var k = 0; k < cabins.length; k++) {
				var cabin = segment.cabins[k];
				for ( var l = 0; l < cabin.productOffers.length; l++) {
					var product = cabin.productOffers[l];
					// PGSGS-940惠通天下2产品订单中折扣显示 ，全价就是Y舱的MZMK的价格
					if (product.code == "MZMK" && cabin.cabinCode == 'Y') {
						cabinyprice = product.adultFare.marketFare;
					}
					if (product.adultFare.farePrice < lowestPd.adultFare.farePrice) {
						lowestCabin = cabin;
						lowestPd = product;
					}
					var ca = lowCabinMap.get(product.code);
					if (ca == null) {
						lowCabinMap.put(product.code, cabin);
					} else {
						var caPd;
						for ( var m = 0; m < ca.productOffers.length; m++) {
							if (ca.productOffers[m].code = product.code)
								caPd = ca.productOffers[m];
						}
						if (product.adultFare.farePrice < caPd.adultFare.farePrice) {
							lowCabinMap.put(product.code, cabin);
						}
					}
				}
			}
			// 承运人
			if (segment.carrier == 'JD') {
				txt6 += '<tr><td>首都航空</td>';
			} else if (segment.carrier == 'GS') {
				txt6 += '<tr><td>天津航空</td>';
			} else if (segment.carrier == 'PN') {
				txt6 += '<tr><td>西部航空</td>';
			} else if (segment.carrier == '8L') {
				txt6 += '<tr><td>祥鹏航空</td>';
			} else if (segment.carrier == 'HU') {
				txt6 += '<tr><td>海南航空</td>';
			}
			// 航班号
			txt6 += '<td>' + segment.carrier + segment.flightNo + '</td>';
			// 起飞到达
			txt6 += '<td><strong>' + (segment.departureTime).substring(11, 16)
					+ '</strong><br/>' + segment.arriveTime.substring(11, 16)
					+ '</td>';
			var stoptimes = '无';
			if (segment.stops > 0) {
				stoptimes = segment.stops + '次';
			}
			// 机型
			// PGSGS-1189 订座系统中的32F机型在网站中对应显示为320
			// PGSGS-1228 订座系统中的32G机型在网站中对应显示为320
			var airStyle = '';
			if (segment.aircraftStyle == '32F'
					|| segment.aircraftStyle == '32G') {
				airStyle = '320';
			} else {
				airStyle = segment.aircraftStyle;
			}
			if (lowCabinMap.keyArray().length > 0) {
				for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
					var key = lowCabinMap.keyArray()[m];
					// 舱位
					var tmpCabin = lowCabinMap.get(key);
					var seatHave = tmpCabin.inventory;
					if (tmpCabin.status == 'AP') {
						seatHave = '*需申请*';
					} else if (tmpCabin.inventory > 9) {
						seatHave = '充足';
					} else {
						seatHave = tmpCabin.inventory;
					}
					for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
						var tmpPd = tmpCabin.productOffers[p];
						if (key == tmpPd.code) {
							displaycount2++;
							// PGSGS-940惠通天下2产品订单中折扣显示
							// ，全价就是Y舱的MZMK的价格，折扣就是惠通天下的价格除以Y舱MZMK的价格
							var nCabinDiscount2 = "";
							var remarkjjjj = tmpCabin.remark
									+ '<br/>4、仅限目的地为北京的旅客购买。';
							if (cabinyprice != 0) {
								if (tmpPd.adultFare.farePrice < cabinyprice) {
									nCabinDiscount2 = (tmpPd.adultFare.farePrice
											/ cabinyprice * 10).toFixed(1)
											+ "折";
								}
								if (tmpPd.adultFare.farePrice == cabinyprice) {
									nCabinDiscount2 = "全价";
								}
							}
							var radioVal = lowestCabin.cabinCode + ';'
									+ segment.carrier + ';' + segment.flightNo
									+ ';' + segment.originCity + ';'
									+ segment.destinationCity + ';'
									+ lowestPd.code;
							// jjjj列
							txt6 += '<td><input type="radio" name="' + selected
									+ '" id="' + selected + '_' + tmpPd.code
									+ '_' + tmpCabin.cabinCode + '_'
									+ fltBlockId + '" value="' + radioVal
									+ '" onclick="selectCabinOp(\''
									+ fltBlockId + '\', \'' + remarkjjjj
									+ '\', \'' + tmpCabin.remark + '\', \''
									+ selected + '\', \'' + seatHave + '\', \''
									+ tmpPd.adultFare.airportTax + '+'
									+ tmpPd.adultFare.fuelSurcharge + '\',\''
									+ tmpPd.adultFare.returnPoint + '\', \''
									+ nCabinDiscount2 + '\', \'' + stoptimes
									+ '\', \'' + airStyle + '\', \''
									+ cabinType + '\',this)"/><strong>￥'
									+ tmpPd.adultFare.farePrice + '</strong>';
							txt6 += '</td>';
						}
					}
				}
				txt6 += '</tr>';
			}
		}
	}
	txt6 += '</tbody></table></tr><div class="info_box" id="box" style="display:none"><div class="fly_info" id="fly_info"></div><div class="use_rule" id="use_rule"></div></div></div>';
	if (displaycount2 > 0) {
		var jproduct = document.getElementById('jproduct');
		var jcontent = document.getElementById('jcontent');
		jcontent.innerHTML = txt5 + txt6;
		jproduct.style.display = 'block';
	}
	// 非jjjj
	for ( var i = 0; i < JSONobj.flights.length; i++) {
		var flight = JSONobj.flights[i];
		for ( var j = 0; j < flight.segments.length; j++) {
			var segment = flight.segments[j];
			var cabins = segment.cabins;
			var fltBlockId = '' + i + '' + j + segment.carrier
					+ segment.flightNo;
			var lowestCabin = null, lowestPd = null;
			var lowCabinMap = new Map();
			var lowMZMKMap = new Map();
			var cabinType = JSONobj.cabinType;
			var stoptimes = '无';
			if (segment.stops > 0) {
				stoptimes = segment.stops + '次';
			}
			// 机型
			// PGSGS-1189 订座系统中的32F机型在网站中对应显示为320
			// PGSGS-1228 订座系统中的32G机型在网站中对应显示为320
			var airStyle = '';
			if (segment.aircraftStyle == '32F'
					|| segment.aircraftStyle == '32G') {
				airStyle = '320';
			} else {
				airStyle = segment.aircraftStyle;
			}
			// 经济舱
			if (cabinType == "economyClass") {
				for ( var k = 0; k < cabins.length; k++) {
					var cabin = segment.cabins[k];
					if (!(cabin.cabinCode == "F" || cabin.cabinCode == "P")) {
						for ( var l = 0; l < cabin.productOffers.length; l++) {
							var product = cabin.productOffers[l];
							if (product.code != "FClass"
									&& product.code != "GSPC") {
								if (lowestCabin == null && lowestPd == null
										&& product.code != "JMJP"
										&& product.code != "WEB") {
									lowestCabin = cabin;
									lowestPd = product;
								} else if (product.code != "JMJP"
										&& product.code != "WEB"
										&& product.adultFare.farePrice < lowestPd.adultFare.farePrice) {
									lowestCabin = cabin;
									lowestPd = product;
								}
							}
							if (product.code == "MZMK"
									&& cabin.cabinCode == "Y") {
								cabinyprice = product.adultFare.marketFare;
							}
							var ca = lowCabinMap.get(product.code);
							if (ca == null && product.code != "MZMK") {
								lowCabinMap.put(product.code, cabin);
							} else if (ca == null && product.code == "MZMK"
									&& cabin.cabinCode == "Y") {
								lowCabinMap.put(product.code, cabin);
							} else if (product.code == "MZMK") {
								var caPds;
								for ( var m = 0; m < ca.productOffers.length; m++) {
									if (ca.productOffers[m].code == product.code)
										caPds = ca.productOffers[m];
								}
								if (product.adultFare.farePrice < caPds.adultFare.farePrice) {
									lowMZMKMap.put(product.code, cabin);
								}
							} else if (ca != null && product.code != "MZMK") {
								var caPd;
								for ( var m = 0; m < ca.productOffers.length; m++) {
									if (ca.productOffers[m].code == product.code)
										caPd = ca.productOffers[m];
								}
								if (product.adultFare.farePrice < caPd.adultFare.farePrice) {
									lowCabinMap.put(product.code, cabin);
								}
							}
						}
					}
				}
			} else if (cabinType == "FClass") {// 头等舱
				for ( var k = 0; k < cabins.length; k++) {
					var cabin = segment.cabins[k];
					if (cabin.cabinCode == "F" || cabin.cabinCode == "P"
							|| cabin.cabinCode == "A") {
						for ( var l = 0; l < cabin.productOffers.length; l++) {
							var product = cabin.productOffers[l];
							if (product.code == "FClass"
									|| product.code == "FCTD"
									|| product.code == "GSPC") {
								if (lowestCabin == null && lowestPd == null) {
									lowestCabin = cabin;
									lowestPd = product;
								} else if (product.adultFare.farePrice < lowestPd.adultFare.farePrice) {
									lowestCabin = cabin;
									lowestPd = product;
								}
							}
							var ca = lowCabinMap.get(product.code);
							if (ca == null) {
								lowCabinMap.put(product.code, cabin);
							} else {
								var caPd;
								for ( var m = 0; m < ca.productOffers.length; m++) {
									if (ca.productOffers[m].code == product.code)
										caPd = ca.productOffers[m];
								}
								if (product.adultFare.farePrice < caPd.adultFare.farePrice) {
									lowCabinMap.put(product.code, cabin);
								}
							}
						}
					}
				}
			}
			// 经济舱标志位
			var displaycount = 0;
			// 头等舱标志位
			var displaycount1 = 0;
			if (lowestCabin != null) {
				/*if (document.getElementById("orgCity1").value == segment.originCity) {
					if (document.getElementById("lowestPrice").value == 0
							|| document.getElementById("lowestPrice").value > lowestPd.adultFare.farePrice) {
						document.getElementById("lowestPrice").value = lowestPd.adultFare.farePrice;
					}
				}*/
			}
			if (lowCabinMap.keyArray().length > 0) {
				// 头等舱显示
				if (cabinType == "FClass") {
					for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
						var key = lowCabinMap.keyArray()[m];
						if (!(key == "FClass" || key == "FCTD" || key == "GSPC"))
							continue;
						// 舱位
						var tmpCabin = lowCabinMap.get(key);
						for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
							var tmpPd = tmpCabin.productOffers[p];
							if (key == tmpPd.code && key == "FClass") {
								displaycount1++;
							} else if (key == tmpPd.code && key == "GSPC") {
								displaycount1++;
							} else if (key == tmpPd.code && key == "FCTD") {
								displaycount1++;
							}
						}
						if (displaycount1 > 0) {
							ass1++;
						}
					}
				}
				if (cabinType == "economyClass") {
					for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
						var key = lowCabinMap.keyArray()[m];
						if (key == "FClass" || key == "GSPC")
							continue;
						// 舱位
						var tmpCabin = lowCabinMap.get(key);
						var tmpCabinCode = tmpCabin.cabinCode;
						for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
							var tmpPd = tmpCabin.productOffers[p];
							if (key == tmpPd.code && tmpPd.code == "FCTD") {
								displaycount++;
							} else if (key == tmpPd.code
									&& tmpPd.code == "MZMK"
									&& tmpCabinCode == "Y") {
								displaycount++;
							} else if (key == tmpPd.code
									&& (tmpPd.code == "HTTY"
											|| tmpPd.code == "HTTX"
											|| tmpPd.code == "CDJ" || tmpPd.code == "BXL")) {
								displaycount++;
							}
							if (displaycount > 0) {
								ass++;
							}
						}
					}
					for ( var n = 0; n < lowMZMKMap.keyArray().length; n++) {
						var key = lowMZMKMap.keyArray()[n];
						if (key == "FClass" || key == "GSPC")
							continue;
						var tmpCabin = lowMZMKMap.get(key);
						var tmpCabinCode = tmpCabin.cabinCode;
						for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
							var tmpPd = tmpCabin.productOffers[p];
							if (key == tmpPd.code/* && tmpPd.code== "MZMK" */) {
								displaycount++;
							}
							if (displaycount > 0) {
								ass++;
							}
						}
					}
				}
			}
			if (displaycount > 0 || displaycount1 > 0) {
				ass++;
				// 承运人
				if (segment.carrier == 'JD') {
					txt1 += '<tr><td >首都航空</td>';
					txt3 += '<tr><td >首都航空</td>';
				} else if (segment.carrier == 'GS') {
					txt1 += '<tr><td >天津航空</td>';
					txt3 += '<tr><td >天津航空</td>';
				} else if (segment.carrier == 'PN') {
					txt1 += '<tr><td >西部航空</td>';
					txt3 += '<tr><td >西部航空</td>';
				} else if (segment.carrier == '8L') {
					txt1 += '<tr><td >祥鹏航空</td>';
					txt3 += '<tr><td >祥鹏航空</td>';
				} else if (segment.carrier == 'HU') {
					txt1 += '<tr><td >海南航空</td>';
					txt3 += '<tr><td >海南航空</td>';
				}
				// 航班号
				txt1 += '<td>' + segment.carrier + segment.flightNo + '</td>';
				txt3 += '<td>' + segment.carrier + segment.flightNo + '</td>';
				// 起飞到达
				txt1 += '<td><strong>'
						+ (segment.departureTime).substring(11, 16)
						+ '</strong><br/>'
						+ segment.arriveTime.substring(11, 16) + '</td>';
				txt3 += '<td><strong>'
						+ (segment.departureTime).substring(11, 16)
						+ '</strong><br/>'
						+ segment.arriveTime.substring(11, 16) + '</td>';
				// 经济舱1列标志位
				var a = true;
				// 经济舱2列标志位
				var b = true;
				// 经济舱3列标志位
				var q = true;
				// 经济舱4列标志位
				var w = true;
				// 头等舱1列标志位
				var a1 = true;
				// 头等舱2列标志位
				var b1 = true;
				// 头等舱3列标志位
				var c1 = true;
				if (lowCabinMap.keyArray().length > 0) {
					if (cabinType == "FClass") {
						// 头等舱
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (!(key == "FClass" || key == "FCTD" || key == "GSPC"))
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							var seatHave = tmpCabin.inventory;
							if (tmpCabin.status == 'AP') {
								seatHave = '*需申请*';
							} else if (tmpCabin.inventory > 9) {
								seatHave = '充足';
							} else {
								seatHave = tmpCabin.inventory;
							}
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								if (key == tmpPd.code && key == "FClass") {
									// 全价就是Y舱的MZMK的价格，折扣就是惠通天下的价格除以Y舱MZMK的价格
									var nCabinDiscount2 = "";
									if (cabinyprice != 0) {
										if (tmpPd.adultFare.farePrice < cabinyprice) {
											nCabinDiscount2 = (tmpPd.adultFare.farePrice
													/ cabinyprice * 10)
													.toFixed(1)
													+ "折";
										}
										if (tmpPd.adultFare.farePrice == cabinyprice) {
											nCabinDiscount2 = "全价";
										}
									}
									var radioVal = tmpCabin.cabinCode + ';'
											+ segment.carrier + ';'
											+ segment.flightNo + ';'
											+ segment.originCity + ';'
											+ segment.destinationCity + ';'
											+ tmpPd.code;
									txt3 += '<td><input type="radio" name="'
											+ selected + '" id="' + selected
											+ '_' + tmpPd.code + '_'
											+ tmpCabin.cabinCode + '_'
											+ fltBlockId + '" value="'
											+ radioVal
											+ '" onclick="selectCabinOp(\''
											+ fltBlockId + '\', \''
											+ tmpCabin.remark + '\', \''
											+ tmpCabin.remark + '\', \''
											+ selected + '\', \'' + seatHave
											+ '\', \''
											+ tmpPd.adultFare.airportTax + '+'
											+ tmpPd.adultFare.fuelSurcharge
											+ '\',\''
											+ tmpPd.adultFare.returnPoint
											+ '\', \'' + nCabinDiscount2
											+ '\', \'' + stoptimes + '\', \''
											+ airStyle + '\', \'' + cabinType
											+ '\',\'' + tmpCabin.cabinCode
											+ '\',this, \'' + '头等舱'
											+ '\')"/><strong>￥'
											+ tmpPd.adultFare.farePrice
											+ '</strong>';
									txt3 += '</td>';
									a1 = false;
								}
							}
						}
						if (a1) {
							txt3 += '<td class="td1"><strong>--</strong></td>';
						}
						// 头等悠享
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (!(key == "FClass" || key == "FCTD" || key == "GSPC"))
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							var seatHave = tmpCabin.inventory;
							if (tmpCabin.status == 'AP') {
								seatHave = '*需申请*';
							} else if (tmpCabin.inventory > 9) {
								seatHave = '充足';
							} else {
								seatHave = tmpCabin.inventory;
							}
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								if (key == tmpPd.code && key == "GSPC") {
									// 全价就是Y舱的MZMK的价格，折扣就是惠通天下的价格除以Y舱MZMK的价格
									var nCabinDiscount2 = "";
									if (cabinyprice != 0) {
										if (tmpPd.adultFare.farePrice < cabinyprice) {
											nCabinDiscount2 = (tmpPd.adultFare.farePrice
													/ cabinyprice * 10)
													.toFixed(1)
													+ "折";
										}
										if (tmpPd.adultFare.farePrice == cabinyprice) {
											nCabinDiscount2 = "全价";
										}
									}
									var radioVal = tmpCabin.cabinCode + ';'
											+ segment.carrier + ';'
											+ segment.flightNo + ';'
											+ segment.originCity + ';'
											+ segment.destinationCity + ';'
											+ tmpPd.code;
									txt3 += '<td><input type="radio" name="'
											+ selected + '" id="' + selected
											+ '_' + tmpPd.code + '_'
											+ tmpCabin.cabinCode + '_'
											+ fltBlockId + '" value="'
											+ radioVal
											+ '" onclick="selectCabinOp(\''
											+ fltBlockId + '\', \''
											+ tmpCabin.remark + '\', \''
											+ tmpCabin.remark + '\', \''
											+ selected + '\', \'' + seatHave
											+ '\', \''
											+ tmpPd.adultFare.airportTax + '+'
											+ tmpPd.adultFare.fuelSurcharge
											+ '\',\''
											+ tmpPd.adultFare.returnPoint
											+ '\', \'' + nCabinDiscount2
											+ '\', \'' + stoptimes + '\', \''
											+ airStyle + '\', \'' + cabinType
											+ '\',\'' + tmpCabin.cabinCode
											+ '\',this, \'' + '头等悠享'
											+ '\')"/><strong>￥'
											+ tmpPd.adultFare.farePrice
											+ '</strong>';
									txt3 += '</td>';
									c1 = false;
								}
							}
						}
						if (c1) {
							txt3 += '<td class="td1"><strong>--</strong></td>';
						}
						// 飞尝头等
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (!(key == "FClass" || key == "FCTD" || key == "GSPC"))
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							var seatHave = tmpCabin.inventory;
							if (tmpCabin.status == 'AP') {
								seatHave = '*需申请*';
							} else if (tmpCabin.inventory > 9) {
								seatHave = '充足';
							} else {
								seatHave = tmpCabin.inventory;
							}
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								if (key == tmpPd.code && key == "FCTD") {
									// 全价就是Y舱的MZMK的价格，折扣就是惠通天下的价格除以Y舱MZMK的价格
									var nCabinDiscount2 = "";
									if (cabinyprice != 0) {
										if (tmpPd.adultFare.farePrice < cabinyprice) {
											nCabinDiscount2 = (tmpPd.adultFare.farePrice
													/ cabinyprice * 10)
													.toFixed(1)
													+ "折";
										}
										if (tmpPd.adultFare.farePrice == cabinyprice) {
											nCabinDiscount2 = "全价";
										}
									}
									var radioVal = tmpCabin.cabinCode + ';'
											+ segment.carrier + ';'
											+ segment.flightNo + ';'
											+ segment.originCity + ';'
											+ segment.destinationCity + ';'
											+ tmpPd.code;
									txt3 += '<td><input type="radio" name="'
											+ selected + '" id="' + selected
											+ '_' + tmpPd.code + '_'
											+ tmpCabin.cabinCode + '_'
											+ fltBlockId + '" value="'
											+ radioVal
											+ '" onclick="selectCabinOp(\''
											+ fltBlockId + '\', \''
											+ tmpCabin.remark + '\', \''
											+ tmpCabin.remark + '\', \''
											+ selected + '\', \'' + seatHave
											+ '\', \''
											+ tmpPd.adultFare.airportTax + '+'
											+ tmpPd.adultFare.fuelSurcharge
											+ '\',\''
											+ tmpPd.adultFare.returnPoint
											+ '\', \'' + nCabinDiscount2
											+ '\', \'' + stoptimes + '\', \''
											+ airStyle + '\', \'' + cabinType
											+ '\',\'' + tmpCabin.cabinCode
											+ '\',this,\'' + '飞尝头等'
											+ '\')"/><strong>￥'
											+ tmpPd.adultFare.farePrice
											+ '</strong>';
									txt3 += '</td>';
									b1 = false;
								}
							}
						}
						if (b1) {
							txt3 += '<td class="td1"><strong>--</strong></td>';
						}
						txt3 += '</tr>';
					}
					if (cabinType == "economyClass") {
						// 经济舱第一列产品
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (key == "FClass" || key == "GSPC")
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							var seatHave = tmpCabin.inventory;
							if (tmpCabin.status == 'AP') {
								seatHave = '*需申请*';
							} else if (tmpCabin.inventory > 9) {
								seatHave = '充足';
							} else {
								seatHave = tmpCabin.inventory;
							}
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								if (key == tmpPd.code && tmpPd.code == "FCTD") {
									// 全价就是Y舱的MZMK的价格，折扣就是惠通天下的价格除以Y舱MZMK的价格
									var nCabinDiscount2 = "";
									if (cabinyprice != 0) {
										if (tmpPd.adultFare.farePrice < cabinyprice) {
											nCabinDiscount2 = (tmpPd.adultFare.farePrice
													/ cabinyprice * 10)
													.toFixed(1)
													+ "折";
										}
										if (tmpPd.adultFare.farePrice == cabinyprice) {
											nCabinDiscount2 = "全价";
										}
									}
									var radioVal = tmpCabin.cabinCode + ';'
											+ segment.carrier + ';'
											+ segment.flightNo + ';'
											+ segment.originCity + ';'
											+ segment.destinationCity + ';'
											+ tmpPd.code;
									var cRemark = tmpCabin.remark;
									// FCTD 飞尝头等舱 产品的第一点使用说明标红
									if (tmpPd.code == 'FCTD') {
										cRemark = '<font style = color:red>'
												+ cRemark.substring(0, cRemark
														.indexOf("；") + 1)
												+ '</font>'
												+ cRemark.substring(cRemark
														.indexOf("；") + 1,
														cRemark.length);
									}
									txt1 += '<td><input type="radio" name="'
											+ selected + '" id="' + selected
											+ '_' + tmpPd.code + '_'
											+ tmpCabin.cabinCode + '_'
											+ fltBlockId + '" value="'
											+ radioVal
											+ '" onclick="selectCabinOp(\''
											+ fltBlockId + '\', \'' + cRemark
											+ '\', \'' + tmpCabin.remark
											+ '\', \'' + selected + '\', \''
											+ seatHave + '\', \''
											+ tmpPd.adultFare.airportTax + '+'
											+ tmpPd.adultFare.fuelSurcharge
											+ '\',\''
											+ tmpPd.adultFare.returnPoint
											+ '\', \'' + nCabinDiscount2
											+ '\', \'' + stoptimes + '\', \''
											+ airStyle + '\', \'' + cabinType
											+ '\',\'' + tmpCabin.cabinCode
											+ '\',this,\'' + '飞尝头等舱'
											+ '\')"/><strong>￥'
											+ tmpPd.adultFare.farePrice
											+ '</strong>';
									txt1 += '</td>';
									a = false;
								}
							}
						}
						if (a) {
							txt1 += '<td class="td1"><strong>--</strong></td>';
						}
						// 经济舱第二列产品
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (key == "FClass" || key == "GSPC")
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							var seatHave = tmpCabin.inventory;
							if (tmpCabin.status == 'AP') {
								seatHave = '*需申请*';
							} else if (tmpCabin.inventory > 9) {
								seatHave = '充足';
							} else {
								seatHave = tmpCabin.inventory;
							}
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								var tmpCabinCode = tmpCabin.cabinCode;
								if (key == tmpPd.code && tmpPd.code == "MZMK"
										&& tmpCabinCode == "Y") {
									var nCabinDiscount2 = "";
									if (cabinyprice != 0) {
										if (tmpPd.adultFare.farePrice < cabinyprice) {
											nCabinDiscount2 = (tmpPd.adultFare.farePrice
													/ cabinyprice * 10)
													.toFixed(1)
													+ "折";
										}
										if (tmpPd.adultFare.farePrice == cabinyprice) {
											nCabinDiscount2 = "全价";
										}
									}
									var radioVal = tmpCabin.cabinCode + ';'
											+ segment.carrier + ';'
											+ segment.flightNo + ';'
											+ segment.originCity + ';'
											+ segment.destinationCity + ';'
											+ tmpPd.code;
									txt1 += '<td class="td1"><input type="radio" name="'
											+ selected
											+ '" id="'
											+ selected
											+ '_'
											+ tmpPd.code
											+ '_'
											+ tmpCabin.cabinCode
											+ '_'
											+ fltBlockId
											+ '" value="'
											+ radioVal
											+ '" onclick="selectCabinOp(\''
											+ fltBlockId
											+ '\', \''
											+ tmpCabin.remark
											+ '\', \''
											+ tmpCabin.remark
											+ '\', \''
											+ selected
											+ '\', \''
											+ seatHave
											+ '\', \''
											+ tmpPd.adultFare.airportTax
											+ '+'
											+ tmpPd.adultFare.fuelSurcharge
											+ '\',\''
											+ tmpPd.adultFare.returnPoint
											+ '\', \''
											+ nCabinDiscount2
											+ '\', \''
											+ stoptimes
											+ '\', \''
											+ airStyle
											+ '\', \''
											+ cabinType
											+ '\',\''
											+ tmpCabin.cabinCode
											+ '\',this,\''
											+ '全价舱'
											+ '\')"/><strong>￥'
											+ tmpPd.adultFare.farePrice
											+ '</strong>';
									txt1 += '</td>';
									b = false;
								}
							}
						}
						if (b) {
							txt1 += '<td class="td1"><strong>--</strong></td>';
						}
						// 经济舱第三列产品
						var lc = null;
						var lp = null;
						var proName = null;
						for ( var m = 0; m < lowMZMKMap.keyArray().length; m++) {
							var key = lowMZMKMap.keyArray()[m];
							if (key == "FClass" || key == "FClass")
								continue;
							// 舱位
							var tmpCabin = lowMZMKMap.get(key);
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								var tmpCabinCode = tmpCabin.cabinCode;
								var ye = "YBHKLMQXUE".indexOf(tmpCabinCode) + 1;
								if (key == tmpPd.code
										&& (("YBHKLMQXUE".indexOf(tmpCabinCode) + 1)
												|| tmpCabinCode == "Z"
												|| tmpCabinCode == "T"
												|| tmpCabinCode == "M1" || tmpCabinCode == "Q1")) {
									for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
										var tmpPd = tmpCabin.productOffers[p];
										var tmpPt = tmpPd;
										if (key == tmpPd.code) {
											if (tmpPd.code == "MZMK") {
												proName = "明折明扣";
											}
											if (lc == null && lp == null) {
												lc = tmpCabin;
												lp = tmpPd;
											} else if (tmpPd.adultFare.farePrice < lp.adultFare.farePrice) {
												lc = tmpCabin;
												lp = tmpPd;
											}
										}
									}
									var seatHave = lc.inventory;
									if (lc.status == 'AP') {
										seatHave = '*需申请*';
									} else if (lc.inventory > 9) {
										seatHave = '充足';
									} else {
										seatHave = lc.inventory;
									}
								}
							}
						}
						if (lc != null && lp != null) {
							if (cabinyprice != 0) {
								var nCabinDiscount = 0;
								var nCabinDiscount2 = "";
								if (cabinyprice != 0) {
									nCabinDiscount = round(
											(lp.adultFare.farePrice / cabinyprice) * 100,
											0);
								}
								if (lp.adultFare.farePrice < cabinyprice) {
									nCabinDiscount2 = (lp.adultFare.farePrice
											/ cabinyprice * 10).toFixed(1)
											+ "折";
								}
								if (lp.adultFare.farePrice == cabinyprice) {
									nCabinDiscount2 = "全价";
								}
							}
							var remark3th = '';
							remark3th = lc.remark;
							if (nCabinDiscount < 40 && lp.code == "HTTX") {
								remark3th = '仅限网上退票，只退燃油附加费和民航发展基金。';
							}
							if (lp.code == 'HTTY') {
								remark3th += "<br/>5、仅限网购。";
							}
							var radioVal = lc.cabinCode + ';' + segment.carrier
									+ ';' + segment.flightNo + ';'
									+ segment.originCity + ';'
									+ segment.destinationCity + ';' + lp.code;
							txt1 += '<td class="td1"><input type="radio" name="'
									+ selected
									+ '" id="'
									+ selected
									+ '_'
									+ lp.code
									+ '_'
									+ lc.cabinCode
									+ '_'
									+ fltBlockId
									+ '" value="'
									+ radioVal
									+ '" onclick="selectCabinOp(\''
									+ fltBlockId
									+ '\', \''
									+ remark3th
									+ '\', \''
									+ lc.remark
									+ '\', \''
									+ selected
									+ '\', \''
									+ seatHave
									+ '\', \''
									+ lp.adultFare.airportTax
									+ '+'
									+ lp.adultFare.fuelSurcharge
									+ '\',\''
									+ lp.adultFare.returnPoint
									+ '\', \''
									+ nCabinDiscount2
									+ '\', \''
									+ stoptimes
									+ '\', \''
									+ airStyle
									+ '\', \''
									+ cabinType
									+ '\',\''
									+ lc.cabinCode
									+ '\',this, \''
									+ proName
									+ '\')"/><strong>￥'
									+ lp.adultFare.farePrice + '</strong>';
							txt1 += '</td>';
							q = false;
						}
						if (q) {
							txt1 += '<td class="td1"><strong>--</strong></td>';
						}
						// 经济舱第四列产品
						var lc = null;
						var lp = null;
						var proName = null;
						for ( var m = 0; m < lowCabinMap.keyArray().length; m++) {
							var key = lowCabinMap.keyArray()[m];
							if (key == "FClass" || key == "FClass")
								continue;
							// 舱位
							var tmpCabin = lowCabinMap.get(key);
							for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
								var tmpPd = tmpCabin.productOffers[p];
								if (key == tmpPd.code
										&& (tmpPd.code == "HTTY"
												|| tmpPd.code == "HTTX"
												|| tmpPd.code == "CDJ" || tmpPd.code == "BXL")) {
									for ( var p = 0; p < tmpCabin.productOffers.length; p++) {
										var tmpPd = tmpCabin.productOffers[p];
										var tmpPt = tmpPd;
										if (key == tmpPd.code) {
											if (lc == null && lp == null) {
												lc = tmpCabin;
												lp = tmpPd;
												if (tmpPd.code == "HTTY") {
													proName = "惠通天下1";
												} else if (tmpPd.code == "HTTX") {
													proName = "惠通天下2";
												} else if (tmpPd.code == "BXL") {
													proName = "远期特价";
												} else if (tmpPd.code == "CDJ") {
													proName = "特价舱";
												}
											} else if (tmpPd.adultFare.farePrice < lp.adultFare.farePrice) {
												lc = tmpCabin;
												lp = tmpPd;
												if (tmpPd.code == "HTTY") {
													proName = "惠通天下1";
												} else if (tmpPd.code == "HTTX") {
													proName = "惠通天下2";
												} else if (tmpPd.code == "BXL") {
													proName = "远期特价";
												} else if (tmpPd.code == "CDJ") {
													proName = "特价舱";
												}
											}
										}
									}
									var seatHave = lc.inventory;
									if (lc.status == 'AP') {
										seatHave = '*需申请*';
									} else if (lc.inventory > 9) {
										seatHave = '充足';
									} else {
										seatHave = lc.inventory;
									}
								}
							}
						}
						if (lc != null && lp != null) {
							if (cabinyprice != 0) {
								var nCabinDiscount = 0;
								var nCabinDiscount2 = "";
								if (cabinyprice != 0 && lp.code == "HTTX") {
									nCabinDiscount = round(
											(lp.adultFare.farePrice / cabinyprice) * 100,
											0);
								}
								if (lp.adultFare.farePrice < cabinyprice) {
									nCabinDiscount2 = (lp.adultFare.farePrice
											/ cabinyprice * 10).toFixed(1)
											+ "折";
								}
								if (lp.adultFare.farePrice == cabinyprice) {
									nCabinDiscount2 = "全价";
								}
							}
							var remark4th = '';
							var remark4th = lc.remark;
							/*
							 * if(nCabinDiscount<40&& lp.code=="HTTX"){
							 * remark4th = '仅限网上退票，只退燃油附加费和民航发展基金。'; }
							 */
							if (lp.code == 'HTTY') {
								remark4th += "<br/>5、仅限网购。";
								remark4th += "<br/>6、您购买的惠通天下产品只在乘机后才返还积分，如果由于您个人的原因发生的机票变更和退票，将不能享受此项奖励。";
							}
							var radioVal = lc.cabinCode + ';' + segment.carrier
									+ ';' + segment.flightNo + ';'
									+ segment.originCity + ';'
									+ segment.destinationCity + ';' + lp.code;
							txt1 += '<td class="td1"><input type="radio" name="'
									+ selected
									+ '" id="'
									+ selected
									+ '_'
									+ lp.code
									+ '_'
									+ lc.cabinCode
									+ '_'
									+ fltBlockId
									+ '" value="'
									+ radioVal
									+ '" onclick="selectCabinOp(\''
									+ fltBlockId
									+ '\', \''
									+ remark4th
									+ '\', \''
									+ lc.remark
									+ '\', \''
									+ selected
									+ '\', \''
									+ seatHave
									+ '\', \''
									+ lp.adultFare.airportTax
									+ '+'
									+ lp.adultFare.fuelSurcharge
									+ '\',\''
									+ lp.adultFare.returnPoint
									+ '\', \''
									+ nCabinDiscount2
									+ '\', \''
									+ stoptimes
									+ '\', \''
									+ airStyle
									+ '\', \''
									+ cabinType
									+ '\',\''
									+ lc.cabinCode
									+ '\',this, \''
									+ proName
									+ '\')"/><strong>￥'
									+ lp.adultFare.farePrice + '</strong>';
							txt1 += '</td>';
							w = false;
						}
						if (w) {
							txt1 += '<td class="td1"><strong>--</strong></td>';
						}
						txt1 += '</tr>';
					}
				}
			}
		}
	}
	txt1 += '</tbody></table></tr>';
	if (selected == "selectedFlight") {
		txt1 += '<div class="info_box" id="box1" style="display:none"><div class="fly_info" id="fly_info1"></div><div class="use_rule" id="use_rule1"></div></div></div>';
	} else if (selected == "backFlight") {
		txt1 += '<div class="info_box" id="box1b" style="display:none"><div class="fly_info" id="fly_info1b"></div><div class="use_rule" id="use_rule1b"></div></div></div>';
	}
	txt3 += '</tbody></table></tr>';
	if (selected == "selectedFlight") {
		txt3 += '<div class="info_box" id="box2" style="display:none"><div class="fly_info" id="fly_info2"></div><div class="use_rule" id="use_rule2"></div></div></div>';
	} else if (selected == "backFlight") {
		txt3 += '<div class="info_box" id="box2b" style="display:none"><div class="fly_info" id="fly_info2b"></div><div class="use_rule" id="use_rule2b"></div></div></div>';
	}
	if (cabinType == "economyClass") {
		if (ass < 1) {
			txt = '<tr>&nbsp<table width="100%"><td width="100%" style="font-size:15pt;color:#F90;text-align:center;padding: 20px 10px;">sorry,票被抢光啦，请试试查询其他日期的航班。<br/>有问题拨打950710问我吧！</td></table></tr>';
			return txt;
		} else {
			return txt2 + txt1;
		}
	}
	if (cabinType == "FClass") {
		if (ass1 < 1) {
			txt = '<tr>&nbsp<table width="100%"><td width="100%" style="font-size:15pt;color:#F90;text-align:center;padding: 20px 10px;">sorry,票被抢光啦，请试试查询其他日期的航班。<br/>有问题拨打950710问我吧！</td></table></tr>';
			return txt;
		} else {
			return txt4 + txt3;
		}
	}
}
// 注：目前本js方法只支持一个中转站（两个航段），后期如需增加航段，需要修改本方法。注意segmentCount...
function renderFlight1(request, selected, divConnect) {
	var JSONobj = eval('(' + request + ')');
	var txt = '';
	if (JSONobj.flights1 == null || JSONobj.flights1.length == 0) {
		document.getElementById(divConnect).style.display = 'none';
		txt += '<br/><br/>很抱歉，没有符合条件的查询结果！<br/><br/>';
		return txt;
	}
	document.getElementById(divConnect).style.display = 'block';
	var tbNum = 0;
	for ( var i = 0; i < JSONobj.flights1.length; i++) {
		txt += '<table width="97%" border="0" cellpadding="5" cellspacing="0" class="table_ticket border_white_table">';
		txt += '<tr><td align="right" class="border_white_bottom font_black12" width="100%">';
		var flight = JSONobj.flights1[i];
		var segmentCount = 0;
		for ( var j = 0; j < flight.segments.length; j++) {
			segmentCount = segmentCount + 1;
			var arr = new Array();
			var segment = flight.segments[j];
			txt += '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ticket" id="'
					+ selected + 'Tb_' + tbNum + '"><tr>';
			txt += '<td class="font_grey12bold td_title">航段<font color="red">'
					+ (j + 1) + '</font></td>';
			txt += '<td class="font_grey12bold td_title">承运人</td>';
			txt += '<td class="font_grey12bold td_title">航班号</td>';
			txt += '<td class="font_grey12bold td_title">航班日期</td>';
			txt += '<td class="font_grey12bold td_title">起飞时间</td>';
			txt += '<td class="font_grey12bold td_title">到达时间</td>';
			txt += '<td class="font_grey12bold td_title">经停</td>';
			txt += '<td class="font_grey12bold td_title">机型</td>';
			txt += '<td class="font_grey12bold td_title">飞行里程</td>';
			txt += '</tr><tr>';
			txt += ' <td class="td_bg border_grey_bottom">'
					+ segment.orgAirport + '-' + segment.dstAirport + '</td>';
			if (segment.carrier == 'JD') {
				txt += ' <td class="td_bg border_grey_bottom"><font color="red">首都航空</font></div>';
				txt += ' <td class="td_bg border_grey_bottom">';
				txt += '<div class="aclogo_dxh">' + segment.carrier
						+ segment.flightNo + '</div></td>';
			}
			if (segment.carrier == 'GS') {
				txt += ' <td class="td_bg border_grey_bottom"><font color="red">天津航空</font></div>';
				txt += ' <td class="td_bg border_grey_bottom">';
				txt += '<div class="aclogo_dxh_gs">' + segment.carrier
						+ segment.flightNo + '</div></td>';
			}
			if (segment.carrier == 'PN') {
				txt += ' <td class="td_bg border_grey_bottom"><font color="red">西部航空</font></div>';
				txt += ' <td class="td_bg border_grey_bottom">';
				txt += '<div class="aclogo_dxh_pn">' + segment.carrier
						+ segment.flightNo + '</div></td>';
			}
			if (segment.carrier == '8L') {
				txt += ' <td class="td_bg border_grey_bottom"><font color="red">祥鹏航空</font></div>';
				txt += ' <td class="td_bg border_grey_bottom">';
				txt += '<div>' + segment.carrier + segment.flightNo
						+ '</div></td>';
			}
			if (segment.carrier == 'HU') {
				txt += ' <td class="td_bg border_grey_bottom"><font color="red">海南航空</font></div>';
				txt += ' <td class="td_bg border_grey_bottom">';
				txt += '<div">' + segment.carrier + segment.flightNo
						+ '</div></td>';
			}
			txt += ' <td class="td_bg border_grey_bottom"><span class="font_black12 td_bottom_padd1">'
					+ (segment.departureTime).substr(0, 10) + '</span></td>';
			txt += ' <td class="td_bg border_grey_bottom">'
					+ (segment.departureTime).substr(11, 16) + '</td>';
			txt += ' <td class="td_bg border_grey_bottom">'
					+ segment.arriveTime.substr(11, 16) + '</td>';
			txt += ' <td class="td_bg border_grey_bottom">' + segment.stops
					+ '次' + '</td>';
			// PGSGS-1189 订座系统中的32F机型在网站中对应显示为320
			// PGSGS-1228 订座系统中的32G机型在网站中对应显示为320
			if (segment.aircraftStyle == '32F'
					|| segment.aircraftStyle == '32G') {
				txt += ' <td class="td_bg border_grey_bottom" onclick="openpt();">'
						+ '320' + '</td>';
			} else {
				txt += ' <td class="td_bg border_grey_bottom" onclick="openpt();">'
						+ segment.aircraftStyle + '</td>';
			}
			txt += ' <td class="td_bg border_grey_bottom">' + segment.mileage
					+ 'KM</td>';
			txt += ' </tr><tr>';
			txt += ' <td class="td_bg font_grey12bold">产品名称</td>';
			txt += ' <td class="td_bg font_grey12bold">剩余座位</td>';
			txt += ' <td class="td_bg font_grey12bold">票价</td>';
			txt += ' <td class="td_bg font_grey12bold">优惠价</td>';
			txt += ' <td class="td_bg font_grey12bold">民航发展基金燃油费</td>';
			txt += ' <td class="td_bg font_grey12bold" colspan="3">&nbsp;</td>';
			txt += ' <td class="td_bg font_grey12bold">选择</td>';
			txt += ' </tr>';
			for ( var k = 0; k < segment.cabins.length; k++) {
				var cabin = segment.cabins[k];
				for ( var l = 0; l < cabin.productOffers.length; l++) {
					var product = cabin.productOffers[l];
					if (cabin.finalProductOffer != null
							&& product.recommended != null) {
						var trValue = '';
						var display = '';
						if (product.recommended === false) {
							trValue += ' <tr style="display:none" pn="'
									+ product.code
									+ '" re="'
									+ product.recommended
									+ '" class="trout1" title="header=[使用说明] body=[产品描述：'
									+ product.description + '<br/>使用规定：'
									+ cabin.remark + ']" >';
							trValue += ' <td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%" align="center"></td><td width="50%" align="left">['
									+ cabin.cabinCode
									+ " "
									+ cabin.cabinCodeDesc
									+ '] </td></tr></table></td>';
						} else {
							trValue += ' <tr pn="'
									+ product.code
									+ '" re="'
									+ product.recommended
									+ '" class="trout1" title="header=[使用说明] body=[产品描述：'
									+ product.description + '<br/>使用规定：'
									+ cabin.remark + ']" >';
							trValue += ' <td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%" align="center">'
									+ product.name
									+ '</td><td width="50%" align="left">['
									+ cabin.cabinCode
									+ " "
									+ cabin.cabinCodeDesc
									+ '] </td></tr></table></td>';
						}
						trValue += ' <td>';
						if (cabin.status == 'AP') {
							trValue += '*需申请*';
						} else if (cabin.inventory > 9) {
							trValue += '充足';
						} else {
							trValue += cabin.inventory;
						}
						var segDate = segment.departureTime;
						var key = cabin.cabinCode + ';' + segment.carrier + ';'
								+ segment.flightNo + ';';
						key += segment.originCity + ';'
								+ segment.destinationCity + ';' + product.code
								+ ';' + segment.departureTime + ';connect';
						trValue += '</td>';
						trValue += ' <td><del>' + cabin.adultFare.farePrice
								+ '</del></td>';
						if (product.recommended === true) {
							trValue += ' <td><div class="font_red14boldcandara">'
									+ product.adultFare.farePrice
									+ '</div></td>';
						} else {
							trValue += ' <td>' + product.adultFare.farePrice
									+ '</td>';
						}
						trValue += ' <td>' + product.adultFare.airportTax + '+'
								+ product.adultFare.fuelSurcharge + '</td>';
						trValue += ' <td colspan="3">';
						if (product.recommended === true) {
							trValue += '<div id="displayprice"><a href="javascript:void(0)" class="f_down" name="'
									+ selected
									+ '_sh_'
									+ tbNum
									+ '" id="'
									+ selected
									+ '_sh_'
									+ product.code
									+ '_'
									+ tbNum
									+ '_'
									+ k
									+ '_'
									+ l
									+ '" onclick="getHiddenTR(this)">显示全部价格</a></div>';
						}
						trValue += '</td>';
						if (segmentCount == 1) {
							trValue += ' <td><input type="radio" name="'
									+ selected + '" id="' + selected + '_'
									+ product.code + '_' + i + '_' + j
									+ '" value="' + key
									+ '" onclick="checkProduct1(this,\''
									+ selected + '\')"> </td>';
						} else {
							if (selected == 'connFlight') {
								trValue += ' <td><input type="radio" name="connFlightBack" id="backFlight_'
										+ product.code
										+ '_'
										+ i
										+ '_'
										+ j
										+ '" value="' + key + '">';
							} else {
								trValue += ' <td><input type="radio" name="backFlight" style="display:none" id="backFlight_'
										+ product.code
										+ '_'
										+ i
										+ '_'
										+ j
										+ '" value="' + key + '">';
							}
							trValue += ' </td>';
						}
						trValue += '</tr>';
						arr = addText(arr, product.code, trValue,
								product.recommended);
					}
				}
			}
			var txtTemp = '';
			if (arr != null) {
				for ( var m = 0; m < arr.length; m++) {
					var n = arr[m];
					if (n.recStr != "") {
						if (n.hiddenStr == "") {
							n.recStr = n.recStr.replace(
									/<div id="displayprice">(.*)<\/div>/,
									'<div id="displayprice"></div>');
						}
						txtTemp += n.recStr;
						txtTemp += n.hiddenStr;
						txtTemp += '<tr><td colspan="7" height="1"><div style="border-top:1px dashed #D6D3D6;margin:0px;padding:0px"></div></td></tr>';
					}
				}
			}
			txt += txtTemp;
			txt += '</table>';
			tbNum++;
		}
		txt += '</td></tr></table>';
	}
	return txt;
}
function round(v, e) {
	var t = 1;
	for (; e > 0; t *= 10, e--)
		;
	for (; e < 0; t /= 10, e++)
		;
	return Math.round(v * t) / t;
}
function getDefault() {
	return '<table width="100%"><td align="center"><img src="../images/ajax-loading.gif" border="0" /></td></table>';
}
function selectCabinOp(blockId, cabinremark, cabinremark2, sign, seatnum,
		taxes, returnP, discount, stopTimes, flyStyle, cType, whCab, obj, pro) {
	var tax1 = "";// 机建费
	var tax2 = "";// 燃油费
	var l = 1;
	var tax0 = taxes.split("+");
	tax1 += tax0[0];
	tax2 += tax0[1];
	tax1 += "元 ";
	tax2 += "元";
	checkProduct(obj);
	saveRule(cabinremark2, obj);
	// 增加票规框
	if (cType == "economyClass") {
		if (sign == "selectedFlight") {
			document.getElementById("use_rule1").innerHTML = "";
			document.getElementById("fly_info1").innerHTML = "";
			document.getElementById("box1").style.display = "block";
			document.getElementById("fly_info1").innerHTML = "<table style='width:100%' ><tr><td>产品名称：<span class='c_red p_r20'>"
					+ pro
					+ "（"
					+ whCab
					+ "舱）</span></td><td>积分：<span class='c_red p_r20'>"
					+ returnP
					+ "分</span></td>"
					+ "<td>折扣：<span class='c_red p_r20'>"
					+ discount
					+ "</span></td><td>机型：<span class='c_red p_r20' onclick='openpt();'>"
					+ flyStyle
					+ "</span></td></tr>"
					+ "<tr><td>剩余座位：<span class='c_red p_r20'>"
					+ seatnum
					+ "</span></td>"
					+ "<td>航班信息：经停：<span class='c_red p_r20'>"
					+ stopTimes
					+ "</span></td>"
					+ "<td>机建税：<span class='c_red p_r20'>"
					+ tax1
					+ "</span></td>"
					+ "<td>燃油税：<span class='c_red p_r20'>"
					+ tax2 + "</span></td></tr></table>";
			document.getElementById("use_rule1").innerHTML = "<p>使用规定：</p><p style='text-indent:0;padding-left:0'>"
					+ cabinremark + "</p>";
		}
		if (sign == "backFlight") {
			document.getElementById("use_rule1b").innerHTML = "";
			document.getElementById("fly_info1b").innerHTML = "";
			document.getElementById("box1b").style.display = "block";
			document.getElementById("fly_info1b").innerHTML = "<table style='width:100%' ><tr><td>产品名称：<span class='c_red p_r20'>"
					+ pro
					+ "（"
					+ whCab
					+ "舱）</span></td><td>积分：<span class='c_red p_r20'>"
					+ returnP
					+ "分</span></td>"
					+ "<td>折扣：<span class='c_red p_r20'>"
					+ discount
					+ "</span></td><td>机型：<span class='c_red p_r20' onclick='openpt();'>"
					+ flyStyle
					+ "</span></td></tr>"
					+ "<tr><td>剩余座位：<span class='c_red p_r20'>"
					+ seatnum
					+ "</span></td>"
					+ "<td>航班信息：经停：<span class='c_red p_r20'>"
					+ stopTimes
					+ "</span></td>"
					+ "<td>机建税：<span class='c_red p_r20'>"
					+ tax1
					+ "</span></td>"
					+ "<td>燃油税：<span class='c_red p_r20'>"
					+ tax2 + "</span></td></tr></table>";
			document.getElementById("use_rule1b").innerHTML = "<p>使用规定：</p><p style='text-indent:0;padding-left:0'>"
					+ cabinremark + "</p>";
		}
	} else if (cType == "FClass") {
		if (sign == "selectedFlight") {
			document.getElementById("use_rule2").innerHTML = "";
			document.getElementById("fly_info2").innerHTML = "";
			document.getElementById("box2").style.display = "block";
			document.getElementById("fly_info2").innerHTML = "<table style='width:100%' ><tr><td>产品名称：<span class='c_red p_r20'>"
					+ pro
					+ "（"
					+ whCab
					+ "舱）</span></td><td>积分：<span class='c_red p_r20'>"
					+ returnP
					+ "分</span></td>"
					+ "<td>折扣：<span class='c_red p_r20'>"
					+ discount
					+ "</span></td><td>机型：<span class='c_red p_r20' onclick='openpt();'>"
					+ flyStyle
					+ "</span></td></tr>"
					+ "<tr><td>剩余座位：<span class='c_red p_r20'>"
					+ seatnum
					+ "</span></td>"
					+ "<td>航班信息：经停：<span class='c_red p_r20'>"
					+ stopTimes
					+ "</span></td>"
					+ "<td>机建税：<span class='c_red p_r20'>"
					+ tax1
					+ "</span></td>"
					+ "<td>燃油税：<span class='c_red p_r20'>"
					+ tax2 + "</span></td></tr></table>";
			document.getElementById("use_rule2").innerHTML = "<p>使用规定：</p><p style='text-indent:0;padding-left:0'> "
					+ cabinremark + "</p>";
		}
		if (sign == "backFlight") {
			document.getElementById("use_rule2b").innerHTML = "";
			document.getElementById("fly_info2b").innerHTML = "";
			document.getElementById("box2b").style.display = "block";
			document.getElementById("fly_info2b").innerHTML = "<table style='width:100%' ><tr><td>产品名称：<span class='c_red p_r20'>"
					+ pro
					+ "（"
					+ whCab
					+ "舱）</span></td><td>积分：<span class='c_red p_r20'>"
					+ returnP
					+ "分</span></td>"
					+ "<td>折扣：<span class='c_red p_r20'>"
					+ discount
					+ "</span></td><td>机型：<span class='c_red p_r20' onclick='openpt();'>"
					+ flyStyle
					+ "</span></td></tr>"
					+ "<tr><td>剩余座位：<span class='c_red p_r20'>"
					+ seatnum
					+ "</span></td>"
					+ "<td>航班信息：经停：<span class='c_red p_r20'>"
					+ stopTimes
					+ "</span></td>"
					+ "<td>机建税：<span class='c_red p_r20'>"
					+ tax1
					+ "</span></td>"
					+ "<td>燃油税：<span class='c_red p_r20'>"
					+ tax2 + "</span></td></tr></table>";
			document.getElementById("use_rule2b").innerHTML = "<p>使用规定：</p><p style='text-indent:0;padding-left:0'> "
					+ cabinremark + "</p>";
		}
	} else {
		document.getElementById("use_rule").innerHTML = "";
		document.getElementById("fly_info").innerHTML = "";
		document.getElementById("box").style.display = "block";
		document.getElementById("fly_info").innerHTML = "<table style='width:100%' ><tr><td>产品名称：<span class='c_red p_r20'>"
				+ pro
				+ "（"
				+ whCab
				+ "舱）</span></td><td>积分：<span class='c_red p_r20'>"
				+ returnP
				+ "分</span></td>"
				+ "<td>折扣：<span class='c_red p_r20'>"
				+ discount
				+ "</span></td><td>机型：<span class='c_red p_r20' onclick='openpt();'>"
				+ flyStyle
				+ "</span></td></tr>"
				+ "<tr><td>剩余座位：<span class='c_red p_r20'>"
				+ seatnum
				+ "</span></td>"
				+ "<td>航班信息：经停：<span class='c_red p_r20'>"
				+ stopTimes
				+ "</span></td>"
				+ "<td>机建税：<span class='c_red p_r20'>"
				+ tax1
				+ "</span></td>"
				+ "<td>燃油税：<span class='c_red p_r20'>"
				+ tax2
				+ "</span></td></tr></table>";
		document.getElementById("use_rule").innerHTML = "<p>使用规定：</p><p style='text-indent:0;padding-left:0'> "
				+ cabinremark + "</p>";
	}
}
function checkProduct(o) {
	isConnect = "NO";
	if (o != null && o != undefined) {
		if (o.id.indexOf("selectedFlight_") == 0) {
			var obacks = document.getElementsByName("backFlight");
			if (obacks != null && obacks != undefined && obacks.length > 0) {
				var pname = o.id.split("_")[1];
				setSelectedRadio(obacks, pname);
			}
		}
	}
}
function setSelectedRadio(otargets, pname) {
	for ( var i = 0; i < otargets.length; i++) {
		var bname = otargets[i].id.split("_")[1];
		// if(pname=="WFC"||pname=="ZYR" || pname=="LLC")
		if (bname == "ZYR") {
			if (pname != bname) {
				otargets[i].disabled = true;
			} else {
				otargets[i].disabled = false;
			}
		} else {
			// if(bname=="WFC"||bname=="ZYR"||bname=="LLC")
			if (bname == "ZYR") {
				otargets[i].disabled = true;
			} else {
				otargets[i].disabled = false;
			}
		}
	}
	for ( var i = 0; i < otargets.length; i++) {
		if (otargets[i].disabled === true) {
			otargets[i].checked = false;
		}
	}
}
var currentCabinRules = new Map();
function saveRule(val, obj) {
	if (obj.checked = "checked") {
		currentCabinRules.put(obj.name, val);
	}
}
function initBackProduct() {
	var ofroms = document.getElementsByName("selectedFlight");
	if (ofroms != null && ofroms != undefined) {
		for ( var i = 0; i < ofroms.length; i++) {
			if (ofroms[i].checked === true) {
				checkProduct(ofroms[i]);
				break;
			}
		}
	}
}
function openpt() {
	var strurl = "../frontend/information/newInfo/main_one_13.jsp";
	window.open(strurl);
}