function separateFlight(responseText, selected)
{
    var JSONobj = eval('(' + responseText + ')');
    var txt = '';
    //加载团购信息
    if (JSONobj.groupBuys != null && JSONobj.groupBuys.length > 0) {
        loadGroupInfo(JSONobj);
    }
    if (JSONobj.flights == null || JSONobj.flights.length == 0)
    {
        txt += '<br/><br/>很抱歉，没有符合条件的查询结果！<br/><br/>';
        return txt;
    }
    // title data
    txt += '<div class="airTickebox mb20"><table border="0" cellspacing="0" cellpadding="0"><tr><th>承运人</th><th>航班号</th><th>起飞/到达</th>';
    var titlePd = new Map();
    var sortMap = new Map();
    var titlePdKeyLen = 0;
    var innerFlights = new Array();
    var outerFlights = new Array();
    //判断是否有提前购票
    var isTQGP = false;
    if (JSONobj.flights[0].finalDestinationPort == 'SIN' || JSONobj.flights[0].firstDeparturePort == 'SIN') {
        txt += '<th></th><th><th></th></th><th></th>';
    }
    for (var i = 0; i < JSONobj.flights.length; i++)
    {
        var flight = JSONobj.flights[i];
        var isInner = false;
        for (var j = 0; j < flight.segments.length; j++) {
            var segment = flight.segments[j];
            if (segment.carrier == "8L") {
                isInner = true;
            }
        }
        if (isInner) {
            innerFlights.push(flight);
        }
        else {
            outerFlights.push(flight);
        }
    }
    var flights;
    if (innerFlights.length > 0)
    {
        flights = innerFlights;
    }
    else
    {
        var text = '<br/><br/>很抱歉，没有符合条件的查询结果！<br/><br/>';
        return text;
    }
    for (var i = 0; i < flights.length; i++)
    {
        flight = flights[i];
        for (var j = 0; j < flight.segments.length; j++)
        {
            segment = flight.segments[j];
            for (var k = 0; k < segment.cabins.length; k++)
            {
                var cab = segment.cabins[k];
                for (var l = 0; l < cab.productOffers.length; l++)
                {
                    var pro = cab.productOffers[l];
                    /*对产品排序*/
                    if (pro.code == 'FClass') {
                        sortMap.put(pro.code, 0);
                    }
                    if (pro.code == 'YAJH') {
                        sortMap.put(pro.code, 1);
                    }
                    if (pro.code == 'MZMK' && cab.cabinCode == 'Y') {
                        sortMap.put('MZMKY', 2);
                    }
                    if (pro.code == 'MZMK' && cab.cabinCode != 'Y') {
                        sortMap.put(pro.code, 3);
                    }
                    if (pro.code == 'TQGP') {
                        sortMap.put(pro.code, 4);
                        isTQGP = true;
                    }
                    if (pro.code == 'CDJP' && (cab.cabinCode == 'Z' || cab.cabinCode == 'Z1' || cab.cabinCode == 'Z2' || cab.cabinCode == 'Z3' || cab.cabinCode == 'Z4')) {
                        //特价舱增开Z1,Z2,Z3,Z4舱座位Z的子舱位
                        sortMap.put('CDJPZ', 5);
                    }
                    if (pro.code == 'CDJP' && cab.cabinCode == 'T') {
                        sortMap.put('CDJPT', 6);
                    }
                    if (pro.code == 'WEB') {
                        sortMap.put(pro.code, 7);
                    }
                    // if(pro.code=='WFC'){sortMap.put(pro.code, 8);}
                    // if(pro.code=='LLC'){sortMap.put(pro.code, 9);}
                    if (pro.recommended != null && pro.recommended == true)
                    {
                        if ((cab.cabinCode == 'Z' || cab.cabinCode == 'Z1' || cab.cabinCode == 'Z2' || cab.cabinCode == 'Z3' || cab.cabinCode == 'Z4') && pro.code == 'CDJP') {
                            titlePd.put('CDJPZ', pro.name);
                        }
                        else if (cab.cabinCode == 'T' && pro.code == 'CDJP') {
                            titlePd.put('CDJPT', pro.name);
                        }
                        else if (cab.cabinCode == 'Y' && pro.code == 'MZMK') {
                            titlePd.put('MZMKY', pro.name);
                        }
                        else {
                            titlePd.put(pro.code, pro.name);
                        }
                    }
                    else if (pro.recommended != null && pro.recommended == false && cab.cabinCode == 'Y') {
                        titlePd.put('MZMKY', pro.name);
                    }
                }
            }
            //计算最低价
            var lowestPrice = null;
            var cabins = segment.cabins;
            var lowestCabin = cabins[0], lowestPd = cabins[0].productOffers[0];
            var lowCabinMap = new Map();
            for ( var k = 0; k < cabins.length; k++) 
            {
                var cabin = segment.cabins[k];
                for ( var l = 0; l < cabin.productOffers.length; l++) 
                {
                    var product = cabin.productOffers[l];
                    if (product.adultFare.farePrice < lowestPd.adultFare.farePrice) {
                        lowestCabin = cabin;
                        lowestPd = product;
                    }
                    var ca = lowCabinMap.get(product.name);
                    if (ca == null) {
                        lowCabinMap.put(product.name, cabin);
                    }
                    else
                    {
                        var caPd;
                        for (var m = 0; m < ca.productOffers.length; m++) {
                            if (ca.productOffers[m].name = product.name) {
                                caPd = ca.productOffers[m];
                            }
                        }
                        if (product.adultFare.farePrice < caPd.adultFare.farePrice) {
                            lowCabinMap.put(product.name, cabin);
                        }
                    }
                }
            }

            //计算最低价结束
        }
    }
    var pdKeys2 = titlePd.keyArray();
    for (var i = 0; i < pdKeys2.length; i++)
    {
        for (var j = i + 1; j < pdKeys2.length; j++)
        {
            if (sortMap.get(pdKeys2[i]) > sortMap.get(pdKeys2[j])) {
                var temp = pdKeys2[i];
                pdKeys2[i] = pdKeys2[j];
                pdKeys2[j] = temp;
            }
        }
    }
    /**/
    var pdKeys = new Array();
    var j = pdKeys2.length;
    var _8l_index = 0;
    for (var i = 0; i < j; i++)
    {
        if (undefined != pdKeys2[i])
        {
            //产品个数为8时，过滤提前购票和明折明扣----两联程和来回程产品开放后此处需做修改
            //alert(pdKeys2[i]);
            if (i < 8)
            {
                if (pdKeys2[7] != undefined && (pdKeys2[i] == 'TQGP' || pdKeys2[i] == 'MZMK')) {}
                else if ((pdKeys2[6] != undefined && pdKeys2[i] == 'TQGP' && isTQGP) || (pdKeys2[6] != undefined && pdKeys2[i] == 'MZMK' && !isTQGP) ) {
                    //产品个数为7时先过滤提前购票
                }
                else {
                    pdKeys[_8l_index++] = pdKeys2[i];
                }
            }
        }
    }
    //var pdKeys = titlePd.keyArray();
    //最多输出6个产品
    for (var i = 0; i < 6; i++)
    {
        if (flights[0].finalDestinationPort == 'SIN' || flights[0].firstDeparturePort == 'SIN') {
            txt += '<th></th>';
            break;
        }
        if (i < pdKeys.length)
        {
            if (pdKeys[i] == 'WFC') {
                txt += '<th>来回程</th>';
            }
            else if (pdKeys[i] == 'MZMKY') {
                txt += '<th>经济舱全价</th>';
            }
            else if (pdKeys[i] == 'CDJPZ') {
                txt += '<th>特价</th>';
            }
            else {
                txt += '<th>' + titlePd.get(pdKeys[i]) + '</th>';
            }
        }
        else {
            txt += '<th></th>';
        }
    }
    txt += '</tr>';
    // flight data
    for (var i = 0; i < flights.length; i++)
    {
        var flight = flights[i];
        for (var j = 0; j < flight.segments.length; j++)
        {
            // segment
            var segment = flight.segments[j];
            // all cabins
            var cabins = segment.cabins;
            // block id
            var fltBlockId = '' + i + '' + j + segment.carrier + segment.flightNo;
            /********************** Div 1 Recommend Line start ********************/
            // 承运人
            if (segment.carrier == 'JD') {
                txt += '<tr><td>首都航空</td>';
            }
            else if (segment.carrier == 'GS') {
                txt += '<tr><td>天津航空</td>';
            }
            else if (segment.carrier == 'PN') {
                txt += '<tr><td>西部航空</td>';
            }
            else if (segment.carrier == '8L') {
                txt += '<tr><td>祥鹏航空</td>';
            }
            else if (segment.carrier == 'HU') {
                txt += '<tr><td>海南航空</td>';
            }
            // 航班号
            txt += '<td>' + segment.carrier + segment.flightNo + '</td>';
            // PGSLUCKYAIR-956 祥鹏9点以前（包含9:00，不包含00:00）起飞的昆明出港的航班，增加"购机票送早餐"提示
            if (((segment.departureTime.Mid(11, 13) < "09" && segment.departureTime.Mid(11, 16) != "00:00") || (segment.departureTime.Mid(11, 
            13) == "09" && segment.departureTime.Mid(14, 16) == "00")) && segment.carrier == '8L' && segment.originCity == 'KMG')
            {
                txt += '<td class="brw" style="display:block;position:relative;">' + '<img class="breakfast_img" style="position:absolute;" onmouseover="showBreakfastDetail(' + i + ')" onmouseout="hiddenDetail(' + i + ')" src="../images/flight4b2c/breakfast.gif" />' + '<div class="breakfast_detail" id="breakfast_detail_' + i + '">凡乘坐祥鹏航空昆明出港9:00前（含9:00）航班的旅客，乘机当日凭登机牌可在昆明长水机场' + '<font color="blue">小红帽餐厅</font>享用最高价值48元的<font color="blue">免费早餐</font>' + '一份!（地址:过安检后直走80米，16号登机口旁，餐厅咨询热线:0871-67085658） </div>';
            }
            else {
                txt += '<td class="brw" >';
            }
            // 起飞到达
            txt += '<p><b>' + (segment.departureTime).Mid(11, 16) + '</b></p><p>' + segment.arriveTime.Mid(11, 
            16) + '</p></td>';
            //经停
            var stoptimes = '无';
            if (segment.stops > 0) {
                stoptimes = segment.stops + '次';
            }
            if (flights[0].finalDestinationPort == 'SIN' || flights[0].firstDeparturePort == 'SIN')
            {
                txt += '<td style="text-align:left;" colspan="5"><p>预订、咨询请致电 <b style="color:blue">95071950</b></p><p>温馨提示：该航班截止办理乘机手续的时间为离站前40分钟。</p></td></tr></table>';
                return txt;
            }
            var inserted = false;
            for (var m = 0; m < pdKeys.length; m++)
            {
                inserted = false;
                for (var k = 0; k < cabins.length; k++)
                {
                    var cabin = segment.cabins[k];
                    for (var l = 0; l < cabin.productOffers.length; l++)
                    {
                        var product = cabin.productOffers[l];
                        var productCodeY = product.code + 'Y';
                        var productCodeZ = product.code + 'Z';
                        var productCodeT = product.code + 'T';
                        var flag = false;
                        if (product.code == pdKeys[m]) {
                            flag = true;
                        }
                        else if (productCodeY == pdKeys[m] && cabin.cabinCode == 'Y') {
                            flag = true;
                        }
                        else if (productCodeZ == pdKeys[m] && (cabin.cabinCode == 'Z' || cabin.cabinCode == 'Z1' || cabin.cabinCode == 'Z2' || cabin.cabinCode == 'Z3' || cabin.cabinCode == 'Z4')) {
                            flag = true;
                        }
                        else if (productCodeT == pdKeys[m] && cabin.cabinCode == 'T') {
                            flag = true;
                        }
                        if (flag && product.recommended != null && product.recommended == true)
                        {
                            var seatnum;
                            if (cabin.inventory > 9) {
                                seatnum = '充足';
                            }
                            else {
                                seatnum = cabin.inventory;
                            }
                            var radioVal = cabin.cabinCode + ';' + segment.carrier + ';' + segment.flightNo + ';' + segment.originCity + ';' + segment.destinationCity + ';' + product.code;
                            txt += '<td><input type="radio" name="' + selected + '" id="' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '" value="' + radioVal + '" onclick="selectCabinOp(\'' + stoptimes + '\', \'' + cabin.remark + '\', \'' + seatnum + '\',\'' + segment.aircraftStyle + '\',\'' + selected + '\', this)"';
                            //if(product.code == 'FClass'){
                            // txt += ' onfocus="ckRadio(this);" onblur="if(showFclassNode) return false;disCkRadio(this);"';
                            //}
                            if (//product.code != "WEB" &&
                            product.fareLocks != null && product.fareLocks.length > 0)
                            {
                                txt += ' onfocus="ckRadio(this);" onblur="if(showFclassNode) return false;disCkRadio(this);"';
                            }
                            txt += ' />';
                            //A计划title
                            if (product.code == "CZTD")
                            {
                                txt += '<span title="header=[-]body=[可享受空中头等舱服务哟!]">' + product.adultFare.farePrice + '</span>';
                            }
                            else
                            {
                                //if(product.code == 'FClass'){
                                // txt +='<div class="fclass_t" onmouseover="showFclassNode = true;" onmouseout="showFclassNode = false;">';
                                // txt +='<div class="fclass_c">';
                                // txt +='<p class="fclass_arrow"></p>';
                                // txt +='<p>买头等舱送经济舱，详情请查看<span onclick="openFirstClassNode(this);" style="color:blue;cursor: pointer;"> 活动细则 </span></p>';
                                // txt +='</div>';
                                // txt +='</div>';
                                //}
                                if (//product.code != "WEB" &&
                                product.fareLocks != null && product.fareLocks.length > 0)
                                {
                                    txt += '<div class="fclass_t" onmouseover="showFclassNode = true;" onmouseout="showFclassNode = false;">';
                                    txt += '<div class="fclass_c">';
                                    txt += '<p class="fclass_arrow"></p>';
                                    txt += '<p>点击小锁进入票价锁预订流程，详情请查看<span onclick="openFLNode(this);" style="color:blue;cursor: pointer;"> 产品介绍 </span></p>';
                                    txt += '</div>';
                                    txt += '</div>';
                                }
                                txt += '<span><b>￥' + product.adultFare.farePrice + '</b></span>';
                            }
                            //添加锁标识
                            if (JSONobj.flights[0].tripType == "ONEWAY")
                            {
                                if (//product.code != "WEB" &&
                                product.fareLocks != null && product.fareLocks.length > 0)
                                {
                                    //txt +='<a href="../frontend/farelock/FareLockInfo.jsp" target="_blank" onclick="return checkProductByID(\'' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '\')"><img src="../images/flight4b2c/lock.gif" id="farelock"/></a>';
                                    txt += '<img src="../images/flight4b2c/lock.gif" id="farelock" onclick=" checkProductByID(\'' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '\')" />';
                                }
                            }
                            txt += '</td>';
                            inserted = true;
                        }
                        else if (flag && product.recommended != null && product.recommended == false && cabin.cabinCode == 'Y' && pdKeys[m] == "MZMKY")
                        {
                            var seatnum;
                            if (cabin.inventory > 9) {
                                seatnum = '充足';
                            }
                            else {
                                seatnum = cabin.inventory;
                            }
                            var radioVal = cabin.cabinCode + ';' + segment.carrier + ';' + segment.flightNo + ';' + segment.originCity + ';' + segment.destinationCity + ';' + product.code;
                            txt += '<td class="blw"><input type="radio" name="' + selected + '" id="' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '" value="' + radioVal + '" onclick="selectCabinOp(\'' + stoptimes + '\', \'' + cabin.remark + '\', \'' + seatnum + '\',\'' + segment.aircraftStyle + '\',\'' + selected + '\', this)"';
                            if (//product.code != "WEB" &&
                            product.fareLocks != null && product.fareLocks.length > 0)
                            {
                                txt += ' onfocus="ckRadio(this);" onblur="if(showFclassNode) return false;disCkRadio(this);"';
                            }
                            txt += ' />';
                            if (//product.code != "WEB" &&
                            product.fareLocks != null && product.fareLocks.length > 0)
                            {
                                txt += '<div class="fclass_t" onmouseover="showFclassNode = true;" onmouseout="showFclassNode = false;">';
                                txt += '<div class="fclass_c">';
                                txt += '<p class="fclass_arrow"></p>';
                                txt += '<p>点击小锁进入票价锁预订流程，详情请查看<span onclick="openFLNode(this);" style="color:blue;cursor: pointer;"> 产品介绍 </span></p>';
                                txt += '</div>';
                                txt += '</div>';
                            }
                            txt += '<span><b>￥' + product.adultFare.farePrice + '</b></span>';
                            if (JSONobj.flights[0].tripType == "ONEWAY")
                            {
                                if (//product.code != "WEB" &&
                                product.fareLocks != null && product.fareLocks.length > 0)
                                {
                                    //txt +='<a href="../frontend/farelock/FareLockInfo.jsp" target="_blank" onclick="return checkProductByID(\'' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '\')"><img src="../images/flight4b2c/lock.gif" id="farelock"/></a>';
                                    txt += '<img src="../images/flight4b2c/lock.gif" id="farelock" onclick=" checkProductByID(\'' + selected + '_' + product.code + '_' + cabin.cabinCode + '_' + fltBlockId + '\')" />';
                                }
                            }
                            txt += '</td>';
                            inserted = true;
                        }
                    }
                }
                if (!inserted) {
                    txt += '<td>--</td>';
                }
            }
            /*for(var l=0; l < 4- pdKeys.length; l++){
            txt += '<td></td>';
            }*/
            txt += '</tr>';
        }
    }
    txt += '</table>';
    //$('inpLogin2').disabled=false;
    if (selected == 'selectedFlight') {
        txt += '<div class="con" id="detaillist1" style="display:none"></div>';
    }
    if (selected == 'backFlight') {
        txt += '<div class="con" id="detaillist2" style="display:none"></div>';
    }
    txt += "<div align='right'>价格太高？点击进入<span style='color:blue'><a target='_blank' href='http://www.luckyair.net/frontend/passengerService/main9.jsp'>低价预约</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></div>";
    return txt;
}

	  //定义map     
function Map()     
{      
    this.container = {};
}      
  
//将key-value放入map中     
Map.prototype.put = function(key,value){      
    try{      
        if(key!=null && key != "")
            this.container[key] = value;
        }catch(e){
        }
};      
  
//根据key从map中取出对应的value
Map.prototype.get = function(key){      
    try{      
        return this.container[key];      
    }catch(e){
        return e;      
    }
};      

// 判断map中是否包含指定的key
Map.prototype.containsKey=function(key){     
    try{     
        for(var p in this.container)     
        {     
            if(p==key)     
                return true;
        }     
        return false;     
    }catch(e){     
        return e;     
    }
};

//返回map中的key值数组     
Map.prototype.keyArray=function(){     
  var keys=new Array();     
  for(var p in this.container)     
  {     
      keys.push(p);
  }
  return keys;     
};

String.prototype.Mid = function(start,end){
	if(isNaN(start)&&start<0)
	{
			return "";
	}
	if(isNaN(end)&&end<0)
	{
		return "";
	}
	return this.substring(start,end);
} 