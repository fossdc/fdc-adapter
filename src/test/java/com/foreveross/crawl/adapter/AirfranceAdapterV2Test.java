package com.foreveross.crawl.adapter;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AirfranceAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.airfrance.AirfranceUtils2;
import com.foreveross.crawl.common.cfg.ConfigContainer;
import com.foreveross.proxyip.ProxyipProperties;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 一句话描述
 * 
 * @author fb
 */
@SuppressWarnings("deprecation")
public class AirfranceAdapterV2Test extends BaseAdapterTest{
	public final static Logger logger = LoggerFactory.getLogger(AirfranceAdapterV2Test.class);
	
	private AirfranceAdapter afa;

	@Before
	public void init() {
		ConfigContainer.getInstance().register(ProxyipProperties.class);
		TaskModel tm = new TaskModel();
		tm.setFromCity("BJS"); 
		tm.setToCity("FRA");
		tm.setFlightDate("2014-10-01");
		tm.setReturnGrabDate("2014-11-14");
		tm.setIsInternational(1);
		tm.setIsReturn(1);
		afa = new AirfranceAdapter(tm);
	}

	@Test
	public void standardCalendarPageVerifyTest() throws IOException, URISyntaxException {
		URI uri = AirfranceAdapterV2Test.class.getClassLoader().getResource("fileData/sgafa_v2.html").toURI();
		String page = FileUtils.readFileToString(new File(uri));
		System.out.println(standardCalendarPageVerify(page));
	}
	
	@Test
	public void dataAssemblyTest() throws Exception {
		afa.fetch(null);
	}
	
	@Test
	public void detailInfoAssembly() throws Exception {
		URI uri = AirfranceAdapterV2Test.class.getClassLoader().getResource("fileData/sgafa_datilv2.html").toURI();
		String page = FileUtils.readFileToString(new File(uri));
		Document detailDoc = Jsoup.parse(page);
		String nakedPrce = null;
		String totalPrice =  null;
		nakedPrce = detailDoc.select("#totalPriceForMostExpensiveTypo").text();
		totalPrice =  detailDoc.select("#miniRecapTarifVols").text();
		nakedPrce = StringUtils.isBlank(nakedPrce) ? "0" : nakedPrce.replaceAll("[^0-9]", "");
		totalPrice = StringUtils.isBlank(totalPrice) ? "0" : totalPrice.replaceAll("[^0-9]", "");
		
		System.out.printf("nakedPrce:%s  totalPrice:%s", nakedPrce, totalPrice);
		
	}
	
	@Test
	public void flightInfoAssembly() throws Exception {
		URI uri = AirfranceAdapterV2Test.class.getClassLoader().getResource("fileData/sgafa_v2.html").toURI();
		String page = FileUtils.readFileToString(new File(uri));
		Document doc = Jsoup.parse(page);
		
		Elements outbound = doc.select("div#tabscontent tr[id^=flight_]");
		Elements inbound = doc.select("div#tabscontent-back tr[id^=flight-back_]");
		List<Map<String, Object>> outboundsData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> inboundsData = new ArrayList<Map<String, Object>>();
		Map<String, Object> outboundData = null;
		Map<String, Object> inboundData = null;
		List<Map<String, String>> outboundTransits = null;
		List<Map<String, String>> inboundTransits= null;
		Map<String, String> outboundTransit = null;
		Map<String, String> inboundTransit = null;
		Elements tempElement = null;
		Elements tempElementTd = null;
		Element tempParent = null;
		String tempData = null;
		String tempArray[] = null;
		String id = null;
		String index = null;
		
		for (Element element : outbound) {
			outboundData = new HashMap<String, Object>();
			outboundTransits = new ArrayList<Map<String,String>>();
			tempParent = element.parent();
			index = element.attr("id").split("_")[1];
			id = index.split("-")[0];
			outboundData.put("id", id);                              
			outboundData.put("priceOne", element.select("label[class=txttraceurBlue cursorPointer]").text());
			outboundData.put("priceTwo", element.select("label[class=txttraceurBlue cursorDefault]").text());
			// 总时长
			tempElement = tempParent.select("#moreStopovers_" + index);
			
			// 中转,则先获取总时长 
			if (!tempElement.isEmpty()) {
				outboundData.put("duration", tempElement.select("span[class=bold]").text().split("：")[1]);
			}
			
			tempElement = tempParent.select("tr#features_" + index + " #flightFeatures tr");
			
			for (Element transit : tempElement) {
				outboundTransit = new HashMap<String, String>();
				tempElementTd = transit.select("td");
				outboundTransit.put("flightNo", transit.select("#flight_number").text());
				outboundTransit.put("company", tempElementTd.get(2).select("span:eq(0)").text());
				outboundTransit.put("flightType", tempElementTd.get(3).select("span:eq(0)").text().split(":")[1]);
				outboundTransit.put("cabinName", tempElementTd.get(3).select("span:eq(1)").text().split(":")[1]);
				// 开始时间与地址
				tempData = tempElementTd.get(1).select("span:eq(0)").text();
				tempArray = AirfranceUtils2.timeAddressSeparator(tempData);
				outboundTransit.put("fromDate", tempArray[0]);
				outboundTransit.put("fromAddress", tempArray[1]);
				// 结束时间与地址
				tempData = tempElementTd.get(1).select("span:eq(1)").text();
				tempArray = AirfranceUtils2.timeAddressSeparator(tempData);
				outboundTransit.put("toDate", tempArray[0]);
				outboundTransit.put("toAddress", tempArray[1]);
				
				if (outboundData.get("duration") == null) {
					outboundData.put("duration", tempElementTd.get(1).select("#duration").text().split(" : ")[1]);
				}
				
				outboundTransits.add(outboundTransit);
			}

			outboundData.put("transits", outboundTransits);
			
			outboundsData.add(outboundData);
			
			for (Map<String, Object> out : outboundsData) {
				for (Entry<String, Object> entry : out.entrySet()){
					logger.debug(entry.getKey() + ": " + entry.getValue());
				}
				
			   logger.debug(" ---------------------------------- ");
			}
			
			logger.debug(" =============================================== ");
		}
		
		System.out.println("======================反程=====================");
		
		for (Element element : inbound) {
			inboundData = new HashMap<String, Object>();
			inboundTransits = new ArrayList<Map<String,String>>();
			tempParent = element.parent();
			index = element.attr("id").split("_")[1];
			id = index.split("-")[0];
			inboundData.put("id", id);
			inboundData.put("priceOne", element.select("label[class=txttraceurBlue cursorPointer]").text());
			inboundData.put("priceTwo", element.select("label[class=txttraceurBlue cursorDefault]").text());

			// 总时长
			tempElement = tempParent.select("#moreStopovers-back_" + index);
			
			// 中转,则先获取总时长 
			if (!tempElement.isEmpty()) {
				inboundData.put("duration", tempElement.select("span[class=bold]").text().split("：")[1]);
			}
			
			tempElement = tempParent.select("tr#features-back_" + index + " #flightFeatures tr");
			
			for (Element transit : tempElement) {
				inboundTransit = new HashMap<String, String>();
				tempElementTd = transit.select("td");
				inboundTransit.put("flightNo", transit.select("#flight_number").text());
				inboundTransit.put("company", tempElementTd.get(2).select("span:eq(0)").text());
				inboundTransit.put("flightType", tempElementTd.get(3).select("span:eq(0)").text().split(":")[1]);
				inboundTransit.put("cabinName", tempElementTd.get(3).select("span:eq(1)").text().split(":")[1]);

				// 开始时间与地址
				tempData = tempElementTd.get(1).select("span:eq(0)").text();
				tempArray = AirfranceUtils2.timeAddressSeparator(tempData);
				inboundTransit.put("fromDate", tempArray[0]);
				inboundTransit.put("fromAddress", tempArray[1]);
				// 结束时间与地址
				tempData = tempElementTd.get(1).select("span:eq(1)").text();
				tempArray = AirfranceUtils2.timeAddressSeparator(tempData);
				inboundTransit.put("toDate", tempArray[0]);
				inboundTransit.put("toAddress", tempArray[1]);
				
				if (inboundData.get("duration") == null) {
					inboundData.put("duration", tempElementTd.get(1).select("#duration").text().split(" : ")[1]);
				}
				
				inboundTransits.add(inboundTransit);
			}

			inboundData.put("transits", inboundTransits);
			
			inboundsData.add(inboundData);
			
			for (Map<String, Object> out : inboundsData) {
				for (Entry<String, Object> entry : out.entrySet()){
					logger.debug(entry.getKey() + ": " + entry.getValue());
				}
				
			   logger.debug(" ---------------------------------- ");
			}
			
			logger.debug(" =============================================== ");
		}
	}
	
	// 网页参数分析 
	public static void paramsAnalysis(String url) {
		String paramsStr = url.substring(url.indexOf("?") + 1);
		String paramsTuple[] = paramsStr.split("&");
		
		for (String param : paramsTuple) {
			String paramtwin[] =  param.split("=");
			System.out.println(".append(\"&" + paramtwin[0] + "=" + (paramtwin.length == 1 ? null : paramtwin[1]) + "\")");
		}
	}
	
	private boolean standardCalendarPageVerify(String page) {
		boolean result  = false;
		Document doc = Jsoup.parse(page);
		
		if (StringUtils.isNotBlank(page)) {
			Elements form  = doc.select("div.blocMain > form[name=standardCalendarForm]");
			result = form.isEmpty() ? result : true;
		}
		
		return result;
	}
	

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		String url = "http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/process/standardbooking/ValidateSearchAction.do?arrival=JFK&arrival=BJS&cabin=Y&dayDate=11&dayDate=02&departure=BJS&departure=JFK&familyTrip=NON&haul=LH&idArrivalTrip1Lib=%E7%BA%BD%E7%BA%A6%2C%20John%20F%20Kennedy%20International%20(JFK)%20NY%20-%20%E7%BE%8E%E5%9B%BD&idDepartureTrip1Lib=%E5%8C%97%E4%BA%AC%2C%20%E6%89%80%E6%9C%89%E6%9C%BA%E5%9C%BA%20(BJS)%20-%20%E4%B8%AD%E5%9B%BD&isUM=&jourAllerFin=06&jourAllerOrigine=11&moisAllerFin=201509&moisAllerOrigine=201409&nbEnfants=&nbPassenger=1&paxTypoList=ADT&paxTypoList=ADT&paxTypoList=ADT&paxTypoList=ADT&plusOptions=&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectPreviousSearch=BJS-JFK_2014811&subCabin=MCHER&typeTrip=2&yearMonthDate=201409&yearMonthDate=201410";
		String detail = "http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/process/standardbooking/FareAction.do?indexItinerary=0&price=5630.0&price=5630.0&price=5630.0&price=5630.0&price=13580.0&price=13580.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=7030.0&price=15400.0&price=7030.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5110.0&price=5110.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5590.0&price=5590.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=18680.0&priceMultiPax=18680.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=12130.0&priceMultiPax=20510.0&priceMultiPax=12130.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10700.0&priceMultiPax=10700.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&radioIti0=6&radioIti1=8&recommendation=6&recommendation=8&selectedFF=FFLHAFKLY1&selectedFF=FFLHAFKLY1&sortType=DEPARTURE&sortType=DEPARTURE&sortType0=DEPARTURE&sortType1=DEPARTURE";
		
		AirfranceAdapterV2Test.paramsAnalysis(detail);

	}
}
