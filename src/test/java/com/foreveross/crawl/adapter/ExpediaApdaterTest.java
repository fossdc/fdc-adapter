package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.ExpediaAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

public class ExpediaApdaterTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("XMN");
		task.setFromCityName("厦门");
		task.setToCity("SIN");
		task.setToCityName("新加坡");
		task.setFlightDate("2014-09-15");
		task.setUseProxyip(false);
		task.setIsReturn(1);
		task.setIsInternational(1);
		task.setReturnGrabDate("2014-09-22");
		task.setAttachHbaseKey("00009");
		ExpediaApdaterTest test = new ExpediaApdaterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				ExpediaAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}