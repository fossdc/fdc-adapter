package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.CSAirAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

public class CSAireApdateTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("PEK");
		task.setFromCityName("北京");
		task.setToCity("FRA");
		task.setToCityName("法兰克福");
		task.setFlightDate("2014-09-06");
		task.setUseProxyip(false);
		task.setIsReturn(1);
		task.setIsInternational(1);
		task.setReturnGrabDate("2014-09-21");
		task.setAttachHbaseKey("20005");
		CSAireApdateTest test = new CSAireApdateTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				CSAirAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}