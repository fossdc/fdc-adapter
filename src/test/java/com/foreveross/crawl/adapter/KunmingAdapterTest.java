package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.KunmingAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class KunmingAdapterTest extends BaseAdapterTest {
	public static void main(String args[]) throws Exception{
		TaskModel task = new TaskModel();
		task.setFromCity("KMG");
		task.setFromCityName("昆明");
		task.setToCity("HRB");
		task.setToCityName("哈尔滨");
		task.setFlightDate("2014-10-25");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-21");
		task.setAttachHbaseKey("20005");
		KunmingAdapterTest test = new KunmingAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				KunmingAdapter.class, task);
		test.printJson(results);
	}
}
