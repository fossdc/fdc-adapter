package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.JuneYaoAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class JuneYaoAdapterTest extends BaseAdapterTest {
	
	public static void main(String[] arg0) throws Exception{
		TaskModel taskModel = new TaskModel();
		taskModel.setFromCity("KWE");
		taskModel.setFromCityName("贵阳");
		taskModel.setToCity("CSX");
		taskModel.setToCityName("长沙");
		taskModel.setFlightDate("2014-09-24");
		taskModel.setReturnGrabDate("2014-09-30");
		taskModel.setIsReturn(0);
		taskModel.setIsInternational(0);
		taskModel.setAttachHbaseKey("20016");
		JuneYaoAdapterTest test =new JuneYaoAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(JuneYaoAdapter.class, taskModel);
		test.printJson(results);
	}

}
