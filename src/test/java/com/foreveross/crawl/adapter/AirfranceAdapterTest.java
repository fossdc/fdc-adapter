package com.foreveross.crawl.adapter;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AirfranceAdapter;
import com.foreveross.crawl.common.cfg.ConfigContainer;
import com.foreveross.proxyip.ProxyipProperties;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 一句话描述
 * 
 * @author fb
 */
@SuppressWarnings("deprecation")
public class AirfranceAdapterTest extends BaseAdapterTest{
	public final static Logger logger = LoggerFactory.getLogger(AirfranceAdapterTest.class);
	
	private AirfranceAdapter afa;

	@Before
	public void init() {
		ConfigContainer.getInstance().register(ProxyipProperties.class);
		TaskModel tm = new TaskModel();
		tm.setFromCity("BJS"); 
		tm.setToCity("FRA");
		tm.setFlightDate("2014-10-01");
		tm.setReturnGrabDate("2014-11-14");
		tm.setIsInternational(1);
		tm.setIsReturn(1);
		afa = new AirfranceAdapter(tm);
	}

	@Test
	public void standardCalendarPageVerifyTest() throws IOException, URISyntaxException {
		URI uri = AirfranceAdapterTest.class.getClassLoader().getResource("fileData/sgafa.html").toURI();
		String page = FileUtils.readFileToString(new File(uri));
		System.out.println(standardCalendarPageVerify(page));
	}
	
	@Test
	public void dataAssemblyTest() throws Exception {
		afa.fetch(null);
	}
	
	@Test
	public void flightInfoAssembly() throws Exception {
		URI uri = AirfranceAdapterTest.class.getClassLoader().getResource("fileData/sgafa.html").toURI();
		String page = FileUtils.readFileToString(new File(uri));
		Document doc = Jsoup.parse(page);
		
		Elements outbound = doc.select("div#idUpsell0 table.upsell div[id^=Iti0Rec]");
		Elements inbound = doc.select("div#idUpsell1 table.upsell div[id^=Iti1Rec]");
		List<List<Map<String, String>>> outboundsData = new ArrayList<List<Map<String, String>>>();
		List<List<Map<String, String>>> inboundsData = new ArrayList<List<Map<String, String>>>();
		List<Map<String, String>> outbounds = null;
		List<Map<String, String>> inbounds = null;
		Map<String, String> outboundData = null;
		Map<String, String> inboundData = null;
		Elements flightAfterTd = null;
		String id = null;
		int index = 0;
		
		for (Element element : outbound) {
			outbounds = new ArrayList<Map<String, String>>();
			index = 0;
			id = element.attr("id");
			id = id.substring(id.length() - 1);

			for (Element companyTr : element.select("table.upsellRow tr[class!=lowSeatAvailable]")) {
				index++;
				outboundData = new HashMap<String, String>();
				outboundData.put("id", id);
				outboundData.put("priceOne", companyTr.select("#idPrice0Rec" + id + "FFFFLHAFKLY" + index).text());
				outboundData.put("priceTwo", companyTr.select("#idPrice0Rec" + id + "FFFFLHAFW" + index).text());
				flightAfterTd = companyTr.select("#idUpsellIti0_Rec" + id + "_Flight_" + index + " ~ td");
				outboundData.put("flightNo", companyTr.select("#idUpsellIti0_Rec" + id + "_Flight_" + index).text());
				outboundData.put("duration", flightAfterTd.get(4).text());
				outboundData.put("company", flightAfterTd.get(5).text());
				outboundData.put("fromDate", flightAfterTd.get(0).text());
				outboundData.put("toDate", flightAfterTd.get(2).text());
				outboundData.put("fromAddress", flightAfterTd.get(1).text());
				outboundData.put("toAddress", flightAfterTd.get(3).text());
				outbounds.add(outboundData);
			}
			
			outboundsData.add(outbounds);
			
			for (Map<String, String> out : outbounds) {
				for (Entry<String, String> entry : out.entrySet()){
					logger.debug(entry.getKey() + ": " + entry.getValue());
				}
				
			   logger.debug(" ---------------------------------- ");
			}
			
			logger.debug(" =============================================== ");
		}
		
		System.out.println("======================反程=====================");
		
		for (Element element : inbound) {
			inbounds = new ArrayList<Map<String, String>>();
			index = 0;
			id = element.attr("id");
			id = id.substring(id.length() - 1);

			for (Element companyTr : element.select("table.upsellRow tr[class!=lowSeatAvailable]")) {
				index++;
				inboundData = new HashMap<String, String>();
				inboundData.put("id", id);
				inboundData.put("priceOne", companyTr.select("#idPrice1Rec" + id + "FFFFLHAFKLY" + index).text());
				inboundData.put("priceTwo", companyTr.select("#idPrice1Rec" + id + "FFFFLHAFW" + index).text());
				flightAfterTd = companyTr.select("#idUpsellIti1_Rec" + id + "_Flight_" + index + " ~ td");
				inboundData.put("flightNo", companyTr.select("#idUpsellIti1_Rec" + id + "_Flight_" + index).text());
				inboundData.put("duration", flightAfterTd.get(4).text());
				inboundData.put("company", flightAfterTd.get(5).text());
				inboundData.put("fromDate", flightAfterTd.get(0).text());
				inboundData.put("toDate", flightAfterTd.get(2).text());
				inboundData.put("fromAddress", flightAfterTd.get(1).text());
				inboundData.put("toAddress", flightAfterTd.get(3).text());
				inbounds.add(inboundData);
			}
			
			inboundsData.add(inbounds);
			
			for (Map<String, String> out : inbounds) {
				for (Entry<String, String> entry : out.entrySet()){
					logger.debug(entry.getKey() + ": " + entry.getValue());
				}
				
			   logger.debug(" ---------------------------------- ");
			}
			
			logger.debug(" =============================================== ");
		}
	}
	
	// 网页参数分析 
	public static void paramsAnalysis(String url) {
		String paramsStr = url.substring(url.indexOf("?") + 1);
		String paramsTuple[] = paramsStr.split("&");
		
		for (String param : paramsTuple) {
			String paramtwin[] =  param.split("=");
			System.out.println(".append(\"&" + paramtwin[0] + "=" + (paramtwin.length == 1 ? null : paramtwin[1]) + "\")");
		}
	}
	
	private boolean standardCalendarPageVerify(String page) {
		boolean result  = false;
		Document doc = Jsoup.parse(page);
		
		if (StringUtils.isNotBlank(page)) {
			Elements form  = doc.select("div.blocMain > form[name=standardCalendarForm]");
			result = form.isEmpty() ? result : true;
		}
		
		return result;
	}
	

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		String url = "http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/process/standardbooking/ValidateSearchAction.do?arrival=JFK&arrival=BJS&cabin=Y&dayDate=11&dayDate=02&departure=BJS&departure=JFK&familyTrip=NON&haul=LH&idArrivalTrip1Lib=%E7%BA%BD%E7%BA%A6%2C%20John%20F%20Kennedy%20International%20(JFK)%20NY%20-%20%E7%BE%8E%E5%9B%BD&idDepartureTrip1Lib=%E5%8C%97%E4%BA%AC%2C%20%E6%89%80%E6%9C%89%E6%9C%BA%E5%9C%BA%20(BJS)%20-%20%E4%B8%AD%E5%9B%BD&isUM=&jourAllerFin=06&jourAllerOrigine=11&moisAllerFin=201509&moisAllerOrigine=201409&nbEnfants=&nbPassenger=1&paxTypoList=ADT&paxTypoList=ADT&paxTypoList=ADT&paxTypoList=ADT&plusOptions=&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectCabin=Y_MCHER&selectPreviousSearch=BJS-JFK_2014811&subCabin=MCHER&typeTrip=2&yearMonthDate=201409&yearMonthDate=201410";
		String detail = "http://www.airfrance.com.cn/cgi-bin/AF/CN/zh/local/process/standardbooking/FareAction.do?indexItinerary=0&price=5630.0&price=5630.0&price=5630.0&price=5630.0&price=13580.0&price=13580.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=7030.0&price=15400.0&price=7030.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5600.0&price=15400.0&price=5110.0&price=5110.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5590.0&price=5590.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5100.0&price=5100.0&price=5100.0&price=5100.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&price=5590.0&price=15400.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=10730.0&priceMultiPax=18680.0&priceMultiPax=18680.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=12130.0&priceMultiPax=20510.0&priceMultiPax=12130.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10700.0&priceMultiPax=10700.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10210.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&priceMultiPax=10700.0&priceMultiPax=20510.0&radioIti0=6&radioIti1=8&recommendation=6&recommendation=8&selectedFF=FFLHAFKLY1&selectedFF=FFLHAFKLY1&sortType=DEPARTURE&sortType=DEPARTURE&sortType0=DEPARTURE&sortType1=DEPARTURE";
		
		AirfranceAdapterTest.paramsAnalysis(detail);

	}
}
