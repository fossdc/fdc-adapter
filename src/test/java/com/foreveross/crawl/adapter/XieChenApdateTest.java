package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

public class XieChenApdateTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("BJS");
		task.setFromCityName("北京");
		task.setToCity("FRA");
		task.setToCityName("法兰克福");
		task.setFlightDate("2014-10-18");
		task.setUseProxyip(false);
		task.setIsReturn(1);
		task.setIsInternational(1);
		task.setReturnGrabDate("2014-11-01");
		task.setAttachHbaseKey("00003");
		XieChenApdateTest test = new XieChenApdateTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				XieChenAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}