package com.foreveross.crawl.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AmericanairlinesAdapter;
import com.foreveross.crawl.common.cfg.ConfigContainer;
import com.foreveross.proxyip.ProxyipProperties;
import com.foreveross.taskservice.common.bean.TaskModel;

/**
 * 一句话描述
 * 
 * @author fb
 */

@SuppressWarnings("deprecation")
public class AmericanairlinesAdapterTest extends BaseAdapterTest{
	
	private final static Logger log = LoggerFactory.getLogger(AmericanairlinesAdapterTest.class);

	AmericanairlinesAdapter ala = null;
	TaskModel tm = null;
	
	@Before
	public void init() {
		ConfigContainer.getInstance().register(ProxyipProperties.class);
		tm = new TaskModel();
		tm.setFromCity("bjs");
		tm.setToCity("nyc");
		tm.setFlightDate("2014-07-19");
		tm.setReturnGrabDate("2014-07-20");
		tm.setIsInternational(1);
		tm.setIsReturn(1);
		tm.setFromCityName("");
		tm.setToCityName("");
		ala = new AmericanairlinesAdapter(tm);
	}
	
	@Test
	public void run() throws Exception {
		String fight = (String) ala.fetch(null);
		if (ala.validateFetch(fight)) { 
			ala.paraseToVo(fight);
		}
	}
	
	@Test
	public void paraseToVoTest() throws Exception {
		ala.paraseToVo(getFileData("americanairlines/data.html"));
	}
	
	@Test
	public void flightDetailAssemblyTest() {
		// 测试时改成public 
		//ala.flightDetailAssembly(getFileData("americanairlines/detail.html"), new DoublePlaneInfoEntity(), new ReturnDoublePlaneInfoEntity() ,"经济仓");
	}
	
	@Test
	public void jsonAnalysisTest() {
		// 测试时改成public 
		//ala.jsonAnalysis(getFileData("americanairlines/data.html"));
	}
	
	
	private String getFileData(String file) {
		StringBuffer buffer = new StringBuffer();
		InputStream input = null;
		
		try {
			input = AmericanairlinesAdapter.class.getClassLoader().getResourceAsStream("fileData/" + file);
			byte[] tmp = new byte[1024];
			int l;
			while ((l = input.read(tmp)) != -1) {
				buffer.append(new String(tmp, 0, l, "gbk"));
			}
		} catch (Exception e) {
			log.error("获取模拟文件数据出错：", e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.error("关闭文件流异常：", e);
				}
			}
		}
		
		return buffer.toString();
	}
	
	@SuppressWarnings("unused")
	private void paramsCreate(String url) throws UnsupportedEncodingException {
		for (String kv : url.split("&")) {
			String kva[] = kv.split("=");
			System.out.println("params.put(\"" + kva[0] + "\", \""
					+ URLDecoder.decode(kva[1], "utf-8") + "\");");
		}
	}
}
