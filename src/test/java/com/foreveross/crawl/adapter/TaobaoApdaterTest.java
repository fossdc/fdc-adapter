package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.ExpediaAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.TaobaoAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;
/**
 * 淘宝适配器测试
 * @author xiangsf 2014
 *
 */
public class TaobaoApdaterTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("SZX");
		task.setFromCityName("深圳");
		task.setToCity("SIA");
		task.setToCityName("西安");
		task.setFlightDate("2014-10-31");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-10-31");
		task.setAttachHbaseKey("00001");
		TaobaoApdaterTest test = new TaobaoApdaterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(TaobaoAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}