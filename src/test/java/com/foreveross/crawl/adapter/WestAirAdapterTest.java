package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.WestAirAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class WestAirAdapterTest extends BaseAdapterTest {
	
	public static void main(String[] arg0) throws Exception{
		TaskModel taskModel = new TaskModel();
		taskModel.setFromCity("CKG");
		taskModel.setFromCityName("重庆");
		taskModel.setToCity("CSX");
		taskModel.setToCityName("长沙");
		taskModel.setFlightDate("2014-09-20");
		taskModel.setReturnGrabDate("2014-10-15");
		taskModel.setIsReturn(0);
		taskModel.setIsInternational(0);
		taskModel.setAttachHbaseKey("20016");
		WestAirAdapterTest test =new WestAirAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(WestAirAdapter.class, taskModel);
		test.printJson(results);
	}

}
