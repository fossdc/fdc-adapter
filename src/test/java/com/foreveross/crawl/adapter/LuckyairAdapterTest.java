package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.LuckyairAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class LuckyairAdapterTest extends BaseAdapterTest {
	
	public static void main(String[] args) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("KMG");
		task.setFromCityName("昆明");
		task.setToCity("LJG");
		task.setToCityName("丽江");
		task.setFlightDate("2014-09-23");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-29");
		task.setAttachHbaseKey("20000");
		LuckyairAdapterTest test=new LuckyairAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				LuckyairAdapter.class, task);
		test.printJson(results);
	}

}
