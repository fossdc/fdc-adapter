package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.CSAirAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.ShenzhengAirAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

public class ShenzhenAirApdateTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("SHA");
		task.setFromCityName("上海");
		task.setToCity("SZX");
		task.setToCityName("深圳	");
		task.setFlightDate("2014-09-26");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-29");
		task.setAttachHbaseKey("20000");
		ShenzhenAirApdateTest test = new ShenzhenAirApdateTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				ShenzhengAirAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}