package com.foreveross.crawl.adapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.ItaAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.TianxunAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;
/**
 * 天巡测试
 * @author Administrator
 *
 */
public class TianxunAdapterTest extends BaseAdapterTest {

	@Test
	public void test() throws Exception {
		TaskModel model = new TaskModel();

		model.setIsInternational(1);
		model.setFlightDate("2014-10-16");
		model.setReturnGrabDate("2014-10-31");
		model.setIsReturn(1);
		model.setFromCity("PVG");
		model.setFromCityName("上海");
		model.setToCity("LAX");
		model.setToCityName("洛杉矶");
		Long start = System.currentTimeMillis();

		List<AbstractPlaneInfoEntity> objs = super.fetchData(TianxunAdapter.class, model);

		System.out.println("USED： " + (System.currentTimeMillis() - start) + "MS");
		String json = printJson(objs);
		System.out.println(json);
		// printObj(objs);

	}

}
