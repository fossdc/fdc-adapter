package com.foreveross.crawl.adapter;

import java.util.List;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.LufthansaAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;
/**
 * 德国汉莎测试
 * @author xiangsf 2014-09-17
 *
 */
public class LufthansaAdapterTest extends BaseAdapterTest{
	@Test
	public void test() throws Exception {
		TaskModel model = new TaskModel();

		model.setIsInternational(1);
		model.setFlightDate("2015-01-22");
		model.setReturnGrabDate("2015-04-22");
		model.setIsReturn(1);
		model.setFromCity("PEK");
		model.setFromCityName("北京");
		model.setToCity("MUC");
		model.setToCityName("慕尼黑");
//		Long start = System.currentTimeMillis();

		List<AbstractPlaneInfoEntity> objs = super.fetchData(
				LufthansaAdapter.class, model);
		
		
		
//		System.out.println("USED： " + (System.currentTimeMillis() - start) + "MS");
		String  json  = printJson(objs);
		System.out.println(json);
	}
}
