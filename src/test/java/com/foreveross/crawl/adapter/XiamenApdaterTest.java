package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.XiaMenAirAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class XiamenApdaterTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("PEK");
		task.setFromCityName("北京");
		task.setToCity("CAN");
		task.setToCityName("法兰克福");
		task.setFlightDate("2014-10-18");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-11-01");
		task.setAttachHbaseKey("20007");
		XiamenApdaterTest test = new XiamenApdaterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				XiaMenAirAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}