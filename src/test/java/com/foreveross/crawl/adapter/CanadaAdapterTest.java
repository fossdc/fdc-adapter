package com.foreveross.crawl.adapter;

import java.util.List;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.CanadaAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class CanadaAdapterTest extends BaseAdapterTest{
	
	@Test
	public void test() throws Exception {
		TaskModel model = new TaskModel();

		model.setIsInternational(1);
		model.setFlightDate("2014-09-12");
		model.setReturnGrabDate("2014-09-30");
		model.setIsReturn(1);
		model.setFromCity("PEK");
		model.setFromCityName("北京");
		model.setToCity("SEA");
		model.setToCityName("西雅图");
		Long start = System.currentTimeMillis();

		List<AbstractPlaneInfoEntity> objs = super.fetchData(CanadaAdapter.class, model);
		
		serialFile(objs, "C:\\canada_list.serial");
		
		System.out.println("USED： " + (System.currentTimeMillis() - start) + "MS");
		String  json  = printJson(objs);
		System.out.println(json);
	}
}
