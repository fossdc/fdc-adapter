package com.foreveross.crawl.adapter;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.YiqifeiAdapter;
import com.foreveross.taskservice.common.bean.TaskModel;

public class YiqifeiAdapterTest extends BaseAdapterTest {
	@Test
	public void test() throws Exception{
		TaskModel taskModel = new TaskModel();
        taskModel.setFromCity("PEK"); 
        taskModel.setFromCityName("北京");
        taskModel.setToCity("HAK");
        taskModel.setToCityName("海口");
        taskModel.setFlightDate("2014-09-01");
        taskModel.setIsReturn(0);
        taskModel.setIsInternational(0);
        taskModel.setReturnGrabDate("2014-09-06");
		
		printJson(fetchData(YiqifeiAdapter.class, taskModel));
	}
}
