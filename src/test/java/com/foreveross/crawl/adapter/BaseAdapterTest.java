package com.foreveross.crawl.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.CabinEntity;
import com.foreveross.crawl.domain.airfreight.TransitEntity;
import com.foreveross.crawl.domain.airfreight.doub.CabinRelationEntity;
import com.foreveross.crawl.domain.airfreight.doub.DoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnCabinEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnDoublePlaneInfoEntity;
import com.foreveross.crawl.domain.airfreight.doub.ReturnTransitEntity;
import com.foreveross.crawl.domain.airfreight.single.SinglePlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 测试基类.
 * 
 * @author luomingliang@foreveross.com
 * @version 1.0.0
 */
@Ignore
public abstract class BaseAdapterTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	// 抓取数据
	public List<AbstractPlaneInfoEntity> fetchData(Class<? extends AbstractAdapter> clazz, String fromCity, String toCity, String fromDate, String returnDate, RouteTypeEnum routeType) throws Exception {
		TaskModel model = new TaskModel();
		if (routeType == RouteTypeEnum.DOMESTIC_ONEWAYTRIP) {
			model.setIsInternational(0);
			model.setIsReturn(0);
		} else if (routeType == RouteTypeEnum.DOMESTIC_ROUNDTRIP) {
			model.setIsInternational(0);
			model.setIsReturn(1);
		} else if (routeType == RouteTypeEnum.INTERNATIONAL_ONEWAY) {
			model.setIsInternational(1);
			model.setIsReturn(0);
		} else if (routeType == RouteTypeEnum.INTERNATIONAL_ROUND) {
			model.setIsInternational(1);
			model.setIsReturn(1);
		}

		model.setFlightDate(fromDate);
		model.setReturnGrabDate(returnDate);
		model.setFromCity(fromCity);
		model.setToCity(toCity);
		model.setFromCityName("");
		model.setToCityName("");
		return fetchData(clazz, model);
	}

	// 抓取数据
	public List<AbstractPlaneInfoEntity> fetchData(Class<? extends AbstractAdapter> clazz, TaskModel model) throws Exception {
		System.out.println("类：" + getClass());
		List<AbstractPlaneInfoEntity> objs = Lists.newArrayList();
		Constructor construtor = clazz.getConstructor(TaskModel.class);
		AbstractAdapter adapter = (AbstractAdapter) construtor.newInstance(model);
		Object fetchObject = adapter.fetch("");
		adapter.validateFetch(fetchObject);
		List<Object> results = adapter.paraseToVo(fetchObject);
		System.out.println("数据抓取完成 。");

		for (Object obj : results) {
			AbstractPlaneInfoEntity en = (obj instanceof SinglePlaneInfoEntity) ? (SinglePlaneInfoEntity) obj : (DoublePlaneInfoEntity) obj;
			objs.add(en);
		}
		return objs;
	}

	// 打印 Json
	public String printJson(List<AbstractPlaneInfoEntity> objs) throws IOException {
		List<String> excList = Lists.newArrayList();
		excList.add("baseEntityRepository");
		excList.add("planeInfoEntity");
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes(false);
		config.setExcludes(excList.toArray(new String[] {}));
		JSONArray array = JSONArray.fromObject(objs, config);
		String json = array.toString();
		System.out.println(json);
		return json;
	}

	// 打印 Json 到文件
	public void printJson(List<AbstractPlaneInfoEntity> objs, String filePath) throws IOException {
		String json = printJson(objs);
		if (filePath != null) FileUtils.writeStringToFile(new File(filePath), json, "UTF-8");
	}

	/**
	 * 序列化。
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public void serialFile(List<AbstractPlaneInfoEntity> objs, String fileName) throws Exception {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream(fileName);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(objs);
		} catch (Exception e) {} finally {
			fos.flush();
			fos.close();
			oos.flush();
			oos.close();
		}
	}

	/**
	 * 反序列化。
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public Object reSerialFile(String fileName) throws Exception {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		Object obj = null;
		try {
			fis = new FileInputStream(fileName);
			ois = new ObjectInputStream(fis);
			obj = ois.readObject();
		} catch (Exception e) {} finally {
			fis.close();
			ois.close();
		}
		return obj;
	}

	public void roundTest(Class<? extends AbstractAdapter> clazz, TaskModel model, Long sleep, Integer count) throws Exception {
		Integer trycount = count == null ? 10 : count;
		String msg = "";
		Map<String, Boolean> result = Maps.newHashMap();
		int i = 0;
		Boolean isSuccess = false;
		while (i < trycount) {
			Long start = System.currentTimeMillis();
			i++;
			try {
				this.fetchData(clazz, model);
				msg = String.format("第[%s]次抓取,用时：[%s]", i, (System.currentTimeMillis() - start));
				isSuccess = true;
			} catch (Exception e) {
				e.printStackTrace();
				msg = String.format("第[%s]次失败,用时：[%s]，原因：[%s]", i, (System.currentTimeMillis() - start), e.getMessage());
				isSuccess = false;
			}
			result.put(msg, isSuccess);
			Thread.sleep(sleep == null ? 5000 : sleep);
		}

		logger.warn("=====================国航[ " + trycount + " ]次抓取执行完成===========================");
		Double success = 0D;
		Double fail = 0D;
		for (String message : result.keySet()) {
			if (result.get(message)) {
				success++;
			} else {
				fail++;
				logger.error(message);
			}
		}
		logger.warn("====");
		logger.warn("====");
		logger.info(String.format(">>>统计:成功[%s],失败[%s],成功率[%s", success, fail, (success / trycount) * 100) + "%]<<<");
	}

	// 打印详细信息
	public void printObj(List<AbstractPlaneInfoEntity> objs) {
		if (objs.size() <= 0) {
			System.out.println("没有抓到数据哦.");
			return;
		}

		for (AbstractPlaneInfoEntity en : objs) {
			if (en instanceof SinglePlaneInfoEntity) {
				SinglePlaneInfoEntity d = PlaneInfoEntityBuilder.getSingleEntity(en);
				System.out.println("========================");

				System.out.println(String.format("%s(%s-%s)(%s-%s):", d.getFlightNo(), d.getFromCity(), d.getToCity(), d.getStartTime(), d.getEndTime()));

				System.out.println("中转信息：");
				List<TransitEntity> trans = d.getTransits();
				for (TransitEntity tran : trans) {
					System.out.println(tran.getFlightNo() + ":" + tran.getFromAirPort() + "->" + tran.getToAirPort() + tran.getStartTime() + tran.getEndTime());
				}

				System.out.println("仓位信息：");
				Set<CabinEntity> cbs = d.getCabins();
				for (CabinEntity cb : cbs) {
					System.out.println(String.format("%s[%s]:%s+%s=%s", cb.getProductName(), cb.getCabinType(), cb.getTaxesPrice(), cb.getPrice(), cb.getOriginalPrice()));
				}

				System.out.println(String.format("税价：%s~%s", d.getTotalLowestTaxesPrice(), d.getTotalHighestTaxesPrice()));
				System.out.println(String.format("价格：%s~%s", d.getTotalLowestPrice(), d.getTotalHighestPrice()));
				System.out.println(String.format("总价：%s~%s", d.getSumLowestPrice(), d.getSumHighestPrice()));

			} else {
				DoublePlaneInfoEntity d = PlaneInfoEntityBuilder.getDoubleEntity(en);

				System.out.println(String.format("航班[%s](%s-%s)(%s-%s)=================================>", d.getFlightNo(), d.getFromCity(), d.getToCity(), d.getStartTime(), d.getEndTime()));

				System.out.println(String.format("税价：%s~%s", d.getTotalLowestTaxesPrice(), d.getTotalHighestTaxesPrice()));
				System.out.println(String.format("价格：%s~%s", d.getTotalLowestPrice(), d.getTotalHighestPrice()));
				System.out.println(String.format("总价：%s~%s", d.getSumLowestPrice(), d.getSumHighestPrice()));

				System.out.println("中转信息：");
				List<TransitEntity> trans = d.getTransits();
				for (TransitEntity tran : trans) {
					System.out.println(tran.getFlightNo() + ":" + tran.getFromAirPort() + "->" + tran.getToAirPort() + tran.getStartTime() + tran.getEndTime());
				}

				System.out.println("仓位信息：");
				Set<CabinEntity> cbs = d.getCabins();
				for (CabinEntity cb : cbs) {
					System.out.println(String.format("%s[%s]:%s+%s=%s", cb.getProductName(), cb.getCabinType(), cb.getTaxesPrice(), cb.getPrice(), cb.getOriginalPrice()));
				}

				System.out.println("回程航班-----------------------------");
				for (ReturnDoublePlaneInfoEntity rp : d.getReturnPlaneInfos()) {
					System.out.println(String.format("回程航班[%s](%s-%s)(%s-%s)////>:", rp.getFlightNo(), rp.getFromCity(), rp.getToCity(), rp.getStartTime(), rp.getEndTime()));
					System.out.println(String.format("税价：%s~%s", rp.getTotalLowestTaxesPrice(), rp.getTotalHighestTaxesPrice()));
					System.out.println(String.format("价格：%s~%s", rp.getTotalLowestPrice(), rp.getTotalHighestPrice()));
					System.out.println(String.format("总价：%s~%s", rp.getSumLowestPrice(), rp.getSumHighestPrice()));

					System.out.println("回程仓位：");
					Set<ReturnCabinEntity> recbs = rp.getReturnCabins();
					for (ReturnCabinEntity cb : recbs) {
						System.out.println(String.format("%s[%s]:%s+%s=%s", cb.getProductName(), cb.getCabinType(), cb.getTaxesPrice(), cb.getPrice(), cb.getOriginalPrice()));
					}

					System.out.println("回程中转：");
					List<ReturnTransitEntity> retrans = rp.getReturnTransits();
					for (ReturnTransitEntity tran : retrans) {
						System.out.println(tran.getFlightNo() + ":[" + tran.getFromAirPort() + "->" + tran.getToAirPort() + "]" + tran.getStartTime() + tran.getEndTime());
					}
				}

				System.out.println("仓位关系：------------------------------");
				List<CabinRelationEntity> relations = Lists.newArrayList();
				relations.addAll(d.getCabinRelations());
				PlaneInfoEntityBuilder.orderByPrice(relations);
				for (CabinRelationEntity cr : relations) {
					System.out.println(cr.getTaxesPrice() + "+" + cr.getFullPrice() + "=" + cr.getTotalFullPrice());
				}
			}
		}
	}
}
