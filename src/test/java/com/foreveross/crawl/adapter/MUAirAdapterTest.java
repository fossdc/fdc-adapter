package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.MUAirAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class MUAirAdapterTest extends BaseAdapterTest {
	public static void main(String[] args) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("PEK");
		task.setFromCityName("北京");
		task.setToCity("SHA");
		task.setToCityName("法兰克福");
		task.setFlightDate("2014-10-17");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-21");
		task.setAttachHbaseKey("20005");
		MUAirAdapterTest test = new MUAirAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				MUAirAdapter.class, task);
		test.printJson(results);

	}
}
