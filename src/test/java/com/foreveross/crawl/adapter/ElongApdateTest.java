package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.ELongAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class ElongApdateTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("CKG");
		task.setFromCityName("重庆|Chongqing");
		task.setToCity("LJG");
		task.setToCityName("丽江|Lijiang");
		task.setFlightDate("2014-09-22");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2015-01-17");
		task.setAttachHbaseKey("00004");
		ElongApdateTest test = new ElongApdateTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				ELongAdapter.class, task);
		System.out.println(test.printJson(results));
		// IOUtils.write(data, output);
	}
}