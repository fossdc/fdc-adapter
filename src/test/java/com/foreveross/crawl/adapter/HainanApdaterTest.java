package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.HaiNanAirAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class HainanApdaterTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("XIY");
		task.setFromCityName("西安");
		task.setToCity("PEK");
		task.setToCityName("北京");
		task.setFlightDate("2014-09-24");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-08");
		task.setAttachHbaseKey("20004");
		HainanApdaterTest test = new HainanApdaterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				HaiNanAirAdapter.class, task);
		test.printJson(results);
	}
}