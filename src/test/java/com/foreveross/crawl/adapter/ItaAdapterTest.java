package com.foreveross.crawl.adapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.ItaAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class ItaAdapterTest extends BaseAdapterTest {

	@Test
	public void test() throws Exception {
		TaskModel model = new TaskModel();

		model.setIsInternational(1);
		model.setFlightDate("2015-01-24");
		model.setReturnGrabDate("2015-02-27");
		model.setIsReturn(1);
		model.setFromCity("PEK");
		model.setFromCityName("北京");
		model.setToCity("FRA");
		model.setToCityName("法兰克福");
		Long start = System.currentTimeMillis();

		List<AbstractPlaneInfoEntity> objs = super.fetchData(ItaAdapter.class, model);

		System.out.println("USED： " + (System.currentTimeMillis() - start) + "MS");
		String json = printJson(objs);
		System.out.println(json);
		// printObj(objs);

	}

}
