package com.foreveross.crawl.adapter;

import java.io.IOException;
import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AeroflotAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class AeroflotAdapterTest extends BaseAdapterTest {
	
	public static void main(String[] args) {
		TaskModel taskModel = new TaskModel();
		taskModel.setFromCity("PEK");
		taskModel.setFromCityName("北京");
		taskModel.setToCity("CDG");
		taskModel.setToCityName("巴黎");
		taskModel.setFlightDate("2014-10-19");
		taskModel.setReturnGrabDate("2014-10-26");
		taskModel.setIsReturn(1);
		taskModel.setIsInternational(1);
		taskModel.setAttachHbaseKey("20024");
		AeroflotAdapterTest test =new AeroflotAdapterTest();
		List<AbstractPlaneInfoEntity> results=null;
		try {
			results = test.fetchData(AeroflotAdapter.class, taskModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			test.printJson(results);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
