package com.foreveross.crawl.adapter;

import java.util.List;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AirfranceAdapterV3;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class AirfranceAdapterV3Test extends BaseAdapterTest{
		@Test
		public void test() throws Exception {
			TaskModel model = new TaskModel();

			model.setIsInternational(1);
			model.setFlightDate("2015-02-13");
			model.setReturnGrabDate("2015-02-18");
			model.setIsReturn(1);
			model.setFromCity("PEK");
			model.setFromCityName("北京");
			model.setToCity("CDG");
			model.setToCityName("西雅图");
			Long start = System.currentTimeMillis();

			List<AbstractPlaneInfoEntity> objs = super.fetchData(AirfranceAdapterV3.class, model);
			
//			for(Object o:objs){
//				AbstractPlaneInfoEntity p=(AbstractPlaneInfoEntity)o;
//				System.out.println(p.generateRowKey());
//				System.out.println("存入完成");
//			}
//			
			serialFile(objs, "e:\\canada_list.serial");
			
			System.out.println("USED： " + (System.currentTimeMillis() - start) + "MS");
			String  json  = printJson(objs);
			System.out.println(json);
		}
}
