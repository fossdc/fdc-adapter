package com.foreveross.crawl.adapter;

import org.junit.Test;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.KoreanAirAdapter;
import com.foreveross.taskservice.common.bean.TaskModel;

public class KoreanAirAdapterTest extends BaseAdapterTest {
	@Test
	public void test() throws Exception{
		TaskModel taskModel = new TaskModel();
		taskModel.setFromCity("PEK");
		taskModel.setFromCityName("北京");
		//taskModel.setToCity("ICN");
		//taskModel.setToCityName("首尔");
		taskModel.setToCity("IAD");
		taskModel.setToCityName("华盛顿");
		taskModel.setFlightDate("2014-10-02");
		taskModel.setIsReturn(1);
		taskModel.setIsInternational(1);
		taskModel.setReturnGrabDate("2014-10-25");
		
//		KoreanAirAdapterTest test = new KoreanAirAdapterTest();
//		List<AbstractPlaneInfoEntity> results = test.fetchData(KoreanAirAdapter.class, taskModel);
//		System.out.println(test.printJson(results));
		
		printJson(fetchData(KoreanAirAdapter.class, taskModel));
	}
}
