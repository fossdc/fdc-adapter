package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.TaobaoAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.TianjinAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class TianJinAdapterTest extends BaseAdapterTest {
	
	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("NNG");
		task.setFromCityName("南宁");
		task.setToCity("WUH");
		task.setToCityName("武汉");
		task.setFlightDate("2014-10-18");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-10-31");
		task.setAttachHbaseKey("20013");
		TianJinAdapterTest test = new TianJinAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(TianjinAdapter.class, task);
		test.printJson(results);
	}
}
