package com.foreveross.crawl.adapter;

import java.util.List;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.DongHaiAirAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.taskservice.common.bean.TaskModel;

public class DongHaiAirAdapterTest extends BaseAdapterTest {
	public void main(String []args) throws Exception{
		
		TaskModel task = new TaskModel();
		task.setFromCity("HLD");
		task.setFromCityName("海拉尔");
		task.setToCity("HRB");
		task.setToCityName("哈尔滨");
		task.setFlightDate("2014-10-09");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-21");
		task.setAttachHbaseKey("20005");
		DongHaiAirAdapterTest test = new DongHaiAirAdapterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(DongHaiAirAdapter.class, task);
		test.printJson(results);
		
	}
}
