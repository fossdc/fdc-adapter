package com.foreveross.crawl.adapter;

import java.util.List;

import org.apache.commons.io.IOUtils;

import com.foreveross.crawl.adapter.sub.impl20140402.v3.AirchinaAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.CSAirAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.QunarAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.ShenzhengAirAdapter;
import com.foreveross.crawl.adapter.sub.impl20140402.v3.XieChenAdapter;
import com.foreveross.crawl.domain.airfreight.AbstractPlaneInfoEntity;
import com.foreveross.crawl.enums.RouteTypeEnum;
import com.foreveross.taskservice.common.bean.TaskModel;

public class QunarApdaterTest extends BaseAdapterTest {

	public static void main(String[] arg0) throws Exception {
		TaskModel task = new TaskModel();
		task.setFromCity("SZX");
		task.setFromCityName("深圳");
		task.setToCity("HAK");
		task.setToCityName("海口");
		task.setFlightDate("2014-09-18");
		task.setUseProxyip(false);
		task.setIsReturn(0);
		task.setIsInternational(0);
		task.setReturnGrabDate("2014-09-29");
		task.setAttachHbaseKey("00001");
		QunarApdaterTest test = new QunarApdaterTest();
		List<AbstractPlaneInfoEntity> results = test.fetchData(
				QunarAdapter.class, task);
		test.printJson(results);
		// IOUtils.write(data, output);
	}
}